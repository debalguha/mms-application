package com.sms.util;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.not;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.Customer;
import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.RFQEntity;
import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.SystemTreeItemType;
import com.sms.persistence.service.InventoryService;
import com.sms.persistence.service.POEntityService;
import com.sms.persistence.service.RFQService;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.InventoryDTO;
import com.sms.web.model.RFQObjectDTO;
import com.sms.web.model.SearchDTO;
import com.sms.web.model.TREE_NODE_TYPE;
import com.sms.web.model.TreeDTO;
import com.sms.web.model.UploadResponseDTO;

public class DTOUtils {
	private static String[] renderableTypes = new String[] { "Part", "Vendor", "Picture", "Drawing", "Line Item" };
	private static DateFormat searchDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	public static Collection<TreeDTO> convertTreesToDTOs(Collection<? extends AbstractTree> roots, String selectedGuid) throws Exception {
		Collection<TreeDTO> dtos = Lists.newArrayList();
		for (AbstractTree root : roots)
			dtos.add(convertTreeToDTO(root, selectedGuid));
		return dtos;
	}

	public static boolean isRenederable(SystemTreeItemType itemType) {
		return ArrayUtils.contains(renderableTypes, itemType);
	}

	public static TreeDTO convertTreeToDTO(AbstractTree root, String selectedGuid) {
		TreeDTO dto = new TreeDTO();
		dto.setGuid(root.getGuid());
		dto.setText(root.getNomenclature());
		dto.setNodeType(root.getNodeType());
		dto.setRenderable(true);
		try {
			dto.setItemTypeGuid(root.getTreeItemType().getGuid());
		} catch (Exception e) {}
		if(!Strings.isNullOrEmpty(selectedGuid) && root.getGuid().equals(selectedGuid))
			dto.getState().setSelected(true);
		List<TreeDTO> children = Lists.newArrayList();
		dto.setChildren(children);
		if (!CollectionUtils.isEmpty(root.getChildren())) {
			for (AbstractTree aChild : root.getChildren())
				children.add(convertTreeToDTO(aChild, selectedGuid));
		}
		return dto;
	}

	public static TREE_NODE_TYPE determineNodeTypeFromTreeItemType(SystemTreeItemType treeItemType) {
		if (treeItemType.getItemType().equals("Contract"))
			return TREE_NODE_TYPE.CONTRACT;
		else if (treeItemType.getItemType().equals("Service"))
			return TREE_NODE_TYPE.SERVICE;
		else if (treeItemType.getItemType().equals("Part"))
			return TREE_NODE_TYPE.PART;
		else if (treeItemType.getItemType().equals("Vendor"))
			return TREE_NODE_TYPE.CONTRACT;
		else if (treeItemType.getItemType().equals("Picture"))
			return TREE_NODE_TYPE.PICTURE;
		else if (treeItemType.getItemType().equals("Drawing"))
			return TREE_NODE_TYPE.DRAWING;
		else if (treeItemType.getItemType().equals("Line Item"))
			return TREE_NODE_TYPE.LINEITEM;
		return TREE_NODE_TYPE.UNKNOWN;
	}

	public static Collection<TreeDTO> groupChildOfNodes(Collection<TreeDTO> rootDTOs) {
		if (CollectionUtils.isEmpty(rootDTOs))
			return rootDTOs;
		for (TreeDTO aDTO : rootDTOs) {
			if (aDTO.getNodeType().equals(TREE_NODE_TYPE.PART) || aDTO.getNodeType().equals(TREE_NODE_TYPE.CONTRACT)
					|| aDTO.getNodeType().equals(TREE_NODE_TYPE.LINEITEM) || aDTO.getNodeType().equals(TREE_NODE_TYPE.SERVICE))
				aDTO.setChildren(groupChildren(aDTO.getChildren(), aDTO.getNodeType()));
			if(!CollectionUtils.isEmpty(aDTO.getChildren()))
				groupChildOfNodes(aDTO.getChildren());
		}
		return rootDTOs;
	}

	public static TreeDTO groupChildOfPart(TreeDTO rootDTO) {
		if (rootDTO.getNodeType().equals(TREE_NODE_TYPE.PART) || rootDTO.getNodeType().equals(TREE_NODE_TYPE.CONTRACT)
				|| rootDTO.getNodeType().equals(TREE_NODE_TYPE.LINEITEM) || rootDTO.getNodeType().equals(TREE_NODE_TYPE.SERVICE))
			rootDTO.setChildren(groupChildren(rootDTO.getChildren(), rootDTO.getNodeType()));
		else
			groupChildOfNodes(rootDTO.getChildren());
		return rootDTO;
	}

	private static Collection<TreeDTO> groupChildren(Collection<TreeDTO> children, TREE_NODE_TYPE parentNodeType) {
		TreeDTO vendorGroupParentDTO = new TreeDTO(null, "Vendors", false, TREE_NODE_TYPE.VENDORS_GROUP, null);
		/*TreeDTO pictureGroupParentDTO = new TreeDTO(null, "Picture", false, TREE_NODE_TYPE.PICTURES_GROUP, null);
		TreeDTO drawingGroupParentDTO = new TreeDTO(null, "Drawings", false, TREE_NODE_TYPE.DRAWINGS_GROUP, null);*/
		Collection<TreeDTO> subCollection = Lists.newArrayListWithCapacity(4);
		if(parentNodeType == TREE_NODE_TYPE.PART)
			subCollection.add(vendorGroupParentDTO);
		/*subCollection.add(pictureGroupParentDTO);
		subCollection.add(drawingGroupParentDTO);*/
		subCollection.addAll(select(children, having(on(TreeDTO.class).getNodeType(), not(isOneOf(TREE_NODE_TYPE.VENDOR, TREE_NODE_TYPE.PICTURE, TREE_NODE_TYPE.DRAWING)))));
		return subCollection;
	}

	public static Collection<UploadResponseDTO> convertDrawingsToUploadResponse(Collection<SystemDrawing> allDrawingsForPart, File uploadDirectory, String uploader) throws IOException {
		List<UploadResponseDTO> responseDTOs = Lists.newArrayListWithCapacity(allDrawingsForPart.size());
		for (SystemDrawing systemDrawing : allDrawingsForPart)
				responseDTOs.add(new UploadResponseDTO(systemDrawing.getUploadedFileName(), SMSUtil.toString(systemDrawing.getCreationDate()), SMSUtil.findNumberOfPages(new File(uploadDirectory, systemDrawing.getUploadedFileName())), uploader, systemDrawing.getGuid()));
		
		Collections.sort(responseDTOs, new Comparator<UploadResponseDTO>() {
			@Override
			public int compare(UploadResponseDTO o1, UploadResponseDTO o2) {
				return o1.getFileName().compareTo(o2.getFileName());
			}
		});
		return responseDTOs;
	}

	public static Collection<UploadResponseDTO> convertPicturesToUploadResponse(Collection<SystemPicture> allpicturesForPart, File uploadDirectory, String uploader) throws IOException {
		List<UploadResponseDTO> responseDTOs = Lists.newArrayListWithCapacity(allpicturesForPart.size());
		for (SystemPicture systemPicture : allpicturesForPart)
			responseDTOs.add(new UploadResponseDTO(systemPicture.getUploadedFileName(), SMSUtil.toString(systemPicture.getCreationDate()), SMSUtil.findNumberOfPages(new File(uploadDirectory, systemPicture.getUploadedFileName())), uploader, systemPicture.getGuid()));
		Collections.sort(responseDTOs, new Comparator<UploadResponseDTO>() {
			@Override
			public int compare(UploadResponseDTO o1, UploadResponseDTO o2) {
				return o1.getFileName().compareTo(o2.getFileName());
			}
		});
		return responseDTOs;
	}

	public static DataTableModel<Customer> convertCustomersToDataTableModel(Collection<Customer> allCustomers) {
		return new DataTableModel<Customer>(allCustomers);
	}

	public static InventoryDTO convertInventoryToDTO(Inventory inv, InventoryService invService) {
		InventoryDTO invDTO = new InventoryDTO();
		invDTO.setGuid(inv.getGuid());
		invDTO.setHighUnitPrice(!Strings.isNullOrEmpty(inv.getHighUnitPricePOGuid()) ? invService.getHighUnitPricePO(inv) : null);
		invDTO.setIdCode(inv.getIdCode());
		invDTO.setItemDescription(inv.getSystemPart().getAssemblyName());
		invDTO.setLowQuantityThreshold(inv.getLowQuantityThreshold());
		invDTO.setLowUnitPrice(!Strings.isNullOrEmpty(inv.getLowUnitPricePOGuid()) ? invService.getLowUnitPricePO(inv) : null);
		invDTO.setQuantityOnHand(inv.getQuantityOnHand());
		invDTO.setWbs(inv.getSystemPart().getWbsTree() != null ? inv.getSystemPart().getWbsTree().getDescription() : "");
		invDTO.setVendorAttached(invService.hasVendors(inv.getGuid()));
		invDTO.setPoAvailable(invService.isPOAvailable(inv.getGuid()));
		invDTO.setActive(inv.getActive()==1);
		return invDTO;
	}

	public static Collection<? extends InventoryDTO> convertInventorysToDTO(Collection<? extends Inventory> allInventoryRecords, InventoryService inventoryService) {
		Collections.sort(Lists.newArrayList(allInventoryRecords));
		Collection<InventoryDTO> dtos = Lists.newArrayList();
		for (Inventory inv : allInventoryRecords)
			dtos.add(DTOUtils.convertInventoryToDTO(inv, inventoryService));
		return dtos;
	}

	public static Collection<? extends RFQObjectDTO> convertRFQEntityToDTO(Collection<? extends RFQEntity> rfqEntities, RFQService rfqService, POEntityService poService) {
		Collection<RFQObjectDTO> rfqObjects = Lists.newArrayList();
		for(RFQEntity rfqEntity : rfqEntities){
			RFQObjectDTO dto = new RFQObjectDTO();
			dto.setGuid(rfqEntity.getGuid());
			dto.setIssueDate(rfqEntity.getIssuedDate());
			dto.setNotes(rfqEntity.getNotes());
			dto.setRfqNumber(rfqEntity.getRfqNumber());
			dto.setRfqType(rfqEntity.getRfqType().getType());
			dto.setStatusDateCreated(rfqEntity.getStatusDateCreated());
			dto.setResponseCount(rfqService.findResponseCount(rfqEntity));
			rfqObjects.add(dto);
		}
		return rfqObjects;
	}

	public static Collection<? extends SearchDTO> convertSearchResultToDTO(List<Object[]> searchResult) throws ParseException {
		Collection<SearchDTO> searchDTOs = Lists.newArrayList();
		for(Object[] row : searchResult)
			searchDTOs.add(new SearchDTO(row[0].toString(), row[1].toString(), row[2].toString(), searchDateFormat.parse(row[3].toString())));
		return searchDTOs;
	}

}
	
	


