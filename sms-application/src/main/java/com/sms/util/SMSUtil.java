package com.sms.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.model.WbsTree;

public class SMSUtil {
	private static Logger logger = LoggerFactory.getLogger(SMSUtil.class);
	private static final DateFormat onlyDateFormat = new SimpleDateFormat("yyyy-MMM-dd");

	public static String toString(Date date) {
		return onlyDateFormat.format(date);
	}

	public static int findNumberOfPages(File pdfFile) throws IOException {
		PDDocument pdDocument = null;
		RandomAccess scratchFile = null;
		try {
			File tempPDFFile = File.createTempFile(pdfFile.getName(), FilenameUtils.getExtension(pdfFile.getName()));
			FileUtils.copyFile(pdfFile, tempPDFFile);
			scratchFile = new RandomAccessFile(tempPDFFile, "rw");
			pdDocument = PDDocument.loadNonSeq(tempPDFFile, scratchFile);
			int numberOfPages = pdDocument.getNumberOfPages();
			pdDocument.close();
			return numberOfPages;
		} catch (Exception e) {
			logger.error("Unable to find the number of pages.");
			return 0;
		} finally {
			if (pdDocument != null)
				pdDocument.close();
			if (scratchFile != null)
				scratchFile.close();
		}
	}

	public static AbstractTree cloneTreeNodeFrom(AbstractTree toBeCloned) {
		AbstractTree aTreeNode = null;
		if (toBeCloned instanceof SystemTree)
			aTreeNode = cloneSystemTree((SystemTree) toBeCloned);
		else if (toBeCloned instanceof WbsTree)
			aTreeNode = cloneWbsTree((WbsTree) toBeCloned);
		return aTreeNode;
	}

	private static WbsTree cloneWbsTree(WbsTree toBeCloned) {
		WbsTree aTreeNode = new WbsTree();
		aTreeNode.setDescription(toBeCloned.getDescription());
		aTreeNode.setNomenclature(toBeCloned.getNomenclature());
		return aTreeNode;
	}

	private static SystemTree cloneSystemTree(SystemTree toBeCloned) {
		SystemTree aTreeNode = new SystemTree();
		aTreeNode.setCreationDate(new Date());
		aTreeNode.setDescription(toBeCloned.getDescription());
		aTreeNode.setNomenclature(toBeCloned.getNomenclature());
		aTreeNode.setTreeItemType(toBeCloned.getTreeItemType());
		return aTreeNode;
	}

	public static void cloneObjectFrom(Object sourceObj, Object targetObj, String... ignoreProperties) throws Throwable {
		Map<String, String> propertyMap = BeanUtilsBean2.getInstance().describe(targetObj);
		Object[] propsToIgnore = ArrayUtils.add(ignoreProperties, "class");
		for (String property : propertyMap.keySet()) {
			if (ArrayUtils.contains(propsToIgnore, property))
				continue;
			try {
				BeanUtils.copyProperty(targetObj, property, BeanUtils.getProperty(sourceObj, property));
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
	}

	public static boolean validateIfStringIsGuid(String guid) {
		return Pattern.matches("^(\\{){0,1}[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}(\\}){0,1}$", guid);
	}

	public static void validatePart(SystemPart systemPart) {
		if (systemPart.getMaterialName() != null) {
			if (Strings.isNullOrEmpty(systemPart.getMaterialName().getMaterialName()))
				systemPart.setMaterialName(null);
		}
		if (systemPart.getMaterialType() != null) {
			if (Strings.isNullOrEmpty(systemPart.getMaterialType().getMaterialType()))
				systemPart.setMaterialType(null);
		}
		if (systemPart.getMaterialSpec() != null) {
			if (Strings.isNullOrEmpty(systemPart.getMaterialSpec().getMaterialSpecification()))
				systemPart.setMaterialSpec(null);
		}
		if (systemPart.getProcess1() != null) {
			if (Strings.isNullOrEmpty(systemPart.getProcess1().getPartProcess()))
				systemPart.setProcess1(null);
		}
		if (systemPart.getProcess2() != null) {
			if (Strings.isNullOrEmpty(systemPart.getProcess2().getPartProcess()))
				systemPart.setProcess2(null);
		}
		if (systemPart.getProcess3() != null) {
			if (Strings.isNullOrEmpty(systemPart.getProcess3().getPartProcess()))
				systemPart.setProcess3(null);
		}
	}
}
