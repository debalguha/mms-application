package com.sms.service.daemon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sms.persistence.model.EmailLog;
import com.sms.persistence.service.ContactSupportService;

@Component
public class MailSendDaemon {
	//private static final Logger LOG = LoggerFactory.getLogger(MailSendDaemon.class);
	@Autowired
	private ContactSupportService supportService;
	@Autowired
	private JavaMailSender mailSender;
	@Value("${service.email}")
	private String serviceEmailAddress;
	
	//@Scheduled(fixedRate = 10000)
	public void sendPendingEmails(){
		for(EmailLog emailLog : supportService.findAllPendingEmailsToSend()){
			SimpleMailMessage supportMessage = new SimpleMailMessage();
			supportMessage.setFrom(serviceEmailAddress);
			supportMessage.setTo(emailLog.getTo());
			supportMessage.setCc(emailLog.getFrom());
			supportMessage.setSubject(emailLog.getSubject());
			supportMessage.setText(emailLog.getSubject());
			mailSender.send(supportMessage);
			supportService.updateEmailLogToComplete(emailLog.getGuid());
		}
	}
	/*public void run() {
		while(true){
			try {
				
			} catch(Throwable e){LOG.error("Problem while sending support email.", e);}
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {e.printStackTrace();}
		}
	}*/

}
