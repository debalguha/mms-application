package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(schema = "dbo", name = "workorder_items")
public class WorkOrderItem {
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_guid", nullable = false)
	private User createByUser;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tech_guid", nullable = true)
	private User techUserGuid;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "system_tree_guid", nullable = true)
	private SystemTree systemTree;	
	
	@Column(name = "date_promised")
	@Temporal(TemporalType.TIMESTAMP)
	private Date promisedDate;
	
	@Column(name = "date_completed")
	@Temporal(TemporalType.TIMESTAMP)
	private Date completedDate;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "workorder_guid")
	private WorkOrder workOrder;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public User getCreateByUser() {
		return createByUser;
	}

	public void setCreateByUser(User createByUser) {
		this.createByUser = createByUser;
	}

	public User getTechUserGuid() {
		return techUserGuid;
	}

	public void setTechUserGuid(User techUserGuid) {
		this.techUserGuid = techUserGuid;
	}

	public SystemTree getSystemTree() {
		return systemTree;
	}

	public void setSystemTree(SystemTree systemTree) {
		this.systemTree = systemTree;
	}

	public Date getPromisedDate() {
		return promisedDate;
	}

	public void setPromisedDate(Date promisedDate) {
		this.promisedDate = promisedDate;
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkOrderItem other = (WorkOrderItem) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}
	public WorkOrder getWorkOrder() {
		return workOrder;
	}

	public void setWorkOrder(WorkOrder workOrder) {
		this.workOrder = workOrder;
	}

	@Override
	public String toString() {
		return "WorkOrderItem [guid=" + guid + ", creationDate=" + creationDate + ", createByUser=" + createByUser + ", techUserGuid=" + techUserGuid + ", systemTree=" + systemTree + ", promisedDate=" + promisedDate + ", completedDate=" + completedDate + "]";
	}
	
}
