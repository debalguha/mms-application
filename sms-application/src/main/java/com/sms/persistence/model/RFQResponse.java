package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "rfq_responses")
public class RFQResponse implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	private String notes;
	
	@Column(name = "response_file")
	private String responseFile;
	
	@Column(name = "delivery_by_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deliveryByDate;
	
	@Column(name = "response_id")
	private String responseId;
	
	@Column(name = "accepted")
	public int accepted;
	@Column(name = "accepted_date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date acceptedDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rfq_guid")
	private RFQEntity rfqEntity;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "vendor_guid")
	private SystemVendor systemVendor;
	
	@OneToMany(mappedBy = "rfqResponse", fetch = FetchType.EAGER)
	private Set<RFQResponseItem> rfqResponseItems;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getResponseFile() {
		return responseFile;
	}

	public void setResponseFile(String responseFile) {
		this.responseFile = responseFile;
	}

	public RFQEntity getRfqEntity() {
		return rfqEntity;
	}

	public void setRfqEntity(RFQEntity rfqEntity) {
		this.rfqEntity = rfqEntity;
	}

	public SystemVendor getSystemVendor() {
		return systemVendor;
	}

	public void setSystemVendor(SystemVendor systemVendor) {
		this.systemVendor = systemVendor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RFQResponse other = (RFQResponse) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RFQResponse [guid=" + guid + ", creationDate=" + creationDate + ", notes=" + notes + ", responseFile=" + responseFile + ", rfqEntity=" + rfqEntity + ", systemVendor=" + systemVendor + "]";
	}

	public Set<RFQResponseItem> getRfqResponseItems() {
		return rfqResponseItems;
	}

	public void setRfqResponseItems(Set<RFQResponseItem> rfqResponseItems) {
		this.rfqResponseItems = rfqResponseItems;
	}

	public Date getDeliveryByDate() {
		return deliveryByDate;
	}

	public void setDeliveryByDate(Date deliveryByDate) {
		this.deliveryByDate = deliveryByDate;
	}

	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public int getAccepted() {
		return accepted;
	}

	public void setAccepted(int accepted) {
		this.accepted = accepted;
	}

	public Date getAcceptedDate() {
		return acceptedDate;
	}

	public void setAcceptedDate(Date acceptedDate) {
		this.acceptedDate = acceptedDate;
	}
}
