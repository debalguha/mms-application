package com.sms.persistence.model;

public enum ACCESS_LEVEL {
	ADMIN(2), USER(1);
	private int level; 
	
	private ACCESS_LEVEL(int level){
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
	public static ACCESS_LEVEL fromValue(int value){
		switch(value){
		case 1: 
			return USER;
		case 2:
			return ADMIN;
		default:
			return null;
		}
	}
}
