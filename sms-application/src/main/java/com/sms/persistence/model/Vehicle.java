package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;

@Entity
@Table(schema = "dbo", name = "vehicles")
public class Vehicle implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	@Column(name = "vehicle_year")
	private int vehicleYear;
	@Column(name = "vehicle_vin")
	private String vehicleVin;
	@Column(name = "serialnumber")
	private String serialNumber;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "customer_guid")
	private Customer customer;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "vehicle_type")
	private VehicleType vehicleType;
	
	@Column(name = "date_placed_in_service", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date serviceDate;
	@Column(name = "Warranty_Start_Date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date warrantyStartDate;
	@Column(name = "Warranty_End_Date", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date warrantyEndDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Next_Higher_Assembly", nullable = true)
	@JsonIgnore
	private Vehicle nextHigherAssembly;
	
	@Column(nullable = true)
	private String partNumber;
	@Column(nullable = true)
	private String nomenclature;
	
	private Integer active;
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getNomenclature() {
		return nomenclature;
	}
	public void setNomenclature(String nomenclature) {
		this.nomenclature = nomenclature;
	}
	public Vehicle(){this.active = 1;}
	public Vehicle(String guid){
		this.guid = Strings.isNullOrEmpty(guid)?null:guid;
	}
	

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getVehicleYear() {
		return vehicleYear;
	}

	public void setVehicleYear(int vehicleYear) {
		this.vehicleYear = vehicleYear;
	}

	public String getVehicleVin() {
		return vehicleVin;
	}

	public void setVehicleVin(String vehicleVin) {
		this.vehicleVin = vehicleVin;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public VehicleType getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vehicle [guid=" + guid + ", creationDate=" + creationDate + ", vehicleYear=" + vehicleYear + ", vehicleVin=" + vehicleVin + ", serialNumber=" + serialNumber + ", customer=" + customer + ", vehicleType=" + vehicleType + "]";
	}

	public Date getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}

	public Date getWarrantyStartDate() {
		return warrantyStartDate;
	}

	public void setWarrantyStartDate(Date warrantyStartDate) {
		this.warrantyStartDate = warrantyStartDate;
	}

	public Date getWarrantyEndDate() {
		return warrantyEndDate;
	}

	public void setWarrantyEndDate(Date warrantyEndDate) {
		this.warrantyEndDate = warrantyEndDate;
	}


	public Vehicle getNextHigherAssembly() {
		return nextHigherAssembly;
	}

	public void setNextHigherAssembly(Vehicle nextHigherAssembly) {
		this.nextHigherAssembly = nextHigherAssembly;
	}
	public Integer getActive() {
		return active;
	}
	public void setActive(Integer active) {
		this.active = active;
	}
	
}
