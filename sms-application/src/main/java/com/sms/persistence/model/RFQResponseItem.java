package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "rfq_vendor_response_items")
public class RFQResponseItem implements InventoryItem{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "rfq_response_guid")
	@JsonIgnore
	private RFQResponse rfqResponse;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rfq_item_guid")
	private RFQItem rfqItem;
	
	private int quantity;
	@Column(name = "unit_price")
	private long unitPrice;
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@JsonIgnore
	public RFQResponse getRfqResponse() {
		return rfqResponse;
	}
	@JsonIgnore
	public void setRfqResponse(RFQResponse rfqResponse) {
		this.rfqResponse = rfqResponse;
	}
	public RFQItem getRfqItem() {
		return rfqItem;
	}
	public void setRfqItem(RFQItem rfqItem) {
		this.rfqItem = rfqItem;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public long getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(long unitPrice) {
		this.unitPrice = unitPrice;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RFQResponseItem other = (RFQResponseItem) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "RFQResponseItem [guid=" + guid + ", creationDate=" + creationDate + ", quantity=" + quantity + ", unitPrice=" + unitPrice + "]";
	}
	public SystemPart getSystemPart() {
		return rfqItem.getSystemPart();
	}
	
	
}
