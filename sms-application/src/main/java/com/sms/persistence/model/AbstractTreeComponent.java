package com.sms.persistence.model;

import javax.persistence.Transient;

public abstract class AbstractTreeComponent {
	@Transient
	private Double commonCurrentTargetPrice;
	
	public abstract SystemTree getSystemTree();
	
	public Double getCommonCurrentTargetPrice() {
		return commonCurrentTargetPrice;
	}

	public void setCommonCurrentTargetPrice(Double commonCurrentTargetPrice) {
		this.commonCurrentTargetPrice = commonCurrentTargetPrice;
	}

}
