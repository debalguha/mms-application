package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Null;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.google.common.base.Strings;

@Entity
@Table(schema = "dbo", name = "System_Parts_Data")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SystemPart extends AbstractTreeComponent{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;

	@Column(name = "`Field 1`", nullable = true)
	private String field1;
	@Column(name = "`Field 2`", nullable = true)
	private String field2;
	@Column(name = "`Field 3`", nullable = true)
	private String field3;

	@Column(name = "assembly_number", nullable = true)
	private String assemblyNumber;
	@Column(name = "assembly_name", nullable = true)
	private String assemblyName;
	@Column(name = "barcode", nullable = true)
	private String barCode;
	private String manufacturer;
	@Column(name = "manufacturer_part_number", nullable = true)
	private String manufacturerPartNumber;
	@Column(name = "manufacturer_part_url", nullable = true)
	private String manufacturerPartURL;
	@Column(name = "last_order_url", nullable = true)
	private String lastOrderURL;
	
	@Column(name = "Quantity")
	protected Integer quantity;
	@Column(name = "UnitPrice")
	protected Double unitPrice=0.0d;
	@Column(name = "ExtendedPrice")
	protected Double extendedPrice=0.0d;
	@Column(name = "CommonCurrencyPrice")
	protected Double commonCurrencyPrice=0.0d;
	/**
	 * Columns added after first UAT
	 */
	private String oem;
	private String quoteCurrency;
	private String oemPartNum;
	private String subIdCode;
	private String ui;
	private String category;
	
	@Column(name="wbs_root")
	private String wbsRoot;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "wbs", nullable = true)
	//@JsonIgnore
	private WbsTree wbsTree;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
	@JoinColumn(name = "System_Tree_Guid")
	@Null
	private SystemTree systemTree;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "higher_assembly_node", nullable = true)
	@Null
	private SystemTree nextHigherAssembly;
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="systemPart")
	@JsonIgnore
	private Set<SystemPartVendor> systemPartVendors;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "systemPart")
	private Inventory inventory;
	
	@Transient
	private String nomenclature;
	
	/**
	 * Columns added after 
	 * 2nd UAT
	 */
	@Column(name = "CAGE_CODE", nullable = true)
	private String cageCode;
	@Column(name = "Material_Shape", nullable = true)
	private String materialShape;
	@Column(name = "Quantity_per_Application", nullable = true)
	private int quantityPerApplication;
	@Column(name = "Rev", nullable = true)
	private String rev;
	@Column(name = "Sequence", nullable = true)
	private String sequence;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	@JoinColumn(name = "Material_Name", nullable = true)
	private SystemPartMaterialName materialName;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	@JoinColumn(name = "Material_Type", nullable = true)
	private SystemPartMaterialType materialType;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	@JoinColumn(name = "Material_Specification", nullable = true)
	private SystemPartMaterialSpec materialSpec;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	@JoinColumn(name = "Process1", nullable = true)
	private SystemPartProcess process1;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	@JoinColumn(name = "Process2", nullable = true)
	private SystemPartProcess process2;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	@JoinColumn(name = "Process3", nullable = true)
	private SystemPartProcess process3;
	
	public String getNomenclature() {
		return nomenclature;
	}
	public void setNomenclature(String nomenclature) {
		this.nomenclature = nomenclature;
	}
	
	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getAssemblyNumber() {
		return assemblyNumber;
	}

	public void setAssemblyNumber(String assemblyNumber) {
		this.assemblyNumber = assemblyNumber;
	}

	public String getAssemblyName() {
		return assemblyName;
	}

	public void setAssemblyName(String assemblyName) {
		this.assemblyName = assemblyName;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getManufacturerPartNumber() {
		return manufacturerPartNumber;
	}

	public void setManufacturerPartNumber(String manufacturerPartNumber) {
		this.manufacturerPartNumber = manufacturerPartNumber;
	}

	public String getManufacturerPartURL() {
		return manufacturerPartURL;
	}

	public void setManufacturerPartURL(String manufacturerPartURL) {
		this.manufacturerPartURL = manufacturerPartURL;
	}

	public String getLastOrderURL() {
		return lastOrderURL;
	}

	public void setLastOrderURL(String lastOrderURL) {
		this.lastOrderURL = lastOrderURL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((barCode == null) ? 0 : barCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemPart other = (SystemPart) obj;
		if (barCode == null) {
			if (other.barCode != null)
				return false;
		} else if (!barCode.equals(other.barCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SystemPart [guid=" + guid + ", field1=" + field1 + ", field2=" + field2 + ", field3=" + field3 + ", assemblyNumber=" + assemblyNumber + ", assemblyName=" + assemblyName + ", barCode=" + barCode + ", manufacturer=" + manufacturer + ", manufacturerPartNumber=" + manufacturerPartNumber + ", manufacturerPartURL=" + manufacturerPartURL + ", lastOrderURL=" + lastOrderURL + "]";
	}

	public SystemTree getSystemTree() {
		return systemTree;
	}

	public void setSystemTree(SystemTree systemTree) {
		this.systemTree = systemTree;
	}

	public String getWbsRoot() {
		return wbsRoot;
	}

	public void setWbsRoot(String wbsRoot) {
		this.wbsRoot = wbsRoot;
	}
	//@JsonIgnore
	public WbsTree getWbsTree() {
		return wbsTree;
	}
	//@JsonIgnore
	public void setWbsTree(WbsTree wbsTree) {
		this.wbsTree = wbsTree;
	}

	@JsonIgnore
	public Set<SystemPartVendor> getSystemPartVendors() {
		return systemPartVendors;
	}

	@JsonIgnore
	public void setSystemPartVendors(Set<SystemPartVendor> systemPartVendors) {
		this.systemPartVendors = systemPartVendors;
	}
	@JsonIgnore
	public Inventory getInventory() {
		return inventory;
	}
	@JsonProperty
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getOem() {
		return oem;
	}
	public void setOem(String oem) {
		this.oem = oem;
	}
	public String getQuoteCurrency() {
		return quoteCurrency;
	}
	public void setQuoteCurrency(String quoteCurrency) {
		this.quoteCurrency = quoteCurrency;
	}
	public String getOemPartNum() {
		return oemPartNum;
	}
	public void setOemPartNum(String oemPartNum) {
		this.oemPartNum = oemPartNum;
	}
	public String getSubIdCode() {
		return subIdCode;
	}
	public void setSubIdCode(String subIdCode) {
		this.subIdCode = subIdCode;
	}
	public String getUi() {
		return ui;
	}
	public void setUi(String ui) {
		this.ui = ui;
	}
	public String getCageCode() {
		return cageCode;
	}
	public void setCageCode(String cageCode) {
		this.cageCode = cageCode;
	}
	public String getMaterialShape() {
		return materialShape;
	}
	public void setMaterialShape(String materialShape) {
		this.materialShape = materialShape;
	}
	public int getQuantityPerApplication() {
		return quantityPerApplication;
	}
	public void setQuantityPerApplication(int quantityPerApplication) {
		this.quantityPerApplication = quantityPerApplication;
	}
	public String getRev() {
		return rev;
	}
	public void setRev(String rev) {
		this.rev = rev;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public SystemPartMaterialName getMaterialName() {
		return materialName;
	}
	public void setMaterialName(SystemPartMaterialName materialName) {
		this.materialName = materialName;
	}
	public SystemPartMaterialType getMaterialType() {
		return materialType;
	}
	public void setMaterialType(SystemPartMaterialType materialType) {
		this.materialType = materialType;
	}
	public SystemPartMaterialSpec getMaterialSpec() {
		return materialSpec;
	}
	public void setMaterialSpec(SystemPartMaterialSpec materialSpec) {
		this.materialSpec = materialSpec;
	}
	public SystemPartProcess getProcess1() {
		return process1;
	}
	public void setProcess1(SystemPartProcess process1) {
		this.process1 = process1;
	}
	public SystemPartProcess getProcess2() {
		return process2;
	}
	public void setProcess2(SystemPartProcess process2) {
		this.process2 = process2;
	}
	public SystemPartProcess getProcess3() {
		return process3;
	}
	public void setProcess3(SystemPartProcess process3) {
		this.process3 = process3;
	}
	public SystemTree getNextHigherAssembly() {
		return nextHigherAssembly;
	}
	public void setNextHigherAssembly(SystemTree nextHigherAssembly) {
		this.nextHigherAssembly = nextHigherAssembly;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getExtendedPrice() {
		return extendedPrice;
	}
	public void setExtendedPrice(Double extendedPrice) {
		this.extendedPrice = extendedPrice;
	}
	public Double getCommonCurrencyPrice() {
		return commonCurrencyPrice;
	}
	public void setCommonCurrencyPrice(Double commonCurrencyPrice) {
		this.commonCurrencyPrice = commonCurrencyPrice;
	}
	
	@JsonSetter("quantity")
	public void setQuantity(String quantity) {
		setQuantity(Strings.isNullOrEmpty(quantity)?0:Integer.parseInt(quantity.replaceAll(",", "")));
	}

	@JsonSetter("unitPrice")
	public void setUnitPrice(String unitPrice) {
		setUnitPrice(Strings.isNullOrEmpty(unitPrice)?0.0d:Double.parseDouble(unitPrice.replaceAll(",", "")));
	}

	@JsonSetter("extendedPrice")
	public void setExtendedPrice(String extendedPrice) {
		setExtendedPrice(Strings.isNullOrEmpty(extendedPrice)?0.0d:Double.parseDouble(extendedPrice.replaceAll(",", "")));
	}

	@JsonSetter("commonCurrencyPrice")
	public void setCommonCurrencyPrice(String commonCurrencyPrice) {
		setCommonCurrencyPrice(Strings.isNullOrEmpty(commonCurrencyPrice)?0.0d:Double.parseDouble(commonCurrencyPrice.replaceAll(",", "")));
	}
}
