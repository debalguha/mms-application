package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "sync_settings")
public class SyncSetting implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@Column(name = "url")
	private String url;
	@Column(name = "frequency_hours")
	private String frequencyHours;
	@Column(name = "start_hour")
	private String startHour;
	@Column(name = "active")
	private String active;

	
	public SyncSetting(){}
	public SyncSetting(String guid){this.guid = guid;}
	public SyncSetting(String url, String frequencyHours, String startHoury, String active, String startHour) {
		super();
		this.url = url;
		this.frequencyHours = frequencyHours;
		this.startHour = startHour;
		this.active = active;

	}


	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFrequencyHours() {
		return frequencyHours;
	}
	public void setFrequencyHours(String frequencyHours) {
		this.frequencyHours = frequencyHours;
	}
	public String getStartHour() {
		return startHour;
	}
	public void setStartHour(String startHour) {
		this.startHour = startHour;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyncSetting other = (SyncSetting) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SyncSetting [guid=" + guid + ", url=" + url + ", frequencyHours=" + frequencyHours + ", startHour=" + startHour + ", active=" + active + "]";
	}
}
