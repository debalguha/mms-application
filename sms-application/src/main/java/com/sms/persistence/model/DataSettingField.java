	package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "data_setttings_fields")
public class DataSettingField {
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_guid")
	private User user;
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "data_settings_guid")
	private DataSetting dataSetting;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "data_type_guid")
	private DataSettingFieldType fieldType;
	@Column(name = "field_name")
	private String fieldName;
	@Column(name = "field_value_int")
	private Integer fieldValueInt;
	@Column(name = "field_value_real")
	private Float fieldValueReal;
	@Column(name = "field_value_varchar")
	private String fieldValueVarchar;
	@Column(name = "field_value_date")
	private Date fieldValueDate;
	private int active;
	@Column(name = "field_required")
	private int required;
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public DataSetting getDataSetting() {
		return dataSetting;
	}
	public void setDataSetting(DataSetting dataSetting) {
		this.dataSetting = dataSetting;
	}
	public DataSettingFieldType getFieldType() {
		return fieldType;
	}
	public void setFieldType(DataSettingFieldType fieldType) {
		this.fieldType = fieldType;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public Integer getFieldValueInt() {
		return fieldValueInt;
	}
	public void setFieldValueInt(Integer fieldValueInt) {
		this.fieldValueInt = fieldValueInt;
	}
	public Float getFieldValueReal() {
		return fieldValueReal;
	}
	public void setFieldValueReal(Float fieldValueReal) {
		this.fieldValueReal = fieldValueReal;
	}
	public String getFieldValueVarchar() {
		return fieldValueVarchar;
	}
	public void setFieldValueVarchar(String fieldValueVarchar) {
		this.fieldValueVarchar = fieldValueVarchar;
	}
	public Date getFieldValueDate() {
		return fieldValueDate;
	}
	public void setFieldValueDate(Date fieldValueDate) {
		this.fieldValueDate = fieldValueDate;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataSettingField other = (DataSettingField) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "DataSettingFields [guid=" + guid + ", creationDate=" + creationDate + ", user=" + user + ", dataSetting=" + dataSetting + ", fieldType=" + fieldType + ", fieldName=" + fieldName + ", fieldValueInt=" + fieldValueInt + ", fieldValueReal=" + fieldValueReal + ", fieldValueVarchar=" + fieldValueVarchar + ", fieldValueDate=" + fieldValueDate + ", active=" + active + "]";
	}
	public int getRequired() {
		return required;
	}
	public void setRequired(int required) {
		this.required = required;
	}
	
}
