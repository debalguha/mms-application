package com.sms.persistence.model;

public interface InventoryItem extends BaseEntity{
	public int getQuantity();
	public long getUnitPrice();
	public SystemPart getSystemPart();
}
