package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.sms.util.DTOUtils;
import com.sms.web.model.TREE_NODE_TYPE;

@Entity
@Table(schema = "dbo", name = "system_tree")
public class SystemTree implements AbstractTree {
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	private String nomenclature;
	private String description;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "system_tree_item_type_guid")
	private SystemTreeItemType treeItemType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_guid")
	@JsonIgnore
	private SystemTree parent;
	@OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
	private Set<SystemTree> children;
	
	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	public SystemTree(String guid){
		this.guid = Strings.isNullOrEmpty(guid)?null:guid;
	}

	public SystemTree(){
		children = Sets.newHashSet();
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNomenclature() {
		return nomenclature;
	}

	public void setNomenclature(String nomenclature) {
		this.nomenclature = nomenclature;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SystemTreeItemType getTreeItemType() {
		return treeItemType;
	}

	public void setTreeItemType(SystemTreeItemType treeItemType) {
		this.treeItemType = treeItemType;
	}
	@JsonIgnore
	public SystemTree getParent() {
		return parent;
	}
	@JsonIgnore
	public void setParent(SystemTree parent) {
		this.parent = parent;
	}

	public Set<SystemTree> getChildren() {
		return children;
	}

	public void setChildren(Set<SystemTree> children) {
		this.children = children;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemTree other = (SystemTree) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SystemTree [guid=" + guid + ", nomenclature=" + nomenclature + ", description=" + description + ", treeItemType=" + treeItemType + "]";
	}

	public TREE_NODE_TYPE getNodeType() {
		return DTOUtils.determineNodeTypeFromTreeItemType(treeItemType);
	}

	public boolean isRenderable() {
		return DTOUtils.isRenederable(treeItemType);
	}

	public void setParent(AbstractTree parentNode) {
		setParent((SystemTree)parentNode);
	}


}
