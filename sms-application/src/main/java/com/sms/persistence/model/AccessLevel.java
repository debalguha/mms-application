package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "access_levels", schema = "dbo")
public class AccessLevel  implements BaseEntity{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="access_level")
	private int id;
	@Column(name="Access_Level_Name", nullable = false)
	private String accessLevelName;
	@Column(name="date_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	public AccessLevel(){}
	
	public AccessLevel(String accessLevelName, Date creationDate) {
		super();
		this.accessLevelName = accessLevelName;
		this.creationDate = creationDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccessLevelName() {
		return accessLevelName;
	}
	public void setAccessLevelName(String accessLevelName) {
		this.accessLevelName = accessLevelName;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accessLevelName == null) ? 0 : accessLevelName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccessLevel other = (AccessLevel) obj;
		if (accessLevelName == null) {
			if (other.accessLevelName != null)
				return false;
		} else if (!accessLevelName.equals(other.accessLevelName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AccessLevel [id=" + id + ", accessLevelName=" + accessLevelName + ", creationDate=" + creationDate + "]";
	}
}
