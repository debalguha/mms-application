package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "System_Picture_Data")
public class SystemPicture {
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;
	
	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date creationDate;
	
	@Column(name="UploadedFileName", nullable = false)
	private String uploadedFileName;
	@Column(name="StoredFileName", nullable = false)
	private String storedFileName;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinColumn(name = "System_Tree_Guid")
	@JsonIgnore
	private SystemTree systemTree;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getUploadedFileName() {
		return uploadedFileName;
	}

	public void setUploadedFileName(String uploadedFileName) {
		this.uploadedFileName = uploadedFileName;
	}

	public String getStoredFileName() {
		return storedFileName;
	}

	public void setStoredFileName(String storedFileName) {
		this.storedFileName = storedFileName;
	}
	@JsonIgnore
	public SystemTree getSystemTree() {
		return systemTree;
	}
	@JsonIgnore
	public void setSystemTree(SystemTree systemTree) {
		this.systemTree = systemTree;
	}

	@Override
	public String toString() {
		return "SystemPicture [guid=" + guid + ", uploadedFileName=" + uploadedFileName + ", storedFileName=" + storedFileName + ", systemTree=" + systemTree + "]";
	}
	
}
