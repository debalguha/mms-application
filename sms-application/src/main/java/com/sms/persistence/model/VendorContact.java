package com.sms.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "vendor_contact", schema = "dbo")
@AssociationOverrides({
		@AssociationOverride(name = "pk.contact", 
			joinColumns = @JoinColumn(name = "contact_guid")),
		@AssociationOverride(name = "pk.vendor", 
			joinColumns = @JoinColumn(name = "vendor_guid")) })
public class VendorContact implements Serializable{
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	@JsonIgnore
	private VendorContactPK pk = new VendorContactPK();
	
	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	public VendorContact(){}
	public VendorContact(VendorContactPK pk){this.pk = pk;}
	
	@Transient
	@JsonIgnore
	public SystemVendor getVendor(){
		return getPk().getVendor();
	}
	
	public void setVendor(SystemVendor vendor){
		getPk().setVendor(vendor);
	}
	
	@Transient
	public Contact getContact(){
		return getPk().getContact();
	}
	
	public void setContact(Contact contact){
		getPk().setContact(contact);
	}

	@JsonIgnore
	public VendorContactPK getPk() {
		return pk;
	}

	public void setPk(VendorContactPK pk) {
		this.pk = pk;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendorContact other = (VendorContact) obj;
		if (pk == null) {
			if (other.pk != null)
				return false;
		} else if (!pk.equals(other.pk))
			return false;
		return true;
	}
	
}
