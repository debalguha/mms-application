package com.sms.persistence.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(SystemPicture.class)
public class SystemPicture_ {
	public static volatile SingularAttribute<SystemPicture, String> storedFileName;
}
