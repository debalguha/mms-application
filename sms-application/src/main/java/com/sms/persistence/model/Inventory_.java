package com.sms.persistence.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Inventory.class)
public class Inventory_ {
	public static volatile SingularAttribute<Inventory, SystemPart> systemPart;
	public static volatile SingularAttribute<Inventory, String> idCode;
}
