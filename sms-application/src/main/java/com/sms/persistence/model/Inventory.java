package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "inventory")
public class Inventory implements BaseEntity, Comparable<Inventory>{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "part_guid")
	private SystemPart systemPart;
	
	@Column(name = "high_unit_price_po")
	private String highUnitPricePOGuid;
	@Column(name = "low_unit_price_po")
	private String lowUnitPricePOGuid;
	@Column(name = "last_order_po_guid")
	private String lastOrderPOGuid;
	
	@Column(name = "quantity_on_hand")
	private int quantityOnHand;
	@Column(name = "low_quantity_threshold")
	private int lowQuantityThreshold;
	@Column(name = "standard_order_quantity")
	private int standardOrderQuantity;
	@Column(name = "id_code")
	private String idCode;
	
	private int active;

	@OneToMany(mappedBy = "inventory", fetch = FetchType.EAGER)
	@JsonIgnore
	@NotFound(action=NotFoundAction.IGNORE)
	private Set<InventoryVendor> inventoryVendors;
	
	@Column(name = "adjustment_quantity")
	private int adjustmentQuantity;
	@Column(name = "verified_quantity")
	private int verifiedQuantity;
	@Column(columnDefinition = "text")
	private String remarks;
	@Column(name = "adjustment_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date adjustmentDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "location")
	private Location location;
	
	public Inventory(){}
	public Inventory(int quantityOnHand, int lowQuantityThreshold, int standardOrderQuantity, String idCode){
		this.quantityOnHand = quantityOnHand;
		this.lowQuantityThreshold = lowQuantityThreshold;
		this.standardOrderQuantity = standardOrderQuantity;
		this.idCode = idCode;
	}
	
	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public SystemPart getSystemPart() {
		return systemPart;
	}

	public void setSystemPart(SystemPart systemPart) {
		this.systemPart = systemPart;
	}

	public String getHighUnitPricePOGuid() {
		return highUnitPricePOGuid;
	}

	public void setHighUnitPricePOGuid(String highUnitPricePOGuid) {
		this.highUnitPricePOGuid = highUnitPricePOGuid;
	}

	public String getLowUnitPricePOGuid() {
		return lowUnitPricePOGuid;
	}

	public void setLowUnitPricePOGuid(String lowUnitPricePOGuid) {
		this.lowUnitPricePOGuid = lowUnitPricePOGuid;
	}

	public String getLastOrderPOGuid() {
		return lastOrderPOGuid;
	}

	public void setLastOrderPOGuid(String lastOrderPOGuid) {
		this.lastOrderPOGuid = lastOrderPOGuid;
	}

	public int getQuantityOnHand() {
		return quantityOnHand;
	}

	public void setQuantityOnHand(int quantityOnHand) {
		this.quantityOnHand = quantityOnHand;
	}

	public int getLowQuantityThreshold() {
		return lowQuantityThreshold;
	}

	public void setLowQuantityThreshold(int lowQuantityThreshold) {
		this.lowQuantityThreshold = lowQuantityThreshold;
	}

	public int getStandardOrderQuantity() {
		return standardOrderQuantity;
	}

	public void setStandardOrderQuantity(int standardOrderQuantity) {
		this.standardOrderQuantity = standardOrderQuantity;
	}

	public String getIdCode() {
		return idCode;
	}

	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inventory other = (Inventory) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Inventory [guid=" + guid + ", creationDate=" + creationDate + ", systemPart=" + systemPart + ", highUnitPricePOGuid=" + highUnitPricePOGuid + ", lowUnitPricePOGuid=" + lowUnitPricePOGuid + ", lastOrderPOGuid=" + lastOrderPOGuid + ", quantityOnHand=" + quantityOnHand + ", lowQuantityThreshold=" + lowQuantityThreshold + ", standardOrderQuantity=" + standardOrderQuantity
				+ ", idCode=" + idCode + ", active=" + active + "]";
	}
	@JsonIgnore
	public Set<InventoryVendor> getInventoryVendors() {
		return inventoryVendors;
	}
	@JsonIgnore
	public void setInventoryVendors(Set<InventoryVendor> inventoryVendors) {
		this.inventoryVendors = inventoryVendors;
	}
	public int compareTo(Inventory o) {
		return o.creationDate.compareTo(this.creationDate);
	}
	public int getAdjustmentQuantity() {
		return adjustmentQuantity;
	}
	public void setAdjustmentQuantity(int adjustmentQuantity) {
		this.adjustmentQuantity = adjustmentQuantity;
	}
	public int getVerifiedQuantity() {
		return verifiedQuantity;
	}
	public void setVerifiedQuantity(int verifiedQuantity) {
		this.verifiedQuantity = verifiedQuantity;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Date getAdjustmentDate() {
		return adjustmentDate;
	}
	public void setAdjustmentDate(Date adjustmentDate) {
		this.adjustmentDate = adjustmentDate;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
}
