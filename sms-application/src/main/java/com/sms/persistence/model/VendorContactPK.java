package com.sms.persistence.model;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Embeddable
public class VendorContactPK implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@ManyToOne(fetch = FetchType.LAZY)
	private SystemVendor vendor;
	@ManyToOne(fetch = FetchType.LAZY)
	private Contact contact;
	
	public VendorContactPK(){}
	public VendorContactPK(SystemVendor vendor, Contact contact){
		this.vendor = vendor;
		this.contact = contact;
	}
	
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public SystemVendor getVendor() {
		return vendor;
	}
	public void setVendor(SystemVendor vendor) {
		this.vendor = vendor;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contact == null) ? 0 : contact.hashCode());
		result = prime * result + ((vendor == null) ? 0 : vendor.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendorContactPK other = (VendorContactPK) obj;
		if (contact == null) {
			if (other.contact != null)
				return false;
		} else if (!contact.equals(other.contact))
			return false;
		if (vendor == null) {
			if (other.vendor != null)
				return false;
		} else if (!vendor.equals(other.vendor))
			return false;
		return true;
	}
	
	
}
