package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "rfq")
public class RFQEntity implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@Column(name = "rfq_Number")
	private String rfqNumber;
	@Column(name = "date_issued")
	@Temporal(TemporalType.TIMESTAMP)
	private Date issuedDate;
	
	@Column(name = "status_date_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date statusDateCreated;
	
	@Column(name = "expiration_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expirationDate;
	
	private String notes;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rfq_type")
	private RFQType rfqType;
	
	@OneToMany(mappedBy = "rfqEntity", fetch = FetchType.EAGER)
	private Set<RFQItem> rfqItems;
	
	@OneToMany(mappedBy = "rfqEntity", fetch = FetchType.EAGER)
	private Set<RFQVendor> rfqVendors;
	
	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getRfqNumber() {
		return rfqNumber;
	}

	public void setRfqNumber(String rfqNumber) {
		this.rfqNumber = rfqNumber;
	}

	public Date getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	public Date getStatusDateCreated() {
		return statusDateCreated;
	}

	public void setStatusDateCreated(Date statusDateCreated) {
		this.statusDateCreated = statusDateCreated;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public RFQType getRfqType() {
		return rfqType;
	}

	public void setRfqType(RFQType rfqType) {
		this.rfqType = rfqType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RFQEntity other = (RFQEntity) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RFQEntity [guid=" + guid + ", creationDate=" + creationDate + ", rfqNumber=" + rfqNumber + ", issuedDate=" + issuedDate + ", statusDateCreated=" + statusDateCreated + ", notes=" + notes + ", rfqType=" + rfqType + "]";
	}

	public Set<RFQItem> getRfqItems() {
		return rfqItems;
	}

	public void setRfqItems(Set<RFQItem> rfqItems) {
		this.rfqItems = rfqItems;
	}

	public Set<RFQVendor> getRfqVendors() {
		return rfqVendors;
	}

	public void setRfqVendors(Set<RFQVendor> rfqVendors) {
		this.rfqVendors = rfqVendors;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
}
