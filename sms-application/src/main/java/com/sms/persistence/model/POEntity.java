package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "po")
public class POEntity implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@Column(name = "PO_Number")
	private String poNumber;
	@Column(name = "date_issued")
	@Temporal(TemporalType.TIMESTAMP)
	private Date issueDate;
	
	@Column(name = "status_date_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date statusDateCreated;
	
	@Column(name = "delivery_by_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deliveryByDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "po_type")
	private POType poType;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "vendor")
	private SystemVendor vendor;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rfq")
	private RFQEntity rfq;
	
	private String notes;
	@Column(name = "delivery_instructions")
	private String deliveryInstruction;
	@Column(name = "delivery_address")
	private String deliveryAddress;
	
	@OneToMany(mappedBy = "po", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	private Set<POItem> poItems;
	
	@Column(nullable = true)
	private float sAndH;
	@Column(nullable = true)
	private float taxes;
	@Column(nullable = true)
	private float duty;
	@Column(nullable = true)
	private float discount;
	//@Column(nullable = false)
	@Transient
	private double total;
	
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public Date getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	public Date getStatusDateCreated() {
		return statusDateCreated;
	}
	public void setStatusDateCreated(Date statusDateCreated) {
		this.statusDateCreated = statusDateCreated;
	}
	public POType getPoType() {
		return poType;
	}
	public void setPoType(POType poType) {
		this.poType = poType;
	}
	public SystemVendor getVendor() {
		return vendor;
	}
	public void setVendor(SystemVendor vendor) {
		this.vendor = vendor;
	}
	public RFQEntity getRfq() {
		return rfq;
	}
	public void setRfq(RFQEntity rfq) {
		this.rfq = rfq;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getDeliveryInstruction() {
		return deliveryInstruction;
	}
	public void setDeliveryInstruction(String deliveryInstruction) {
		this.deliveryInstruction = deliveryInstruction;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		POEntity other = (POEntity) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "POEntity [guid=" + guid + ", creationDate=" + creationDate + ", poNumber=" + poNumber + ", issueDate=" + issueDate + ", statusDateCreated=" + statusDateCreated + ", poType=" + poType + ", vendor=" + vendor + ", rfq=" + rfq + ", notes=" + notes + ", deliveryInstruction=" + deliveryInstruction + ", deliveryAddress=" + deliveryAddress + "]";
	}
	public Set<POItem> getPoItems() {
		return poItems;
	}
	public void setPoItems(Set<POItem> poItems) {
		this.poItems = poItems;
	}
	public Date getDeliveryByDate() {
		return deliveryByDate;
	}
	public void setDeliveryByDate(Date deliveryByDate) {
		this.deliveryByDate = deliveryByDate;
	}
	public float getsAndH() {
		return sAndH;
	}
	public void setsAndH(float sAndH) {
		this.sAndH = sAndH;
	}
	public float getTaxes() {
		return taxes;
	}
	public void setTaxes(float taxes) {
		this.taxes = taxes;
	}
	public float getDuty() {
		return duty;
	}
	public void setDuty(float duty) {
		this.duty = duty;
	}
	public float getDiscount() {
		return discount;
	}
	public void setDiscount(float discount) {
		this.discount = discount;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
}
