package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "System_Contract_Data")
public class SystemContract extends AbstractTreeComponent{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
	@JoinColumn(name = "System_Tree_Guid")
	private SystemTree systemTree;
	
	@Column(name = "Country")
	private String country;
	@Column(name = "AgencyCompany")
	private String agencyCompany;
	@Column(name = "Organization")
	private String organization;
	@Column(name = "`Customer/End User`")
	private String customerEnduser;
	@Column(name = "`Contract Number`")
	private String contractNumber;
	@Column(name = "`Contract Date`")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date contractDate;
	@Column(name = "`Shipping Address`")
	private String shippingAddress;
	@Column(name = "`Contract Currency`")
	private String contractCurrency;
	@Column(name = "`Common Currency`")
	private String commonCurrency;
	@Column(name = "`Exchange Rate`")
	private double exchangeRate;
	@Column(name = "`Shipping Address 1`")
	private String shippingAddress1;
	@Column(name = "`Shipping Address 2`")
	private String shippingAddress2;
	@Column(name = "`Shipping Address 3`")
	private String shippingAddress3;
	@Column(name = "`Shipping Address 4`")
	private String shippingAddress4;
	@Column(name = "`Shipping Address 5`")
	private String shippingAddress5;
	
	@Transient
	private String nomenclature;
	
	public String getNomenclature() {
		return nomenclature;
	}
	public void setNomenclature(String nomenclature) {
		this.nomenclature = nomenclature;
	}
	
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	@JsonIgnore
	public Date getCreationDate() {
		return creationDate;
	}
	@JsonIgnore
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public SystemTree getSystemTree() {
		return systemTree;
	}
	public void setSystemTree(SystemTree systemTree) {
		this.systemTree = systemTree;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAgencyCompany() {
		return agencyCompany;
	}
	public void setAgencyCompany(String agencyCompany) {
		this.agencyCompany = agencyCompany;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getCustomerEnduser() {
		return customerEnduser;
	}
	public void setCustomerEnduser(String customerEnduser) {
		this.customerEnduser = customerEnduser;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	public Date getContractDate() {
		return contractDate;
	}
	public void setContractDate(Date contractDate) {
		this.contractDate = contractDate;
	}
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getContractCurrency() {
		return contractCurrency;
	}
	public void setContractCurrency(String contractCurrency) {
		this.contractCurrency = contractCurrency;
	}
	public String getCommonCurrency() {
		return commonCurrency;
	}
	public void setCommonCurrency(String commonCurrency) {
		this.commonCurrency = commonCurrency;
	}
	public double getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(float exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public String getShippingAddress1() {
		return shippingAddress1;
	}
	public void setShippingAddress1(String shippingAddress1) {
		this.shippingAddress1 = shippingAddress1;
	}
	public String getShippingAddress2() {
		return shippingAddress2;
	}
	public void setShippingAddress2(String shippingAddress2) {
		this.shippingAddress2 = shippingAddress2;
	}
	public String getShippingAddress3() {
		return shippingAddress3;
	}
	public void setShippingAddress3(String shippingAddress3) {
		this.shippingAddress3 = shippingAddress3;
	}
	public String getShippingAddress4() {
		return shippingAddress4;
	}
	public void setShippingAddress4(String shippingAddress4) {
		this.shippingAddress4 = shippingAddress4;
	}
	public String getShippingAddress5() {
		return shippingAddress5;
	}
	public void setShippingAddress5(String shippingAddress5) {
		this.shippingAddress5 = shippingAddress5;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}
	@Override
	public String toString() {
		return "SystemContract [guid=" + guid + ", country=" + country + ", agencyCompany=" + agencyCompany + ", organization=" + organization + ", customerEnduser=" + customerEnduser + ", contractNumber=" + contractNumber + ", contractDate=" + contractDate + ", shippingAddress=" + shippingAddress + ", contractCurrency=" + contractCurrency + ", commonCurrency=" + commonCurrency
				+ ", exchangeRate=" + exchangeRate + ", shippingAddress1=" + shippingAddress1 + ", shippingAddress2=" + shippingAddress2 + ", shippingAddress3=" + shippingAddress3 + ", shippingAddress4=" + shippingAddress4 + ", shippingAddress5=" + shippingAddress5 + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemContract other = (SystemContract) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}
}
