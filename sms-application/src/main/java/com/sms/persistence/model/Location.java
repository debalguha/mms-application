package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="locations")
public class Location implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;
	
	@Column(name="Location_Name", nullable = false)
	private String locationName;
	@Column(name="date_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	public Location(){}
	public Location(String guid){
		this.guid = guid;
	}
	
	public Location(String locationName, Date creationDate) {
		super();
		this.locationName = locationName;
		this.creationDate = creationDate;
	}


	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((locationName == null) ? 0 : locationName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (locationName == null) {
			if (other.locationName != null)
				return false;
		} else if (!locationName.equals(other.locationName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Location [guid=" + guid + ", locationName=" + locationName + ", creationDate=" + creationDate + "]";
	}


	public String getGuid() {
		return guid;
	}


	public void setGuid(String guid) {
		this.guid = guid;
	}
	
}
