package com.sms.persistence.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(SystemPart.class)
public class SystemPart_ {
	public static volatile SingularAttribute<SystemPart, String> assemblyName;
}
