package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;
import com.sms.util.SMSUtil;

@Entity
@Table(schema = "dbo", name = "System_Parts_Data_Material_Specifications")
public class SystemPartMaterialSpec implements BaseEntity {
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;

	@Column(name = "material_specification")
	private String materialSpecification;

	public SystemPartMaterialSpec() {
		creationDate = new Date();
	}

	public SystemPartMaterialSpec(String materialSpecification) {
		super();
		if (!Strings.isNullOrEmpty(materialSpecification)) {
			if (!SMSUtil.validateIfStringIsGuid(materialSpecification))
				this.materialSpecification = materialSpecification;
			else
				this.guid = materialSpecification;
			this.creationDate = new Date();
		}
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemPartMaterialSpec other = (SystemPartMaterialSpec) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SystemPartMaterialName [guid=" + guid + ", creationDate=" + creationDate + ", materialSpecification=" + materialSpecification + "]";
	}

	public String getMaterialSpecification() {
		return materialSpecification;
	}

	public void setMaterialSpecification(String materialSpecification) {
		this.materialSpecification = materialSpecification;
	}
}
