package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "audit_log", schema = "dbo")
public class AuditLog implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;
	@Column(name="Timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@Column(name= "`Entry Type`")
	private String entryType;
	@Column(name= "`Action`")
	private String action;
	@Column(name= "`Notes`")
	private String notes;
	@Column(name= "`Associated File`")
	private String associatedFile;
	@Column(name= "client_ip_address")
	private String clientIpAddress;
	@Column(name= "host_ip_address")
	private String hostIpAddress;
	@Column(name= "host_name_address")
	private String hostNameAddress;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private User user;
	
	public AuditLog(){}
	
	public AuditLog(Date creationDate, String entryType, String action, String notes, String associatedFile, String clientIpAddress, String hostIpAddress, String hostNameAddress, User user) {
		super();
		this.creationDate = creationDate;
		this.entryType = entryType;
		this.action = action;
		this.notes = notes;
		this.associatedFile = associatedFile;
		this.clientIpAddress = clientIpAddress;
		this.hostIpAddress = hostIpAddress;
		this.hostNameAddress = hostNameAddress;
		this.user = user;
	}


	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getEntryType() {
		return entryType;
	}
	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getAssociatedFile() {
		return associatedFile;
	}
	public void setAssociatedFile(String associatedFile) {
		this.associatedFile = associatedFile;
	}
	public String getClientIpAddress() {
		return clientIpAddress;
	}
	public void setClientIpAddress(String clientIpAddress) {
		this.clientIpAddress = clientIpAddress;
	}
	public String getHostIpAddress() {
		return hostIpAddress;
	}
	public void setHostIpAddress(String hostIpAddress) {
		this.hostIpAddress = hostIpAddress;
	}
	public String getHostNameAddress() {
		return hostNameAddress;
	}
	public void setHostNameAddress(String hostNameAddress) {
		this.hostNameAddress = hostNameAddress;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuditLog other = (AuditLog) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AuditLog [guid=" + guid + ", creationDate=" + creationDate + ", entryType=" + entryType + ", action=" + action + ", associatedFile=" + associatedFile + ", clientIpAddress=" + clientIpAddress + ", hostIpAddress=" + hostIpAddress + ", hostNameAddress=" + hostNameAddress + "]";
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
