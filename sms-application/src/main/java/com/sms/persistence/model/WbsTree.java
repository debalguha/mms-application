package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.sms.web.model.TREE_NODE_TYPE;

@Entity
@Table(schema = "dbo", name = "wbs")
public class WbsTree implements AbstractTree{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	private String nomenclature;
	private String description;
	private String wbs;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_guid")
	@JsonIgnore
	private WbsTree parent;
	@OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<WbsTree> children;
	
	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;

	public WbsTree(){
		children = Sets.newHashSet();
	}
	
	public WbsTree(String guid){
		children = Sets.newHashSet();
		this.guid = Strings.isNullOrEmpty(guid)?null:guid;
	}
	
	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNomenclature() {
		return nomenclature;
	}

	public void setNomenclature(String nomenclature) {
		this.nomenclature = nomenclature;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@JsonIgnore
	public WbsTree getParent() {
		return parent;
	}
	@JsonIgnore
	public void setParent(WbsTree parent) {
		this.parent = parent;
	}
	@JsonIgnore
	public Set<WbsTree> getChildren() {
		return children;
	}
	@JsonIgnore
	public void setChildren(Set<WbsTree> children) {
		this.children = children;
	}
	@JsonIgnore
	public Date getCreationDate() {
		return creationDate;
	}
	@JsonIgnore
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WbsTree other = (WbsTree) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}

	public WbsTree(String guid, String nomenclature, String description, WbsTree parent, Set<WbsTree> children, Date creationDate) {
		super();
		this.guid = guid;
		this.nomenclature = nomenclature;
		this.description = description;
		this.parent = parent;
		this.children = children;
		this.creationDate = creationDate;
	}

	public TREE_NODE_TYPE getNodeType() {
		return TREE_NODE_TYPE.WBS;
	}

	public boolean isRenderable() {
		return true;
	}
	@JsonIgnore
	public void setParent(AbstractTree parentNode) {
		setParent((WbsTree)parentNode);
	}
	@JsonIgnore
	public SystemTreeItemType getTreeItemType() {
		throw new IllegalStateException("Not a System Tree");
	}

	public String getWbs() {
		return wbs;
	}

	public void setWbs(String wbs) {
		this.wbs = wbs;
	}
}
