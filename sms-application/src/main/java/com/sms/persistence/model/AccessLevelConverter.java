package com.sms.persistence.model;

import javax.persistence.AttributeConverter;

public class AccessLevelConverter implements AttributeConverter<ACCESS_LEVEL, Integer>{

	public Integer convertToDatabaseColumn(ACCESS_LEVEL attribute) {
		return attribute.getLevel();
	}

	public ACCESS_LEVEL convertToEntityAttribute(Integer dbData) {
		return ACCESS_LEVEL.fromValue(dbData);
	}

}
