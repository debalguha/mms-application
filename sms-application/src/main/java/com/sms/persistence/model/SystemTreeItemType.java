package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(schema = "dbo", name="system_tree_item_types")
public class SystemTreeItemType {
	
	public enum TreeItemTypes {
		CONTRACT("Contract"), SERVICE("Service"), PART("Part"), VENDOR("Vendor"), PICTURE("Picture"), DRAWING("Drawing"), LINE_ITEM("Line Item");
		private String itemTypeText;
		TreeItemTypes(String itemTypeText){ this.itemTypeText = itemTypeText;}
		@Override
		public String toString() {
			return this.itemTypeText;
		}
	}
	
	
	
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;
	
	@Column(name = "item_type", nullable = false)
	private String itemType;
	
	@Column(name = "description", nullable = false)
	private String description;
	@Column(name="data_table", nullable = false)
	private String dataTable;
	
	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDataTable() {
		return dataTable;
	}

	public void setDataTable(String dataTable) {
		this.dataTable = dataTable;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataTable == null) ? 0 : dataTable.hashCode());
		result = prime * result + ((itemType == null) ? 0 : itemType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemTreeItemType other = (SystemTreeItemType) obj;
		if (dataTable == null) {
			if (other.dataTable != null)
				return false;
		} else if (!dataTable.equals(other.dataTable))
			return false;
		if (itemType == null) {
			if (other.itemType != null)
				return false;
		} else if (!itemType.equals(other.itemType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SystemTreeItemType [guid=" + guid + ", itemType=" + itemType + ", description=" + description + ", dataTable=" + dataTable + "]";
	}
	
	
}
