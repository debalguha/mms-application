package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "customer", uniqueConstraints = {@UniqueConstraint(columnNames={"customer_number"})})
public class Customer implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@Column(name = "customer_number")
	private String customerNumber;
	@Column(name = "country")
	private String country;
	@Column(name = "agency_company")
	private String agencyCompany;
	@Column(name = "organization")
	private String organization;
	@Column(name = "customer_end_user")
	private String customerEndUser;
	@Column(name = "customer_date")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date customerDate;
	@Column(name = "contract_number")
	private String contractNumber;
	
	@OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<Vehicle> vehicles;
	
	
	public Customer(){}
	public Customer(String guid){this.guid = guid;}
	public Customer(String customerNumber, String country, String agencyCompany, String organization, String customerEndUser, Date customerDate,String contractNumber) {
		super();
		this.customerNumber = customerNumber;
		this.country = country;
		this.agencyCompany = agencyCompany;
		this.organization = organization;
		this.customerEndUser = customerEndUser;
		this.customerDate = customerDate;
		this.contractNumber = contractNumber;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAgencyCompany() {
		return agencyCompany;
	}
	public void setAgencyCompany(String agencyCompany) {
		this.agencyCompany = agencyCompany;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getCustomerEndUser() {
		return customerEndUser;
	}
	public void setCustomerEndUser(String customerEndUser) {
		this.customerEndUser = customerEndUser;
	}
	public Date getCustomerDate() {
		return customerDate;
	}
	public void setCustomerDate(Date customerDate) {
		this.customerDate = customerDate;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public Set<Vehicle> getVehicles() {
		return vehicles;
	}
	public void setVehicles(Set<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Customer [guid=" + guid + ", creationDate=" + creationDate + ", customerNumber=" + customerNumber + ", country=" + country + ", agencyCompany=" + agencyCompany + ", organization=" + organization + ", customerEndUser=" + customerEndUser + ", customerDate=" + customerDate + ", contractNumber=" + contractNumber + "]";
	}

}
