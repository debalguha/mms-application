package com.sms.persistence.model;

import java.util.Set;

import com.sms.web.model.TREE_NODE_TYPE;

public interface AbstractTree {
	public String getGuid();
	public String getNomenclature();
	public TREE_NODE_TYPE getNodeType();
	public Set<? extends AbstractTree> getChildren();
	public boolean isRenderable();
	public void setParent(AbstractTree parentNode);
	public SystemTreeItemType getTreeItemType();
}
