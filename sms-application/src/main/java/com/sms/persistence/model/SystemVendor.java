package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "dbo", name = "System_Vendor_Data")
public class SystemVendor implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinColumn(name = "System_Tree_Guid")
	@JsonIgnore
	private SystemTree systemTree;
	
	@Column(name = "Name", nullable = false)
	private String name;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Location")
	private Location location;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "pk.vendor")
	@NotFound(action=NotFoundAction.IGNORE)
	private Set<VendorContact> contacts;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="Type")
	private VendorType type;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="systemVendor")
	@JsonIgnore
	private Set<SystemPartVendor> systemPartVendor;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}
	@JsonIgnore
	public Date getCreationDate() {
		return creationDate;
	}
	@JsonIgnore
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@JsonIgnore
	public SystemTree getSystemTree() {
		return systemTree;
	}
	@JsonIgnore
	public void setSystemTree(SystemTree systemTree) {
		this.systemTree = systemTree;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public VendorType getType() {
		return type;
	}
	public void setType(VendorType type) {
		this.type = type;
	}
	@JsonIgnore
	public Set<SystemPartVendor> getSystemPartVendor() {
		return systemPartVendor;
	}
	@JsonIgnore
	public void setSystemPartVendor(Set<SystemPartVendor> systemPartVendor) {
		this.systemPartVendor = systemPartVendor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemVendor other = (SystemVendor) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SystemVendor [guid=" + guid + ", creationDate=" + creationDate + ", name=" + name + ", location=" + location + ", type=" + type + "]";
	}

	public Set<VendorContact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<VendorContact> contacts) {
		this.contacts = contacts;
	}
	
}
