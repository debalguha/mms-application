package com.sms.persistence.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.google.common.base.Strings;

@Entity
@Table(schema = "dbo", name = "System_Line_Item_data")
public class LineItem extends AbstractTreeComponent{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date creationDate;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
	@JoinColumn(name = "System_Tree_Guid")
	private SystemTree systemTree;
	
	@Column(name = "LineItem")
	private String lineItem;
	@Column(name = "IDCode")
	private String idCode;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Root_WBS_Guid")
	@JsonIgnore
	private WbsTree wbsTree;
	
	@Column(name = "Quantity")
	@JsonProperty
	protected Integer quantity;
	@Column(name = "UnitPrice")
	@JsonProperty
	protected Double unitPrice=0.0d;
	@Column(name = "ExtendedPrice")
	@JsonProperty
	protected Double extendedPrice=0.0d;
	@Column(name = "CommonCurrencyPrice")
	@JsonProperty
	protected Double commonCurrencyPrice=0.0d;
	
	@Column(name = "Nomenclature")
	private String nomenclature;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "System_Line_Item_Data_Type")
	private LineItemType lineItemType;
	
	public String getNomenclature() {
		return nomenclature;
	}
	public void setNomenclature(String nomenclature) {
		this.nomenclature = nomenclature;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	@JsonIgnore
	public Date getCreationDate() {
		return creationDate;
	}
	@JsonIgnore
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getLineItem() {
		return lineItem;
	}
	public void setLineItem(String lineItem) {
		this.lineItem = lineItem;
	}
	public String getIdCode() {
		return idCode;
	}
	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}
	@JsonIgnore
	public WbsTree getWbsTree() {
		return wbsTree;
	}
	@JsonIgnore
	public void setWbsTree(WbsTree wbsTree) {
		this.wbsTree = wbsTree;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LineItem other = (LineItem) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "LineItem [guid=" + guid + ", creationDate=" + creationDate + ", systemTree=" + systemTree + ", lineItem=" + lineItem + ", idCode=" + idCode + ", wbsTree=" + wbsTree + ", quantity=" + quantity + ", unitPrice=" + unitPrice + ", extendedPrice=" + extendedPrice + ", commonCurrencyPrice=" + commonCurrencyPrice + ", nomenclature=" + nomenclature + ", lineItemType=" + lineItemType + "]";
	}
	public SystemTree getSystemTree() {
		return systemTree;
	}
	public void setSystemTree(SystemTree systemTree) {
		this.systemTree = systemTree;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getExtendedPrice() {
		return extendedPrice;
	}
	public void setExtendedPrice(Double extendedPrice) {
		this.extendedPrice = extendedPrice;
	}
	public Double getCommonCurrencyPrice() {
		return commonCurrencyPrice;
	}
	public void setCommonCurrencyPrice(Double commonCurrencyPrice) {
		this.commonCurrencyPrice = commonCurrencyPrice;
	}
	public LineItemType getLineItemType() {
		return lineItemType;
	}
	public void setLineItemType(LineItemType lineItemType) {
		this.lineItemType = lineItemType;
	}
	
	@JsonSetter("quantity")
	public void setQuantity(String quantity) {
		setQuantity(Strings.isNullOrEmpty(quantity)?0:Integer.parseInt(quantity.replaceAll(",", "")));
	}

	@JsonSetter("unitPrice")
	public void setUnitPrice(String unitPrice) {
		setUnitPrice(Strings.isNullOrEmpty(unitPrice)?0.0d:Double.parseDouble(unitPrice.replaceAll(",", "")));
	}

	@JsonSetter("extendedPrice")
	public void setExtendedPrice(String extendedPrice) {
		setExtendedPrice(Strings.isNullOrEmpty(extendedPrice)?0.0d:Double.parseDouble(extendedPrice.replaceAll(",", "")));
	}

	@JsonSetter("commonCurrencyPrice")
	public void setCommonCurrencyPrice(String commonCurrencyPrice) {
		setCommonCurrencyPrice(Strings.isNullOrEmpty(commonCurrencyPrice)?0.0d:Double.parseDouble(commonCurrencyPrice.replaceAll(",", "")));
	}
	
}
