package com.sms.persistence.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Sets;

@Entity
@Table(schema = "dbo", name = "workorders")
public class WorkOrder implements BaseEntity{
	@Id
	@GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	@GeneratedValue(generator = "generator")
	@Column(name = "GUID", columnDefinition = "uniqueidentifier")
	private String guid;

	@Column(name = "date_created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_guid", nullable = false)
	private User createByUser;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tech_guid", nullable = true)
	private User techUserGuid;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "supervisor_guid", nullable = true)
	private User superVisorGuid;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "customer_guid", nullable = true)
	private Customer customer;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "location_guid", nullable = true)
	private Location location;
	
	private int priority;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "work_order_type_guid", nullable = true)
	private WorkOrderType workOrderType;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "vehicle_guid", nullable = true)
	private Vehicle vehicle;
	
	@Column(name = "date_scheduled")
	@Temporal(TemporalType.TIMESTAMP)
	private Date scheduledDate;
	
	@Column(name = "date_promised")
	@Temporal(TemporalType.TIMESTAMP)
	private Date promisedDate;
	
	@Column(name = "date_completed")
	@Temporal(TemporalType.TIMESTAMP)
	private Date completedDate;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "workOrder", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private Set<WorkOrderItem> workOrderItems = Sets.newHashSet();

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public User getCreateByUser() {
		return createByUser;
	}

	public void setCreateByUser(User createByUser) {
		this.createByUser = createByUser;
	}

	public User getTechUserGuid() {
		return techUserGuid;
	}

	public void setTechUserGuid(User techUserGuid) {
		this.techUserGuid = techUserGuid;
	}

	public User getSuperVisorGuid() {
		return superVisorGuid;
	}

	public void setSuperVisorGuid(User superVisorGuid) {
		this.superVisorGuid = superVisorGuid;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public WorkOrderType getWorkOrderType() {
		return workOrderType;
	}

	public void setWorkOrderType(WorkOrderType workOrderType) {
		this.workOrderType = workOrderType;
	}

	public Date getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public Date getPromisedDate() {
		return promisedDate;
	}

	public void setPromisedDate(Date promisedDate) {
		this.promisedDate = promisedDate;
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkOrder other = (WorkOrder) obj;
		if (guid == null) {
			if (other.guid != null)
				return false;
		} else if (!guid.equals(other.guid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WorkOrder [guid=" + guid + ", creationDate=" + creationDate + ", createByUser=" + createByUser + ", techUserGuid=" + techUserGuid + ", superVisorGuid=" + superVisorGuid + ", customer=" + customer + ", location=" + location + ", priority=" + priority + ", workOrderType=" + workOrderType + ", scheduledDate=" + scheduledDate + ", promisedDate=" + promisedDate + ", completedDate="
				+ completedDate + "]";
	}
	@JsonIgnore
	public Set<WorkOrderItem> getWorkOrderItems() {
		return workOrderItems;
	}

	public void setWorkOrderItems(Set<WorkOrderItem> workOrderItems) {
		this.workOrderItems = workOrderItems;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
}
