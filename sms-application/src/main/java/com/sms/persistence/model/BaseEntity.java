package com.sms.persistence.model;

import java.util.Date;

public interface BaseEntity {
	public Date getCreationDate();
}
