package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sms.persistence.model.EmailLog;

public interface EmailLogRepository extends JpaRepository<EmailLog, String>{
	@Query("from EmailLog where status = 'PENDING'")
	public Collection<EmailLog> findAllPendingEmailToSend();
}
