package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.ContentType;

public interface ContentTypeRepository extends JpaRepository<ContentType, String>{
	@Query("from ContentType where contentTypeName = :contentTypeName")
	ContentType getContentTypeByTypeName(@Param("contentTypeName") String contentTypeName);

}
