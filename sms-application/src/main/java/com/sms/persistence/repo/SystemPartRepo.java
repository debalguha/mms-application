package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemTree;

public interface SystemPartRepo extends JpaRepository<SystemPart, String> {
	@Query("from SystemPart p where p.systemTree = :tree")
	public SystemPart findByTreeGuid(@Param("tree") SystemTree tree);

	public Collection<SystemPart> findByManufacturerPartNumber(String partNumber);
}
