package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.SystemPartMaterialType;

public interface SystemPartMaterialTypeRepo extends JpaRepository<SystemPartMaterialType, String>{
}
