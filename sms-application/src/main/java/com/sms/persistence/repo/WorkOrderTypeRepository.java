package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.WorkOrderType;

public interface WorkOrderTypeRepository extends JpaRepository<WorkOrderType, String>{}
