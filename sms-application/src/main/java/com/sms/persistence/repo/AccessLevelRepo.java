package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.AccessLevel;

public interface AccessLevelRepo extends JpaRepository<AccessLevel, Integer>{
	@Query("from AccessLevel l where l.accessLevelName = :accessLevelName")
	public AccessLevel findByName(@Param("accessLevelName")String accessLevelName);
}
