package com.sms.persistence.repo;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.AuditLog;

public interface AuditLogRepository extends JpaRepository<AuditLog, String>{
	@Query("from AuditLog a where creationDate between :startDate and :endDate")
	public Collection<AuditLog> findAuditLogWithDateRange(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
