package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.model.VendorContact;
import com.sms.persistence.model.VendorContactPK;

public interface VendorContactRepository extends JpaRepository<VendorContact, VendorContactPK> {
	@Query("from VendorContact where pk.vendor=:vendor")
	public Collection<VendorContact> findAllContactsForVendor(@Param("vendor") SystemVendor vendor);
}
