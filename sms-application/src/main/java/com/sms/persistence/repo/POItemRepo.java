package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.POEntity;
import com.sms.persistence.model.POItem;
import com.sms.persistence.model.SystemPart;

public interface POItemRepo extends JpaRepository<POItem, String>{
	@Query("from POItem where po=:po")
	public Collection<POItem> findByPOEntity(@Param("po") POEntity po);
	@Query("from POItem where systemPart=:systemPart")
	public Collection<POItem> findBySystempart(@Param("systemPart") SystemPart systemPart);
}
