package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.Content;

public interface ContentRepository extends JpaRepository<Content, String>{

}
