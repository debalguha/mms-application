package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemPartVendor;
import com.sms.persistence.model.SystemVendor;

public interface SystemPartVendorRepo extends JpaRepository<SystemPartVendor, String>{
	@Query("from SystemPartVendor where systemPart = :part")
	public Collection<SystemPartVendor> findSystemPartVendorByPart(@Param("part") SystemPart systemPart);
	
	@Query("from SystemPartVendor where systemVendor = :vendor")
	public Collection<SystemPartVendor> findSystemPartVendorByVendor(@Param("vendor") SystemVendor vendor);
}
