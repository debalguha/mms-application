package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemServiceData;
import com.sms.persistence.model.SystemTree;

public interface SystemServiceDataRepo extends JpaRepository<SystemServiceData, String>{
	@Query("from SystemServiceData s where s.systemTree = :systemTreeNode")
	public SystemServiceData findBySystemTreeNode(@Param("systemTreeNode") SystemTree systemTreeNode);
}
