package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.model.SystemTreeItemType;

public interface SystemTreeRepo  extends JpaRepository<SystemTree, String>{
	@Query("from SystemTree where treeItemType = :treeItemType and nomenclature like CONCAT(:nomenclature, '%')")
	public Collection<SystemTree> findByItemTypeAndNomenclature(@Param("treeItemType") SystemTreeItemType treeItemType, @Param("nomenclature") String nomenclature);
	@Query("from SystemTree where treeItemType = :lineItemType or treeItemType = :serviceType or treeItemType = :partType")
	public Collection<? extends AbstractTree> findAllSystemTreeOfLineItemAndService(@Param("lineItemType") SystemTreeItemType lineItemType, @Param("serviceType") SystemTreeItemType serviceType
			, @Param("partType") SystemTreeItemType partType);
	public Collection<? extends SystemTree> findByNomenclature(String nomenclature);

}
