package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.User;

public interface UserRepository extends JpaRepository<User, String>{
	@Query("from User u where email = :email")
	public User findByEmail(@Param("email") String email);
	@Query("from User u where firstName like CONCAT(:firstName, '%')")
	public Collection<User> findMatchingName(@Param("firstName") String firstName);
}
