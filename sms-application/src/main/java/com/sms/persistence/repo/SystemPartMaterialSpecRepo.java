package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.SystemPartMaterialSpec;

public interface SystemPartMaterialSpecRepo extends JpaRepository<SystemPartMaterialSpec, String>{
}
