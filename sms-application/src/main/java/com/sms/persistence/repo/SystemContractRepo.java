package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemContract;
import com.sms.persistence.model.SystemTree;

public interface SystemContractRepo extends JpaRepository<SystemContract, String>{
	@Query("from SystemContract s where s.contractNumber = :contractNumber")
	public SystemContract findByContractNumber(@Param("contractNumber") String contractNumber);
	@Query("from SystemContract s where s.systemTree = :systemTreeNode")
	public SystemContract findBySystemTreeNode(@Param("systemTreeNode") SystemTree systemTreeNode);
}
