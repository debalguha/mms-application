package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.DataSetting;

public interface DataSettingRepo extends JpaRepository<DataSetting, String> {

}
