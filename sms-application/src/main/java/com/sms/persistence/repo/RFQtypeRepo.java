package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.RFQType;

public interface RFQtypeRepo extends JpaRepository<RFQType, String> {
	@Query("from RFQType where type = :rfqType")
	public RFQType findRFQTypeByName(@Param("rfqType") String rfqType);
}
