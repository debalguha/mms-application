package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sms.persistence.model.RFQEntity;

public interface RFQRepo extends JpaRepository<RFQEntity, String> {
	
}
