package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.DataSettingFieldType;

public interface DataSettingFieldTypeRepo extends JpaRepository<DataSettingFieldType, String> {

}
