package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.RFQEntity;
import com.sms.persistence.model.RFQVendor;
import com.sms.persistence.model.SystemVendor;

public interface RFQVendorRepo extends JpaRepository<RFQVendor, String> {
	
	@Query("from RFQVendor where rfqEntity = :rfqEntity and systemVendor = :systemVendor")
	public RFQVendor findByRFQAndVendor(@Param("rfqEntity")RFQEntity rfqEntity, @Param("systemVendor")SystemVendor systemVendor);

}
