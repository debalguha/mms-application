package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.Customer;

public interface CustomerRepo extends JpaRepository<Customer, String>{
	@Query("from Customer where customerEndUser like CONCAT(:customerEndUser, '%')")
	public Collection<Customer> findByCustomerName(@Param("customerEndUser") String customerEndUser);

}
