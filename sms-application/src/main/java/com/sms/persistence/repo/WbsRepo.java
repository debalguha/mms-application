package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sms.persistence.model.WbsTree;

public interface WbsRepo extends JpaRepository<WbsTree, String>{

}
