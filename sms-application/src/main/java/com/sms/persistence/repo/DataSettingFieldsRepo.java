package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.DataSetting;
import com.sms.persistence.model.DataSettingField;

public interface DataSettingFieldsRepo extends  JpaRepository<DataSettingField, String> {
	@Query("from DataSettingField where active=1")
	public Collection<DataSettingField> findAllActiveDataSettingFields();
	@Query("from DataSettingField where active=1 and dataSetting = :dataSetting")
	public Collection<DataSettingField> findAllActiveDataSettingFields(@Param("dataSetting") DataSetting dataSetting);
	@Query("from DataSettingField where dataSetting = :dataSetting")
	public Collection<DataSettingField> findAllDataSettingFields(@Param("dataSetting") DataSetting dataSetting);
}
