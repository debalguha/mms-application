package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.WbsPicture;
import com.sms.persistence.model.WbsTree;

public interface WbsPictureRepo extends JpaRepository<WbsPicture, String>{
	@Query("from WbsPicture where wbsTree = :wbsTree")
	public Collection<WbsPicture> findAllAssociatedPicture(@Param("wbsTree") WbsTree wbsTree);
	@Query("from WbsPicture where wbsTree = :wbsTree and systemPicture = :systemPicture")
	public WbsPicture findByWbsAndPictureGuid(@Param("wbsTree") WbsTree wbsTree, @Param("systemPicture") SystemPicture systemPicture);

}
