package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.RFQEntity;
import com.sms.persistence.model.RFQResponse;
import com.sms.persistence.model.SystemVendor;

public interface RFQResponseRepo extends JpaRepository<RFQResponse, String>{
	@Query("from RFQResponse r where r.systemVendor = :rfqVendor and rfqEntity = :rfqEntity")
	public RFQResponse findResponseByVendor(@Param("rfqVendor") SystemVendor rfqVendor, @Param("rfqEntity") RFQEntity rfqEntity);
	@Query("select count(guid) from RFQResponse where rfqEntity = :rfqEntity")
	public int getResponseCount(@Param("rfqEntity") RFQEntity rfqEntity);

}
