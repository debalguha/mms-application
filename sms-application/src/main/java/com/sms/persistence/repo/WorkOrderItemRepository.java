package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.WorkOrderItem;

public interface WorkOrderItemRepository extends JpaRepository<WorkOrderItem, String>{

}
