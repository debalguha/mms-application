package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.Customer;
import com.sms.persistence.model.Vehicle;

public interface VehicleRepository extends JpaRepository<Vehicle, String>{
	@Query("from Vehicle where customer= :customer and active=1")
	Collection<Vehicle> findByCustomer(@Param("customer") Customer customer);
	@Query("from Vehicle where active= 1")
	Collection<Vehicle> findAllActiveVehicles();
	@Query("from Vehicle where active= 1 and nomenclature=:nomenclature")
	Collection<Vehicle> findAllActiveVehiclesForNomenclature(@Param("nomenclature") String nomenclature);
}
