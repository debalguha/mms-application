package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.VendorType;

public interface VendorTypeRepo extends JpaRepository<VendorType, String>{
	@Query("from VendorType where name=:typeName")
	public VendorType findVendorTypeByName(@Param("typeName") String typeName);
}
