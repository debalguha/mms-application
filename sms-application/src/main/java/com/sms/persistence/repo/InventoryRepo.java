package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.SystemPart;

public interface InventoryRepo extends JpaRepository<Inventory, String>, JpaSpecificationExecutor<Inventory> {
	@Query("from Inventory where systemPart = :systemPart")
	public Inventory findBysystemPart(@Param("systemPart") SystemPart systemPart);
	@Query("select count(distinct i.guid) from Inventory i where i.idCode like CONCAT('%', CONCAT(:searchTerm, '%')) or i.systemPart.assemblyName like CONCAT('%', CONCAT(:searchTerm, '%'))")
	public int findTotalNumberOfRecords(@Param("searchTerm") String searchTerm);
	@Query("select count(distinct i.guid) from Inventory i")
	public int findTotalNumberOfRecords();

}
