package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.VehicleType;

public interface VehicleTypeRepo extends JpaRepository<VehicleType, String>{}
