package com.sms.persistence.repo;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.Customer;
import com.sms.persistence.model.WorkOrder;

public interface WorkOrderRepository extends JpaRepository<WorkOrder, String>{
	@Query("from WorkOrder where completedDate is not null and completedDate < :today")
	public Collection<WorkOrder> findAllCompletedWorkOrders(@Param("today") Date today);
	
	@Query("from WorkOrder where completedDate is null and promisedDate is not null and promisedDate > :today")
	public Collection<WorkOrder> findAllInProgressWorkOrders(@Param("today") Date today);
	
	@Query("from WorkOrder where completedDate is null and scheduledDate is not null and scheduledDate > :today")
	public Collection<WorkOrder> findAllInScheduledWorkOrders(@Param("today") Date today);

	@Query("from WorkOrder where customer = :customer")
	public Collection<WorkOrder> findAllWorkOrdersOf(@Param("customer") Customer customer);
}
