package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.POType;

public interface POTypeRepo extends JpaRepository<POType, String> {

}
