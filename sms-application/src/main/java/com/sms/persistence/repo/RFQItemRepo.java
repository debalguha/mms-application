package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.RFQItem;

public interface RFQItemRepo extends JpaRepository<RFQItem, String> {

}
