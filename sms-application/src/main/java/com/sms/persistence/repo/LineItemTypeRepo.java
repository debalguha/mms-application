package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.LineItemType;

public interface LineItemTypeRepo extends JpaRepository<LineItemType, String>{
	@Query("from LineItemType where lineItemType = :lineItemType")
	public LineItemType findLineItemByType(@Param("lineItemType")String lineItemType);

}
