package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemTreeItemType;

public interface SystemTreeItemTypeRepo extends JpaRepository<SystemTreeItemType, String> {
	@Query("from SystemTreeItemType where itemType = :itemType")
	public SystemTreeItemType getItemTypeByName(@Param("itemType") String itemType);
	
	@Query("from SystemTreeItemType where dataTable = :dataTable")
	public SystemTreeItemType getItemTypeByDataTable(@Param("dataTable") String dataTable);
}
