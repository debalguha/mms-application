package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemTree;

public interface SystemDrawingRepo extends JpaRepository<SystemDrawing, String> , JpaSpecificationExecutor<SystemDrawing>{
	@Query("from SystemDrawing d where d.systemTree = :tree")
	public SystemDrawing findByTreeGuid(@Param("tree") SystemTree tree);
	@Query("from SystemDrawing d where d.storedFileName like CONCAT('%', CONCAT(:fileNameLike, '%'))")
	public Collection<SystemDrawing> findByPictureWithNameAlike(@Param("fileNameLike") String fileNameLike);
	@Query("select count(d.guid) from SystemDrawing d")
	public int getTotalDrawingCount();
	@Query("select count(d.guid) from SystemDrawing d where d.storedFileName like CONCAT('%', CONCAT(:fileNameLike, '%'))")
	public int getTotalDrawingCount(@Param("fileNameLike") String fileNameLike);
}
