package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.RFQItem;
import com.sms.persistence.model.RFQResponseItem;

public interface RFQResponseItemRepo extends JpaRepository<RFQResponseItem, String> {
	@Query("from RFQResponseItem ri where ri.rfqItem = :item")
	public Collection<RFQResponseItem> findReponsesForItem(@Param("item") RFQItem rfqItem);
}
