package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.SystemTree;

public interface SystemPictureRepo extends JpaRepository<SystemPicture, String>, JpaSpecificationExecutor<SystemPicture> {
	@Query("from SystemPicture p where p.systemTree = :tree")
	public SystemPicture findByTreeGuid(@Param("tree") SystemTree tree);
	@Query("from SystemPicture p where p.storedFileName like CONCAT('%', CONCAT(:fileNameLike, '%'))")
	public Collection<SystemPicture> findByPictureWithNameAlike(@Param("fileNameLike" )String fileNameLike);
	@Query("select count(p.guid) from SystemPicture p")
	public int getTotalPictureCount();
	@Query("select count(p.guid) from SystemPicture p where p.storedFileName like CONCAT('%', CONCAT(:fileNameLike, '%'))")
	public int getTotalPictureCount(@Param("fileNameLike" )String fileNameLike);
}
