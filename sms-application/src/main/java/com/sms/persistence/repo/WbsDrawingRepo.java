package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.WbsDrawing;
import com.sms.persistence.model.WbsTree;

public interface WbsDrawingRepo extends JpaRepository<WbsDrawing, String>{
	@Query("from WbsDrawing where wbsTree = :wbsTree")
	public Collection<WbsDrawing> findAllAssociatedPicture(@Param("wbsTree") WbsTree wbsTree);
	@Query("from WbsDrawing where wbsTree = :wbsTree and systemDrawing = :systemDrawing")
	public WbsDrawing findByWbsAndDrawingGuid(@Param("wbsTree") WbsTree wbsTree, @Param("systemDrawing") SystemDrawing systemDrawing);

}
