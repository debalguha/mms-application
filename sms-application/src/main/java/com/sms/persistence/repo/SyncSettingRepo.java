package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.SyncSetting;

public interface SyncSettingRepo extends JpaRepository<SyncSetting, String>{

}
