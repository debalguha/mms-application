package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.POEntity;

public interface POEntityRepo  extends JpaRepository<POEntity, String> {
}
