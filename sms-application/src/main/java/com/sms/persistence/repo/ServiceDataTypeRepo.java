package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.ServiceDataType;

public interface ServiceDataTypeRepo extends JpaRepository<ServiceDataType, String> {
	@Query("from ServiceDataType where serviceType = :serviceType")
	public ServiceDataType findServiceDataTypeByType(@Param("serviceType")String serviceDataTypeName);

}
