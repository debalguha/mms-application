package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.LineItem;
import com.sms.persistence.model.SystemTree;

public interface LineItemRepo extends JpaRepository<LineItem, String>{
	@Query("from LineItem s where s.idCode = :idCode")
	public LineItem findByIDCode(@Param("idCode") String idCode);
	@Query("from LineItem s where s.systemTree = :systemTreeNode")
	public LineItem findBySystemTreeNode(@Param("systemTreeNode") SystemTree systemTreeNode);
}
