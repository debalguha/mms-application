package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.SystemVendor;

public interface SystemVendorRepo extends JpaRepository<SystemVendor, String>{
	@Query("from SystemVendor where name = :vendorName")
	public SystemVendor findByVendorName(@Param("vendorName") String vendorName);
}
