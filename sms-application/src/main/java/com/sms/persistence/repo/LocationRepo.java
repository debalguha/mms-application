package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.Location;

public interface LocationRepo extends JpaRepository<Location, String>{
	@Query("from Location l where l.locationName = :locationName")
	public Location findByName(@Param("locationName")String locationName);
}
