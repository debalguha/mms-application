package com.sms.persistence.repo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.InventoryVendor;
import com.sms.persistence.model.SystemVendor;

public interface InventoryVendorRepo extends JpaRepository<InventoryVendor, String>{
	@Query("from InventoryVendor where inventory = :inventory")
	public Collection<InventoryVendor> findInventoryVendorByInventory(@Param("inventory") Inventory inventory);
	
	@Query("from InventoryVendor where vendor = :vendor")
	public Collection<InventoryVendor> findInventoryVendorByVendor(@Param("vendor") SystemVendor vendor);
}
