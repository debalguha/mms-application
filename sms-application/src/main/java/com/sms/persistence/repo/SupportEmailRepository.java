package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.SupportEmail;

public interface SupportEmailRepository extends JpaRepository<SupportEmail, String>{

}
