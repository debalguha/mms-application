package com.sms.persistence.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sms.persistence.model.SystemPartMaterialName;

public interface SystemPartMaterialNameRepo extends JpaRepository<SystemPartMaterialName, String>{
}
