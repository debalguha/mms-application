package com.sms.persistence.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.sms.persistence.model.Vehicle;
import com.sms.persistence.model.VehicleType;
import com.sms.persistence.repo.CustomerRepo;
import com.sms.persistence.repo.VehicleRepository;
import com.sms.persistence.repo.VehicleTypeRepo;

@Service
@Transactional
public class VehicleService {
	private static final Logger logger = LoggerFactory.getLogger(VehicleService.class);
	@Autowired
	private VehicleRepository vehicleRepo;
	@Autowired
	private VehicleTypeRepo vehicleTypeRepo;
	@Autowired
	private CustomerRepo customerRepo;
	
	@Transactional(readOnly = true)
	public Collection<Vehicle> findAllVehicles(){
		return vehicleRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Collection<Vehicle> findAllActiveVehicles(){
		return vehicleRepo.findAllActiveVehicles();
	}
	
	@Transactional(readOnly = true)
	public Collection<VehicleType> findAllVehicleTypes(){
		return vehicleTypeRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Collection<Vehicle> searchVehiclesForCustomer(String customerGuid){
		try {
			return vehicleRepo.findByCustomer(customerRepo.findOne(customerGuid));
		} catch (Exception e) {
			logger.error("Unable to find a vehicle for customer with guid: "+customerGuid, e);
			return Collections.emptyList();
		}
	}
	
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Vehicle createVehicle(Vehicle vehicle){
		if(Strings.isNullOrEmpty(vehicle.getGuid()))
			vehicle.setGuid(null);
		if(vehicle.getNextHigherAssembly()!=null && Strings.isNullOrEmpty(vehicle.getNextHigherAssembly().getGuid()))
			vehicle.setNextHigherAssembly(null);
		vehicle.setCustomer(customerRepo.findOne(vehicle.getCustomer().getGuid()));
		vehicle.setVehicleType(vehicleTypeRepo.findOne(vehicle.getVehicleType().getGuid()));
		vehicle.setCreationDate(new Date());
		return vehicleRepo.save(vehicle);
	}

	@Transactional(readOnly = true)
	public Vehicle findVehicleByGuid(String vehicleGuid) {
		return vehicleRepo.findOne(vehicleGuid);
	}
	
	@Transactional(readOnly = true)
	public int findNumberOfActiveVehiclesForNomenclature(String nomenclature){
		return vehicleRepo.findAllActiveVehiclesForNomenclature(nomenclature).size();
	}

	public Collection<Vehicle> findActiveVehiclesWithNomenclature(String nomenclature) {
		return vehicleRepo.findAllActiveVehiclesForNomenclature(nomenclature);
	}
}
