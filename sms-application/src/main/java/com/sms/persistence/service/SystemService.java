package com.sms.persistence.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.AbstractTreeComponent;
import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.LineItem;
import com.sms.persistence.model.SystemContract;
import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.SystemServiceData;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.model.SystemTreeItemType;
import com.sms.persistence.model.SystemTreeItemType.TreeItemTypes;
import com.sms.persistence.repo.InventoryRepo;
import com.sms.persistence.repo.SystemPartMaterialNameRepo;
import com.sms.persistence.repo.SystemPartMaterialSpecRepo;
import com.sms.persistence.repo.SystemPartMaterialTypeRepo;
import com.sms.persistence.repo.SystemPartProcessRepo;
import com.sms.persistence.repo.SystemTreeItemTypeRepo;
import com.sms.util.SMSUtil;

@Service
@Transactional
public class SystemService extends AbstractCloningService {
	@Autowired
	private InventoryRepo inventoryRepo;
	@Autowired
	private SystemTreeItemTypeRepo treeItemTypeRepo;
	@Autowired
	private DrawingService drawingService;
	@Autowired
	private PictureService pictureService;
	@Autowired
	private SystemPartMaterialNameRepo materialNameRepo;
	@Autowired
	private SystemPartMaterialSpecRepo materialSpecRepo;
	@Autowired
	private SystemPartMaterialTypeRepo materialTypeRepo;
	@Autowired
	private SystemPartProcessRepo processRepo;

	@Transactional(readOnly = true)
	public Collection<SystemPart> findAllParts() {
		return partRepo.findAll();
	}

	@Transactional(readOnly = true)
	public SystemTree findSystemTreeByGUID(String guid) {
		return systemTreeRepo.findOne(guid);
	}

	@Transactional(readOnly = true)
	public SystemPart getPartByGuid(String guid) {
		return partRepo.findOne(guid);
	}

	@Transactional(readOnly = true)
	public SystemPart getPartByTreeGuid(String guid) {
		SystemPart part = partRepo.findByTreeGuid(systemTreeRepo.findOne(guid));
		calculateCommonCurrentTargetPrice(part);
		return part;
	}

	@Transactional(readOnly = true)
	public Collection<SystemTree> findAllSystemTree() {
		List<SystemTree> allTree = systemTreeRepo.findAll();
		List<SystemTree> retList = Lists.newArrayList();
		for (SystemTree aTree : allTree) {
			if (aTree.getParent() != null)
				continue;
			retList.add(aTree);
		}
		return retList;
	}

	@Transactional(readOnly = true)
	public SystemTreeItemType findTreeItemTypeByName(String itemTypeName) {
		return treeItemTypeRepo.getItemTypeByName(itemTypeName);
	}

	@Transactional(readOnly = true)
	public Collection<SystemTreeItemType> findAllTreeItemTypes() {
		return treeItemTypeRepo.findAll();
	}

	@Transactional(readOnly = true)
	public SystemTree getSystemTreeForPart(SystemPart aPart) {
		return partRepo.findOne(aPart.getGuid()).getSystemTree();
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemPart updatePart(SystemPart systemPart) {
		SMSUtil.validatePart(systemPart);
		SystemTree systemTree = getSystemTreeForPart(systemPart);
		if (systemTree != null) {
			systemTree.setNomenclature(systemPart.getNomenclature());
			systemPart.setSystemTree(systemTree);
		}
		if(systemPart.getNextHigherAssembly()!=null){
			systemPart.setNextHigherAssembly(systemTreeRepo.findOne(systemPart.getNextHigherAssembly().getGuid()));
		}
		systemPart.setCreationDate(getPartByGuid(systemPart.getGuid()).getCreationDate());
		systemPart.setField1("");
		systemPart.setField2("");
		systemPart.setField3("");
		Inventory inventoryFromDB = partRepo.findOne(systemPart.getGuid()).getInventory();
		if (inventoryFromDB != null && systemPart.getInventory() != null) {
			inventoryFromDB.setQuantityOnHand(systemPart.getInventory().getQuantityOnHand());
			inventoryFromDB.setLowQuantityThreshold(systemPart.getInventory().getLowQuantityThreshold());
			inventoryFromDB.setStandardOrderQuantity(systemPart.getInventory().getStandardOrderQuantity());
			inventoryFromDB.setIdCode(systemPart.getInventory().getIdCode());
			inventoryFromDB.setRemarks(systemPart.getInventory().getRemarks());
			inventoryFromDB.setLocation(systemPart.getInventory().getLocation());
			inventoryFromDB.setAdjustmentDate(systemPart.getInventory().getAdjustmentDate());
			inventoryFromDB.setAdjustmentQuantity(systemPart.getInventory().getAdjustmentQuantity());
			systemPart.setInventory(inventoryFromDB);
		} else {
			if (systemPart.getInventory() != null)
				systemPart.getInventory().setCreationDate(systemPart.getCreationDate());
		}
		if(systemPart.getWbsTree()==null || Strings.isNullOrEmpty(systemPart.getWbsTree().getGuid()))
			systemPart.setWbsTree(null);
				
		SystemPart systemPartFromDB = partRepo.findOne(systemPart.getGuid());
		loadAdditionalPartRelations(systemPart);
		BeanUtils.copyProperties(systemPart, systemPartFromDB, new String[] { "systemPartVendors" });
		if(systemPartFromDB.getNextHigherAssembly()!=null && Strings.isNullOrEmpty(systemPartFromDB.getNextHigherAssembly().getGuid()))
			systemPartFromDB.setNextHigherAssembly(null);
		saveMaterialRelatedData(systemPart);
		partRepo.save(systemPartFromDB);
		if (systemPart.getInventory() != null) {
			systemPartFromDB.getInventory().setSystemPart(systemPartFromDB);
			inventoryRepo.save(systemPartFromDB.getInventory());
		}
		return systemPartFromDB;
	}

	private void loadAdditionalPartRelations(SystemPart systemPart) {
		if (systemPart.getMaterialName() != null) {
			if (!Strings.isNullOrEmpty(systemPart.getMaterialName().getGuid()))
				systemPart.setMaterialName(materialNameRepo.findOne(systemPart.getMaterialName().getGuid()));
			else
				systemPart.setMaterialName(null);
		}
		if (systemPart.getMaterialSpec() != null) {
			if (!Strings.isNullOrEmpty(systemPart.getMaterialSpec().getGuid()))
				systemPart.setMaterialSpec(materialSpecRepo.findOne(systemPart.getMaterialSpec().getGuid()));
			else
				systemPart.setMaterialSpec(null);
		}
		if (systemPart.getMaterialType() != null) {
			if (!Strings.isNullOrEmpty(systemPart.getMaterialType().getGuid()))
				systemPart.setMaterialType(materialTypeRepo.findOne(systemPart.getMaterialType().getGuid()));
			else
				systemPart.setMaterialType(null);
		}
		if (systemPart.getProcess1() != null) {
			if (!Strings.isNullOrEmpty(systemPart.getProcess1().getGuid()))
				systemPart.setProcess1(processRepo.findOne(systemPart.getProcess1().getGuid()));
			else
				systemPart.setProcess1(null);
		}
		if (systemPart.getProcess2() != null) {
			if (!Strings.isNullOrEmpty(systemPart.getProcess2().getGuid()))
				systemPart.setProcess2(processRepo.findOne(systemPart.getProcess2().getGuid()));
			else
				systemPart.setProcess2(null);
		}
		if (systemPart.getProcess3() != null) {
			if (!Strings.isNullOrEmpty(systemPart.getProcess3().getGuid()))
				systemPart.setProcess3(processRepo.findOne(systemPart.getProcess3().getGuid()));
			else
				systemPart.setProcess3(null);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemPart renamePart(String systemTreeGuid, String newName) {
		SystemPart systemPart = getPartByTreeGuid(systemTreeGuid);
		systemPart.setAssemblyNumber(newName);
		systemPart.getSystemTree().setNomenclature(newName);
		return partRepo.save(systemPart);
	}

	@Transactional(readOnly = true)
	public Collection<SystemPart> getAllParts() {
		return partRepo.findAll();
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public String deleteTreeNodeByGuid(String guid) {
		SystemTree treeNode = systemTreeRepo.findOne(guid);
		return deleteTreeNode(treeNode);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public String deleteTreeNode(SystemTree treeNode) {
		SystemPart systemPart = null;
		SystemContract systemContract = null;
		SystemServiceData systemServiceData = null;
		LineItem lineItem = null;
		SystemDrawing drawing = null;
		SystemPicture picture = null;
		try {
			systemPart = getPartByTreeGuid(treeNode.getGuid());
		} catch (Throwable e) {
		}
		try {
			systemContract = ((ContractService) serviceMap.get("Contract")).findBySystemTreeGuid(treeNode.getGuid());
		} catch (Throwable e) {
		}
		try {
			systemServiceData = ((SystemServiceDataService) serviceMap.get("Service"))
					.findBySystemTreeGuid(treeNode.getGuid());
		} catch (Throwable e) {
		}
		try {
			lineItem = ((LineItemService) serviceMap.get("Line Item")).findBySystemTreeGuid(treeNode.getGuid());
		} catch (Throwable e) {
		}
		try {
			drawing = drawingService.findBySystemTreeGuid(treeNode.getGuid());
		} catch (Throwable e) {
		}
		try {
			picture = pictureService.findBySystemTreeGuid(treeNode.getGuid());
		} catch (Throwable e) {
		}
		if (systemPart != null) {
			systemPart.setSystemTree(null);
			partRepo.save(systemPart);
		}
		if (systemContract != null) {
			systemContract.setSystemTree(null);
			contractRepo.save(systemContract);
		}
		if (systemServiceData != null) {
			systemServiceData.setSystemTree(null);
			serviceRepo.save(systemServiceData);
		}
		if (lineItem != null) {
			lineItem.setSystemTree(null);
			lineItemRepo.save(lineItem);
		}
		if (drawing != null) {
			drawing.setSystemTree(null);
			drawingRepo.save(drawing);
		}
		if (picture != null) {
			picture.setSystemTree(null);
			pictureRepo.save(picture);
		}
		for (SystemTree child : treeNode.getChildren()) {
			deleteTreeNode(child);
		}
		try {
			systemTreeRepo.delete(treeNode);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return "SUCCESS";
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemTree moveSystemTreeNode(String currentNodeGuid, String newParentGuid) {
		SystemTree treeNode = systemTreeRepo.findOne(currentNodeGuid);
		if (newParentGuid == null)
			treeNode.setParent(null);
		else
			treeNode.setParent(systemTreeRepo.findOne(newParentGuid));
		return systemTreeRepo.save(treeNode);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public AbstractTree copySystemTreeNode(String currentNodeGuid, String newParentGuid) throws Throwable {
		SystemTree sourceTreeNode = systemTreeRepo.findOne(currentNodeGuid);
		SystemTree parentNode = systemTreeRepo.findOne(newParentGuid);
		return copyTreeNodeRecursively(sourceTreeNode, parentNode);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemTree createSystemServiceDataUnder(com.sms.persistence.model.SystemServiceData systemServiceData,
			SystemTree parentNode) {
		SystemTree aTreeNode = new SystemTree();
		aTreeNode.setParent(parentNode);
		aTreeNode.setCreationDate(new Date());
		aTreeNode.setDescription(systemServiceData.getIdCode());
		aTreeNode.setNomenclature(systemServiceData.getNomenclature());
		aTreeNode.setTreeItemType(treeItemTypeRepo.getItemTypeByName(TreeItemTypes.SERVICE.toString()));
		systemServiceData.setSystemTree(aTreeNode);
		((SystemServiceDataService) serviceMap.get("Service")).createSystemService(systemServiceData);
		return aTreeNode;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemTree createLineItemUnder(LineItem lineItem, SystemTree parentNode) {
		SystemTree aTreeNode = new SystemTree();
		aTreeNode.setParent(parentNode);
		aTreeNode.setCreationDate(new Date());
		aTreeNode.setDescription(lineItem.getIdCode());
		aTreeNode.setNomenclature(lineItem.getNomenclature());
		aTreeNode.setTreeItemType(treeItemTypeRepo.getItemTypeByName(TreeItemTypes.LINE_ITEM.toString()));
		lineItem.setSystemTree(aTreeNode);
		((LineItemService) serviceMap.get("Line Item")).createLineItem(lineItem);
		return aTreeNode;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemTree createContractUnder(SystemContract contract, SystemTree parentNode) {
		SystemTree aTreeNode = new SystemTree();
		aTreeNode.setParent(parentNode);
		aTreeNode.setCreationDate(new Date());
		aTreeNode.setDescription(contract.getContractNumber());
		aTreeNode.setNomenclature(contract.getNomenclature());
		aTreeNode.setTreeItemType(treeItemTypeRepo.getItemTypeByName(TreeItemTypes.CONTRACT.toString()));
		contract.setShippingAddress1("");
		contract.setShippingAddress2("");
		contract.setShippingAddress3("");
		contract.setShippingAddress4("");
		contract.setShippingAddress5("");
		contract.setSystemTree(aTreeNode);
		((ContractService) serviceMap.get("Contract")).createContract(contract);
		return aTreeNode;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemTree createPartUnder(SystemPart systemPart, SystemTree parentNode) {
		if (Strings.isNullOrEmpty(systemPart.getGuid()))
			systemPart.setGuid(null);
		Date now = new Date();
		SystemTree aTreeNode = new SystemTree();
		aTreeNode.setParent(parentNode);
		aTreeNode.setCreationDate(now);
		aTreeNode.setDescription(systemPart.getAssemblyName());
		aTreeNode.setNomenclature(systemPart.getNomenclature());
		aTreeNode.setTreeItemType(treeItemTypeRepo.getItemTypeByName(TreeItemTypes.PART.toString()));
		systemPart.setField1("");
		systemPart.setField2("");
		systemPart.setField3("");
		systemPart.setCreationDate(now);
		systemPart.setSystemTree(aTreeNode);
		if(Strings.isNullOrEmpty(systemPart.getNextHigherAssembly().getGuid()))
			systemPart.setNextHigherAssembly(null);
		if(systemPart.getWbsTree()==null || Strings.isNullOrEmpty(systemPart.getWbsTree().getGuid()))
			systemPart.setWbsTree(null);
		loadAdditionalPartRelations(systemPart);
		partRepo.save(systemPart);
		saveMaterialRelatedData(systemPart);
		if (systemPart.getInventory() != null) {
			systemPart.getInventory().setCreationDate(systemPart.getCreationDate());
			systemPart.getInventory().setSystemPart(systemPart);
			inventoryRepo.save(systemPart.getInventory());
		}

		return aTreeNode;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	private void saveMaterialRelatedData(SystemPart systemPart) {
		/*
		 * private SystemPartMaterialSpecRepo materialSpecRepo;
		 * 
		 * @Autowired private SystemPartMaterialTypeRepo materialTypeRepo;
		 * 
		 * @Autowired private SystemPartProcessRepo processRepo;
		 */
		if(systemPart.getMaterialName()!=null && Strings.isNullOrEmpty(systemPart.getMaterialName().getGuid()))
			materialNameRepo.save(systemPart.getMaterialName());
		if(systemPart.getMaterialSpec()!=null && Strings.isNullOrEmpty(systemPart.getMaterialSpec().getGuid()))
			materialSpecRepo.save(systemPart.getMaterialSpec());
		if(systemPart.getMaterialType()!=null && Strings.isNullOrEmpty(systemPart.getMaterialType().getGuid()))
			materialTypeRepo.save(systemPart.getMaterialType());
		if(systemPart.getProcess1()!=null && Strings.isNullOrEmpty(systemPart.getProcess1().getGuid()))
			processRepo.save(systemPart.getProcess1());
		if(systemPart.getProcess2()!=null && Strings.isNullOrEmpty(systemPart.getProcess2().getGuid()))
			processRepo.save(systemPart.getProcess2());
		if(systemPart.getProcess3()!=null && Strings.isNullOrEmpty(systemPart.getProcess3().getGuid()))
			processRepo.save(systemPart.getProcess3());
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemTree renameTreeNode(String systemTreeGuid, String newName) {
		SystemTree systemTree = findSystemTreeByGUID(systemTreeGuid);
		systemTree.setNomenclature(newName);
		return systemTreeRepo.save(systemTree);
	}

	@Transactional(readOnly = true)
	public Collection<? extends AbstractTree> findSystemTreeByItemTypeAndNomenclature(String itemTypeGuid,
			String term) {
		return systemTreeRepo.findByItemTypeAndNomenclature(treeItemTypeRepo.findOne(itemTypeGuid), term);
	}

	@Transactional(readOnly = true)
	public Collection<? extends AbstractTree> findSystemTreeForWorkOrder() {
		return systemTreeRepo.findAllSystemTreeOfLineItemAndService(treeItemTypeRepo.getItemTypeByName("Line Item"),
				treeItemTypeRepo.getItemTypeByName("Service"), treeItemTypeRepo.getItemTypeByName("Part"));
	}

	public Double getCommonCurrencyPrice(AbstractTreeComponent domainElement) {
		return ((SystemPart)domainElement).getCommonCurrencyPrice()==null?0.0d:((SystemPart)domainElement).getCommonCurrencyPrice();
	}

	@Transactional(readOnly = true)
	public AbstractTreeComponent getDomainObjectBySystemTree(AbstractTree treeElement) {
		return getPartByTreeGuid(treeElement.getGuid());
	}

	@Transactional(readOnly = true)
	public Collection<SystemTree> findByNomenclatureAndPartNumber(String nomenclature, String partNumber) {
		Collection<SystemPart> partsByPartNumber = null; 
		Collection<SystemTree> treeItems = Lists.newArrayList();
		if(partNumber != null){
			partsByPartNumber = partRepo.findByManufacturerPartNumber(partNumber);
			if(!CollectionUtils.isEmpty(partsByPartNumber)){
				for(SystemPart part : partsByPartNumber)
					if(part.getSystemTree()!=null){
						if(Strings.isNullOrEmpty(nomenclature))
							treeItems.add(part.getSystemTree());
						else if(part.getSystemTree().getNomenclature().equals(nomenclature))
							treeItems.add(part.getSystemTree());
						
					}
			}
		}else{
			if(nomenclature!=null)
				treeItems.addAll(systemTreeRepo.findByNomenclature(nomenclature));
		}
		return treeItems;
	}
}
