package com.sms.persistence.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.AbstractTreeComponent;
import com.sms.persistence.model.SystemContract;
import com.sms.persistence.repo.SystemContractRepo;
import com.sms.persistence.repo.SystemTreeRepo;

@Service
@Transactional
public class ContractService extends AbstractCloningService{
	@Autowired
	private SystemContractRepo contractRepo;
	
	@Autowired
	private SystemTreeRepo systemTreeRepo;
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemContract createContract(SystemContract contract){
		SystemContract systemContractFromDB = findByContractGuid(contract.getGuid());
		if(systemContractFromDB != null){
			contract.setSystemTree(systemContractFromDB.getSystemTree());
			contract.setCreationDate(systemContractFromDB.getCreationDate());
			contract.getSystemTree().setNomenclature(contract.getNomenclature());
			if(Strings.isNullOrEmpty(contract.getShippingAddress()))
				contract.setShippingAddress(systemContractFromDB.getShippingAddress());
			if(Strings.isNullOrEmpty(contract.getShippingAddress1()))
				contract.setShippingAddress1(systemContractFromDB.getShippingAddress1());
			if(Strings.isNullOrEmpty(contract.getShippingAddress2()))
				contract.setShippingAddress2(systemContractFromDB.getShippingAddress2());
			if(Strings.isNullOrEmpty(contract.getShippingAddress3()))
				contract.setShippingAddress3(systemContractFromDB.getShippingAddress3());
			if(Strings.isNullOrEmpty(contract.getShippingAddress4()))
				contract.setShippingAddress4(systemContractFromDB.getShippingAddress4());
			if(Strings.isNullOrEmpty(contract.getShippingAddress5()))
				contract.setShippingAddress5(systemContractFromDB.getShippingAddress5());
		}else
			contract.setCreationDate(new Date());
		return contractRepo.save(contract);
	}
	
	@Transactional(readOnly = true)
	public SystemContract findBySystemTreeGuid(String systemTreeGuid){
		return contractRepo.findBySystemTreeNode(systemTreeRepo.findOne(systemTreeGuid));
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemContract renameContract(String systemTreeGuid, String newName){
		SystemContract systemContract = findBySystemTreeGuid(systemTreeGuid);
		systemContract.setContractNumber(newName);
		systemContract.getSystemTree().setNomenclature(newName);
		return contractRepo.save(systemContract);
	}
	
	@Transactional(readOnly = true)
	public SystemContract findByContractGuid(String contractGuid){
		if(Strings.isNullOrEmpty(contractGuid))
			return null;
		return contractRepo.findOne(contractGuid);
	}

	public Double getCommonCurrencyPrice(AbstractTreeComponent domainElement) {
		return new Double(0);
	}

	@Transactional(readOnly = true)
	public AbstractTreeComponent getDomainObjectBySystemTree(AbstractTree treeElement) {
		return findBySystemTreeGuid(treeElement.getGuid());
	}

}
