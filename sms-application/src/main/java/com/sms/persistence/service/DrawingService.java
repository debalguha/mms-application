package com.sms.persistence.service;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.repo.SystemDrawingRepo;
import com.sms.persistence.repo.SystemPartRepo;
import com.sms.persistence.repo.SystemTreeItemTypeRepo;
import com.sms.persistence.repo.SystemTreeRepo;
import com.sms.persistence.service.predicates.DrawingPredicateSpec;

@Service
@Transactional
public class DrawingService {
	@Autowired
	private SystemTreeRepo systemTreeRepo;
	
	@Autowired
	private SystemTreeItemTypeRepo treeItemTypeRepo;
	
	@Autowired
	private SystemPartRepo partRepo;
	
	@Autowired
	private SystemDrawingRepo drawingRepo;
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemDrawing createDrawing(SystemDrawing drawing) {
		return drawingRepo.save(drawing);
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemDrawing> getAllDrawingsForPart(String partGuid) {
		return getAllDrawingsForPartTreeNode(partRepo.findOne(partGuid).getSystemTree().getGuid());
		/*SystemTree partTreeNode = partRepo.findOne(partGuid).getSystemTree();
		if(CollectionUtils.isEmpty(partTreeNode.getChildren()))
			return Collections.emptyList();
		List<SystemTree> drawingTreeItems = select(partTreeNode.getChildren(), having(on(SystemTree.class).getTreeItemType().getItemType(), equalTo(treeItemTypeRepo.getItemTypeByName("Drawing").getItemType())));
		Collection<SystemDrawing> systemDrawings = Lists.newArrayListWithCapacity(drawingTreeItems.size());
		for(SystemTree drawingTreeNode : drawingTreeItems)
			systemDrawings.add(drawingRepo.findByTreeGuid(drawingTreeNode));
		return systemDrawings;*/
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemDrawing> getAllDrawingsForPartTreeNode(String partGuid) {
		SystemTree partTreeNode = systemTreeRepo.findOne(partGuid);
		if(CollectionUtils.isEmpty(partTreeNode.getChildren()))
			return Collections.emptyList();
		List<SystemTree> drawingTreeItems = select(partTreeNode.getChildren(), having(on(SystemTree.class).getTreeItemType().getItemType(), equalTo(treeItemTypeRepo.getItemTypeByName("Drawing").getItemType())));
		Collection<SystemDrawing> systemDrawings = Lists.newArrayListWithCapacity(drawingTreeItems.size());
		for(SystemTree drawingTreeNode : drawingTreeItems)
			systemDrawings.add(drawingRepo.findByTreeGuid(drawingTreeNode));
		return systemDrawings;
	}
	
	@Transactional(readOnly = true)
	public SystemDrawing getDrawingByGuid(String drawingGuid) {
		return drawingRepo.findOne(drawingGuid);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void removeDrawingByGuid(String drawingGuid) {
		SystemDrawing drawing = drawingRepo.findOne(drawingGuid);
		SystemTree systemTree = drawing.getSystemTree();
		drawing.setSystemTree(null);
		systemTreeRepo.delete(systemTree);
		drawingRepo.save(drawing);
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemDrawing> getAllDrawings(int pageIndex, int numberOfElements) {
		return drawingRepo.findAll(new PageRequest(pageIndex, numberOfElements)).getContent();
	}
	
	@Transactional(readOnly = true)
	public Collection<? extends SystemDrawing> getAllDrawings(String searchTerm, int start, int length, int orderColumn, String direction) {
		if(!Strings.isNullOrEmpty(searchTerm))
			return drawingRepo.findAll(DrawingPredicateSpec.fileNameIsLike(searchTerm), new PageRequest(start, length, Direction.fromStringOrNull(direction), orderColumn==0?"StoredFileName":"creationDate")).getContent();
		else
			return drawingRepo.findAll(new PageRequest(start, length, Direction.fromStringOrNull(direction), orderColumn==0?"StoredFileName":"creationDate")).getContent();
	}	
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public String addDrawingsToPartTreeNode(String[] drawingGuids, String partGuid) {
		SystemTree partNode = systemTreeRepo.findOne(partGuid);
		Date now = new Date();
		for(String drawingGuid : drawingGuids){
			SystemDrawing drawing = drawingRepo.findOne(drawingGuid);
			if(drawing.getSystemTree() == null){
				SystemTree drawingTree = new SystemTree();
				drawingTree.setCreationDate(now);
				drawingTree.setTreeItemType(treeItemTypeRepo.getItemTypeByName("Drawing"));
				drawingTree.setNomenclature(drawing.getStoredFileName());
				drawingTree.setDescription(drawing.getStoredFileName());
				drawing.setSystemTree(drawingTree);
			}
			drawing.getSystemTree().setParent(partNode);
			drawingRepo.save(drawing);
		}
		return "SUCCESS";
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemDrawing> getAllDrawingsNotForPartTreeNode(String partGuid) {
		Set<SystemDrawing> allDrawingsForPart = Sets.newHashSet(getAllDrawingsForPartTreeNode(partGuid));
		Set<SystemDrawing> drawingRepoCollection = Sets.newHashSet(drawingRepo.findAll());
		return Sets.difference(drawingRepoCollection, allDrawingsForPart);
	}
	@Transactional(readOnly = true)
	public Collection<SystemDrawing> getAllDrawingsNotForPart(String partGuid) {
		Set<SystemDrawing> allDrawingsForPart = Sets.newHashSet(getAllDrawingsForPart(partGuid));
		Set<SystemDrawing> drawingRepoCollection = Sets.newHashSet(drawingRepo.findAll());
		return Sets.difference(drawingRepoCollection, allDrawingsForPart);
	}
	@Transactional(readOnly = true)
	public SystemDrawing findBySystemTreeGuid(String drawingTreeGuid) {
		return drawingRepo.findByTreeGuid(systemTreeRepo.findOne(drawingTreeGuid));
	}

	@Transactional(readOnly = true)
	public int getTotalNumberOfDrawings(String searchTerm) {
		return Strings.isNullOrEmpty(searchTerm)?drawingRepo.getTotalDrawingCount():drawingRepo.getTotalDrawingCount(searchTerm);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void deleteAllDrawings(String[] selectedGuids) {
		for(String guid : selectedGuids)
			drawingRepo.delete(guid);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean renameDrawings(String drawingGuid, String newName, String uploadDirectory) {
		SystemDrawing drawing = drawingRepo.findOne(drawingGuid);
		String storedFileName = drawing.getStoredFileName();
		File storedFile = new File(uploadDirectory, storedFileName);
		String extension = FilenameUtils.getExtension(drawing.getStoredFileName());
		drawing.setUploadedFileName(newName.endsWith(extension)?newName:newName.concat(".").concat(extension));
		drawing.setStoredFileName(newName.endsWith(extension)?newName:newName.concat(".").concat(extension));
		drawingRepo.save(drawing);
		if(!storedFile.renameTo(new File(uploadDirectory, drawing.getStoredFileName())))
			throw new RuntimeException("Rename failed!");
		return true;
	}		
}
