package com.sms.persistence.service;

import java.util.Collection;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sms.persistence.model.AuditLog;
import com.sms.persistence.repo.AuditLogRepository;

@Service
@Transactional
public class AuditLogService {
	@Autowired
	private AuditLogRepository auditLogRepo;
	
	@Transactional(readOnly = true)
	public Collection<AuditLog> findAllAuditLogs(){
		return auditLogRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Collection<AuditLog> findAuditLogWithinDateRange(Date startDate, Date endDate){
		DateTime startDateTime = new DateTime(startDate.getTime());
		DateTime endDateTime = new DateTime(endDate.getTime());
		return auditLogRepo.findAuditLogWithDateRange(startDateTime.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate(), 
				endDateTime.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate());
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void createAuditLogs(Collection<AuditLog> auditLogs){
		auditLogRepo.save(auditLogs);
	}
}
