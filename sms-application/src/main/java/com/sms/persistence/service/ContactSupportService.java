package com.sms.persistence.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sms.persistence.model.EmailLog;
import com.sms.persistence.model.SupportEmail;
import com.sms.persistence.repo.EmailLogRepository;
import com.sms.persistence.repo.SupportEmailRepository;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ContactSupportService {
	@Autowired
	private EmailLogRepository emailLogRepo;
	@Autowired
	private SupportEmailRepository supportEmailRepo;
	
	@Transactional(readOnly = true)
	public Collection<SupportEmail> findAllSupportEmails(){
		return supportEmailRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Collection<EmailLog> findAllPendingEmailsToSend(){
		return emailLogRepo.findAllPendingEmailToSend();
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void createEmailLog(Collection<EmailLog> emailLogs){
		emailLogRepo.save(emailLogs);
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void updateEmailLogToComplete(String guid) {
		EmailLog emailLog = emailLogRepo.findOne(guid);
		emailLog.setStatus("SENT");
		emailLogRepo.save(emailLog);
	}
}
