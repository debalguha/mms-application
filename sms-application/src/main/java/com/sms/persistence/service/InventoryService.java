package com.sms.persistence.service;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;
import static org.hamcrest.Matchers.equalTo;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.lambdaj.Lambda;
import ch.lambdaj.function.convert.Converter;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.InventoryVendor;
import com.sms.persistence.model.POItem;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.repo.InventoryRepo;
import com.sms.persistence.repo.InventoryVendorRepo;
import com.sms.persistence.repo.SystemPartRepo;
import com.sms.persistence.service.predicates.InventoryPredicateSpec;

@Service
@Transactional
public class InventoryService {
	@Autowired
	private InventoryVendorRepo inventoryVendorRepo;
	@Autowired
	private InventoryRepo inventoryRepo;
	@Autowired
	private SystemPartRepo partRepo;
	@Autowired
	private VendorService vendorService;
	@Autowired
	private POEntityService poEntityService;
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Inventory createInventoryRecord(@NotNull Inventory inventory){
		Inventory inventoryFromDB = inventory.getGuid() == null ? inventory : inventoryRepo.findOne(inventory.getGuid());
		inventoryFromDB.setCreationDate(inventoryFromDB == null ? new Date() : inventoryFromDB.getCreationDate());
		inventoryFromDB.setHighUnitPricePOGuid(inventory.getHighUnitPricePOGuid());
		inventoryFromDB.setIdCode(inventory.getIdCode());
		inventoryFromDB.setLowQuantityThreshold(inventory.getLowQuantityThreshold());
		inventoryFromDB.setQuantityOnHand(inventory.getQuantityOnHand());
		inventoryFromDB.setStandardOrderQuantity(inventory.getStandardOrderQuantity());
		return inventoryRepo.save(inventoryFromDB);
	}

	@Transactional(readOnly = true)
	public Collection<Inventory> findInvenorySuppliedByVendor(String vendorGuid) {
		/*Collection<InventoryVendor> inventoryVendorsByVendor = inventoryVendorRepo.findInventoryVendorByVendor(vendorService.findVendorByGuid(vendorGuid));
		Collection<Inventory> retList = Lists.newArrayList();
		for(InventoryVendor invVendor : inventoryVendorsByVendor)
			retList.add(invVendor.getInventory());*/
		Collection<InventoryVendor> inventoryVendorsByVendor = inventoryVendorRepo.findInventoryVendorByVendor(vendorService.findVendorByGuid(vendorGuid));
		Set<Inventory> suppliedInventories = Sets.newHashSet();
		for(InventoryVendor invVendor : inventoryVendorsByVendor)
			suppliedInventories.add(invVendor.getInventory());		
		return suppliedInventories;
	}
	
	@Transactional(readOnly = true)
	public Collection<Inventory> findInvenoryNotSuppliedByVendor(String vendorGuid) {
		Collection<InventoryVendor> inventoryVendorsByVendor = inventoryVendorRepo.findInventoryVendorByVendor(vendorService.findVendorByGuid(vendorGuid));
		Set<Inventory> suppliedInventories = Sets.newHashSet();
		for(InventoryVendor invVendor : inventoryVendorsByVendor)
			suppliedInventories.add(invVendor.getInventory());
		return Sets.difference(Sets.newHashSet(inventoryRepo.findAll()), suppliedInventories);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void associateInventoriesWithVendor(String vendorGuid, String[] inventoryGuids) {
		for(String inventoryGuid : inventoryGuids){
			Inventory invntory = inventoryRepo.findOne(inventoryGuid);
			SystemVendor vendor = vendorService.findVendorByGuid(vendorGuid);
			InventoryVendor invVendor = new InventoryVendor();
			invVendor.setInventory(invntory);
			invVendor.setVendor(vendor);
			invVendor.setCreationDate(new Date());
			inventoryVendorRepo.save(invVendor);
		}
	}
	@Transactional(readOnly = true)
	public Collection<? extends Inventory> findAllInventoryRecords() {
		return inventoryRepo.findAll();
	}
	@Transactional(readOnly = true)
	public Collection<? extends Inventory> findAllInventoryRecords(String searchTerm, int start, int length, int orderColumn, String direction) {
		if(!Strings.isNullOrEmpty(searchTerm))
			return inventoryRepo.findAll(InventoryPredicateSpec.buildInventorySearchSpecification(searchTerm), new PageRequest(start, length, Direction.fromStringOrNull(direction), orderColumn==0?"idCode":orderColumn==2?"systemPart.assemblyName":orderColumn==5?"quantityOnHand":orderColumn==6?"lowQuantityThreshold":"creationDate")).getContent();
		else
			return inventoryRepo.findAll(new PageRequest(start, length, Direction.fromStringOrNull(direction), "creationDate")).getContent();
			//return inventoryRepo.findAll(new PageRequest(start, length, Direction.fromStringOrNull(direction), orderColumn==0?"id_code":orderColumn==2?"assembly_name":orderColumn==5?"quantity_on_hand":orderColumn==6?"low_quantity_threshold":"date_created")).getContent();
		
	}
	@Transactional(readOnly = true)
	public int getTotalNumberOfInventoryRecords(String searchTerm){
		return Strings.isNullOrEmpty(searchTerm)?inventoryRepo.findTotalNumberOfRecords():inventoryRepo.findTotalNumberOfRecords(searchTerm);
	}
	@Transactional(readOnly = true)
	public Long getHighUnitPricePO(Inventory inv) {
		return select(poEntityService.findPOEntityByGuid(inv.getHighUnitPricePOGuid()).getPoItems(), having(on(POItem.class).getSystemPart(), equalTo(inv.getSystemPart()))).iterator().next().getUnitPrice();
	}
	@Transactional(readOnly = true)
	public Long getLowUnitPricePO(Inventory inv) {
		return select(poEntityService.findPOEntityByGuid(inv.getLowUnitPricePOGuid()).getPoItems(), having(on(POItem.class).getSystemPart(), equalTo(inv.getSystemPart()))).iterator().next().getUnitPrice();
	}
	@Transactional(readOnly = true)
	public boolean hasVendors(String invGuid) {
		return !CollectionUtils.isEmpty(inventoryRepo.findOne(invGuid).getInventoryVendors());
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Inventory activateInventory(String guid) {
		Inventory inventory = inventoryRepo.findOne(guid);
		inventory.setActive(1);
		return inventoryRepo.save(inventory);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Inventory deActivateInventory(String guid) {
		Inventory inventory = inventoryRepo.findOne(guid);
		inventory.setActive(0);
		return inventoryRepo.save(inventory);
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void activateInventories(String[] invSelect) {
		for(String invGuid : invSelect)
			activateInventory(invGuid);
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void deActivateInventories(String[] invSelect) {
		for(String invGuid : invSelect)
			deActivateInventory(invGuid);
	}

	@Transactional(readOnly = true)
	public Collection<Inventory> findInventories(String[] invSelect) {
		Collection<Inventory> invs = Lists.newArrayList();
		for(String invGuid : invSelect)
			invs.add(inventoryRepo.findOne(invGuid));
		return invs;
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemVendor> findvendorSuppliers(String[] invSelect) {
		Collection<SystemVendor> vendors = Sets.newHashSet();
		for(String invGuid : invSelect){
			for(InventoryVendor invVendor : inventoryRepo.findOne(invGuid).getInventoryVendors())
				vendors.add(invVendor.getVendor());
		}
		return vendors;
	}

	@Transactional(readOnly = true)
	public Inventory findInventoryByGuid(String guid) {
		return inventoryRepo.findOne(guid);
	}

	@Transactional(readOnly = true)
	public Inventory findInventoryOfPart(SystemPart systemPart) {
		return inventoryRepo.findBysystemPart(systemPart);
	}

	@Transactional(readOnly = true)
	public Collection<? extends Inventory> findAllInventoryRecordsHavingSupplier() {
		return Sets.newHashSet(Lambda.convert(inventoryVendorRepo.findAll(), new Converter<InventoryVendor, Inventory>() {
			public Inventory convert(InventoryVendor from) {
				return from.getInventory();
			}
		}));
	}

	@Transactional(readOnly = true)
	public Inventory findInventoryOfPart(String partGuid) {
		return findInventoryOfPart(partRepo.findOne(partGuid));
	}
	
	@Transactional(readOnly = true)
	public boolean isPOAvailable(String inventoryGuid){
		return !CollectionUtils.isEmpty(poEntityService.findPOItemsBySystemart(findInventoryByGuid(inventoryGuid).getSystemPart()));
		
	}

}
