package com.sms.persistence.service;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.AbstractTreeComponent;
import com.sms.persistence.model.LineItem;
import com.sms.persistence.model.SystemContract;
import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemPartVendor;
import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.model.SystemTreeItemType.TreeItemTypes;
import com.sms.persistence.model.WbsTree;
import com.sms.persistence.repo.LineItemRepo;
import com.sms.persistence.repo.SystemContractRepo;
import com.sms.persistence.repo.SystemDrawingRepo;
import com.sms.persistence.repo.SystemPartRepo;
import com.sms.persistence.repo.SystemPartVendorRepo;
import com.sms.persistence.repo.SystemPictureRepo;
import com.sms.persistence.repo.SystemServiceDataRepo;
import com.sms.persistence.repo.SystemTreeItemTypeRepo;
import com.sms.persistence.repo.SystemTreeRepo;
import com.sms.util.DTOUtils;
import com.sms.util.SMSUtil;
import com.sms.web.model.TREE_NODE_TYPE;

@Transactional
public abstract class AbstractCloningService implements ComponentService {
	TreeItemTypes[] inScopeItemsForCalculation = new TreeItemTypes[] { TreeItemTypes.CONTRACT, TreeItemTypes.PART,
			TreeItemTypes.SERVICE, TreeItemTypes.LINE_ITEM };
	@Resource
	protected Map<String, ComponentService> serviceMap;

	@Autowired
	protected SystemTreeRepo systemTreeRepo;

	@Autowired
	protected SystemTreeItemTypeRepo treeItemTypeRepo;

	@Autowired
	protected SystemPartRepo partRepo;

	@Autowired
	protected SystemDrawingRepo drawingRepo;

	@Autowired
	protected SystemPictureRepo pictureRepo;

	@Autowired
	protected LineItemRepo lineItemRepo;

	@Autowired
	protected SystemContractRepo contractRepo;

	@Autowired
	protected SystemServiceDataRepo serviceRepo;

	@Autowired
	protected SystemPartVendorRepo systemPartVendorRepo;

	@Transactional(propagation = Propagation.MANDATORY, rollbackFor = Throwable.class)
	protected AbstractTree copyTreeNodeRecursively(AbstractTree sourceTreeNode, AbstractTree parentNode)
			throws Throwable {
		AbstractTree treeNode = SMSUtil.cloneTreeNodeFrom(sourceTreeNode);
		treeNode.setParent(parentNode);
		TREE_NODE_TYPE nodeType = TREE_NODE_TYPE.UNKNOWN;
		try {
			nodeType = DTOUtils.determineNodeTypeFromTreeItemType(treeNode.getTreeItemType());
		} catch (IllegalStateException e) {
			nodeType = TREE_NODE_TYPE.WBS;
		}
		switch (nodeType) {
		case CONTRACT:
			copyContract((SystemTree) sourceTreeNode, (SystemTree) treeNode);
			break;
		case LINEITEM:
			copyLineItem((SystemTree) sourceTreeNode, (SystemTree) treeNode);
			break;
		case PART:
			copyPart((SystemTree) sourceTreeNode, (SystemTree) treeNode);
			break;
		case SERVICE:
			copySystemServiceData((SystemTree) sourceTreeNode, (SystemTree) treeNode);
			break;
		case PICTURE:
			copyPicture((SystemTree) sourceTreeNode, (SystemTree) treeNode);
			break;
		case DRAWING:
			copyDrawing((SystemTree) sourceTreeNode, (SystemTree) treeNode);
			break;
		case WBS:
			((WbsTree) treeNode).setCreationDate(new Date());
			break;
		default:
			throw new IllegalArgumentException("Node type " + nodeType + " is not supported for copy paste!!");

		}
		for (AbstractTree child : sourceTreeNode.getChildren())
			copyTreeNodeRecursively(child, treeNode);
		return treeNode;
	}

	private void copyPart(SystemTree sourceTreeNode, SystemTree treeNode) throws Throwable {
		SystemPart sourcePart = (SystemPart) getDomainObjectBySystemTree(sourceTreeNode);
		Date now = new Date();
		if (sourcePart == null)
			return;
		SystemPart clonedPart = new SystemPart();
		SMSUtil.cloneObjectFrom(sourcePart, clonedPart,
				new String[] { "guid", "systemTree", "creationDate", "wbsTree", "systemPartVendors"
					, "nextHigherAssembly", "materialName", "materialType", "materialSpec", "process1", "process2", "process3" });
		clonedPart.setSystemTree(treeNode);
		clonedPart.setCreationDate(new Date());
		clonedPart.setNextHigherAssembly(sourcePart.getNextHigherAssembly());
		clonedPart.setMaterialName(sourcePart.getMaterialName());
		clonedPart.setMaterialType(sourcePart.getMaterialType());
		clonedPart.setMaterialSpec(sourcePart.getMaterialSpec());
		clonedPart.setProcess1(sourcePart.getProcess1());
		clonedPart.setProcess2(sourcePart.getProcess2());
		clonedPart.setProcess3(sourcePart.getProcess3());
		clonedPart.setWbsTree(sourcePart.getWbsTree());
		if (!CollectionUtils.isEmpty(sourcePart.getSystemPartVendors())) {
			for (SystemPartVendor systemPartVendor : sourcePart.getSystemPartVendors()) {
				SystemPartVendor aSystemPartVendor = new SystemPartVendor();
				aSystemPartVendor.setCreationDate(now);
				aSystemPartVendor.setSystemPart(clonedPart);
				aSystemPartVendor.setSystemVendor(systemPartVendor.getSystemVendor());
				systemPartVendorRepo.save(aSystemPartVendor);
			}
		} else
			partRepo.save(clonedPart);
	}

	private void copySystemServiceData(SystemTree sourceTreeNode, SystemTree treeNode) throws Throwable {
		com.sms.persistence.model.SystemServiceData sourceService = ((SystemServiceDataService) serviceMap
				.get("Service")).findBySystemTreeGuid(sourceTreeNode.getGuid());
		com.sms.persistence.model.SystemServiceData clonedService = new com.sms.persistence.model.SystemServiceData();
		if (sourceService == null)
			return;
		SMSUtil.cloneObjectFrom(sourceService, clonedService,
				new String[] { "guid", "systemTree", "creationDate", "wbsTree" });
		clonedService.setSystemTree(treeNode);
		clonedService.setWbsTree(sourceService.getWbsTree());
		((SystemServiceDataService) serviceMap.get("Service")).createSystemService(clonedService);
	}

	private void copyLineItem(SystemTree sourceTreeNode, SystemTree treeNode) throws Throwable {
		LineItem sourceLineItem = ((LineItemService) serviceMap.get("Line Item"))
				.findBySystemTreeGuid(sourceTreeNode.getGuid());
		LineItem clonedLineItem = new LineItem();
		if (sourceLineItem == null)
			return;
		SMSUtil.cloneObjectFrom(sourceLineItem, clonedLineItem,
				new String[] { "guid", "systemTree", "creationDate", "wbsTree" });
		clonedLineItem.setSystemTree(treeNode);
		((LineItemService) serviceMap.get("Line Item")).createLineItem(clonedLineItem);
	}

	private void copyContract(SystemTree sourceTreeNode, SystemTree treeNode) throws Throwable {
		SystemContract sourceContract = ((ContractService) serviceMap.get("Contract"))
				.findBySystemTreeGuid(sourceTreeNode.getGuid());
		SystemContract clonedContract = new SystemContract();
		if (sourceContract == null)
			return;
		SMSUtil.cloneObjectFrom(sourceContract, clonedContract, new String[] { "guid", "systemTree", "creationDate" });
		clonedContract.setSystemTree(treeNode);
		((ContractService) serviceMap.get("Contract")).createContract(clonedContract);
	}

	private void copyPicture(SystemTree sourceTreeNode, SystemTree treeNode) throws Throwable {
		SystemPicture sourcePicture = pictureRepo.findByTreeGuid(sourceTreeNode);
		SystemPicture clonedPicture = new SystemPicture();
		if (sourcePicture == null)
			return;
		SMSUtil.cloneObjectFrom(sourcePicture, clonedPicture, new String[] { "guid", "systemTree", "creationDate" });
		clonedPicture.setSystemTree(treeNode);
		clonedPicture.setCreationDate(new Date());
		pictureRepo.save(clonedPicture);
	}

	private void copyDrawing(SystemTree sourceTreeNode, SystemTree treeNode) throws Throwable {
		SystemDrawing sourceDrawing = drawingRepo.findByTreeGuid(sourceTreeNode);
		SystemDrawing clonedDrawing = new SystemDrawing();
		if (sourceDrawing == null)
			return;
		SMSUtil.cloneObjectFrom(sourceDrawing, clonedDrawing, new String[] { "guid", "systemTree", "creationDate" });
		clonedDrawing.setSystemTree(treeNode);
		clonedDrawing.setCreationDate(new Date());
		drawingRepo.save(clonedDrawing);
	}

	@Transactional(readOnly = true)
	public Double calculateCommonCurrentTargetPrice(AbstractTreeComponent component) {
		if (CollectionUtils.isEmpty(component.getSystemTree().getChildren()))
			return serviceMap.get(component.getSystemTree().getTreeItemType().getItemType())
					.getCommonCurrencyPrice(component);
		ComponentService parentService = serviceMap.get(component.getSystemTree().getTreeItemType().getItemType());
		double commonCurrentTargetPrice = parentService.getCommonCurrencyPrice(component);
		for (AbstractTree treeElement : component.getSystemTree().getChildren()) {
			if (!ArrayUtils.contains(inScopeItemsForCalculation, TreeItemTypes
					.valueOf(treeElement.getTreeItemType().getItemType().toUpperCase().replaceAll(" ", "_"))))
				continue;
			ComponentService childService = serviceMap.get(treeElement.getTreeItemType().getItemType());
			try {
				commonCurrentTargetPrice += childService
						.calculateCommonCurrentTargetPrice(childService.getDomainObjectBySystemTree(treeElement));
			} catch (Exception e) {
			}
		}
		return commonCurrentTargetPrice;
	}
}
