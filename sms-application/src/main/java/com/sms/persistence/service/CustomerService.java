package com.sms.persistence.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.sms.persistence.model.Customer;
import com.sms.persistence.repo.CustomerRepo;
import com.sms.persistence.repo.VehicleRepository;
import com.sms.persistence.repo.WorkOrderRepository;

@Service
@Transactional
public class CustomerService {
	@Autowired
	private CustomerRepo customerRepo;
	@Autowired
	private VehicleRepository vehicleRepo;
	@Autowired
	private WorkOrderRepository woRepo;
	
	@Transactional(readOnly = true)
	public Collection<Customer> findAll(){
		List<Customer> allCustomers = customerRepo.findAll();
		for(Customer customer : allCustomers)
			customer.setVehicles(Sets.newHashSet(vehicleRepo.findByCustomer(customer)));
		return customerRepo.findAll();
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Customer createACustomer(Customer customer){
		Customer customerFromDB = StringUtils.isBlank(customer.getGuid())?null:customerRepo.findOne(customer.getGuid());
		if(customerFromDB == null){
			customerFromDB = customer;
			customerFromDB.setCreationDate(new Date());
		}else
			BeanUtils.copyProperties(customer, customerFromDB, new String[]{"guid", "creationDate"});
		if(Strings.isNullOrEmpty(customerFromDB.getGuid()))
			customerFromDB.setGuid(null);
		return customerRepo.save(customerFromDB);
	}
	@Transactional(readOnly = true)
	public Customer findCustomerByGuid(String customerGuid) {
		return customerRepo.findOne(customerGuid);
	}
	@Transactional(readOnly = true)
	public Collection<Customer> searchCustomerByName(String name){
		return customerRepo.findByCustomerName(name);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void deleteAllCustomers(String[] selectedGuids) {
		for(String guid : selectedGuids){
			Customer customer = customerRepo.findOne(guid);
			woRepo.delete(woRepo.findAllWorkOrdersOf(customer));
			vehicleRepo.delete(vehicleRepo.findByCustomer(customer));
			customerRepo.delete(customer);
		}
	}
}
