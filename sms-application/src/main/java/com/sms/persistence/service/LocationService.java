package com.sms.persistence.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sms.persistence.model.Location;
import com.sms.persistence.repo.LocationRepo;

@Service
@Transactional
public class LocationService {
	@Autowired
	private LocationRepo locationRepo;
	
	@Transactional(readOnly = true)
	public Location findLocationByName(String locationName){
		return locationRepo.findByName(locationName);
	}
	@Transactional(readOnly = true)
	public Collection<Location> findAllLocations() {
		return locationRepo.findAll();
	}
	@Transactional(readOnly = true)
	public Location getByLocationByGUID(String guid) {
		return locationRepo.findOne(guid);
	}
}
