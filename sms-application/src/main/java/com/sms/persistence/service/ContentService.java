package com.sms.persistence.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.sms.persistence.model.Content;
import com.sms.persistence.model.ContentType;
import com.sms.persistence.repo.ContentRepository;
import com.sms.persistence.repo.ContentTypeRepository;

@Service
@Transactional
public class ContentService {
	@Autowired
	private ContentTypeRepository contentTypeRepo;
	@Autowired
	private ContentRepository contentRepo;
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Content saveContent(Content content){
		Date now = new Date();
		if(Strings.isNullOrEmpty(content.getGuid())){
			content.setGuid(null);
			content.setCreationDate(now);
		}else
			content.setCreationDate(contentRepo.findOne(content.getGuid()).getCreationDate());
		/*if(content.getContentType().getGuid() == null)
			content.setContentType(contentTypeRepo.getContentTypeByTypeName(content.getContentType().getContentTypeName()));*/
		return contentRepo.save(content);
	}
	@Transactional(readOnly = true)
	public Map<String, String> getContentSettingsMap() {
		List<Content> allContent = contentRepo.findAll();
		Map<String, String> retMap = Maps.newHashMap();
		for(Content content : allContent)
			retMap.put(content.getGuid(), content.getContentName());
		return retMap;
	}
	public Collection<Content> findAllContents() {
		return contentRepo.findAll();
	}
	@Transactional(readOnly = true)
	public Content findContentByGuid(String guid) {
		return contentRepo.findOne(guid);
	}
	@Transactional(readOnly = true)
	public Collection<ContentType> findAllContentTypes() {
		return contentTypeRepo.findAll();
	}
}
