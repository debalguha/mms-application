package com.sms.persistence.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sms.persistence.model.SystemPartMaterialName;
import com.sms.persistence.model.SystemPartMaterialSpec;
import com.sms.persistence.model.SystemPartMaterialType;
import com.sms.persistence.model.SystemPartProcess;
import com.sms.persistence.repo.SystemPartMaterialNameRepo;
import com.sms.persistence.repo.SystemPartMaterialSpecRepo;
import com.sms.persistence.repo.SystemPartMaterialTypeRepo;
import com.sms.persistence.repo.SystemPartProcessRepo;

@Service
@Transactional
public class SystemPartHelperService {
	@Autowired
	private SystemPartProcessRepo partProcesRepo;
	@Autowired
	private SystemPartMaterialTypeRepo materialTypeRepo;
	@Autowired
	private SystemPartMaterialNameRepo materialNameRepo;
	@Autowired
	private SystemPartMaterialSpecRepo materialSpecRepo;
	
	@Transactional(readOnly = true)
	public Collection<SystemPartProcess> findAllProcesses(){
		return partProcesRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemPartMaterialType> findAllMaterialTypes(){
		return materialTypeRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemPartMaterialName> findAllMaterialNames(){
		return materialNameRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemPartMaterialSpec> findAllMaterialSpecs(){
		return materialSpecRepo.findAll();
	}
}
