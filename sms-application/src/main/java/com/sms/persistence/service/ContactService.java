package com.sms.persistence.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.sms.persistence.model.Contact;
import com.sms.persistence.repo.ContactRepository;

@Service
@Transactional
public class ContactService {
	@Autowired
	private ContactRepository contactRepo;
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Contact createContact(Contact contact){
		if(Strings.isNullOrEmpty(contact.getGuid()))
			contact.setGuid(null);
		if(contact.getCreationDate()==null)
			contact.setCreationDate(new Date());
		return contactRepo.save(contact);
	}
	
	@Transactional(readOnly = true)
	public Contact findContactByGuid(String guid){
		return contactRepo.findOne(guid);
	}

	@Transactional(readOnly = true)
	public boolean doesEmailExists(String guid, String email) {
		Contact contact = contactRepo.findByEmail(email);
		if(Strings.isNullOrEmpty(guid)){
			if(contact!=null)
				return true;
			return false;
		}
		if(contact!=null && !contact.getGuid().equals(guid))
			return true;
		return false;
	}
}
