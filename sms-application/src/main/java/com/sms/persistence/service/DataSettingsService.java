package com.sms.persistence.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.sms.persistence.model.DataSetting;
import com.sms.persistence.model.DataSettingField;
import com.sms.persistence.model.DataSettingFieldType;
import com.sms.persistence.repo.DataSettingFieldTypeRepo;
import com.sms.persistence.repo.DataSettingFieldsRepo;
import com.sms.persistence.repo.DataSettingRepo;

@Service
@Transactional
public class DataSettingsService {
	@Autowired
	private DataSettingFieldTypeRepo fieldTypeRepo;
	@Autowired
	private DataSettingRepo dataSettingRepo;
	@Autowired
	private DataSettingFieldsRepo dataSettingFieldsRepo;
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void createDataSettingFields(Collection<DataSettingField> dataSettingFields){
		Date now = new Date();
		for(DataSettingField settingField : dataSettingFields){
			settingField.setCreationDate(now);
			settingField.getDataSetting().setCreationDate(now);
			dataSettingRepo.save(settingField.getDataSetting());
		}
		dataSettingFieldsRepo.save(dataSettingFields);
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void createDataSettingFields(DataSettingField settingField, String dataSettingsGuid){
		Date now = new Date();
		if(Strings.isNullOrEmpty(settingField.getGuid()))
			settingField.setGuid(null);
		settingField.setCreationDate(now);
		settingField.setDataSetting(dataSettingRepo.findOne(dataSettingsGuid));
		dataSettingFieldsRepo.save(settingField);
	}
	@Transactional(readOnly = true)
	public Collection<DataSettingField> getAllActiveDataSettingFields(){
		return dataSettingFieldsRepo.findAllActiveDataSettingFields();
	}
	
	@Transactional(readOnly = true)
	public Collection<DataSettingField> getAllActiveDataSettingFieldsForSetting(String settingGuid){
		return dataSettingFieldsRepo.findAllDataSettingFields(dataSettingRepo.findOne(settingGuid));
	}
	
	@Transactional(readOnly = true)
	public Collection<DataSetting> findAllDataSettings(){
		return dataSettingRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Collection<DataSettingFieldType> getAllFieldTypes(){
		return fieldTypeRepo.findAll();
	}
}
