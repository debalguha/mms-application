package com.sms.persistence.service.predicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.Inventory_;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemPart_;

public class InventoryPredicateSpec extends AbstractPredicateSpec{
	public static Specification<Inventory> buildInventorySearchSpecification(final String searchTerm){
		return new Specification<Inventory>() {
			public Predicate toPredicate(Root<Inventory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.or(buildPredicateForIdCode(root, query, cb, searchTerm), buildPredicateForPartDescription(root, query, cb, searchTerm));
			}
		};
	}
	public static Specification<Inventory> idCodeIsLike(final String searchTerm){
		return new Specification<Inventory>() {
			public Predicate toPredicate(Root<Inventory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				String likePattern = getLikePattern(searchTerm);
				return cb.like(cb.lower(root.<String> get(Inventory_.idCode)), likePattern);
			}
		};
	}	
	
	public static Predicate buildPredicateForPartDescription(Root<Inventory> root, CriteriaQuery<?> query, CriteriaBuilder cb, final String searchTerm){
		return cb.like(cb.lower(root.<SystemPart> get(Inventory_.systemPart).<String>get(SystemPart_.assemblyName)), getLikePattern(searchTerm));
	}
	
	public static Predicate buildPredicateForIdCode(Root<Inventory> root, CriteriaQuery<?> query, CriteriaBuilder cb, final String searchTerm){
		return cb.like(cb.lower(root.<String> get(Inventory_.idCode)), getLikePattern(searchTerm));
	}
}
