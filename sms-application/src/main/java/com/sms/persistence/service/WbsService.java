package com.sms.persistence.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.AbstractTreeComponent;
import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.WbsDrawing;
import com.sms.persistence.model.WbsPicture;
import com.sms.persistence.model.WbsTree;
import com.sms.persistence.repo.SystemDrawingRepo;
import com.sms.persistence.repo.SystemPictureRepo;
import com.sms.persistence.repo.WbsDrawingRepo;
import com.sms.persistence.repo.WbsPictureRepo;
import com.sms.persistence.repo.WbsRepo;
import com.sms.web.model.WbsDTO;

@Service
@Transactional
public class WbsService extends AbstractCloningService{
	@Autowired
	private WbsRepo wbsRepo;
	@Autowired
	private SystemPictureRepo systemPictureRepo;
	@Autowired
	private SystemDrawingRepo systemDrawingRepo;
	@Autowired
	private WbsPictureRepo wbsPictureRepo;
	@Autowired
	private WbsDrawingRepo wbsDrawingRepo;
	
	@Transactional(readOnly = true)
	public Collection<WbsTree> getAllTopLevelWBSElements(){
		List<WbsTree> allWbsTree = wbsRepo.findAll();
		Collection<WbsTree> retList = Lists.newArrayList();
		for(WbsTree tree : allWbsTree){
			if(tree.getParent()!=null)
				continue;
			retList.add(tree);
		}
		return retList;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public WbsTree moveWbsTreeNode(String currentNodeGuid, String newParentGuid) {
		WbsTree wbsNode = wbsRepo.findOne(currentNodeGuid);
		if(newParentGuid == null)
			wbsNode.setParent(null);
		else
			wbsNode.setParent(wbsRepo.findOne(newParentGuid));
		return wbsRepo.save(wbsNode);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public WbsTree copyWbsTreeNode(String currentNodeGuid, String newParentGuid) throws Throwable {
		WbsTree wbsNode = wbsRepo.findOne(currentNodeGuid);
		return wbsRepo.save((WbsTree)super.copyTreeNodeRecursively(wbsNode, wbsRepo.findOne(newParentGuid)));
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public String deleteWbsTreeNode(String guid) {
		return deleteWbsTreeNode(wbsRepo.findOne(guid));
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public String deleteWbsTreeNode(WbsTree wbsTree) {
		for(WbsTree child : wbsTree.getChildren())
			deleteWbsTreeNode(child);
		wbsRepo.delete(wbsTree);
		return "SUCCESS";
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public WbsTree renameWbsTreeNode(String guid, String newName) {
		WbsTree wbsNode = wbsRepo.findOne(guid);
		wbsNode.setNomenclature(newName);
		return wbsRepo.save(wbsNode);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public WbsTree createWbsTreeNode(WbsDTO wbsDTO) {
		WbsTree wbsFromDB = Strings.isNullOrEmpty(wbsDTO.getGuid())?null:wbsRepo.findOne(wbsDTO.getGuid());
		if(wbsFromDB==null){
			wbsFromDB = new WbsTree();
			wbsFromDB.setCreationDate(new Date());
			wbsFromDB.setParent(Strings.isNullOrEmpty(wbsDTO.getParentGuid())?null:wbsRepo.findOne(wbsDTO.getParentGuid()));
		}
		wbsFromDB.setDescription(wbsDTO.getDescription());
		wbsFromDB.setNomenclature(wbsDTO.getNomenclature());
		wbsFromDB.setWbs(wbsDTO.getWbs());
			
		return wbsRepo.save(wbsFromDB);
	}

	@Transactional(readOnly = true)
	public WbsTree getWbsTreeNode(String guid) {
		return wbsRepo.findOne(guid);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Collection<SystemPicture> associatePicture(String wbsGuid, String []pictureGuids){
		List<WbsPicture> associations = Lists.newArrayList();
		for(String pictureGuid : pictureGuids)
			associations.add(associatePicture(wbsGuid, pictureGuid));
		return Lists.transform(associations, new Function<WbsPicture, SystemPicture>() {
			public SystemPicture apply(WbsPicture input) {
				return input.getSystemPicture();
			};
		});
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public WbsPicture associatePicture(String wbsGuid, String pictureGuid){
		WbsTree wbsTree = wbsRepo.findOne(wbsGuid);
		SystemPicture picture = systemPictureRepo.findOne(pictureGuid);
		WbsPicture wbsPicture = new WbsPicture();
		wbsPicture.setCreationDate(new Date());
		wbsPicture.setSystemPicture(picture);
		wbsPicture.setWbsTree(wbsTree);
		return wbsPictureRepo.save(wbsPicture);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Collection<SystemDrawing> associateDrawing(String wbsGuid, String[] drawingGuids) {
		List<WbsDrawing> associations = Lists.newArrayList();
		for(String drawingGuid : drawingGuids)
			associations.add(associateDrawing(wbsGuid, drawingGuid));
		return Lists.transform(associations, new Function<WbsDrawing, SystemDrawing>() {
			public SystemDrawing apply(WbsDrawing input) {
				return input.getSystemDrawing();
			};
		});
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public WbsDrawing associateDrawing(String wbsGuid, String drawingGuid) {
		WbsTree wbsTree = wbsRepo.findOne(wbsGuid);
		SystemDrawing dawing = systemDrawingRepo.findOne(drawingGuid);
		WbsDrawing wbsDrawing = new WbsDrawing();
		wbsDrawing.setCreationDate(new Date());
		wbsDrawing.setSystemDrawing(dawing);
		wbsDrawing.setWbsTree(wbsTree);
		return wbsDrawingRepo.saveAndFlush(wbsDrawing);
	}

	@Transactional(readOnly = true)
	public Collection<SystemPicture> getAllPicturesAssociated(String wbsGuid) {
		return Lists.transform(Lists.newArrayList(wbsPictureRepo.findAllAssociatedPicture(wbsRepo.findOne(wbsGuid))), new Function<WbsPicture, SystemPicture>() {
			public SystemPicture apply(WbsPicture input) {
				return input.getSystemPicture();
			};
		});
	}

	public Collection<SystemDrawing> getAllDrawingsAssociated(String wbsGuid) {
		return Lists.transform(Lists.newArrayList(wbsDrawingRepo.findAllAssociatedPicture(wbsRepo.findOne(wbsGuid))), new Function<WbsDrawing, SystemDrawing>() {
			public SystemDrawing apply(WbsDrawing input) {
				return input.getSystemDrawing();
			};
		});
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void disAssociatePicture(String wbsGuid, String pictureGuid) {
		wbsPictureRepo.delete(wbsPictureRepo.findByWbsAndPictureGuid(wbsRepo.findOne(wbsGuid), systemPictureRepo.findOne(pictureGuid)));
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void disAssociateDrawing(String wbsGuid, String drawingGuid) {
		wbsDrawingRepo.delete(wbsDrawingRepo.findByWbsAndDrawingGuid(wbsRepo.findOne(wbsGuid), systemDrawingRepo.findOne(drawingGuid)));
	}

	public Double getCommonCurrencyPrice(AbstractTreeComponent domainElement) {
		return new Double(-1);
	}

	public AbstractTreeComponent getDomainObjectBySystemTree(AbstractTree treeElement) {
		return null;
	}

}
