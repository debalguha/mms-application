package com.sms.persistence.service;


import java.util.Collection;
import java.util.Date;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.lambdaj.Lambda;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sms.persistence.model.Contact;
import com.sms.persistence.model.InventoryVendor;
import com.sms.persistence.model.Location;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemPartVendor;
import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.model.VendorContact;
import com.sms.persistence.model.VendorContactPK;
import com.sms.persistence.model.VendorType;
import com.sms.persistence.repo.ContactRepository;
import com.sms.persistence.repo.InventoryVendorRepo;
import com.sms.persistence.repo.LocationRepo;
import com.sms.persistence.repo.SystemPartVendorRepo;
import com.sms.persistence.repo.SystemVendorRepo;
import com.sms.persistence.repo.VendorContactRepository;
import com.sms.persistence.repo.VendorTypeRepo;
import com.sms.web.model.VendorDTO;

@Service
@Transactional
public class VendorService {
	@Autowired
	private SystemVendorRepo vendorRepo;
	@Autowired
	private VendorTypeRepo vendorTypeRepo;
	@Autowired
	private SystemPartVendorRepo systemPartVendorRepo;
	@Autowired
	private LocationRepo locationRepo;
	@Autowired
	private SystemService systemService;
	@Autowired
	private ContactRepository contactRepo;
	@Autowired
	private VendorContactRepository vendorContactRepo;
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private InventoryVendorRepo invVendorRepo;
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemVendor createVendor(SystemVendor vendor){
		SystemVendor vendorFromDB=!Strings.isNullOrEmpty(vendor.getGuid())?vendorFromDB = vendorRepo.findByVendorName(vendor.getGuid()):null;
		if(vendorFromDB!=null)
			vendor.setCreationDate(vendorFromDB.getCreationDate());
		else
			vendor.setCreationDate(new Date());
		return vendorRepo.save(vendor);
	}
	
	@Transactional(readOnly = true)
	public VendorType findVendorTypeByName(String type){
		return vendorTypeRepo.findVendorTypeByName(type);
	}
	
	@Transactional(readOnly = true)
	public Collection<VendorType> findAllVendorTypes(){
		return vendorTypeRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemVendor> findAllVendors(){
		return vendorRepo.findAll();
	}
	@Transactional(readOnly = true)
	public Collection<SystemVendor> findVendorsOfPart(SystemPart systemPart) {
		Collection<SystemPartVendor> systemPartVendorsByPart = systemPartVendorRepo.findSystemPartVendorByPart(systemPart);
		Collection<SystemVendor> vendorsOfPart = Lists.newArrayList();
		for(SystemPartVendor systemPartVendor : systemPartVendorsByPart)
			vendorsOfPart.add(systemPartVendor.getSystemVendor());
		return vendorsOfPart;
	}
	@Transactional(readOnly = true)
	public Collection<SystemVendor> getAllVendors(){
		return vendorRepo.findAll();
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public Collection<SystemPartVendor> associateVendorsWithPart(String partGuid, String []vendorGuids){
		SystemPart partByGuid = systemService.getPartByTreeGuid(partGuid);
		Set<SystemPartVendor> partVendors = partByGuid.getSystemPartVendors();
		if(!CollectionUtils.isEmpty(partVendors))
			systemPartVendorRepo.delete(partByGuid.getSystemPartVendors());
		Collection<SystemVendor> selectedVendors = Lists.newArrayListWithCapacity(vendorGuids.length);
		for(String vendorGuid : vendorGuids)
			selectedVendors.add(vendorRepo.findOne(vendorGuid));
		Collection<SystemPartVendor> systemPartvendors = Lists.newArrayListWithCapacity(vendorGuids.length);
		Date now = new Date();
		for(SystemVendor vendor : selectedVendors)
			systemPartvendors.add(new SystemPartVendor(now, partByGuid, vendor));
		return systemPartVendorRepo.save(systemPartvendors);
	}
	@Transactional(readOnly = true)
	public Collection<SystemVendor> findVendorsNotOfPart(SystemPart systemPart) {
		Set<SystemVendor> vendorsOfPart = Sets.newHashSet(findVendorsOfPart(systemPart));
		Set<SystemVendor> allVendors = Sets.newHashSet(vendorRepo.findAll());
		return Sets.difference(allVendors, vendorsOfPart);
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean disassociateVendorsWithPart(String partGuid, String[] vendorGuids) {
		SystemPart partByGuid = systemService.getPartByTreeGuid(partGuid);
		systemPartVendorRepo.delete(Lambda.select(partByGuid.getSystemPartVendors(), Lambda.having(Lambda.on(SystemPartVendor.class).getSystemVendor().getGuid(), Matchers.isIn(vendorGuids))));
		return true;
	}

	@Transactional(readOnly = true)
	public SystemVendor findVendorByGuid(String guid) {
		return vendorRepo.findOne(guid);
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemVendor createVendor(VendorDTO vendorDTO) {
		SystemVendor vendor = Strings.isNullOrEmpty(vendorDTO.getGuid())?null:vendorRepo.findOne(vendorDTO.getGuid());
		VendorType vendorType = vendorTypeRepo.findOne(vendorDTO.getVendorType());
		Location location = locationRepo.findOne(vendorDTO.getLocation());
		Collection<VendorContact> allVendorContacts = vendor==null? null:vendorContactRepo.findAllContactsForVendor(vendor);
		if(vendor == null){
			vendor = new SystemVendor();
			vendor.setCreationDate(new Date());
		}
		vendor.setLocation(location);
		vendor.setName(vendorDTO.getVendorName());
		vendor.setType(vendorType);
		vendor = vendorRepo.save(vendor);
		Collection<Contact> contacts = vendorDTO.getContacts();
		if(!CollectionUtils.isEmpty(allVendorContacts)){
			vendorContactRepo.delete(allVendorContacts);
			allVendorContacts.clear();
		}else
			allVendorContacts = Sets.newHashSet();
		for(Contact contact : contacts){
			contactRepo.save(contact);
			allVendorContacts.add(new VendorContact(new VendorContactPK(vendor, contact)));
		}
		if(!CollectionUtils.isEmpty(allVendorContacts))
			vendorContactRepo.save(allVendorContacts);
		return vendor;
	}

	@Transactional(readOnly = true)
	public SystemPartVendor findSystemPartVendorRecordByGuid(String guid){
		return systemPartVendorRepo.findOne(guid);
	}
	
	@Transactional(readOnly = true)
	public Collection<Contact> findVendorContacts(String guid) {
		Collection<Contact> contacts = Sets.newHashSet();
		for(VendorContact vendorContact : vendorRepo.findOne(guid).getContacts())
			contacts.add(vendorContact.getContact());
		return contacts;
	}

	@Transactional(readOnly = true)
	public Collection<? extends SystemVendor> findInventorySupplyingVendors(String inventoryGuid) {
		Set<SystemVendor> vendors = Sets.newHashSet();
		for(InventoryVendor inventoryVendor : inventoryService.findInventoryByGuid(inventoryGuid).getInventoryVendors())
			vendors.add(inventoryVendor.getVendor());
		return vendors;
	}

	@Transactional(readOnly = true)
	public SystemVendor findVendorForInventoryVendor(String inventoryVendorGuid) {
		return vendorRepo.findOne(invVendorRepo.findOne(inventoryVendorGuid).getVendor().getGuid());
	}

}
