package com.sms.persistence.service.predicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemDrawing_;

public class DrawingPredicateSpec extends AbstractPredicateSpec{
	public static Specification<SystemDrawing> fileNameIsLike(final String searchTerm) {
        
		return new Specification<SystemDrawing>() {
			public Predicate toPredicate(Root<SystemDrawing> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				String likePattern = getLikePattern(searchTerm);
                return cb.like(cb.lower(root.<String> get(SystemDrawing_.storedFileName)), likePattern);
			}
		};
    }
}
