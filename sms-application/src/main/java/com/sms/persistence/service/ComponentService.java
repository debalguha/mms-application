package com.sms.persistence.service;

import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.AbstractTreeComponent;


public interface ComponentService {
	public abstract Double getCommonCurrencyPrice(AbstractTreeComponent domainElement);
	public abstract AbstractTreeComponent getDomainObjectBySystemTree(AbstractTree treeElement);
	public abstract Double calculateCommonCurrentTargetPrice(AbstractTreeComponent part);
}
