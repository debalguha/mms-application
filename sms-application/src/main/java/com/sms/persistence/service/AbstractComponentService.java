package com.sms.persistence.service;

import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.AbstractTreeComponent;
import com.sms.persistence.model.SystemTreeItemType;
import com.sms.persistence.model.SystemTreeItemType.TreeItemTypes;

@Transactional
public abstract class AbstractComponentService implements ComponentService{
	@Autowired
	protected Map<TreeItemTypes, ComponentService> serviceMap;
	
	@Transactional(readOnly = true)
	public Double calculateCommonCurrentTargetPrice(AbstractTreeComponent component) {
		if(CollectionUtils.isEmpty(component.getSystemTree().getChildren()))
			return serviceMap.get(SystemTreeItemType.TreeItemTypes.valueOf(component.getSystemTree().getTreeItemType().getItemType())).getCommonCurrencyPrice(component);
		double commonCurrentTargetPrice = new Double(0);
		for(AbstractTree treeElement : component.getSystemTree().getChildren()){
			ComponentService service = serviceMap.get(SystemTreeItemType.TreeItemTypes.valueOf(treeElement.getTreeItemType().getItemType()));
			commonCurrentTargetPrice+=service.calculateCommonCurrentTargetPrice(service.getDomainObjectBySystemTree(treeElement));
		}
		return commonCurrentTargetPrice;
	}
}
