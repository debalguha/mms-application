package com.sms.persistence.service;

import static ch.lambdaj.Lambda.convert;
import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectUnique;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;

import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.lambdaj.function.convert.PropertyExtractor;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.sms.persistence.model.InventoryVendor;
import com.sms.persistence.model.RFQEntity;
import com.sms.persistence.model.RFQItem;
import com.sms.persistence.model.RFQResponse;
import com.sms.persistence.model.RFQResponseItem;
import com.sms.persistence.model.RFQType;
import com.sms.persistence.model.RFQVendor;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.repo.RFQItemRepo;
import com.sms.persistence.repo.RFQRepo;
import com.sms.persistence.repo.RFQResponseItemRepo;
import com.sms.persistence.repo.RFQResponseRepo;
import com.sms.persistence.repo.RFQVendorRepo;
import com.sms.persistence.repo.RFQtypeRepo;
import com.sms.persistence.repo.SystemVendorRepo;
import com.sms.web.model.RFQItemDTO;
import com.sms.web.model.RFQObjectDTO;
import com.sms.web.model.RFQResponseDTO;
import com.sms.web.model.RFQResponseItemDTO;

@Service
@Transactional
public class RFQService {
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private RFQRepo rfqRepo;
	@Autowired
	private RFQItemRepo rfqItemRepo;
	@Autowired
	private RFQVendorRepo rfqVendorRepo;
	@Autowired
	private RFQResponseRepo rfqResponseRepo;
	@Autowired
	private RFQResponseItemRepo rfqResponseItemRepo;
	@Autowired
	private RFQtypeRepo rfqTypeRepo;
	@Autowired
	private SystemVendorRepo vendorRepo;

	@Transactional(readOnly = true)
	public Collection<RFQType> findAllRFQTypes() {
		return rfqTypeRepo.findAll();
	}

	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public RFQEntity createRFQ(RFQObjectDTO rfqDTO) {
		Date now = new Date();
		RFQEntity rfqEntity = Strings.isNullOrEmpty(rfqDTO.getGuid())?new RFQEntity():findRFQEntityByGuid(rfqDTO.getGuid());
		if(rfqEntity.getCreationDate()==null)
			rfqEntity.setCreationDate(now);
		rfqEntityFromDTO(rfqEntity, rfqDTO);
		for (RFQItemDTO rfqItemDTO : rfqDTO.getRfqItems()) {
			RFQItem rfqItem = Strings.isNullOrEmpty(rfqItemDTO.getItmGuid())?new RFQItem():rfqItemRepo.findOne(rfqItemDTO.getItmGuid());
			rfqItem.setQuantity(rfqItemDTO.getItemQuantity());
			if(!Strings.isNullOrEmpty(rfqItemDTO.getInvGuid()))
				rfqItem.setSystemPart(inventoryService.findInventoryByGuid(rfqItemDTO.getInvGuid()).getSystemPart());
			rfqItem.setUnitPrice(10);
			if(!Strings.isNullOrEmpty(rfqItemDTO.getInvGuid()))
				rfqItem.setCreationDate(now);
			rfqItem.setRfqEntity(rfqEntity);
			rfqItemRepo.save(rfqItem);
		}
		rfqRepo.save(rfqEntity);
		Collection<RFQVendor> rfqVendors = Strings.isNullOrEmpty(rfqDTO.getGuid())?null:rfqEntity.getRfqVendors();
		for (String vendorGuid : rfqDTO.getRfqVendors()) {
			SystemVendor systemVendor = vendorRepo.findOne(vendorGuid);
			RFQVendor rfqVendor = null;
			if(!CollectionUtils.isEmpty(rfqVendors))
				rfqVendor = selectUnique(rfqVendors, allOf(having(on(RFQVendor.class).getRfqEntity(), equalTo(rfqEntity)), having(on(RFQVendor.class).getSystemVendor(), equalTo(systemVendor))));
			/*try {
				rfqVendor = Strings.isNullOrEmpty(rfqDTO.getGuid())?new RFQVendor():rfqVendorRepo.findByRFQAndVendor(rfqEntity, systemVendor);
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			if(rfqVendor == null)
				rfqVendor = new RFQVendor();
			else{
				rfqVendors.remove(rfqVendor);
				continue;
			}
			rfqVendor.setRfqEntity(rfqEntity);
			rfqVendor.setSystemVendor(systemVendor);
			rfqVendor.setCreationDate(now);
			rfqVendorRepo.save(rfqVendor);
		}
		if(!CollectionUtils.isEmpty(rfqVendors))
			rfqVendorRepo.delete(rfqVendors);
		return rfqEntity;
	}

	private void rfqEntityFromDTO(RFQEntity rfqEntity, RFQObjectDTO rfqDTO) {
		RFQType rfqType = rfqTypeRepo.findOne(rfqDTO.getRfqType());
		rfqEntity.setRfqType(rfqType);
		rfqEntity.setIssuedDate(rfqDTO.getIssueDate());
		rfqEntity.setNotes(rfqDTO.getNotes());
		if(Strings.isNullOrEmpty(rfqEntity.getGuid()))
			rfqEntity.setRfqNumber(rfqDTO.getRfqNumber());
		rfqEntity.setStatusDateCreated(rfqDTO.getStatusDateCreated());
		rfqEntity.setExpirationDate(rfqDTO.getExpirationDate());
	}

	@Transactional(readOnly = true)
	public Collection<? extends RFQEntity> findAllRFQs() {
		return rfqRepo.findAll();
	}

	@Transactional(readOnly = true)
	public int findResponseCount(RFQEntity rfqEntity) {
		return rfqResponseRepo.getResponseCount(rfqEntity);
		/*int count = 0;
		for (RFQItem rfqItem : rfqEntity.getRfqItems())
			count += rfqResponseItemRepo.findReponsesForItem(rfqItem).size();
		return count;*/
	}

	@Transactional(readOnly = true)
	public RFQResponse findRFQResponseByGuid(String rfqResponseGuid) {
		return rfqResponseRepo.findOne(rfqResponseGuid);
	}

	@Transactional(readOnly = true)
	public RFQEntity findRFQEntityByGuid(String rfqGuid) {
		return rfqRepo.findOne(rfqGuid);
	}

	@Transactional(readOnly = true)
	public RFQResponse findRFQResponseByRFQVendor(String rfqVendorGuid, String rfqGuid) {
		return rfqResponseRepo.findResponseByVendor(rfqVendorRepo.findOne(rfqVendorGuid).getSystemVendor(), rfqRepo.findOne(rfqGuid));
	}

	@Transactional(readOnly = true)
	public RFQResponse findRFQResponseBySystemVendor(String rfqVendorGuid, String rfqGuid) {
		return rfqResponseRepo.findResponseByVendor(vendorRepo.findOne(rfqVendorGuid), rfqRepo.findOne(rfqGuid));
	}

	@Transactional(readOnly = true)
	public Collection<RFQResponseItem> findRFQResponseItemsForVendor(String rfqGuid, String systemVendorGuid) {
		RFQVendor rfqVendor = selectUnique(rfqRepo.findOne(rfqGuid).getRfqVendors(), having(on(RFQVendor.class).getSystemVendor().getGuid(), equalTo(systemVendorGuid)));
		return findRFQResponseByRFQVendor(rfqVendor.getGuid(), rfqGuid).getRfqResponseItems();
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public RFQResponse saveRFQResponse(RFQResponseDTO rfqResponseDTO) {
		Date now = new Date();
		RFQEntity rfqEntity = rfqRepo.findOne(rfqResponseDTO.getRfqGuid());
		SystemVendor vendor = vendorRepo.findOne(rfqResponseDTO.getVendorGuid());
		RFQResponse rfqResponse = null;
		try {
			rfqResponse = findRFQResponseBySystemVendor(rfqResponseDTO.getVendorGuid(), rfqResponseDTO.getRfqGuid());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (rfqResponse == null) {
			rfqResponse = new RFQResponse();
			rfqResponse.setResponseId(UUID.randomUUID().toString());
			rfqResponse.setNotes(rfqResponseDTO.getNotes());
			rfqResponse.setResponseId(rfqResponseDTO.getResponseId());
			rfqResponse.setDeliveryByDate(rfqResponseDTO.getDeliveryByDate());
			rfqResponse.setRfqEntity(rfqEntity);
			rfqResponse.setSystemVendor(vendor);
			rfqResponse.setCreationDate(now);
			rfqResponse.setAccepted(rfqResponseDTO.getAccepted());
			if(rfqResponse.getAccepted() == 1)
				rfqResponse.setAcceptedDate(now);
			for (RFQResponseItemDTO rfqResponseItemDTO : rfqResponseDTO.getItems()) {
				RFQItem rfqItem = rfqItemRepo.findOne(rfqResponseItemDTO.getGuid());
				RFQResponseItem rfqResponseItem = new RFQResponseItem();
				rfqResponseItem.setCreationDate(now);
				rfqResponseItem.setQuantity(rfqItem.getQuantity());
				rfqResponseItem.setUnitPrice(rfqResponseItemDTO.getUnitPrice());
				rfqResponseItem.setRfqItem(rfqItem);
				rfqResponseItem.setRfqResponse(rfqResponse);
				rfqResponseItemRepo.save(rfqResponseItem);
			}
		} else {
			if(rfqResponse.getAccepted() == 0 && rfqResponseDTO.getAccepted() == 1){
				rfqResponse.setAccepted(rfqResponseDTO.getAccepted());
				rfqResponse.setAcceptedDate(now);
			}
			rfqResponse.setDeliveryByDate(rfqResponseDTO.getDeliveryByDate());
			rfqResponse.setNotes(rfqResponseDTO.getNotes());
			rfqResponse.setResponseId(rfqResponseDTO.getResponseId());
			for (RFQResponseItemDTO rfqResponseItemDTO : rfqResponseDTO.getItems()) {
				RFQResponseItem rfqResponseItem = selectUnique(rfqResponse.getRfqResponseItems(), having(on(RFQResponseItem.class).getGuid(), equalTo(rfqResponseItemDTO.getGuid())));
				rfqResponseItem.setUnitPrice(rfqResponseItemDTO.getUnitPrice());
				rfqResponseItemRepo.save(rfqResponseItem);
			}
		}
		return rfqResponse;
	}

	@Transactional(readOnly = true)
	public Collection<SystemVendor> findRemainingVendorsForRFQ(String rfqGuid) {
		RFQEntity rfqEntity = findRFQEntityByGuid(rfqGuid);
		Collection<SystemPart> allParts = convert(rfqEntity.getRfqItems(), new PropertyExtractor<RFQItem, SystemPart>("systemPart"));
		Set<SystemVendor> associatedRFQVendors = Sets.newHashSet(convert(rfqEntity.getRfqVendors(), new PropertyExtractor<RFQVendor, SystemVendor>("systemVendor")));
		Set<SystemVendor> allVendors = Sets.newHashSet();
		for(SystemPart part : allParts)
			allVendors.addAll(convert(inventoryService.findInventoryOfPart(part).getInventoryVendors(), new PropertyExtractor<InventoryVendor, SystemVendor>("vendor")));
		return Sets.difference(allVendors, associatedRFQVendors);
	}
}
