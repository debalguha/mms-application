package com.sms.persistence.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sms.persistence.model.SyncSetting;
import com.sms.persistence.repo.SyncSettingRepo;

@Service
@Transactional
public class SyncSettingService {
	@Autowired
	private SyncSettingRepo syncSettingRepo;
	
	@Transactional(readOnly = true)
	public Collection<SyncSetting> findAllSyncSettings(){
		return syncSettingRepo.findAll();
		
	}
	
}