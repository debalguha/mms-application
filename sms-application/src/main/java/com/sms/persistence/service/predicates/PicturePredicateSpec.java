package com.sms.persistence.service.predicates;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.SystemPicture_;

public class PicturePredicateSpec extends AbstractPredicateSpec{
	public static Specification<SystemPicture> fileNameIsLike(final String searchTerm) {
        
		return new Specification<SystemPicture>() {
			public Predicate toPredicate(Root<SystemPicture> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				String likePattern = getLikePattern(searchTerm);
                return cb.like(cb.lower(root.<String> get(SystemPicture_.storedFileName)), likePattern);
			}
		};
    }	
}
