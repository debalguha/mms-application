package com.sms.persistence.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.AbstractTreeComponent;
import com.sms.persistence.model.LineItem;
import com.sms.persistence.model.LineItemType;
import com.sms.persistence.repo.LineItemRepo;
import com.sms.persistence.repo.LineItemTypeRepo;
import com.sms.persistence.repo.SystemTreeRepo;
import com.sms.util.SMSUtil;

@Service
@Transactional
public class LineItemService extends AbstractCloningService {
	@Autowired
	private LineItemRepo lineItemRepo;

	@Autowired
	private LineItemTypeRepo lineItemTypeRepo;

	@Autowired
	private SystemTreeRepo systemTreeRepo;

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public LineItem createLineItem(LineItem lineItem) {
		LineItem lineItemFromDB = findByLineItemGuid(lineItem.getGuid());
		LineItemType lineItemType = null;
		if (lineItem.getLineItemType().getGuid() != null)
			lineItemType = lineItemTypeRepo.findOne(lineItem.getLineItemType().getGuid());
		else {
			if (!Strings.isNullOrEmpty(lineItem.getLineItemType().getLineItemType())) {
				if (SMSUtil.validateIfStringIsGuid(lineItem.getLineItemType().getLineItemType()))
					lineItemType = lineItemTypeRepo.findOne(lineItem.getLineItemType().getLineItemType());
				else
					lineItemType = lineItemTypeRepo.findLineItemByType(lineItem.getLineItemType().getLineItemType());
			}
		}
		if (lineItemType == null && !Strings.isNullOrEmpty(lineItem.getLineItemType().getLineItemType()))
			lineItemType = lineItem.getLineItemType();
		if (lineItemFromDB != null) {
			lineItem.setSystemTree(lineItemFromDB.getSystemTree());
			lineItem.getSystemTree().setNomenclature(lineItem.getNomenclature());
			lineItem.setWbsTree(lineItemFromDB.getWbsTree());
			lineItem.setCreationDate(lineItemFromDB.getCreationDate());
		} else
			lineItem.setCreationDate(new Date());
		lineItem.setLineItemType(lineItemType);
		if (Strings.isNullOrEmpty(lineItemType.getGuid())) {
			lineItemType.setCreationDate(new Date());
			lineItemTypeRepo.save(lineItemType);
		}
		return lineItemRepo.save(lineItem);
	}

	@Transactional(readOnly = true)
	public LineItem findBySystemTreeGuid(String systemTreeGuid) {
		return lineItemRepo.findBySystemTreeNode(systemTreeRepo.findOne(systemTreeGuid));
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public LineItem renameLineItem(String systemTreeGuid, String newName) {
		LineItem lineItem = findBySystemTreeGuid(systemTreeGuid);
		lineItem.setIdCode(newName);
		lineItem.getSystemTree().setNomenclature(newName);
		return lineItemRepo.save(lineItem);
	}

	@Transactional(readOnly = true)
	public LineItem findByLineItemGuid(String lineItemGuid) {
		if (Strings.isNullOrEmpty(lineItemGuid))
			return null;
		return lineItemRepo.findOne(lineItemGuid);
	}

	@Transactional(readOnly = true)
	public Collection<LineItemType> findAllLineItemType() {
		return lineItemTypeRepo.findAll();
	}

	public Double getCommonCurrencyPrice(AbstractTreeComponent domainElement) {
		return ((LineItem) domainElement).getCommonCurrencyPrice();
	}

	public AbstractTreeComponent getDomainObjectBySystemTree(AbstractTree treeElement) {
		return findBySystemTreeGuid(treeElement.getGuid());
	}

}
