package com.sms.persistence.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.AbstractTreeComponent;
import com.sms.persistence.model.ServiceDataType;
import com.sms.persistence.model.SystemServiceData;
import com.sms.persistence.repo.ServiceDataTypeRepo;
import com.sms.persistence.repo.SystemServiceDataRepo;
import com.sms.persistence.repo.SystemTreeRepo;
import com.sms.util.SMSUtil;

@Service("systemServiceDataService")
@Transactional
public class SystemServiceDataService extends AbstractCloningService {
	@Autowired
	private SystemServiceDataRepo serviceRepo;

	@Autowired
	private SystemTreeRepo systemTreeRepo;

	@Autowired
	private ServiceDataTypeRepo serviceDataTypeRepo;

	@Transactional(readOnly = true)
	public Collection<ServiceDataType> findAllServiceDataType() {
		return serviceDataTypeRepo.findAll();
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemServiceData createSystemService(SystemServiceData systemService) {
		SystemServiceData systemServiceFromDB = findByServiceGuid(systemService.getGuid());
		ServiceDataType serviceDataType = null;
		if (systemService.getServiceDataType().getGuid() != null)
			serviceDataType = serviceDataTypeRepo.findOne(systemService.getServiceDataType().getGuid());
		else {
			if (Strings.isNullOrEmpty(systemService.getServiceDataType().getServiceType())) {
				if (SMSUtil.validateIfStringIsGuid(systemService.getServiceDataType().getServiceType()))
					serviceDataType = serviceDataTypeRepo.findOne(systemService.getServiceDataType().getServiceType());
				else
					serviceDataType = serviceDataTypeRepo.findServiceDataTypeByType(systemService.getServiceDataType().getServiceType());
			}
		}
		if (serviceDataType == null && !Strings.isNullOrEmpty(systemService.getServiceDataType().getServiceType()))
			serviceDataType = systemService.getServiceDataType();
		if (systemServiceFromDB != null) {
			systemService.setSystemTree(systemServiceFromDB.getSystemTree());
			systemService.getSystemTree().setNomenclature(systemService.getNomenclature());
			systemService.setWbsTree(systemServiceFromDB.getWbsTree());
			systemService.setCreationDate(systemServiceFromDB.getCreationDate());
		} else
			systemService.setCreationDate(new Date());
		systemService.setServiceDataType(serviceDataType);
		if (Strings.isNullOrEmpty(serviceDataType.getGuid())) {
			serviceDataType.setCreationDate(new Date());
			serviceDataTypeRepo.save(serviceDataType);
		}
		return serviceRepo.save(systemService);
	}

	@Transactional(readOnly = true)
	public SystemServiceData findByServiceGuid(String serviceGuid) {
		if (Strings.isNullOrEmpty(serviceGuid))
			return null;
		return serviceRepo.findOne(serviceGuid);
	}

	@Transactional(readOnly = true)
	public SystemServiceData findBySystemTreeGuid(String systemTreeGuid) {
		return serviceRepo.findBySystemTreeNode(systemTreeRepo.findOne(systemTreeGuid));
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemServiceData renameSystemServiceData(String systemTreeGuid, String newName) {
		SystemServiceData systemServiceData = findBySystemTreeGuid(systemTreeGuid);
		systemServiceData.setIdCode(newName);
		systemServiceData.getSystemTree().setNomenclature(newName);
		return serviceRepo.save(systemServiceData);
	}

	@Transactional(readOnly = true)
	public ServiceDataType findServiceDataTypeByType(String serviceDataTypeName) {
		return serviceDataTypeRepo.findServiceDataTypeByType(serviceDataTypeName);
	}

	public Double getCommonCurrencyPrice(AbstractTreeComponent domainElement) {
		return ((SystemServiceData) domainElement).getCommonCurrencyPrice();
	}

	@Transactional(readOnly = true)
	public AbstractTreeComponent getDomainObjectBySystemTree(AbstractTree treeElement) {
		return findBySystemTreeGuid(treeElement.getGuid());
	}

}
