package com.sms.persistence.service;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sms.persistence.model.WorkOrder;
import com.sms.persistence.model.WorkOrderItem;
import com.sms.persistence.model.WorkOrderType;
import com.sms.persistence.repo.WorkOrderItemRepository;
import com.sms.persistence.repo.WorkOrderRepository;
import com.sms.persistence.repo.WorkOrderTypeRepository;

@Service
@Transactional
public class WorkOrderService {
	@Autowired
	private WorkOrderRepository workOrderRepo;
	@Autowired
	private WorkOrderTypeRepository workOrdertypeRepo;
	@Autowired
	private WorkOrderItemRepository workOrderItemRepo;

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public WorkOrder createWorkOrder(WorkOrder workOrder) {
		Date now = new Date();
		workOrder.setCreationDate(now);
		workOrderRepo.save(workOrder);
		if (!CollectionUtils.isEmpty(workOrder.getWorkOrderItems())) {
			for (WorkOrderItem item : workOrder.getWorkOrderItems()) {
				item.setCreationDate(now);
				workOrderItemRepo.save(item);
			}
		}
		return workOrder;
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public WorkOrder editWorkOrder(WorkOrder workOrder) {
		Date now = new Date();
		WorkOrder workOrderDB = workOrderRepo.findOne(workOrder.getGuid());
		workOrderDB.setCompletedDate(workOrder.getCompletedDate());
		workOrderDB.setCreateByUser(workOrder.getCreateByUser());
		workOrderDB.setCustomer(workOrder.getCustomer());
		workOrderDB.setLocation(workOrder.getLocation());
		workOrderDB.setPriority(workOrder.getPriority());
		workOrderDB.setPromisedDate(workOrder.getPromisedDate());
		workOrderDB.setScheduledDate(workOrder.getScheduledDate());
		workOrderDB.setSuperVisorGuid(workOrder.getSuperVisorGuid());
		workOrderDB.setTechUserGuid(workOrder.getTechUserGuid());
		workOrderDB.setWorkOrderType(workOrder.getWorkOrderType());

		workOrderItemRepo.delete(workOrderDB.getWorkOrderItems());
		workOrderRepo.save(workOrderDB);
		if (!CollectionUtils.isEmpty(workOrder.getWorkOrderItems())) {
			for (WorkOrderItem item : workOrder.getWorkOrderItems()) {
				item.setCreationDate(workOrderDB != null ? workOrderDB.getCreationDate() : now);
				item.setWorkOrder(workOrderDB);
				workOrderItemRepo.save(item);
			}
		}
		/*for (WorkOrderItem workOrderItem : workOrder.getWorkOrderItems()) {
			WorkOrderItem matchingItem = selectUnique(workOrderDB.getWorkOrderItems(), having(on(WorkOrderItem.class).getSystemTree(), equalTo(workOrderItem.getSystemTree())));
			workOrderItem.setCreationDate(matchingItem != null ? matchingItem.getCreationDate() : now);
			;
			workOrderItem.setWorkOrder(workOrderDB);
		}
		workOrderItemRepo.delete(workOrderDB.getWorkOrderItems());
		workOrderItemRepo.save(workOrder.getWorkOrderItems());*/
		return workOrder;
	}

	@Transactional(readOnly = true)
	public Collection<WorkOrderType> findAllWorkOrderTypes() {
		return workOrdertypeRepo.findAll();
	}

	@Transactional(readOnly = true)
	public WorkOrderType findWorkOrderTypeByGuid(String workOrderTypeGuid) {
		return workOrdertypeRepo.findOne(workOrderTypeGuid);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean deleteWorkOrder(WorkOrder workOrder) {
		workOrderRepo.delete(workOrder);
		return true;
	}

	@Transactional(readOnly = true)
	public Collection<WorkOrder> findAllWorkOrders() {
		return workOrderRepo.findAll();
	}

	@Transactional(readOnly = true)
	public Collection<WorkOrder> findAllCompletedWorkOrders() {
		return workOrderRepo.findAllCompletedWorkOrders(normalizedDatToZeroHour());
		/*List<WorkOrderItem> woItems = Lists.newArrayList();
		for (final WorkOrder wo : allCompletedWorkOrders) {
			CollectionUtils.forAllDo(wo.getWorkOrderItems(), new Closure() {
				public void execute(Object input) {
					((WorkOrderItem) input).setWorkOrder(wo);
				}
			});
			woItems.addAll(wo.getWorkOrderItems());
		}
		return woItems;*/
	}

	@Transactional(readOnly = true)
	public Collection<WorkOrder> findAllInProgressWorkOrders() {
		return workOrderRepo.findAllInProgressWorkOrders(normalizedDatToZeroHour());
/*		List<WorkOrderItem> woItems = Lists.newArrayList();
		for (final WorkOrder wo : allCompletedWorkOrders) {
			CollectionUtils.forAllDo(wo.getWorkOrderItems(), new Closure() {
				public void execute(Object input) {
					((WorkOrderItem) input).setWorkOrder(wo);
				}
			});
			woItems.addAll(wo.getWorkOrderItems());
		}
		return woItems;*/
	}

	@Transactional(readOnly = true)
	public Collection<WorkOrder> findAllScheduledWorkOrders() {
		return workOrderRepo.findAllInScheduledWorkOrders(normalizedDatToZeroHour());
		/*List<WorkOrderItem> woItems = Lists.newArrayList();
		for (final WorkOrder wo : allScheduledWorkOrders) {
			CollectionUtils.forAllDo(wo.getWorkOrderItems(), new Closure() {
				public void execute(Object input) {
					((WorkOrderItem) input).setWorkOrder(wo);
				}
			});
			woItems.addAll(wo.getWorkOrderItems());
		}
		return woItems;*/
	}

	private static final Date normalizedDatToZeroHour() {
		return DateTime.now().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate();
	}

	@Transactional(readOnly = true)
	public WorkOrder findWorkorderByGuid(String guid) {
		return workOrderRepo.findOne(guid);
	}
}
