package com.sms.persistence.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class StoreProcedureService {
	@Autowired
	private DataSource dataSource;
	@PersistenceContext
	private EntityManager entityManager;
		
	@SuppressWarnings("unchecked")
	public List<Object[]> search(String searchTerm, String userGuid){
		StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search");
		storedProcedure.registerStoredProcedureParameter("search_term", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("user_guid", String.class, ParameterMode.IN);
		
		storedProcedure.setParameter("search_term", searchTerm);
		storedProcedure.setParameter("user_guid", userGuid);
		storedProcedure.execute();
		return storedProcedure.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> reportExample(Date startDate, Date endDate){
		StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("example_report");
		storedProcedure.registerStoredProcedureParameter("d1", Date.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("d2", Date.class, ParameterMode.IN);
		
		storedProcedure.setParameter("d1", startDate);
		storedProcedure.setParameter("d2", endDate);
		storedProcedure.execute();
		return storedProcedure.getResultList();
	}
}
