package com.sms.persistence.service;

import static ch.lambdaj.Lambda.having;
import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.select;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.repo.SystemPartRepo;
import com.sms.persistence.repo.SystemPictureRepo;
import com.sms.persistence.repo.SystemTreeItemTypeRepo;
import com.sms.persistence.repo.SystemTreeRepo;
import com.sms.persistence.service.predicates.PicturePredicateSpec;

@Service
@Transactional
public class PictureService {
	@Autowired
	private SystemTreeRepo systemTreeRepo;
	
	@Autowired
	private SystemTreeItemTypeRepo treeItemTypeRepo;
	
	@Autowired
	private SystemPartRepo partRepo;
	
	@Autowired
	private SystemPictureRepo pictureRepo;
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SystemPicture createPicture(SystemPicture picture) {
		return pictureRepo.save(picture);
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemPicture> getAllPicturesForPart(String partGuid) {
		return getAllPicturesForPartTreeNode(partRepo.findOne(partGuid).getSystemTree().getGuid());
	}
	@Transactional(readOnly = true)
	public Collection<SystemPicture> getAllPicturesForPartTreeNode(String partGuid) {
		SystemTree partTreeNode = systemTreeRepo.findOne(partGuid);
		if (CollectionUtils.isEmpty(partTreeNode.getChildren()))
			return Collections.emptyList();
		List<SystemTree> pictureTreeItems = select(partTreeNode.getChildren(), having(on(SystemTree.class).getTreeItemType().getItemType(), equalTo(treeItemTypeRepo.getItemTypeByName("Picture").getItemType())));
		Collection<SystemPicture> systemPictures = Lists.newArrayListWithCapacity(pictureTreeItems.size());
		for (SystemTree drawingTreeNode : pictureTreeItems)
			systemPictures.add(pictureRepo.findByTreeGuid(drawingTreeNode));
		return systemPictures;
	}

	@Transactional(readOnly = true)
	public SystemPicture getPictureByGuid(String pictureGuid) {
		return pictureRepo.findOne(pictureGuid);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void removePictureByGuid(String pictureGuid) {
		SystemPicture picture = pictureRepo.findOne(pictureGuid);
		SystemTree systemTree = picture.getSystemTree();
		picture.setSystemTree(null);
		systemTreeRepo.delete(systemTree);
		pictureRepo.save(picture);
	}

	@Transactional(readOnly = true)
	public Collection<SystemPicture> getAllPicturesNotForPartTreeNode(String partGuid) {
		Set<SystemPicture> allPicturesForPart = Sets.newHashSet(getAllPicturesForPartTreeNode(partGuid));
		Set<SystemPicture> picturesRepoCollection = Sets.newHashSet(pictureRepo.findAll());
		return Sets.difference(picturesRepoCollection, allPicturesForPart);
	}
	
	@Transactional(readOnly = true)
	public Collection<SystemPicture> getAllPicturesNotForPart(String partGuid) {
		Set<SystemPicture> allPicturesForPart = Sets.newHashSet(getAllPicturesForPart(partGuid));
		Set<SystemPicture> picturesRepoCollection = Sets.newHashSet(pictureRepo.findAll());
		return Sets.difference(picturesRepoCollection, allPicturesForPart);
	}	
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public String addPicturesToPartTreeNode(String[] pictureGuids, String partGuid) {
		SystemTree partNode = systemTreeRepo.findOne(partGuid);
		Date now = new Date();
		for(String pictureGuid : pictureGuids){
			SystemPicture picture = pictureRepo.findOne(pictureGuid);
			if(picture.getSystemTree() == null){
				SystemTree drawingTree = new SystemTree();
				drawingTree.setCreationDate(now);
				drawingTree.setTreeItemType(treeItemTypeRepo.getItemTypeByName("Picture"));
				drawingTree.setNomenclature(picture.getStoredFileName());
				drawingTree.setDescription(picture.getStoredFileName());
				picture.setSystemTree(drawingTree);
			}			
			picture.getSystemTree().setParent(partNode);
			pictureRepo.save(picture);
		}
		return "SUCCESS";
	}
	@Transactional(readOnly = true)
	public Collection<SystemPicture> getAllPictures() {
		return pictureRepo.findAll();
	}
	@Transactional(readOnly = true)
	public Collection<SystemPicture> getAllPictures(int pageIndex, int numberOfElements) {
		return pictureRepo.findAll(new PageRequest(pageIndex, numberOfElements)).getContent();
	}
	@Transactional(readOnly = true)
	public SystemPicture findBySystemTreeGuid(String pictureTreeGuid) {
		return pictureRepo.findByTreeGuid(systemTreeRepo.findOne(pictureTreeGuid));
	}
	@Transactional(readOnly = true)
	public Collection<? extends SystemPicture> getAllPictures(String searchTerm, int start, int length, int orderColumn, String direction) {
		if(!Strings.isNullOrEmpty(searchTerm))
			return pictureRepo.findAll(PicturePredicateSpec.fileNameIsLike(searchTerm), new PageRequest(start, length, Direction.fromStringOrNull(direction), orderColumn==0?"StoredFileName":"creationDate")).getContent();
		else
			return pictureRepo.findAll(new PageRequest(start, length, Direction.fromStringOrNull(direction), orderColumn==0?"StoredFileName":"creationDate")).getContent();
	}
	@Transactional(readOnly = true)
	public int getTotalNumberOfPictures(String searchTerm) {
		return Strings.isNullOrEmpty(searchTerm)?pictureRepo.getTotalPictureCount():pictureRepo.getTotalPictureCount(searchTerm);
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void deleteAllPictures(String[] selectedGuids) {
		for(String guid : selectedGuids)
			pictureRepo.delete(guid);
	}
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean renamePicture(String pictureGuid, String newName, String uploadDirectory) {
		SystemPicture picture = pictureRepo.findOne(pictureGuid);
		String storedFileName = picture.getStoredFileName();
		File storedFile = new File(uploadDirectory, storedFileName);
		String extension = FilenameUtils.getExtension(picture.getStoredFileName());
		picture.setUploadedFileName(newName.endsWith(extension)?newName:newName.concat(".").concat(extension));
		picture.setStoredFileName(newName.endsWith(extension)?newName:newName.concat(".").concat(extension));
		pictureRepo.save(picture);
		if(!storedFile.renameTo(new File(uploadDirectory, picture.getStoredFileName())))
			throw new RuntimeException("Rename failed!");
		return true;
	}	
}
