package com.sms.persistence.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//@Qualifier("springJdbcService")
@Service("springJdbcService")
@Transactional
public class SpringJDBCService{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Value("${organization.query}")
	private String organizationQuery;
	
	@Value("${agencyCompany.query}")
	private String agencyCompanyQuery;
	
	@Value("${commonCurrency.query}")
	private String commonCurrencyQuery;
	
	@Value("${contractCurrency.query}")
	private String contractCurrencyQuery;
	
	@Value("${country.query}")
	private String countryQuery;
	
	@Value("${ui.query}")
	private String uiQuery;
	
	@Value("${quoteCurrency.query}")
	private String quoteCurrencyQuery;
	
	@Value("${category.query}")
	private String categoryQuery;
	
	@Value("${contract.nomenclature.query}")
	private String contractNomenclatureQuery;
	
	@Value("${part.cagecode.query}")
	private String cageCodeQury;
	
	@Transactional(readOnly = true)
	public Collection<String> getContractNomenclatureList(){
		return jdbcTemplate.queryForList(contractNomenclatureQuery, String.class);
	}
	
	@Transactional(readOnly = true)
	public Collection<String> getCountryList(){
		return jdbcTemplate.queryForList(countryQuery, String.class);
	}
	
	@Transactional(readOnly = true)
	public Collection<String> getQuoteCurrencyList(){
		return jdbcTemplate.queryForList(quoteCurrencyQuery, String.class);
	}
	
	@Transactional(readOnly = true)
	public Collection<String> getUIList(){
		return jdbcTemplate.queryForList(uiQuery, String.class);
	}
	
	@Transactional(readOnly = true)
	public Collection<String> getCategoryList(){
		return jdbcTemplate.queryForList(categoryQuery, String.class);
	}
	
	@Transactional(readOnly = true)
	public Collection<String> getOrganizationList(){
		return jdbcTemplate.queryForList(organizationQuery, String.class);
	}
	
	@Transactional(readOnly = true)
	public Collection<String> getAgencyCompanyList(){
		return jdbcTemplate.queryForList(agencyCompanyQuery, String.class);
	}

	@Transactional(readOnly = true)
	public Collection<String> getCommonCurrencyList() {
		return jdbcTemplate.queryForList(commonCurrencyQuery, String.class);
	}

	@Transactional(readOnly = true)
	public Collection<String> getContractCurrencyList() {
		return jdbcTemplate.queryForList(contractCurrencyQuery, String.class);
	}
	
	@Transactional(readOnly = true)
	public Collection<String> getAllcageCodes(){
		return jdbcTemplate.queryForList(cageCodeQury, String.class);
	}

}
