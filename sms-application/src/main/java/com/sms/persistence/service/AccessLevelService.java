package com.sms.persistence.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sms.persistence.model.AccessLevel;
import com.sms.persistence.repo.AccessLevelRepo;

@Service
@Transactional
public class AccessLevelService {
	@Autowired
	private AccessLevelRepo accessLevelRepo;
	
	@Transactional(readOnly = true)
	public AccessLevel findAccessLevelByName(String accessLevelName){
		return accessLevelRepo.findByName(accessLevelName);
	}

	public Collection<AccessLevel> findAllAccessLevels() {
		return accessLevelRepo.findAll();
	}
}
