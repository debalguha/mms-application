package com.sms.persistence.service;


import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.sms.persistence.model.User;
import com.sms.persistence.repo.LocationRepo;
import com.sms.persistence.repo.UserRepository;

@Service
@Transactional
public class UserService {
	@Autowired
	private LocationRepo locationRepo;
	@Autowired
	private UserRepository userRepo;
	
	@Transactional(readOnly = true)
	public Collection<User> findAllUsers(){
		return userRepo.findAll();
	}
	@Transactional(readOnly = true)
	public User findByEmail(String email){
		return userRepo.findByEmail(email);
	}
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveUser(User user, String locationGuid) {
		user.setLocation(locationRepo.findOne(locationGuid));
		saveUser(user);
	}
	@Transactional(propagation = Propagation.REQUIRED)
	public User saveUser(User user){
		User userInDB = null;
		try {
			userInDB = Strings.isNullOrEmpty(user.getGuid())?findByEmail(user.getEmail()):findByGuid(user.getGuid());
		} catch (Exception e) {e.printStackTrace();}
		if(userInDB!=null){
			userInDB.cloneFrom(user);
			userRepo.save(userInDB);
			return userInDB;
		}else{
			user.setCreationDate(new Date());
			userRepo.save(user);
		}
		return user;
	}
	@Transactional(readOnly = true)
	public Collection<User> searchUserWithName(String name){
		return userRepo.findMatchingName(name);
	}
	
	@Transactional(readOnly = true)
	public User findByGuid(String userGuid) {
		return userRepo.findOne(userGuid);
	}

}
