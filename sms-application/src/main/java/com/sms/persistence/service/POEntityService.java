package com.sms.persistence.service;

import static ch.lambdaj.Lambda.on;
import static ch.lambdaj.Lambda.selectMax;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.POEntity;
import com.sms.persistence.model.POItem;
import com.sms.persistence.model.POType;
import com.sms.persistence.model.RFQEntity;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.repo.POEntityRepo;
import com.sms.persistence.repo.POItemRepo;
import com.sms.persistence.repo.POTypeRepo;
import com.sms.persistence.repo.RFQItemRepo;
import com.sms.persistence.repo.RFQRepo;
import com.sms.persistence.repo.RFQResponseItemRepo;
import com.sms.persistence.repo.RFQResponseRepo;
import com.sms.persistence.repo.RFQVendorRepo;
import com.sms.persistence.repo.RFQtypeRepo;
import com.sms.persistence.repo.SystemVendorRepo;
import com.sms.web.model.POEntityDTO;
import com.sms.web.model.POItemDTO;

@Service
@Transactional
public class POEntityService {
	@Autowired
	private POEntityRepo poEntityRepo;
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private RFQRepo rfqRepo;
	@Autowired
	private RFQItemRepo rfqItemRepo;
	@Autowired
	private RFQVendorRepo rfqVendorRepo;
	@Autowired
	private RFQResponseRepo rfqResponseRepo;
	@Autowired
	private RFQResponseItemRepo rfqResponseItemRepo;
	@Autowired
	private RFQtypeRepo rfqTypeRepo;
	@Autowired
	private SystemVendorRepo vendorRepo;	
	@Autowired
	private POItemRepo poItemrepo;
	@Autowired
	private POTypeRepo poTypeRepo;
	@Transactional(readOnly = true)
	public POEntity findPOEntityByGuid(String guid){
		return poEntityRepo.findOne(guid);
	}

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public POEntity createPOFromDTO(POEntityDTO poEntityDTO) {
		Date now = new Date();
		RFQEntity rfq = Strings.isNullOrEmpty(poEntityDTO.getRfqGuid())?null:rfqRepo.findOne(poEntityDTO.getRfqGuid()); 
		SystemVendor systemVendor = vendorRepo.findOne(poEntityDTO.getVendorGuid());
		POType poType = poTypeRepo.findOne(poEntityDTO.getPoType());
		POEntity poEntity = new POEntity();
		poEntity.setDeliveryByDate(poEntityDTO.getDeliveryByDate());
		poEntity.setDeliveryAddress(poEntityDTO.getDeliveryAddress());
		poEntity.setDeliveryInstruction(poEntityDTO.getDeliveryInstruction());
		poEntity.setIssueDate(poEntityDTO.getIssueDate());
		poEntity.setNotes(poEntityDTO.getNotes());
		poEntity.setPoNumber(poEntityDTO.getPoNumber());
		poEntity.setStatusDateCreated(now);
		poEntity.setVendor(systemVendor);
		poEntity.setRfq(rfq);
		poEntity.setPoType(poType);
		poEntity.setCreationDate(now);
		poEntity.setsAndH(poEntityDTO.getsAndH());
		poEntity.setTaxes(poEntityDTO.getTaxes());
		poEntity.setDuty(poEntityDTO.getDuty());
		poEntity.setDiscount(poEntityDTO.getDiscount());
		poEntity.setTotal(poEntityDTO.getTotal());
		for(POItemDTO poItemDTO : poEntityDTO.getPoItems()){
			POItem poItem = new POItem();
			poItem.setPo(poEntity);
			poItem.setCreationDate(now);
			poItem.setQuantity(poItemDTO.getQuantity());
			poItem.setUnitPrice(poItemDTO.getUnitPrice());
			poItem.setPo(poEntity);
			poItem.setSystemPart(systemService.getPartByGuid(poItemDTO.getGuid()));
			poItemrepo.save(poItem);
		}
		return poEntity;
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public POEntity editPOFromDTO(POEntityDTO poEntityDTO) {
		Date now = new Date();
		SystemVendor systemVendor = vendorRepo.findOne(poEntityDTO.getVendorGuid());
		POType poType = poTypeRepo.findOne(poEntityDTO.getPoType());
		POEntity poEntity = poEntityRepo.findOne(poEntityDTO.getPoGuid());
		poEntity.setDeliveryByDate(poEntityDTO.getDeliveryByDate());
		poEntity.setDeliveryAddress(poEntityDTO.getDeliveryAddress());
		poEntity.setDeliveryInstruction(poEntityDTO.getDeliveryInstruction());
		poEntity.setIssueDate(poEntityDTO.getIssueDate());
		poEntity.setNotes(poEntityDTO.getNotes());
		poEntity.setPoNumber(poEntityDTO.getPoNumber());
		poEntity.setVendor(systemVendor);
		poEntity.setsAndH(poEntityDTO.getsAndH());
		poEntity.setTaxes(poEntityDTO.getTaxes());
		poEntity.setDuty(poEntityDTO.getDuty());
		poEntity.setDiscount(poEntityDTO.getDiscount());
		poEntity.setTotal(poEntityDTO.getTotal());		
		if(!poType.equals(poEntity.getPoType()))
			poEntity.setStatusDateCreated(now);
		poEntity.setPoType(poType);
		for(POItemDTO poItemDTO : poEntityDTO.getPoItems()){
			POItem poItem = poItemrepo.findOne(poItemDTO.getGuid());
			poItem.setQuantity(poItemDTO.getQuantity());
			poItem.setUnitPrice(poItemDTO.getUnitPrice());
			poItem.setPo(poEntity);
			poItemrepo.save(poItem);
		}
		return poEntity;
	}
	
	@Transactional(readOnly = true)
	public Collection<POType> findAllPOTypes() {
		return poTypeRepo.findAll();
	}

	@Transactional(readOnly = true)
	public Collection<? extends POEntity> findAllPOs() {
		return poEntityRepo.findAll();
	}
	
	@Transactional(readOnly = true)
	public POEntity findLastPOForInventory(String inventoryGuid){
		Inventory inventory = inventoryService.findInventoryByGuid(inventoryGuid);
		Collection<POItem> poItems = poItemrepo.findBySystempart(inventory.getSystemPart());
		return poEntityRepo.findOne(((POItem)selectMax(poItems, on(POItem.class).getCreationDate())).getPo().getGuid());
	}
	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public POEntity deletePO(String guid) {
		POEntity po = findPOEntityByGuid(guid);
		poItemrepo.delete(poItemrepo.findByPOEntity(po));
		poEntityRepo.delete(po);
		return po;
	}

	@Transactional(readOnly = true)
	public Collection<POItem> findPOItemsBySystemart(SystemPart systemPart){
		return poItemrepo.findBySystempart(systemPart);
	}
}
