package com.sms.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NavigationController {
	@RequestMapping(value = "**/navigation", method = { RequestMethod.POST, RequestMethod.GET })
	public String navigate(@RequestParam(value = "nav-link", required = true) String navPage, HttpServletRequest request) {
		if (navPage.contains("#param")) {
			String param = navPage.substring(navPage.indexOf("#"));
			String navPageActual = navPage.substring(0, navPage.indexOf("#"));
			request.setAttribute("param", param.split("=")[1]);
			return navPageActual;
		}else
			return navPage;
	}
}
