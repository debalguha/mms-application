package com.sms.web;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Strings;
import com.sms.persistence.model.Customer;
import com.sms.persistence.service.CustomerService;
import com.sms.util.DTOUtils;
import com.sms.web.model.DataTableModel;

@Controller
public class CustomerController extends AbstractDateBindingController {
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "customers", method = RequestMethod.GET)
	public ModelAndView displayAllCustomers(){
		return new ModelAndView("customer/home");
	}
	
	@RequestMapping(value = "customers/all", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<Customer> getAllCustomers(){
		return DTOUtils.convertCustomersToDataTableModel(customerService.findAll());
	}
	
	@RequestMapping(value = "customer/save", method = RequestMethod.POST)
	public ModelAndView saveCustomer(@ModelAttribute Customer customer){
		customerService.createACustomer(customer);
		return new ModelAndView("customer/home");
	}
	
	@RequestMapping(value = "customer/edit", method = RequestMethod.POST)
	public ModelAndView editCustomer(@RequestParam(value = "customerGuid", required = false) String customerGuid){
		Customer customer = Strings.isNullOrEmpty(customerGuid)?new Customer():customerService.findCustomerByGuid(customerGuid);
		return new ModelAndView("customer/customer-details", "customer", customer);
	}
	
	@RequestMapping(value = "customers/customer", method = RequestMethod.GET)
	public @ResponseBody Collection<Customer> findCustomerByName(@RequestParam(value = "q[term]", required = true) String q){
		return customerService.searchCustomerByName(q);
	}
	
	@RequestMapping(value = "customers/customer/{guid}", method = RequestMethod.GET)
	public @ResponseBody Customer findCustomerByGuid(@PathVariable(value = "guid") String guid){
		return customerService.findCustomerByGuid(guid);
	}
	
	@RequestMapping(value = "customers/delete", method = RequestMethod.GET)
	public ModelAndView deleteCustomer(@RequestParam("selectedGuids") String selectedGuids){
		try {
			customerService.deleteAllCustomers(selectedGuids.split(","));
			return new ModelAndView("customer/customers", "statusMsg", "Customer(s) successfully deleted.");
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("customer/customers", "errorMsg", "Unable to delete customer(s).");
		}
	}
	
}
