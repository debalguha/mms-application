package com.sms.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sms.persistence.model.LineItem;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.service.LineItemService;
import com.sms.persistence.service.SystemService;

@Controller
public class LineItemController {
	@Autowired
	private LineItemService lineItemService;
	@Autowired
	private SystemService systemService;
	
	private static final Logger logger = LoggerFactory.getLogger(LineItemController.class);
	
	@RequestMapping(value = "system/data/lineitem/{lineItemTreeGuid}", method = RequestMethod.GET)
	public @ResponseBody LineItem findSystemContractByTreeGuid(@PathVariable("lineItemTreeGuid") String lineItemTreeGuid){
		LineItem lineItem = lineItemService.findBySystemTreeGuid(lineItemTreeGuid);
		lineItem.setCommonCurrentTargetPrice(lineItemService.calculateCommonCurrentTargetPrice(lineItem));
		return lineItem;
	}
	
	@RequestMapping(value = "system/lineitem/update", method = RequestMethod.POST)
	public @ResponseBody LineItem updateLineItem(@RequestBody LineItem lineItem){
		logger.info("Line item to update: "+lineItem);
		return lineItemService.createLineItem(lineItem);
	}
	
	@RequestMapping(value = "system/lineitem/create/{treeGuid}", method = RequestMethod.POST)
	public @ResponseBody LineItem createLineItemUnder(@PathVariable("treeGuid") String treeGuid, @RequestBody LineItem lineItem){
		logger.info("Line item to update: "+lineItem);
		SystemTree systemTree = systemService.findSystemTreeByGUID(treeGuid);
		systemTree.setNomenclature(lineItem.getNomenclature());
		lineItem.setSystemTree(systemTree);
		lineItem.setGuid(null);
		return lineItemService.createLineItem(lineItem);
	}
}
