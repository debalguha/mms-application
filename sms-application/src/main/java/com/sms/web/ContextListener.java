package com.sms.web;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sms.persistence.service.ContentService;
import com.sms.persistence.service.DataSettingsService;
import com.sms.persistence.service.LineItemService;
import com.sms.persistence.service.LocationService;
import com.sms.persistence.service.POEntityService;
import com.sms.persistence.service.RFQService;
import com.sms.persistence.service.SystemService;
import com.sms.persistence.service.SystemServiceDataService;
import com.sms.persistence.service.UserService;
import com.sms.persistence.service.VehicleService;
import com.sms.persistence.service.VendorService;
import com.sms.persistence.service.WorkOrderService;
import com.sms.util.SMSConstants;

public class ContextListener implements ServletContextListener{
	private static final Logger logger = LoggerFactory.getLogger(ContextListener.class);	
	public void contextInitialized(final ServletContextEvent sce) {
		new Thread(new Runnable(){
			public void run() {
				ApplicationContext ctx = null;
				do{
					ctx = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
					try {Thread.sleep(5000);} catch (InterruptedException e) {e.printStackTrace();}
					logger.debug("Not initialized yet!!");
				}while(ctx == null);
				sce.getServletContext().setAttribute(SMSConstants.LOCATIONS.name(), ctx.getBean(LocationService.class).findAllLocations());
				sce.getServletContext().setAttribute(SMSConstants.VENDOR_TYPES.name(), ctx.getBean(VendorService.class).findAllVendorTypes());
				sce.getServletContext().setAttribute(SMSConstants.RFQ_TYPES.name(), ctx.getBean(RFQService.class).findAllRFQTypes());
				sce.getServletContext().setAttribute(SMSConstants.VEHICLE_TYPES.name(), ctx.getBean(VehicleService.class).findAllVehicleTypes());
				sce.getServletContext().setAttribute(SMSConstants.PO_TYPES.name(), ctx.getBean(POEntityService.class).findAllPOTypes());
				sce.getServletContext().setAttribute(SMSConstants.SERVICE_DATA_TYPES.name(), ctx.getBean(SystemServiceDataService.class).findAllServiceDataType());
				sce.getServletContext().setAttribute(SMSConstants.LINE_ITEM_TYPES.name(), ctx.getBean(LineItemService.class).findAllLineItemType());
				sce.getServletContext().setAttribute(SMSConstants.WORKORDER_TYPES.name(), ctx.getBean(WorkOrderService.class).findAllWorkOrderTypes());
				sce.getServletContext().setAttribute(SMSConstants.VEHICLES.name(), ctx.getBean(VehicleService.class).findAllActiveVehicles());
				sce.getServletContext().setAttribute(SMSConstants.TREE_ITEM_TYPES.name(), ctx.getBean(SystemService.class).findAllTreeItemTypes());
				sce.getServletContext().setAttribute(SMSConstants.DATA_SETTING_FIELD_TYPES.name(), ctx.getBean(DataSettingsService.class).getAllFieldTypes());
				sce.getServletContext().setAttribute(SMSConstants.DATA_SETTINGS.name(), ctx.getBean(DataSettingsService.class).findAllDataSettings());
				sce.getServletContext().setAttribute(SMSConstants.USERS.name(), ctx.getBean(UserService.class).findAllUsers());
				sce.getServletContext().setAttribute(SMSConstants.CONTENT_TYPE.name(), ctx.getBean(ContentService.class).findAllContentTypes());
			}
		}).start();
	}

	public void contextDestroyed(ServletContextEvent sce) {
		
	}

}
