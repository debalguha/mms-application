package com.sms.web;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.InventoryItem;
import com.sms.persistence.model.RFQEntity;
import com.sms.persistence.model.RFQItem;
import com.sms.persistence.model.RFQResponse;
import com.sms.persistence.model.RFQVendor;
import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.service.InventoryService;
import com.sms.persistence.service.POEntityService;
import com.sms.persistence.service.RFQService;
import com.sms.util.DTOUtils;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.RFQObjectDTO;
import com.sms.web.model.RFQResponseDTO;

@Controller
public class RFQController {
	@Autowired
	private RFQService rfqService;
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private POEntityService poService;
	
	@RequestMapping(value = "rfq/issue", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView issueRFQ(@RequestParam String [] invSelect, @RequestParam("selectedGuids") String selectedInventoryGuids){
		Collection<Inventory> inventories = inventoryService.findInventories(selectedInventoryGuids.substring(0, selectedInventoryGuids.lastIndexOf(",")).split(","));
		Collection<SystemVendor> vendors = inventoryService.findvendorSuppliers(selectedInventoryGuids.substring(0, selectedInventoryGuids.lastIndexOf(",")).split(","));
		Map<String, Collection<?>> modelMap = Maps.newHashMapWithExpectedSize(2);
		modelMap.put("inventories", inventories);
		modelMap.put("vendors", vendors);
		return new ModelAndView("rfq/rfq_details", modelMap);
	}
	
	@RequestMapping(value = "rfq/create", method = RequestMethod.POST)
	public @ResponseBody RFQEntity createRFQ(@RequestBody RFQObjectDTO rfqDTO){
		return rfqService.createRFQ(rfqDTO);
	}
	
	@RequestMapping(value = "rfq/edit", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView editRFQView(@RequestParam("rfqGuid") String rfqGuid){
		RFQEntity rfqEntity = rfqService.findRFQEntityByGuid(rfqGuid);
		Map<RFQItem, Inventory> itemInventoryMap = Maps.newHashMap();
		for(RFQItem item : rfqEntity.getRfqItems())
			itemInventoryMap.put(item, inventoryService.findInventoryOfPart(item.getSystemPart()));
		Map<String, Object> modelMap = Maps.newHashMap();
		modelMap.put("rfq", rfqEntity);
		modelMap.put("rfqItemMap", itemInventoryMap);
		return new ModelAndView("rfq/rfq_edit", modelMap);
	}
	
	@RequestMapping(value = "rfq/list", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<RFQObjectDTO> listAllRFQs(){
		return new DataTableModel<RFQObjectDTO>(DTOUtils.convertRFQEntityToDTO(rfqService.findAllRFQs(), rfqService, poService));
	}
	
	@RequestMapping(value = "rfq/{rfqGuid}/response/vendor/{systemVendorGuid}", method = RequestMethod.GET)
	public @ResponseBody RFQResponse listRFQResponseForVendor(@PathVariable("rfqGuid") String rfqGuid, @PathVariable("systemVendorGuid") String systemVendorGuid){
		return rfqService.findRFQResponseBySystemVendor(systemVendorGuid, rfqGuid);
	}
	
	@RequestMapping(value = "rfq/{rfqGuid}/responseItem/vendor/{systemVendorGuid}", method = RequestMethod.GET)
	public @ResponseBody Collection<? extends InventoryItem> listRFQResponseItemsForVendor(@PathVariable("rfqGuid") String rfqGuid, @PathVariable("systemVendorGuid") String systemVendorGuid){
		Collection<? extends InventoryItem> itemsForVendor = null;
		try {
			itemsForVendor = rfqService.findRFQResponseItemsForVendor(rfqGuid, systemVendorGuid);
		} catch (Throwable e) {
			itemsForVendor = null;
		}
		if(CollectionUtils.isEmpty(itemsForVendor))
			itemsForVendor = rfqService.findRFQEntityByGuid(rfqGuid).getRfqItems();
		return itemsForVendor;
	}	
	
	@RequestMapping(value = "rfq/response/edit", method = RequestMethod.POST)
	public ModelAndView showResponse(@RequestParam(required = false, value="rfqResponseGuid") String rfqResponseGuid, 
			@RequestParam(required = true, value="rfqGuid") String rfqGuid){
		RFQResponse rfqResponse = null;
		RFQEntity rfqEntity = rfqService.findRFQEntityByGuid(rfqGuid);
		List<RFQVendor> rfqVendors = Lists.newArrayList(rfqEntity.getRfqVendors());
		Collections.sort(rfqVendors, new Comparator<RFQVendor>() {
			public int compare(RFQVendor o1, RFQVendor o2) {
				return o1.getGuid().compareTo(o2.getGuid());
			}
		});
		if(!Strings.isNullOrEmpty(rfqResponseGuid))
			rfqResponse = rfqService.findRFQResponseByGuid(rfqResponseGuid);
		else if(!CollectionUtils.isEmpty(rfqVendors))
			rfqResponse = rfqService.findRFQResponseByRFQVendor(rfqVendors.iterator().next().getGuid(), rfqGuid);
		Map<String, Object> modelMap = Maps.newHashMapWithExpectedSize(2);
		if(rfqResponse!=null)
			modelMap.put("rfqResponse", rfqResponse);
		modelMap.put("rfqEntity", rfqEntity);
		modelMap.put("rfqVendors", Lists.transform(rfqVendors, new Function<RFQVendor, SystemVendor>() {
			public SystemVendor apply(RFQVendor rfqVendor) {
				return rfqVendor.getSystemVendor();
			}
		}));
		return new ModelAndView("rfq/edit-response", modelMap);
	}
	
	@RequestMapping(value = "rfq/response/save", method = RequestMethod.POST)
	public @ResponseBody RFQResponse saveRFQResponse(@RequestBody RFQResponseDTO rfqResponseDTO){		
		return rfqService.saveRFQResponse(rfqResponseDTO);
	}

	@RequestMapping(value = "rfq/lookup/additional/vendors/{guid}", method = RequestMethod.GET)
	public @ResponseBody Collection<SystemVendor> getRemainingSystemVendorsForRFQ(@PathVariable("guid") String guid){
		return rfqService.findRemainingVendorsForRFQ(guid);
	}
}
