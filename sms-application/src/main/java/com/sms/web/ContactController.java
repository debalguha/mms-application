package com.sms.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Maps;
import com.sms.persistence.model.Contact;
import com.sms.persistence.service.ContactService;

@Controller
public class ContactController {
	@Autowired
	private ContactService contactService;
	
	@RequestMapping(value = "contact/get", method = RequestMethod.GET)
	public ModelAndView getContactDetails(@RequestParam("guid") String guid){
		return new ModelAndView("inventory/vendor/contact-edit", "contact", contactService.findContactByGuid(guid));
	}
	
	@RequestMapping(value = "contact/validate", method = RequestMethod.GET)
	public @ResponseBody boolean validateContactEmail(@RequestParam("email") String email, @RequestParam(value = "guid", required = false) String guid){
		return !contactService.doesEmailExists(guid, email);
	}
	
	@RequestMapping(value = "contact/save", method = RequestMethod.POST)
	public ModelAndView saveContactDetails(@ModelAttribute Contact contact){
		Map<String, Object> modelMap = Maps.newHashMap();
		try {
			contact = contactService.createContact(contact);
			modelMap.put("contact", contactService.createContact(contact));
			modelMap.put("statusMsg", "Contact successfully updated.");
		} catch (Exception e) {
			e.printStackTrace();
			modelMap.put("errMsg", "Unable to update contact:: "+e.getMessage());
		}
		
		
		return new ModelAndView("inventory/vendor/contact-edit", modelMap);
	}
}	
