package com.sms.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sms.persistence.model.SystemPart;
import com.sms.persistence.service.SystemService;
import com.sms.util.SMSConstants;

@Controller
public class PartController {
	private static final Logger logger = LoggerFactory.getLogger(PartController.class);
	@Autowired
	private SystemService service;
	
	@RequestMapping(value="parts/edit", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView editPart(@RequestParam(value = "partGuid", required = true) String partGuid){
		SystemPart part = service.getPartByGuid(partGuid);
		return new ModelAndView("system/part-edit", SMSConstants.PART_EDIT.name(), part);
	}
	
	@RequestMapping(value = "parts/part/update/save", method = RequestMethod.POST)
	public @ResponseBody SystemPart updatePart(@RequestBody SystemPart systemPart){
		logger.info("Input rcvd: "+systemPart);
		return service.updatePart(systemPart);
	}
}
