package com.sms.web;

import static com.sms.util.SMSConstants.USERBEAN;

import javax.servlet.http.HttpSession;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ch.lambdaj.Lambda;

import com.sms.persistence.model.User;
import com.sms.persistence.service.UserService;
import com.sms.util.EncryptionUtil;
import com.sms.util.SMSConstants;
import com.sms.web.model.UserModel;

@Controller
public class LoginController {
	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);
	@Autowired
	private UserService userService;
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(UserModel model, HttpSession session) {
		LOG.info("User logging in : "+model);
		User existingUser = null;
		try {
			existingUser = userService.findByEmail(model.getEmail());
		} catch (Exception e) {
			LOG.debug("Error while finding user with email", e);
		}
		if(existingUser == null)
			return new ModelAndView("login", "errMsg", "Invalid credentials provided!!");
		
		boolean isPasswordMatched = EncryptionUtil.checkPasswordMatches(existingUser.getPassword(), model.getPassword());
		if(isPasswordMatched){
			session.setAttribute(USERBEAN.name(), existingUser);
			if(existingUser.getAccessLevel().getAccessLevelName().equalsIgnoreCase("admin")){
				session.setAttribute(SMSConstants.USERS.name(), Lambda.filter(Lambda.having(Lambda.on(User.class).getEmail(), Matchers.not(Matchers.equalTo(existingUser.getEmail()))), userService.findAllUsers()));
				return new ModelAndView("admin/home");
			}else
				return new ModelAndView("user/home");
		}else
			return new ModelAndView("login", "errMsg", "Invalid credentials provided!!");
	}
}
