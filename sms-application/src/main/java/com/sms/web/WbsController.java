package com.sms.web;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Maps;
import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.WbsTree;
import com.sms.persistence.service.WbsService;
import com.sms.util.DTOUtils;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.TreeDTO;
import com.sms.web.model.TreeDTOForMove;
import com.sms.web.model.WbsDTO;

@Controller
public class WbsController {
	@Autowired
	private WbsService wbsService;

	@RequestMapping(value = "wbs/tree/view", method = RequestMethod.GET)
	public @ResponseBody Collection<TreeDTO> showWbsTree() throws Exception {
		return DTOUtils.convertTreesToDTOs(wbsService.getAllTopLevelWBSElements(), null);
	}

	@RequestMapping(value = "wbs/tree/node/{guid}", method = RequestMethod.GET)
	public @ResponseBody WbsTree showWbsTreeNode(@PathVariable("guid") String guid) throws Exception {
		return wbsService.getWbsTreeNode(guid);
	}

	@RequestMapping(value = "wbs/tree/move", method = RequestMethod.POST)
	public @ResponseBody TreeDTO moveWbsTreeNode(@ModelAttribute TreeDTOForMove wbsDto) throws Exception {
		return DTOUtils.convertTreeToDTO(wbsService.moveWbsTreeNode(wbsDto.getCurrentNodeGuid(), wbsDto.getNewParentGuid().equals("#") ? null : wbsDto.getNewParentGuid()), null);
	}

	@RequestMapping(value = "wbs/tree/paste", method = RequestMethod.POST)
	public @ResponseBody TreeDTO pasteWbsTreeNode(@ModelAttribute TreeDTOForMove wbsDto) throws Throwable {
		return DTOUtils.convertTreeToDTO(wbsService.copyWbsTreeNode(wbsDto.getCurrentNodeGuid(), wbsDto.getNewParentGuid().equals("#") ? null : wbsDto.getNewParentGuid()), null);
	}

	@RequestMapping(value = "wbs/tree/delete/{guid}", method = RequestMethod.POST)
	public @ResponseBody String deleteWbsTreeNode(@PathVariable("guid") String guid) throws Throwable {
		return wbsService.deleteWbsTreeNode(guid);
	}

	@RequestMapping(value = "wbs/tree/rename/{guid}/to/{newName}", method = RequestMethod.GET)
	public @ResponseBody WbsTree renameWbsTreeNode(@PathVariable("guid") String guid, @PathVariable("newName") String newName) throws Throwable {
		return wbsService.renameWbsTreeNode(guid, newName);
	}

	@RequestMapping(value = "wbs/tree/create", method = RequestMethod.POST)
	public @ResponseBody TreeDTO createWbsTreeNode(WbsDTO wbsDTO) throws Throwable {
		return DTOUtils.convertTreeToDTO(wbsService.createWbsTreeNode(wbsDTO), null);
	}

	@RequestMapping(value = "wbs/tree/picture/associate", method = RequestMethod.POST)
	public ModelAndView createWbsPictureTreeNode(@RequestParam("wbsGuid") String wbsGuid, @RequestParam("systemPictureGuid") String pictureGuid) throws Throwable {
		try {
			wbsService.associatePicture(wbsGuid, pictureGuid);
			return new ModelAndView("system/picture", "statusMsg", "Picture successfully associated.");
		} catch (Throwable e) {
			e.printStackTrace();
			return new ModelAndView("system/picture", "errorMsg", "Unable to associate Picture.");
		}
	}
	
	@RequestMapping(value = "wbs/tree/picture/association/remove", method = RequestMethod.POST)
	public ModelAndView removeWbsPictureTreeNode(@RequestParam("wbsGuidPictureRemove") String wbsGuid, @RequestParam("systemPictureGuidsRemove") String pictureGuid) throws Throwable {
		try {
			wbsService.disAssociatePicture(wbsGuid, pictureGuid);
			Map<String, String> modelMap = Maps.newHashMap();
			modelMap.put("chosenGuid", wbsGuid);
			modelMap.put("statusMsg", "Picture successfully removed.");
			return new ModelAndView("user/maintain-wbs", modelMap);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ModelAndView("user/maintain-wbs", "errorMsg", "Unable to remove Picture.");
		}
	}
	
	@RequestMapping(value = "wbs/tree/pictures/associate", method = RequestMethod.POST)
	public ModelAndView createWbsPicturesTreeNode(@RequestParam("wbsGuidPicture") String wbsGuid, @RequestParam("systemPictureGuids") String pictureGuids) throws Throwable {
		try{
			wbsService.associatePicture(wbsGuid, pictureGuids.split(","));
			Map<String, String> modelMap = Maps.newHashMap();
			modelMap.put("chosenGuid", wbsGuid);
			modelMap.put("statusMsg", "Drawings successfully associated.");
			return new ModelAndView("user/maintain-wbs", modelMap);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ModelAndView("user/maintain-wbs", "errorMsg", "Unable to associate Drawings.");
		}
	}	
	
	@RequestMapping(value = "wbs/tree/drawing/associate", method = RequestMethod.POST)
	public ModelAndView createWbsDrawingTreeNode(@RequestParam("wbsGuid") String wbsGuid, @RequestParam("systemDrawingGuid") String drawingGuid) throws Throwable {
		try {
			wbsService.associateDrawing(wbsGuid, drawingGuid);
			return new ModelAndView("system/drawing", "statusMsg", "Drawing successfully associated.");
		} catch (Throwable e) {
			e.printStackTrace();
			return new ModelAndView("system/drawing", "errorMsg", "Unable to associate drawing.");
		}
	}
	
	@RequestMapping(value = "wbs/tree/drawing/association/remove", method = RequestMethod.POST)
	public ModelAndView removeWbsDrawingTreeNode(@RequestParam("wbsGuidDrawingRemove") String wbsGuid, @RequestParam("systemDrawingGuidsRemove") String drawingGuid) throws Throwable {
		try {
			wbsService.disAssociateDrawing(wbsGuid, drawingGuid);
			Map<String, String> modelMap = Maps.newHashMap();
			modelMap.put("chosenGuid", wbsGuid);
			modelMap.put("statusMsg", "Drawings successfully removed.");
			return new ModelAndView("user/maintain-wbs", modelMap);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ModelAndView("user/maintain-wbs", "errorMsg", "Unable to remove drawing.");
		}
	}
	
	@RequestMapping(value = "wbs/tree/drawings/associate", method = RequestMethod.POST)
	public ModelAndView createWbsDrawingsTreeNode(@RequestParam("wbsGuidDrawing") String wbsGuid, @RequestParam("systemDrawingGuids") String drawingGuids) throws Throwable {
		try{
			wbsService.associateDrawing(wbsGuid, drawingGuids.split(","));
			Map<String, String> modelMap = Maps.newHashMap();
			modelMap.put("chosenGuid", wbsGuid);
			modelMap.put("statusMsg", "Drawings successfully associated.");
			return new ModelAndView("user/maintain-wbs", modelMap);
		} catch (Throwable e) {
			e.printStackTrace();
			return new ModelAndView("user/maintain-wbs", "errorMsg", "Unable to associate Drawings.");
		}
	}
	
	@RequestMapping(value = "wbs/tree/picture/{wbsGuid}", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SystemPicture> getAssociatedPictures(@PathVariable("wbsGuid") String wbsGuid) throws Throwable {
		return new DataTableModel<SystemPicture>(wbsService.getAllPicturesAssociated(wbsGuid));
	}
	
	@RequestMapping(value = "wbs/tree/drawing/{wbsGuid}", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SystemDrawing> getAssociatedDrawings(@PathVariable("wbsGuid") String wbsGuid) throws Throwable {
		return new DataTableModel<SystemDrawing>(wbsService.getAllDrawingsAssociated(wbsGuid));
	}
}
