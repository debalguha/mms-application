package com.sms.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sms.persistence.model.SystemServiceData;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.service.SystemService;
import com.sms.persistence.service.SystemServiceDataService;

@Controller
public class SystemServiceDataController {
	@Autowired
	private SystemServiceDataService service;
	@Autowired
	private SystemService systemService;
	
	private static final Logger logger = LoggerFactory.getLogger(SystemServiceDataController.class);
	
	@RequestMapping(value = "system/data/serviceData/{serviceDataTreeGuid}", method = RequestMethod.GET)
	public @ResponseBody SystemServiceData findSystemServiceByTreeGuid(@PathVariable("serviceDataTreeGuid") String serviceDataTreeGuid){
		SystemServiceData systemServiceData = service.findBySystemTreeGuid(serviceDataTreeGuid);
		systemServiceData.setCommonCurrentTargetPrice(service.calculateCommonCurrentTargetPrice(systemServiceData));
		return systemServiceData;
	}
	
	@RequestMapping(value = "system/serviceData/update", method = RequestMethod.POST)
	public @ResponseBody SystemServiceData updateServiceData(@RequestBody SystemServiceData systemServiceData){
		logger.info("Service data to update: "+systemServiceData);
		return service.createSystemService(systemServiceData);
	}
	
	@RequestMapping(value = "system/serviceData/create/{treeGuid}", method = RequestMethod.POST)
	public @ResponseBody SystemServiceData createServiceDataUnder(@PathVariable("treeGuid") String treeGuid, @RequestBody SystemServiceData systemServiceData){
		logger.info("Service data to update: "+systemServiceData);
		SystemTree systemTree = systemService.findSystemTreeByGUID(treeGuid);
		systemTree.setNomenclature(systemServiceData.getNomenclature());
		systemServiceData.setSystemTree(systemTree);
		systemServiceData.setGuid(null);
		return service.createSystemService(systemServiceData);
	}
}
