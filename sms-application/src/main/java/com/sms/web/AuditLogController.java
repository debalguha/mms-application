package com.sms.web;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sms.persistence.service.AuditLogService;

@Controller
public class AuditLogController {
	@Autowired
	private AuditLogService auditLogService;
	@RequestMapping(value = "audit.do", method = RequestMethod.GET)
	public ModelAndView retrieveAuditLog(@RequestParam(value = "startDate") Date startDate,
			@RequestParam(value = "endDate") Date endDate){
		return new ModelAndView("admin/audit-view", "auditLogs", auditLogService.findAuditLogWithinDateRange(startDate, endDate));
	}
}
