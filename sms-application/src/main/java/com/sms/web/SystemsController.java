package com.sms.web;

import java.text.SimpleDateFormat;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sms.persistence.model.LineItem;
import com.sms.persistence.model.SystemContract;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.service.DrawingService;
import com.sms.persistence.service.PictureService;
import com.sms.persistence.service.SystemService;
import com.sms.util.DTOUtils;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.TREE_NODE_TYPE;
import com.sms.web.model.TreeDTO;
import com.sms.web.model.TreeDTOForMove;

@Controller
public class SystemsController {
	@Autowired
	private SystemService systemService;
	@Autowired
	private DrawingService drawingService;
	@Autowired
	private PictureService pictureService;
	
	
	private static final ObjectMapper objectMapper = new ObjectMapper();
	
	private static final Logger logger = LoggerFactory.getLogger(SystemsController.class);
	
	@PostConstruct
	public void init(){
		objectMapper.setDateFormat(new SimpleDateFormat("MM/DD/YYYY"));
	}
	//system/search/itemType
	@RequestMapping(value = "system/search/itemType/{itemTypeGuid}", method = RequestMethod.GET)
	public @ResponseBody Collection<TreeDTO> searchForNomenclature(@PathVariable("itemTypeGuid") String itemTypeGuid, @RequestParam(value = "q[term]") String term) throws Exception{
		return DTOUtils.convertTreesToDTOs(systemService.findSystemTreeByItemTypeAndNomenclature(itemTypeGuid, term), null);
	}
	@RequestMapping(value = "system/tree/view", method = RequestMethod.GET)
	public @ResponseBody Collection<TreeDTO> showSystemTree(@RequestParam(value = "guid", required = false) String guid) throws Exception {
		return DTOUtils.groupChildOfNodes(DTOUtils.convertTreesToDTOs(systemService.findAllSystemTree(), guid));
	}
	@RequestMapping(value = "system/tree/searchByNomenclatureAndPart", method = RequestMethod.GET)
	public @ResponseBody Collection<SystemTree> findSystemTreeIdByNomenclatureAndPartNumber(@RequestParam(value = "nomenclature", required = false) String nomenclature,
			@RequestParam(value = "partNumber", required = false) String partNumber) throws Exception {
		return systemService.findByNomenclatureAndPartNumber(nomenclature, partNumber);
	}
	@RequestMapping(value = "system/tree/view/workorder", method = RequestMethod.GET)
	public @ResponseBody Collection<TreeDTO> showSystemTreeForWorkOrder(@RequestParam(value = "guid", required = false) String guid) throws Exception {
		return DTOUtils.groupChildOfNodes(DTOUtils.convertTreesToDTOs(systemService.findSystemTreeForWorkOrder(), guid));
	}
	
	
	@RequestMapping(value = "system/parts", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SystemPart> systemParts() throws Exception {
		return new DataTableModel<SystemPart>(systemService.findAllParts());
	}
	
	@RequestMapping(value = "system/data/part/{partGuid}", method = RequestMethod.GET)
	public @ResponseBody SystemPart getPartByGuid(@PathVariable("partGuid") String partGuid) throws Exception {
		SystemPart part = systemService.getPartByTreeGuid(partGuid);
		part.setCommonCurrentTargetPrice(systemService.calculateCommonCurrentTargetPrice(part));
		return part;
	}
	
	@RequestMapping(value = "system/part/update", method = RequestMethod.POST)
	public @ResponseBody SystemPart updatePartWithGuid(@RequestBody SystemPart systemPart){
		logger.info("Input rcvd: "+systemPart);
		return systemService.updatePart(systemPart);
	}
	
	@RequestMapping(value = "system/part/create/{treeGuid}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody SystemPart createPartUnder(@PathVariable("treeGuid") String guid, @RequestBody SystemPart systemPart){
		logger.info("Input rcvd: "+systemPart);
		return systemService.getPartByTreeGuid(systemService.createPartUnder(systemPart, systemService.findSystemTreeByGUID(guid)).getGuid());
	}
	
	@RequestMapping(value = "system/part/{partGuid}/drawing/add", method = RequestMethod.POST)
	public @ResponseBody String addDrawingsToPart(@RequestParam("drwaingGuids") String[] drawingGuids, @PathVariable("partGuid") String partGuid){
		logger.info("Input rcvd: partGuid["+partGuid+"], drawingGuids["+drawingGuids+"]");
		return drawingService.addDrawingsToPartTreeNode(drawingGuids, partGuid);
	}
	
	@RequestMapping(value = "system/part/{partGuid}/picture/add", method = RequestMethod.POST)
	public @ResponseBody String addPicturesToPart(@RequestParam("pictureGuid") String[] picturesGuid, @PathVariable("partGuid") String partGuid){
		logger.info("Input rcvd: partGuid["+partGuid+"], drawingGuids["+picturesGuid+"]");
		return pictureService.addPicturesToPartTreeNode(picturesGuid, partGuid);
	}	

	@RequestMapping(value = "system/tree/delete/{guid}", method = RequestMethod.POST)
	public @ResponseBody String deleteSystemTreeNode(@PathVariable("guid") String guid){
		return systemService.deleteTreeNodeByGuid(guid);
	}
	
	@RequestMapping(value = "system/tree/move", method = RequestMethod.POST)
	public @ResponseBody TreeDTO moveSystemTreeNode(@ModelAttribute TreeDTOForMove treeDTOForMove) throws Exception {
		return DTOUtils.convertTreeToDTO(systemService.moveSystemTreeNode(treeDTOForMove.getCurrentNodeGuid(), treeDTOForMove.getNewParentGuid().equals("#") ? null : treeDTOForMove.getNewParentGuid()), null);
	}
	
	@RequestMapping(value = "system/tree/paste", method = RequestMethod.POST)
	public @ResponseBody TreeDTO pasteSystemTreeNode(@ModelAttribute TreeDTOForMove treeDTOForMove) throws Throwable {
		return DTOUtils.groupChildOfPart(DTOUtils.convertTreeToDTO(systemService.findSystemTreeByGUID(((SystemTree)systemService.copySystemTreeNode(treeDTOForMove.getCurrentNodeGuid(), treeDTOForMove.getNewParentGuid())).getGuid()), null));
	}
	
	@RequestMapping(value = "system/tree/addComponent/{guid}", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody TreeDTO addSystemTreeNode(@PathVariable("guid") String guid, @RequestBody JsonNode jsonNode) throws Throwable {
		logger.info("Json data:: "+jsonNode);
		TREE_NODE_TYPE node_TYPE = TREE_NODE_TYPE.valueOf(((ObjectNode)jsonNode).remove("VIEWID").asText());
		switch(node_TYPE){
		case CONTRACT:
			return addContract(guid, jsonNode);
		case LINEITEM:
			return addLineItem(guid, jsonNode);
		case PART:
			return addPart(guid, jsonNode);
		case SERVICE:
			return addService(guid, jsonNode);
		default:
			break;
		
		}
		return null;//systemService.copySystemTreeNode(treeDTOForMove.getCurrentNodeGuid(), treeDTOForMove.getNewParentGuid());
	}

	private TreeDTO addService(String guid, JsonNode jsonNode) throws Exception {
		SystemTree parentNode =guid.equals("null")?null: systemService.findSystemTreeByGUID(guid);
		com.sms.persistence.model.SystemServiceData systemServiceData = objectMapper.readValue(jsonNode.toString(), com.sms.persistence.model.SystemServiceData.class);
		return DTOUtils.convertTreeToDTO(systemService.createSystemServiceDataUnder(systemServiceData, parentNode), null);
	}

	private TreeDTO addPart(String guid, JsonNode jsonNode) throws Exception {
		SystemTree parentNode = guid.equals("null")?null:systemService.findSystemTreeByGUID(guid);
		SystemPart systemPart = objectMapper.readValue(jsonNode.toString(), SystemPart.class);
		return DTOUtils.convertTreeToDTO(systemService.createPartUnder(systemPart, parentNode), null);
	}

	private TreeDTO addLineItem(String guid, JsonNode jsonNode) throws Exception {
		SystemTree parentNode = guid.equals("null")?null:systemService.findSystemTreeByGUID(guid);
		LineItem lineItem = objectMapper.readValue(jsonNode.toString(), LineItem.class);
		return DTOUtils.convertTreeToDTO(systemService.createLineItemUnder(lineItem, parentNode), null);
	}

	private TreeDTO addContract(String guid, JsonNode jsonNode) throws Exception {
		SystemTree parentNode = guid.equals("null")?null:systemService.findSystemTreeByGUID(guid);
		SystemContract contract = objectMapper.readValue(jsonNode.toString(), SystemContract.class);
		return DTOUtils.convertTreeToDTO(systemService.createContractUnder(contract, parentNode), null);
	}
	
	@RequestMapping(value = "system/tree/{nodeType}/rename/{guid}/to/{newName}", method = RequestMethod.GET)
	public @ResponseBody Object moveSystemTreeNode(@PathVariable("guid") String systemTreeGuid, @PathVariable("nodeType") String nodeType,
			@PathVariable("newName") String newName) throws Exception {
		/*TREE_NODE_TYPE node_TYPE = TREE_NODE_TYPE.valueOf(nodeType);
		switch(node_TYPE){
		case CONTRACT:
			return contractService.renameContract(systemTreeGuid, newName);
		case LINEITEM:
			return lineItemService.renameLineItem(systemTreeGuid, newName);
		case PART:
			return systemService.renamePart(systemTreeGuid, newName);
		case SERVICE:
			return systemServiceDataService.renameSystemServiceData(systemTreeGuid, newName);
		default:
			break;
		}*/
		return systemService.renameTreeNode(systemTreeGuid, newName);
	}
}
