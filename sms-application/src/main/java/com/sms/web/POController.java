package com.sms.web;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sms.persistence.model.POEntity;
import com.sms.persistence.model.RFQEntity;
import com.sms.persistence.model.RFQVendor;
import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.service.POEntityService;
import com.sms.persistence.service.RFQService;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.POEntityDTO;

@Controller
public class POController extends AbstractDateBindingController {
	@Autowired
	private POEntityService poService;
	@Autowired
	private RFQService rfqService;
	
	@RequestMapping(value = "po/rfq/issuePO", method = RequestMethod.POST)
	private ModelAndView issuePO(@RequestParam("rfqGuid") String rfqGuid){
		RFQEntity rfqEntity = rfqService.findRFQEntityByGuid(rfqGuid);
		List<RFQVendor> rfqVendors = Lists.newArrayList(rfqEntity.getRfqVendors());
		Collections.sort(rfqVendors, new Comparator<RFQVendor>() {
			public int compare(RFQVendor o1, RFQVendor o2) {
				return o1.getGuid().compareTo(o2.getGuid());
			}
		});
		Map<String, Object> modelMap = Maps.newHashMapWithExpectedSize(2);
		modelMap.put("rfqEntity", rfqEntity);
		modelMap.put("rfqVendors", Lists.transform(rfqVendors, new Function<RFQVendor, SystemVendor>() {
			public SystemVendor apply(RFQVendor rfqVendor) {
				return rfqVendor.getSystemVendor();
			}
		}));
		return new ModelAndView("po/issuePOFromRFQ", modelMap);
	}
	@RequestMapping(value = "po/save/fromRFQ", method = RequestMethod.POST)
	private @ResponseBody POEntity createPOFromRFQ(@RequestBody POEntityDTO poEntityDTO){
		return poService.createPOFromDTO(poEntityDTO);
	}
	
	@RequestMapping(value = "po/save/fromInventory", method = RequestMethod.POST)
	private @ResponseBody POEntity createPOFromInventory(@RequestBody POEntityDTO poEntityDTO){
		return poService.createPOFromDTO(poEntityDTO);
	}	
	
	@RequestMapping(value = "po/save/fromEdit", method = RequestMethod.POST)
	private @ResponseBody POEntity savePOFromEdit(@RequestBody POEntityDTO poEntityDTO){
		return poService.editPOFromDTO(poEntityDTO);
	}

	@RequestMapping(value = "po/list", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<POEntity> listAllPOs(){
		return new DataTableModel<POEntity>(poService.findAllPOs());
	}

	@RequestMapping(value = "po/edit/{guid}", method = RequestMethod.POST)
	public ModelAndView editPO(@PathVariable String guid){
		return new ModelAndView("po/edit-po", "po", poService.findPOEntityByGuid(guid));
	}
	
	@RequestMapping(value = "po/edit/inventory", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView editPOForInventory(@RequestParam("inventoryGuidForPO") String inventoryGuidForPO){
		return new ModelAndView("po/edit-po", "po", poService.findLastPOForInventory(inventoryGuidForPO));
	}
	
	@RequestMapping(value = "po/edit/success/{guid}", method = RequestMethod.POST)
	public ModelAndView editPOSuccess(@PathVariable String guid){
		return new ModelAndView("po/po-home", "statusMsg", poService.findPOEntityByGuid(guid).getPoNumber()+" edited successfully.");
	}
	
	@RequestMapping(value = "po/delete", method = RequestMethod.POST)
	public ModelAndView deletePO(@RequestParam("poGuid") String guid){
		return new ModelAndView("po/po-home", "statusMsg", poService.deletePO(guid).getPoNumber()+" deleted successfully.");
	}
	
	@RequestMapping(value = "po/duplicate", method = RequestMethod.POST)
	public @ResponseBody POEntity duplicatePO(@RequestBody POEntityDTO poEntityDTO){
		return poService.createPOFromDTO(poEntityDTO);
	}
}
