package com.sms.web;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sms.persistence.model.Contact;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemPartVendor;
import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.service.InventoryService;
import com.sms.persistence.service.SystemService;
import com.sms.persistence.service.VendorService;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.VendorDTO;

@Controller
public class VendorController {
	@Autowired
	private VendorService vendorService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private InventoryService inventoryService;
	@RequestMapping(value = "system/part/{guid}/vendors", method = RequestMethod.GET)
	public @ResponseBody Collection<SystemVendor> findSystemVendorFromPartTreeGuid(@PathVariable("guid") String guid){
		SystemPart systemPart = systemService.getPartByTreeGuid(guid);
		return vendorService.findVendorsOfPart(systemPart);
	}
	
	@RequestMapping(value = "system/part/{guid}/vendors/not", method = RequestMethod.GET)
	public @ResponseBody Collection<SystemVendor> findSystemVendorNotAssociatedWithPartFromPartTreeGuid(@PathVariable("guid") String guid){
		SystemPart systemPart = systemService.getPartByTreeGuid(guid);
		return vendorService.findVendorsNotOfPart(systemPart);
	}
	
	@RequestMapping(value = "system/part/{guid}/vendors/add", method = RequestMethod.POST)
	public @ResponseBody Collection<SystemPartVendor> associateSystemVendorWithPart(@PathVariable("guid") String guid, @RequestParam(value="allVendors") String []vendorGuids){
		return vendorService.associateVendorsWithPart(guid, vendorGuids);
	}
	
	@RequestMapping(value = "system/part/{guid}/vendors/remove", method = RequestMethod.POST)
	public @ResponseBody Boolean disassociateSystemVendorWithPart(@PathVariable("guid") String guid, @RequestParam(value="vendors") String []vendorGuids){
		return vendorService.disassociateVendorsWithPart(guid, vendorGuids);
	}
	@RequestMapping(value = "vendors/all", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SystemVendor> allVendors(){
		return new DataTableModel<SystemVendor>(vendorService.findAllVendors());
	}
	@RequestMapping(value = "vendors/vendor", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView vendorDetail(@RequestParam("guid") String guid){
		Map<String, Object> modelMap = Maps.newHashMap();
		modelMap.put("vendor", vendorService.findVendorByGuid(guid));
		modelMap.put("contacts", vendorService.findVendorContacts(guid));
		return new ModelAndView("inventory/vendor/vendor-details", modelMap);
	}
	
	@RequestMapping(value = "vendor/add", method = RequestMethod.GET)
	public ModelAndView newVendor(){
		return new ModelAndView("inventory/vendor/vendor-details", "vendor", new SystemVendor());
	}
	
	@RequestMapping(value = "vendor/contact", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<Contact> getContacts(@RequestParam(value = "guid", required = false) String guid){
		List<Contact> emptyList = Lists.newArrayList();
		return Strings.isNullOrEmpty(guid)?new DataTableModel<Contact>(emptyList):new DataTableModel<Contact>(vendorService.findVendorContacts(guid));
	}
	
	@RequestMapping(value = "vendor/save", method = RequestMethod.POST)
	public @ResponseBody Boolean saveVendor(@RequestBody VendorDTO vendorDTO){
		try {
			vendorService.createVendor(vendorDTO);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@RequestMapping(value = "vendors/inventory/{inventoryGuid}", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SystemVendor> inventorySupplyingVendords(@PathVariable("inventoryGuid") String inventoryGuid){
		return new DataTableModel<SystemVendor>(vendorService.findInventorySupplyingVendors(inventoryGuid));
	}
}
