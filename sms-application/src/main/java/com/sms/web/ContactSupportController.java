package com.sms.web;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.sms.persistence.model.EmailLog;
import com.sms.persistence.model.SupportEmail;
import com.sms.persistence.model.User;
import com.sms.persistence.service.ContactSupportService;
import com.sms.util.SMSConstants;

@Controller
public class ContactSupportController {
	@Autowired
	private ContactSupportService supportService;
	@Autowired
	private JavaMailSender mailSender;
	
	@RequestMapping(value = "support/email", method = RequestMethod.POST)
	public ModelAndView contactSupport(@RequestParam("subject") String subject, @RequestParam("occurredDate") Date occurredDate, 
			@RequestParam("description") String description, @RequestParam("severity") int severity, HttpSession session){
		Collection<EmailLog> emailLogs = Lists.newArrayList();
		for(SupportEmail supportEmail : supportService.findAllSupportEmails())
			emailLogs.add(new EmailLog(occurredDate, ((User)session.getAttribute(SMSConstants.USERBEAN.name())).getEmail(), supportEmail.getEmail(), subject, description, "PENDING"));
		supportService.createEmailLog(emailLogs);
		return new ModelAndView("user/contact-support", "statusMsg", "Email has been sent to support persons.");
	}
}	
