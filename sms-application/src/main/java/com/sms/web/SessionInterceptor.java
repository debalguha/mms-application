package com.sms.web;

import static com.sms.util.SMSConstants.USERBEAN;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.sms.persistence.model.User;
import com.sms.util.SMSConstants;

public class SessionInterceptor implements HandlerInterceptor{
	private static final Logger LOG = LoggerFactory.getLogger(SessionInterceptor.class);
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		boolean isLoggedIn = request.getSession(true).getAttribute(USERBEAN.name()) != null;
		if(!isLoggedIn)
			LOG.info("No logged in user found!!");
		else
			LOG.info("Logged in as "+((User)request.getSession(true).getAttribute(USERBEAN.name())).getEmail());
		request.getSession().setAttribute(SMSConstants.REQ_URL.name(), request.getRequestURI());
		if(!isLoggedIn){
			if(!"XMLHttpRequest".equals(request.getHeader("X-Requested-With"))){
				request.setAttribute("errMsg", "Your session has expired. Please login again.");
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}else{
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	            response.setHeader("sessionFailed", "TRUE");
	            return false;
			}
		}
		return isLoggedIn;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}

}
