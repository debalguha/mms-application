package com.sms.web;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.sms.persistence.model.ServiceDataType;
import com.sms.persistence.service.SystemServiceDataService;

public class ServiceDataTypeDeserializer extends JsonDeserializer<ServiceDataType>{
	private SystemServiceDataService systemServiceDataService;
	@Override
	public ServiceDataType deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
		return systemServiceDataService.findServiceDataTypeByType(node.get("serviceDataType").asText());
	}
	public void setSystemServiceDataService(SystemServiceDataService systemServiceDataService) {
		this.systemServiceDataService = systemServiceDataService;
	}
}
