package com.sms.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sms.persistence.model.SystemContract;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.service.ContractService;
import com.sms.persistence.service.SystemService;

@Controller
public class ContractController extends AbstractDateBindingController{
	@Autowired
	private ContractService contractService;
	@Autowired
	private SystemService systemService;
	
	private static final Logger logger = LoggerFactory.getLogger(ContractController.class);
	
	@RequestMapping(value = "system/data/contract/{contractTreeGuid}", method = RequestMethod.GET)
	public @ResponseBody SystemContract findSystemContractByTreeGuid(@PathVariable("contractTreeGuid") String contractTreeGuid){
		SystemContract contract = contractService.findBySystemTreeGuid(contractTreeGuid);
		//contract.setCommonCurrentTargetPrice(contractService.calculateCommonCurrentTargetPrice(contract));
		return contract;
	}
	
	@RequestMapping(value = "system/contract/update", method = RequestMethod.POST)
	public @ResponseBody SystemContract updateContract(@ModelAttribute SystemContract contract){
		logger.info("Contract to update: "+contract);
		return contractService.createContract(contract);
	}
	
	@RequestMapping(value = "system/contract/create/{treeGuid}", method = RequestMethod.POST)
	public @ResponseBody SystemContract createContractUnder(@PathVariable("treeGuid") String treeGuid, @ModelAttribute SystemContract contract){
		logger.info("Contract to update: "+contract);
		SystemTree systemTree = systemService.findSystemTreeByGUID(treeGuid);
		systemTree.setNomenclature(contract.getNomenclature());
		contract.setSystemTree(systemTree);
		contract.setGuid(null);
		return contractService.createContract(contract);
	}
}
