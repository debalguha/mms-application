package com.sms.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class CacheControlPostFilter implements Filter {

	public void init(FilterConfig filterConfig) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		chain.doFilter(request, response);
		if (response instanceof HttpServletResponse) {
			((HttpServletResponse)response).setHeader("Expires", "-1");
			((HttpServletResponse)response).setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			((HttpServletResponse)response).addHeader("Cache-Control", "post-check=0, pre-check=0");
			((HttpServletResponse)response).setHeader("Pragma", "no-cache");
		}
	}

	public void destroy() {
		// TODO Auto-generated method stub

	}

}
