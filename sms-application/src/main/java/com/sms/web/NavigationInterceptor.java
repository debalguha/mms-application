package com.sms.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sms.persistence.service.ContentService;
import com.sms.persistence.service.CustomerService;
import com.sms.persistence.service.LineItemService;
import com.sms.persistence.service.SpringJDBCService;
import com.sms.persistence.service.SystemPartHelperService;
import com.sms.persistence.service.SystemServiceDataService;
import com.sms.util.SMSConstants;

public class NavigationInterceptor extends HandlerInterceptorAdapter {
	private CustomerService customerService;
	private SpringJDBCService springJdbcService;
	private ContentService contentService;
	private SystemServiceDataService systemServiceDataService;
	private LineItemService lineItemService;
	private SystemPartHelperService partHelperService;
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		if(request.getParameter("nav-link")!=null && (request.getParameter("nav-link").endsWith("tree-view") || request.getParameter("nav-link").endsWith("workorder-create")
				|| request.getParameter("nav-link").contains("workorder")))
			request.getServletContext().setAttribute(SMSConstants.CUSTOMERS.name(), customerService.findAll());
		if((request.getRequestURL().toString().contains("customer") || (request.getParameter("nav-link")!=null && request.getParameter("nav-link").contains("customer")))
				|| (request.getParameter("nav-link")!=null && request.getParameter("nav-link").endsWith("tree-view"))){
			request.setAttribute(SMSConstants.AGENCY_COMPANY.name(), springJdbcService.getAgencyCompanyList());
			request.setAttribute(SMSConstants.COMMON_CURRENCY.name(), springJdbcService.getCommonCurrencyList());
			request.setAttribute(SMSConstants.CONTRACT_CURRENCY.name(), springJdbcService.getContractCurrencyList());
			request.setAttribute(SMSConstants.ORGANIZATIONS.name(), springJdbcService.getOrganizationList());
			request.setAttribute(SMSConstants.COUNTRY.name(), springJdbcService.getCountryList());
		}
		if(request.getParameter("nav-link")!=null && (request.getParameter("nav-link").endsWith("view-content"))){
			request.setAttribute(SMSConstants.CONTENT_SETTINGS.name(), contentService.getContentSettingsMap());
		}
		if((request.getRequestURL().toString().contains("inventory") || (request.getParameter("nav-link")!=null && request.getParameter("nav-link").contains("inventory")))
				|| (request.getParameter("nav-link")!=null && request.getParameter("nav-link").endsWith("tree-view"))
				|| request.getRequestURL().toString().contains("parts/edit")){
			request.setAttribute(SMSConstants.QUOTE_CURRENCY.name(), springJdbcService.getQuoteCurrencyList());
			request.setAttribute(SMSConstants.UI.name(), springJdbcService.getUIList());
			request.setAttribute(SMSConstants.PART_CATEGORY.name(), springJdbcService.getCategoryList());
			request.setAttribute(SMSConstants.PART_PROCESS.name(), partHelperService.findAllProcesses());
			request.setAttribute(SMSConstants.CAGE_CODE.name(), springJdbcService.getAllcageCodes());
			request.setAttribute(SMSConstants.PART_MATERIAL_TYPE.name(), partHelperService.findAllMaterialTypes());
			request.setAttribute(SMSConstants.PART_MATERIAL_NAME.name(), partHelperService.findAllMaterialNames());
			request.setAttribute(SMSConstants.PART_MATERIAL_SPEC.name(), partHelperService.findAllMaterialSpecs());			
		}
		if(request.getParameter("nav-link")!=null && (request.getParameter("nav-link").endsWith("tree-view"))){
			request.setAttribute(SMSConstants.SERVICE_DATA_TYPES.name(), systemServiceDataService.findAllServiceDataType());
			request.setAttribute(SMSConstants.LINE_ITEM_TYPES.name(), lineItemService.findAllLineItemType());
		}
		if((request.getRequestURL().toString().contains("customer") || (request.getParameter("nav-link")!=null && request.getParameter("nav-link").contains("customer"))))
			request.setAttribute(SMSConstants.CONTRACT_NOMENCLATURE.name(), springJdbcService.getContractNomenclatureList());
		return super.preHandle(request, response, handler);
	}
	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	public void setSpringJdbcService(SpringJDBCService springJdbcService) {
		this.springJdbcService = springJdbcService;
	}
	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}
	public void setSystemServiceDataService(SystemServiceDataService systemServiceDataService) {
		this.systemServiceDataService = systemServiceDataService;
	}
	public void setLineItemService(LineItemService lineItemService) {
		this.lineItemService = lineItemService;
	}
	public void setPartHelperService(SystemPartHelperService partHelperService) {
		this.partHelperService = partHelperService;
	}
}
