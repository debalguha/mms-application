package com.sms.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sms.persistence.model.SyncSetting;
import com.sms.persistence.service.SyncSettingService;
import com.sms.web.model.DataTableModel;

@Controller
public class SyncSettingController {
	@Autowired
	private SyncSettingService syncSettingService;
	
	@RequestMapping(value = "sync/all", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SyncSetting> getAllSyncSetting(){
		return new DataTableModel<SyncSetting>(syncSettingService.findAllSyncSettings());
	}
}