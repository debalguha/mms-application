package com.sms.web;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.sms.persistence.model.Vehicle;
import com.sms.persistence.service.CustomerService;
import com.sms.persistence.service.VehicleService;
import com.sms.util.SMSConstants;
import com.sms.web.model.DataTableModel;

@Controller
public class VehicleController {
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "vehicle/search/customer/{customerGuid}", method = RequestMethod.GET)
	public @ResponseBody Collection<Vehicle> searchVehicles(@PathVariable(value = "customerGuid") String customerGuid){
		return vehicleService.searchVehiclesForCustomer(customerGuid);
	}
	@RequestMapping(value = "vehicle/all", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<Vehicle> getAllVehcles(){
		return new DataTableModel<Vehicle>(vehicleService.findAllActiveVehicles());
	}
	@RequestMapping(value="vehicle/edit", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView editVehicle(@RequestParam(value = "vehicleGuid", required = false) String vehicleGuid, HttpServletRequest request){
		request.getServletContext().setAttribute(SMSConstants.CUSTOMERS.name(), customerService.findAll());
		List<Vehicle> allVehicles = Lists.newArrayList(vehicleService.findAllActiveVehicles());
		Vehicle vehicle = Strings.isNullOrEmpty(vehicleGuid)?null:vehicleService.findVehicleByGuid(vehicleGuid) ;
		if(vehicle!=null){
			allVehicles.remove(vehicle);
		}
		request.getServletContext().setAttribute(SMSConstants.VEHICLES.name(), allVehicles);
		return vehicle==null?new ModelAndView("vehicle/edit-vehicle"):new ModelAndView("vehicle/edit-vehicle", "vehicle", vehicle);
	}
	@RequestMapping(value="vehicle/save", method = RequestMethod.POST)
	public ModelAndView createVehicle(@ModelAttribute Vehicle vehicle, HttpServletRequest request){
		if(!Strings.isNullOrEmpty(request.getParameter("vehicleDelete")) && request.getParameter("vehicleDelete").equals("delete"))
			return deleteVehicle(vehicle);
		vehicle = vehicleService.createVehicle(vehicle);
		List<Vehicle> allVehicles = Lists.newArrayList(vehicleService.findAllVehicles());
		allVehicles.remove(vehicle);
		request.getServletContext().setAttribute(SMSConstants.VEHICLES.name(), allVehicles);
		return new ModelAndView("vehicle/vehicles");
	}
	private ModelAndView deleteVehicle(Vehicle vehicle) {
		vehicle.setActive(0);
		vehicleService.createVehicle(vehicle);
		return new ModelAndView("vehicle/vehicles");
	}
	
	@RequestMapping(value="vehicles/for/nomenclature", method = RequestMethod.GET)
	public @ResponseBody int countVehicleForSystemTreeNode(@RequestParam(value = "value") String nomenclature){
		return vehicleService.findNumberOfActiveVehiclesForNomenclature(nomenclature);
	}
	
	@RequestMapping(value="vehicles/with/nomenclature", method = RequestMethod.GET)
	public @ResponseBody Collection<Vehicle> findVehicleForSystemTreeNode(@RequestParam(value = "value") String nomenclature){
		return vehicleService.findActiveVehiclesWithNomenclature(nomenclature);
	}
}
