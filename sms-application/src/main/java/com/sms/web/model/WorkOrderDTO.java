package com.sms.web.model;

import java.util.Collection;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class WorkOrderDTO {
	private String guid;
	private String userGuid;
	private String techGuid;
	private String supervisorGuid;
	private String customerGuid;
	private String locationGuid;
	private String workOrderTypeGuid;
	private String vehicleGuid;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date scheduledDate;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date promisedDate;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date completedDate;
	private int priority;
	
	private Collection<WorkOrderItemDTO> workOrderItems;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getUserGuid() {
		return userGuid;
	}

	public void setUserGuid(String userGuid) {
		this.userGuid = userGuid;
	}

	public String getTechGuid() {
		return techGuid;
	}

	public void setTechGuid(String techGuid) {
		this.techGuid = techGuid;
	}

	public String getSupervisorGuid() {
		return supervisorGuid;
	}

	public void setSupervisorGuid(String supervisorGuid) {
		this.supervisorGuid = supervisorGuid;
	}

	public String getCustomerGuid() {
		return customerGuid;
	}

	public void setCustomerGuid(String customerguid) {
		this.customerGuid = customerguid;
	}

	public String getLocationGuid() {
		return locationGuid;
	}

	public void setLocationGuid(String locationGuid) {
		this.locationGuid = locationGuid;
	}

	public String getWorkOrderTypeGuid() {
		return workOrderTypeGuid;
	}

	public void setWorkOrderTypeGuid(String workOrderTypeGuid) {
		this.workOrderTypeGuid = workOrderTypeGuid;
	}

	public Date getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public Date getPromisedDate() {
		return promisedDate;
	}

	public void setPromisedDate(Date promisedDate) {
		this.promisedDate = promisedDate;
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public Collection<WorkOrderItemDTO> getWorkOrderItems() {
		return workOrderItems;
	}

	public void setWorkOrderItems(Collection<WorkOrderItemDTO> workOrderItems) {
		this.workOrderItems = workOrderItems;
	}

	public String getVehicleGuid() {
		return vehicleGuid;
	}

	public void setVehicleGuid(String vehicleGuid) {
		this.vehicleGuid = vehicleGuid;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
