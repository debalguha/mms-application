package com.sms.web.model;

public class RFQItemDTO {
	private String itmGuid;
	private String invGuid;
	private int itemQuantity;
	public int getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public String getItmGuid() {
		return itmGuid;
	}
	public void setItmGuid(String itmGuid) {
		this.itmGuid = itmGuid;
	}
	public String getInvGuid() {
		return invGuid;
	}
	public void setInvGuid(String invGuid) {
		this.invGuid = invGuid;
	}
	
}
