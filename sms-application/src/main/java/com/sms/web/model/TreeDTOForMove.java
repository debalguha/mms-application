package com.sms.web.model;

public class TreeDTOForMove {
	private String oldParentGuid;
	private String newParentGuid;
	private String currentNodeGuid;
	public String getOldParentGuid() {
		return oldParentGuid;
	}
	public void setOldParentGuid(String oldParentGuid) {
		this.oldParentGuid = oldParentGuid;
	}
	public String getNewParentGuid() {
		return newParentGuid;
	}
	public void setNewParentGuid(String newParentGuid) {
		this.newParentGuid = newParentGuid;
	}
	public String getCurrentNodeGuid() {
		return currentNodeGuid;
	}
	public void setCurrentNodeGuid(String currentNodeGuid) {
		this.currentNodeGuid = currentNodeGuid;
	}
	@Override
	public String toString() {
		return "WbsDTO [oldParentGuid=" + oldParentGuid + ", newParentGuid=" + newParentGuid + ", currentNodeGuid=" + currentNodeGuid + "]";
	}
}
