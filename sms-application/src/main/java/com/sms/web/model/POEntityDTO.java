package com.sms.web.model;

import java.util.Arrays;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class POEntityDTO {
	private String poNumber;
	private String rfqGuid;
	private String poGuid;
	private String vendorGuid;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date issueDate;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date statusDateCreated;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date deliveryByDate;
	private String poType;
	private String notes;
	private String deliveryInstruction;
	private String deliveryAddress;
	
	private float sAndH;
	private float taxes;
	private float duty;
	private float discount;
	private double total;
	
	private POItemDTO[] poItems;
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public Date getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	public Date getStatusDateCreated() {
		return statusDateCreated;
	}
	public void setStatusDateCreated(Date statusDateCreated) {
		this.statusDateCreated = statusDateCreated;
	}
	public Date getDeliveryByDate() {
		return deliveryByDate;
	}
	public void setDeliveryByDate(Date deliveryByDate) {
		this.deliveryByDate = deliveryByDate;
	}
	public String getPoType() {
		return poType;
	}
	public void setPoType(String poType) {
		this.poType = poType;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getDeliveryInstruction() {
		return deliveryInstruction;
	}
	public void setDeliveryInstruction(String deliveryInstruction) {
		this.deliveryInstruction = deliveryInstruction;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public POItemDTO[] getPoItems() {
		return poItems;
	}
	public void setPoItems(POItemDTO[] poItems) {
		this.poItems = poItems;
	}
	public String getRfqGuid() {
		return rfqGuid;
	}
	public void setRfqGuid(String rfqGuid) {
		this.rfqGuid = rfqGuid;
	}
	public String getVendorGuid() {
		return vendorGuid;
	}
	public void setVendorGuid(String vendorGuid) {
		this.vendorGuid = vendorGuid;
	}
	public String getPoGuid() {
		return poGuid;
	}
	public void setPoGuid(String poGuid) {
		this.poGuid = poGuid;
	}
	public float getsAndH() {
		return sAndH;
	}
	public void setsAndH(float sAndH) {
		this.sAndH = sAndH;
	}
	public float getTaxes() {
		return taxes;
	}
	public void setTaxes(float taxes) {
		this.taxes = taxes;
	}
	public float getDuty() {
		return duty;
	}
	public void setDuty(float duty) {
		this.duty = duty;
	}
	public float getDiscount() {
		return discount;
	}
	public void setDiscount(float discount) {
		this.discount = discount;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "POEntityDTO [poNumber=" + poNumber + ", rfqGuid=" + rfqGuid + ", poGuid=" + poGuid + ", vendorGuid=" + vendorGuid + ", issueDate=" + issueDate + ", statusDateCreated=" + statusDateCreated + ", deliveryByDate=" + deliveryByDate + ", poType=" + poType + ", notes=" + notes + ", deliveryInstruction=" + deliveryInstruction + ", deliveryAddress=" + deliveryAddress + ", sAndH=" + sAndH
				+ ", taxes=" + taxes + ", duty=" + duty + ", discount=" + discount + ", total=" + total + ", poItems=" + Arrays.toString(poItems) + "]";
	}
}
