package com.sms.web.model;

public enum TREE_NODE_TYPE {
	PART, CONTRACT, SERVICE, VENDOR, PICTURE, DRAWING, LINEITEM, VENDORS_GROUP, DRAWINGS_GROUP, PICTURES_GROUP, SUB_PARTS_GROUP, UNKNOWN, WBS
}
