package com.sms.web.model;

import java.util.Collection;

public class TreeDTO {
	private String id;
	private String guid;
	private String text;
	private boolean renderable;
	private TREE_NODE_TYPE nodeType;
	private String itemTypeGuid;
	private Collection<TreeDTO> children;
	private NodeState state;
	
	public TreeDTO(){
		state = new NodeState();
	}
	public TreeDTO(String guid, String text, boolean renderable, TREE_NODE_TYPE nodeType, Collection<TreeDTO> children) {
		super();
		this.guid = guid;
		this.id = guid;
		this.text = text;
		this.renderable = renderable;
		this.nodeType = nodeType;
		this.children = children;
	}


	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
		this.id = guid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Collection<TreeDTO> getChildren() {
		return children;
	}
	public void setChildren(Collection<TreeDTO> children) {
		this.children = children;
	}
	public boolean isRenderable() {
		return renderable;
	}
	public void setRenderable(boolean renderable) {
		this.renderable = renderable;
	}
	public TREE_NODE_TYPE getNodeType() {
		return nodeType;
	}
	public void setNodeType(TREE_NODE_TYPE nodeType) {
		this.nodeType = nodeType;
	}
	public String getId() {
		return id;
	}
	/*public void setId(String id) {
		this.id = id;
	}*/
	public NodeState getState() {
		return state;
	}
	public void setState(NodeState state) {
		this.state = state;
	}
	public String getItemTypeGuid() {
		return itemTypeGuid;
	}
	public void setItemTypeGuid(String itemTypeGuid) {
		this.itemTypeGuid = itemTypeGuid;
	}
}
