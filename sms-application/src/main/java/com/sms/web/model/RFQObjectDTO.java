package com.sms.web.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class RFQObjectDTO {
	private String guid;
	private RFQItemDTO[] rfqItems;
	private String [] rfqVendors;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date statusDateCreated;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date issueDate;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date expirationDate;
	private String rfqNumber;
	private String notes;
	private String rfqType;
	private int responseCount;
	
	public RFQItemDTO[] getRfqItems() {
		return rfqItems;
	}
	public void setRfqItems(RFQItemDTO[] rfqItems) {
		this.rfqItems = rfqItems;
	}
	public String[] getRfqVendors() {
		return rfqVendors;
	}
	public void setRfqVendors(String[] rfqVendors) {
		this.rfqVendors = rfqVendors;
	}
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	public Date getStatusDateCreated() {
		return statusDateCreated;
	}
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	public void setStatusDateCreated(Date statusDateCreated) {
		this.statusDateCreated = statusDateCreated;
	}
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	public Date getIssueDate() {
		return issueDate;
	}
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	public Date getExpirationDate() {
		return expirationDate;
	}
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getRfqNumber() {
		return rfqNumber;
	}
	public void setRfqNumber(String rfqNumber) {
		this.rfqNumber = rfqNumber;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getRfqType() {
		return rfqType;
	}
	public void setRfqType(String rfqType) {
		this.rfqType = rfqType;
	}
	public int getResponseCount() {
		return responseCount;
	}
	public void setResponseCount(int responseCount) {
		this.responseCount = responseCount;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
}
