package com.sms.web.model;

import java.util.Arrays;

public class InventoryVendorAssociation {
	private String vendorGuid;
	private String []inventoryGuids;
	public String getVendorGuid() {
		return vendorGuid;
	}
	public void setVendorGuid(String vendorGuid) {
		this.vendorGuid = vendorGuid;
	}
	public String[] getInventoryGuids() {
		return inventoryGuids;
	}
	public void setInventoryGuids(String[] inventoryGuids) {
		this.inventoryGuids = inventoryGuids;
	}
	@Override
	public String toString() {
		return "InventoryVendorAssociation [vendorGuid=" + vendorGuid + ", inventoryGuids=" + Arrays.toString(inventoryGuids) + "]";
	}
	
}
