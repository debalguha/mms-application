package com.sms.web.model;

import java.util.Date;

public class RFQResponseDTO {
	private String rfqGuid;
	private String vendorGuid;
	private RFQResponseItemDTO[] items;
	private String notes;
	private String responseId;
	private int accepted;
	private Date deliveryByDate;
	
	public String getRfqGuid() {
		return rfqGuid;
	}
	public void setRfqGuid(String rfqGuid) {
		this.rfqGuid = rfqGuid;
	}
	public String getVendorGuid() {
		return vendorGuid;
	}
	public void setVendorGuid(String vendorGuid) {
		this.vendorGuid = vendorGuid;
	}
	public RFQResponseItemDTO[] getItems() {
		return items;
	}
	public void setItems(RFQResponseItemDTO[] items) {
		this.items = items;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getResponseId() {
		return responseId;
	}
	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}
	public Date getDeliveryByDate() {
		return deliveryByDate;
	}
	public void setDeliveryByDate(Date deliveryByDate) {
		this.deliveryByDate = deliveryByDate;
	}
	public int getAccepted() {
		return accepted;
	}
	public void setAccepted(int accepted) {
		this.accepted = accepted;
	}
	
}
