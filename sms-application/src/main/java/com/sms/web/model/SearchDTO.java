package com.sms.web.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class SearchDTO {
	private String guid;
	private String type;
	private String description;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date creationDate;
	public SearchDTO(){}
	
	public SearchDTO(String guid, String type, String description, Date creationDate) {
		super();
		this.guid = guid;
		this.type = type;
		this.description = description;
		this.creationDate = creationDate;
	}

	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	@Override
	public String toString() {
		return "SearchDTO [guid=" + guid + ", type=" + type + ", description=" + description + ", creationDate=" + creationDate + "]";
	}
}
