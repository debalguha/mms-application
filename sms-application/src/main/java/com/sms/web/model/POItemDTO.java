package com.sms.web.model;

public class POItemDTO {
	private String guid;
	private int quantity;
	private long unitPrice;
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public long getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(long unitPrice) {
		this.unitPrice = unitPrice;
	}
	@Override
	public String toString() {
		return "POItemDTO [guid=" + guid + ", quantity=" + quantity + ", unitPrice=" + unitPrice + "]";
	}
}
