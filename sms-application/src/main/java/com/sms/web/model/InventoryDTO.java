package com.sms.web.model;

public class InventoryDTO {
	private String guid;
	private String idCode;
	private String wbs;
	private String itemDescription;
	private Long highUnitPrice;
	private Long lowUnitPrice;
	private int quantityOnHand;
	private int lowQuantityThreshold;
	private boolean vendorAttached;
	private boolean poAvailable;
	private boolean active;
	public String getIdCode() {
		return idCode;
	}
	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}
	public String getWbs() {
		return wbs;
	}
	public void setWbs(String wbs) {
		this.wbs = wbs;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public Long getHighUnitPrice() {
		return highUnitPrice;
	}
	public void setHighUnitPrice(Long highUnitPrice) {
		this.highUnitPrice = highUnitPrice;
	}
	public Long getLowUnitPrice() {
		return lowUnitPrice;
	}
	public void setLowUnitPrice(Long lowUnitPrice) {
		this.lowUnitPrice = lowUnitPrice;
	}
	public int getQuantityOnHand() {
		return quantityOnHand;
	}
	public void setQuantityOnHand(int quantityOnHand) {
		this.quantityOnHand = quantityOnHand;
	}
	public int getLowQuantityThreshold() {
		return lowQuantityThreshold;
	}
	public void setLowQuantityThreshold(int lowQuantityThreshold) {
		this.lowQuantityThreshold = lowQuantityThreshold;
	}
	@Override
	public String toString() {
		return "InventoryDTO [idCode=" + idCode + ", wbs=" + wbs + ", itemDescription=" + itemDescription + ", highUnitPrice=" + highUnitPrice + ", lowUnitPrice=" + lowUnitPrice + ", quantityOnHand=" + quantityOnHand + ", lowQuantityThreshold=" + lowQuantityThreshold + "]";
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public boolean isVendorAttached() {
		return vendorAttached;
	}
	public void setVendorAttached(boolean vendorAttached) {
		this.vendorAttached = vendorAttached;
	}
	public boolean isPoAvailable() {
		return poAvailable;
	}
	public void setPoAvailable(boolean poAvailable) {
		this.poAvailable = poAvailable;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

}
