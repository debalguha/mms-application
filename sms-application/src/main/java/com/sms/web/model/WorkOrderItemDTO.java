package com.sms.web.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class WorkOrderItemDTO {
	private String guid;
	private String systemTreeGuid;
	private String userGuid;
	private String techGuid;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date datePromised;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date dateCompleted;
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getSystemTreeGuid() {
		return systemTreeGuid;
	}
	public void setSystemTreeGuid(String systemTreeGuid) {
		this.systemTreeGuid = systemTreeGuid;
	}
	public String getUserGuid() {
		return userGuid;
	}
	public void setUserGuid(String userGuid) {
		this.userGuid = userGuid;
	}
	public String getTechGuid() {
		return techGuid;
	}
	public void setTechGuid(String techGuid) {
		this.techGuid = techGuid;
	}
	public Date getDatePromised() {
		return datePromised;
	}
	public void setDatePromised(Date datePromised) {
		this.datePromised = datePromised;
	}
	public Date getDateCompleted() {
		return dateCompleted;
	}
	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}
	@Override
	public String toString() {
		return "WorkOrderItemDTO [guid=" + guid + ", systemTreeGuid=" + systemTreeGuid + ", userGuid=" + userGuid + ", techGuid=" + techGuid + ", datePromised=" + datePromised + ", dateCompleted=" + dateCompleted + "]";
	}
}	
