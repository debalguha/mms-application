package com.sms.web.model;

public class WbsDTO {
	private String guid;
	private String wbs;
	private String nomenclature;
	private String description;
	private String parentGuid;
	public String getWbs() {
		return wbs;
	}
	public void setWbs(String wbs) {
		this.wbs = wbs;
	}
	public String getNomenclature() {
		return nomenclature;
	}
	public void setNomenclature(String nomenclature) {
		this.nomenclature = nomenclature;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getParentGuid() {
		return parentGuid;
	}
	public void setParentGuid(String parentGuid) {
		this.parentGuid = parentGuid;
	}
	@Override
	public String toString() {
		return "WbsDTO [guid=" + guid + ", wbs=" + wbs + ", nomenclature=" + nomenclature + ", description=" + description + ", parentGuid=" + parentGuid + "]";
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
}
