package com.sms.web.model;


public class PartDTO {
	private String assemblyNumber;
	private String assemblyName;
	private String barCode;
	private String manufacturerPartNumber;
	private String wbsRoot;
	private String guid;
	public String getAssemblyNumber() {
		return assemblyNumber;
	}
	public void setAssemblyNumber(String assemblyNumber) {
		this.assemblyNumber = assemblyNumber;
	}
	public String getAssemblyName() {
		return assemblyName;
	}
	public void setAssemblyName(String assemblyName) {
		this.assemblyName = assemblyName;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public String getManufacturerPartNumber() {
		return manufacturerPartNumber;
	}
	public void setManufacturerPartNumber(String manufacturerPartNumber) {
		this.manufacturerPartNumber = manufacturerPartNumber;
	}
	public String getWbsRoot() {
		return wbsRoot;
	}
	public void setWbsRoot(String wbsRoot) {
		this.wbsRoot = wbsRoot;
	}
	@Override
	public String toString() {
		return "PartDTO [assemblyNumber=" + assemblyNumber + ", assemblyName=" + assemblyName + ", barCode=" + barCode + ", manufacturerPartNumber=" + manufacturerPartNumber + ", wbsRoot=" + wbsRoot + ", guid=" + guid + "]";
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
}
