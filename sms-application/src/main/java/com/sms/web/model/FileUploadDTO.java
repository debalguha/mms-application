package com.sms.web.model;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadDTO {
	private MultipartFile file;
	private String partGuid; // Translated as wbs guid when uploaded from Wbs tree.
	public String getPartGuid() {
		return partGuid;
	}
	public void setPartGuid(String partGuid) {
		this.partGuid = partGuid;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile files) {
		this.file = files;
	}
}
