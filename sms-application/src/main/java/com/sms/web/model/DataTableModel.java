package com.sms.web.model;

import java.util.Collection;

import com.google.common.collect.Lists;

public class DataTableModel<T> {
	private final Collection<T> data;
	private int recordsTotal;
	private int recordsFiltered;
	public DataTableModel(Collection<? extends T> data){
		this.data = Lists.newArrayList();
		this.data.addAll(data);
	}
	public DataTableModel(Collection<? extends T> data, int recordsTotal, int recordsFiltered){
		this(data);
		this.recordsTotal = recordsTotal;
		this.recordsFiltered = recordsFiltered;
	}
	public Collection<T> getData() {
		return data;
	}
	public int getRecordsTotal() {
		return recordsTotal;
	}
	public int getRecordsFiltered() {
		return recordsFiltered;
	}
}
