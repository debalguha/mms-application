package com.sms.web.model;

public class UploadResponseDTO {
	private String fileName;
	private String uploadDate;
	private int pages;
	private String userFirstName;
	private String entityGuid;
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public UploadResponseDTO(String fileName, String uploadDate, int pages, String userFirstName, String drawingGuid) {
		super();
		this.fileName = fileName;
		this.uploadDate = uploadDate;
		this.pages = pages;
		this.userFirstName = userFirstName;
		this.entityGuid = drawingGuid;
	}
	@Override
	public String toString() {
		return "UploadResponse [fileName=" + fileName + ", uploadDate=" + uploadDate + ", pages=" + pages + ", userFirstName=" + userFirstName + "]";
	}
	public String getEntityGuid() {
		return entityGuid;
	}
	public void setEntityGuid(String entityGuid) {
		this.entityGuid = entityGuid;
	}
	
}
