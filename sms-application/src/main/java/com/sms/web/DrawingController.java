package com.sms.web;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.model.SystemTreeItemType;
import com.sms.persistence.model.User;
import com.sms.persistence.service.DrawingService;
import com.sms.persistence.service.SystemService;
import com.sms.persistence.service.WbsService;
import com.sms.util.DTOUtils;
import com.sms.util.SMSConstants;
import com.sms.util.SMSUtil;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.FileUploadDTO;
import com.sms.web.model.UploadResponseDTO;

@Controller
public class DrawingController extends AbstractDateBindingController {
	@Autowired
	private SystemService systemService;
	
	@Autowired
	private DrawingService drawingService;
	
	@Autowired
	private WbsService wbsService;
	
	@Value("${upload.directory}")
	private String uploadDirectory;
	
	@RequestMapping(value = "system/upload/drawing", method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA,MediaType.APPLICATION_FORM_URLENCODED })
	public @ResponseBody UploadResponseDTO uploadDrawing(@ModelAttribute FileUploadDTO fileUploadDTO, HttpSession session) throws Exception {
		Date now = new Date();
		SystemTree partNode = systemService.findSystemTreeByGUID(fileUploadDTO.getPartGuid());
		SystemTreeItemType itemType = systemService.findTreeItemTypeByName("Drawing");
		SystemTree drawingNode = new SystemTree();
		drawingNode.setCreationDate(now);
		drawingNode.setDescription(fileUploadDTO.getFile().getOriginalFilename());
		drawingNode.setParent(partNode);
		drawingNode.setTreeItemType(itemType);
		SystemDrawing drawing = createAndStoreDrawing(fileUploadDTO, now, drawingNode);
		return new UploadResponseDTO(drawing.getStoredFileName(), SMSUtil.toString(drawing.getCreationDate()), SMSUtil.findNumberOfPages(new File(uploadDirectory, drawing.getStoredFileName())), ((User)session.getAttribute(SMSConstants.USERBEAN.name())).getFirstName(), drawing.getGuid());
	}
	
	@RequestMapping(value = "wbs/upload/drawing", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_FORM_URLENCODED })
	public @ResponseBody SystemDrawing uploadPictureForWbsTree(@ModelAttribute FileUploadDTO fileUploadDTO,
			HttpSession session) throws Exception {
		return wbsService.associateDrawing(fileUploadDTO.getPartGuid(), createAndStoreDrawing(fileUploadDTO, new Date(), null).getGuid()).getSystemDrawing();
	}
	
	private SystemDrawing createAndStoreDrawing(FileUploadDTO fileUploadDTO, Date now, SystemTree drawingNode) throws Exception{
		File storedFile = storeFileToUploadDirectory(new File(uploadDirectory), fileUploadDTO.getFile());
		SystemDrawing drawing = new SystemDrawing();
		drawing.setCreationDate(now);
		drawing.setSystemTree(drawingNode);
		drawing.setStoredFileName(storedFile.getName());
		drawing.setUploadedFileName(storedFile.getName());
		return drawingService.createDrawing(drawing);
	}
	
	@RequestMapping(value = "drawings/download/{drawingGuid}", method = RequestMethod.GET)
	public void downloadDrawingFileFromDrawings(@PathVariable("drawingGuid") String drawingGuid, HttpSession session, HttpServletResponse response) throws Exception {
		downloadDrawingFile(drawingGuid, session, response);
	}
	@RequestMapping(value = "system/download/drawing/{drawingGuid}", method = RequestMethod.GET)
	public void downloadDrawingFile(@PathVariable("drawingGuid") String drawingGuid, HttpSession session, HttpServletResponse response) throws Exception{
		SystemDrawing drawing = drawingService.getDrawingByGuid(drawingGuid);
		File storedFile = new File(new File(uploadDirectory), drawing.getUploadedFileName());
		if(!storedFile.exists())
			throw new IllegalArgumentException("Uploaded file not found!!");
		response.setHeader("Set-Cookie", "fileDownload=true; path=/");
		response.setHeader("Cache-Control", "must-revalidate");
		response.setHeader("Content-Type", "text/csv");
		response.setHeader("Content-disposition", "attachment;filename=\""
				+ storedFile.getName() + "\"");		
		IOUtils.copy(new FileInputStream(storedFile), response.getOutputStream());
		response.getOutputStream().flush();
	}
	
	@RequestMapping(value = "system/part/{partGuid}/drawings", method = RequestMethod.GET)
	public @ResponseBody Collection<UploadResponseDTO> getAllDrawingsForpart(@PathVariable("partGuid") String partGuid, HttpSession session) throws IOException, Exception{
		return DTOUtils.convertDrawingsToUploadResponse(drawingService.getAllDrawingsForPartTreeNode(partGuid), new File(uploadDirectory), ((User)session.getAttribute(SMSConstants.USERBEAN.name())).getFirstName());
	}
	
	@RequestMapping(value = "system/part/{partGuid}/not/drawings", method = RequestMethod.GET)
	public @ResponseBody Collection<UploadResponseDTO> getAllDrawingsNotForpart(@PathVariable("partGuid") String partGuid, HttpSession session) throws IOException, Exception{
		return DTOUtils.convertDrawingsToUploadResponse(drawingService.getAllDrawingsNotForPartTreeNode(partGuid), new File(uploadDirectory), ((User)session.getAttribute(SMSConstants.USERBEAN.name())).getFirstName());
	}
	
	@RequestMapping(value = "system/drawing/remove/{drawingGuid}", method = RequestMethod.POST)
	public @ResponseBody String removeDrawingsFromPart(@PathVariable("drawingGuid") String drawingGuid) throws IOException, Exception{
		drawingService.removeDrawingByGuid(drawingGuid);
		return drawingGuid;
	}	

	@Deprecated
	public File createIfuserDirectoryDoesNotExist(String guid) throws Exception {
		File userDir = new File(uploadDirectory, guid);
		if(!userDir.exists() || !userDir.isDirectory())
			FileUtils.forceMkdir(userDir);
		return userDir;
	}

	private File storeFileToUploadDirectory(File parentDirectory, MultipartFile uploadedFile) throws Exception {
		File fileToSave = new File(parentDirectory, uploadedFile.getOriginalFilename());
		IOUtils.copy(new ByteArrayInputStream(uploadedFile.getBytes()), new FileOutputStream(fileToSave));
		return fileToSave;
	}


	@RequestMapping(value = "drawings/delete", method = RequestMethod.GET)
	public ModelAndView deleteAllDrawings(@RequestParam("selectedGuids") String selectedGuids) {
		try {
			drawingService.deleteAllDrawings(selectedGuids.split(","));
			return new ModelAndView("system/drawing", "statusMsg", "Drawing successfully deleted.");
		} catch (Throwable e) {
			e.printStackTrace();
			return new ModelAndView("system/drawing", "errorMsg", "Unable to delete drawing.");
		}
	}
	
	@RequestMapping(value = "drawings/rename", method = RequestMethod.GET)
	public ModelAndView renameDrawing(@RequestParam("drawingGuid") String drawingGuid, @RequestParam("newName") String newName) {
		boolean renameDrawings = false;
		try {
			renameDrawings = drawingService.renameDrawings(drawingGuid, newName, uploadDirectory);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return renameDrawings?new ModelAndView("system/drawing", "statusMsg", "Drawing renamed successfully")
				:new ModelAndView("system/drawing", "errorMsg", "Error while renaming the drawing");
	}
	
	@RequestMapping(value = "drawings/all", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SystemDrawing> getAllDrawings(@RequestParam(value = "search[value]", required = false) String searchTerm, 
			@RequestParam("start") int start, @RequestParam("length") int length, 
			@RequestParam(value = "order[0][column]", required=false) int orderColumn, 
			@RequestParam(value = "order[0][dir]", required=false) String direction) {
		int totalNumberOfDrawings = drawingService.getTotalNumberOfDrawings(null);
		int totalNumberOfDrawingsAfterFiltering = drawingService.getTotalNumberOfDrawings(searchTerm);
		return new DataTableModel<SystemDrawing>(drawingService.getAllDrawings(searchTerm, start==0?start:start/length, length, orderColumn, direction), totalNumberOfDrawings, totalNumberOfDrawingsAfterFiltering);
	}
	
}
