package com.sms.web;

import java.util.Collection;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Maps;
import com.sms.persistence.model.DataSettingField;
import com.sms.persistence.model.User;
import com.sms.persistence.service.DataSettingsService;
import com.sms.persistence.service.UserService;
import com.sms.util.SMSConstants;

@Controller
public class DataSettingsController {
	@Autowired
	private DataSettingsService dataSettingService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "admin/data", method = RequestMethod.GET)
	public ModelAndView getAllDataSettings(){
		return new ModelAndView("admin/data/view-data", SMSConstants.DATA_SETTINGS.name(), dataSettingService.findAllDataSettings());
	}
	
	@RequestMapping(value = "admin/data/fields/{guid}", method = {RequestMethod.POST, RequestMethod.GET})
	public @ResponseBody Collection<DataSettingField> getAllDataSettingFieldsForSetting(@PathVariable("guid") String settingsGuid){
		Collection<DataSettingField> fieldsForSetting = dataSettingService.getAllActiveDataSettingFieldsForSetting(settingsGuid);
		return fieldsForSetting;
	}
	
	@RequestMapping(value = "admin/data/create/{dataSettingsguid}", method = RequestMethod.POST)
	public ModelAndView saveDataSettings(DataSettingField dataSettingField, @PathVariable("dataSettingsguid") String dataSettingsGuid, HttpSession session){
		dataSettingField.setUser((User)session.getAttribute(SMSConstants.USERBEAN.name()));		
		dataSettingService.createDataSettingFields(dataSettingField, dataSettingsGuid);
		Map<String, Object> modelMap = Maps.newHashMap();
		modelMap.put(SMSConstants.DATA_SETTINGS.name(), dataSettingService.findAllDataSettings());
		modelMap.put("settingsGuid", dataSettingsGuid);
		return new ModelAndView("admin/data/view-data", modelMap);
	}
}
