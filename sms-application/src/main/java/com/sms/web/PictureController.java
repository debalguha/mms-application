package com.sms.web;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.model.SystemTreeItemType;
import com.sms.persistence.model.User;
import com.sms.persistence.service.PictureService;
import com.sms.persistence.service.SystemService;
import com.sms.persistence.service.WbsService;
import com.sms.util.DTOUtils;
import com.sms.util.SMSConstants;
import com.sms.util.SMSUtil;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.FileUploadDTO;
import com.sms.web.model.UploadResponseDTO;

@Controller
public class PictureController {
	private static final Logger logger = LoggerFactory.getLogger(PictureController.class);
	@Autowired
	private SystemService systemService;

	@Autowired
	private PictureService pictureService;
	
	@Autowired
	private WbsService wbsService;

	@Value("${upload.directory}")
	private String uploadDirectory;

	@RequestMapping(value = "system/upload/picture", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_FORM_URLENCODED })
	public @ResponseBody UploadResponseDTO uploadPictureForSystemTree(@ModelAttribute FileUploadDTO fileUploadDTO,
			HttpSession session) throws Exception {
		Date now = new Date();
		SystemTree partNode = systemService.findSystemTreeByGUID(fileUploadDTO.getPartGuid());
		SystemTreeItemType itemType = systemService.findTreeItemTypeByName("Picture");
		SystemTree pictureNode = new SystemTree();
		pictureNode.setCreationDate(now);
		pictureNode.setDescription(fileUploadDTO.getFile().getOriginalFilename());
		pictureNode.setParent(partNode);
		pictureNode.setTreeItemType(itemType);
		SystemPicture picture = createAndStorePicture(fileUploadDTO, now, pictureNode);
		return new UploadResponseDTO(picture.getStoredFileName(), SMSUtil.toString(pictureNode.getCreationDate()),
				SMSUtil.findNumberOfPages(new File(uploadDirectory, picture.getStoredFileName())),
				((User) session.getAttribute(SMSConstants.USERBEAN.name())).getFirstName(), picture.getGuid());
	}
	
	@RequestMapping(value = "wbs/upload/picture", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_FORM_URLENCODED })
	public @ResponseBody SystemPicture uploadPictureForWbsTree(@ModelAttribute FileUploadDTO fileUploadDTO,
			HttpSession session) throws Exception {
		return wbsService.associatePicture(fileUploadDTO.getPartGuid(), createAndStorePicture(fileUploadDTO, new Date(), null).getGuid()).getSystemPicture();
	}
	
	private SystemPicture createAndStorePicture(FileUploadDTO fileUploadDTO, Date now, SystemTree pictureNode) throws Exception{
		File storedFile = storeFileToUploadDirectory(new File(uploadDirectory), fileUploadDTO.getFile());
		SystemPicture picture = new SystemPicture();
		picture.setCreationDate(now);
		picture.setSystemTree(pictureNode);
		picture.setStoredFileName(storedFile.getName());
		picture.setUploadedFileName(storedFile.getName());
		return pictureService.createPicture(picture);
	}

	@RequestMapping(value = "system/download/picture/{pictureGuid}", method = RequestMethod.GET)
	public void downloadPictureFile(@PathVariable("pictureGuid") String pictureGuid, HttpSession session,
			HttpServletResponse response) throws Exception {
		SystemPicture picture = pictureService.getPictureByGuid(pictureGuid);
		File storedFile = new File(uploadDirectory, picture.getUploadedFileName());
		response.setHeader("Set-Cookie", "fileDownload=true; path=/");
		response.setHeader("Cache-Control", "must-revalidate");
		response.setHeader("Content-Type", "text/csv");
		response.setHeader("Content-disposition", "attachment;filename=\"" + storedFile.getName() + "\"");
		IOUtils.copy(new FileInputStream(storedFile), response.getOutputStream());
		response.getOutputStream().flush();
	}

	@RequestMapping(value = "system/part/{partGuid}/pictures", method = RequestMethod.GET)
	public @ResponseBody Collection<UploadResponseDTO> getAllPicturesForpart(@PathVariable("partGuid") String partGuid,
			HttpSession session) throws IOException, Exception {
		return DTOUtils.convertPicturesToUploadResponse(pictureService.getAllPicturesForPartTreeNode(partGuid),
				new File(uploadDirectory), ((User) session.getAttribute(SMSConstants.USERBEAN.name())).getFirstName());
	}

	@RequestMapping(value = "system/part/{partGuid}/not/pictures", method = RequestMethod.GET)
	public @ResponseBody Collection<UploadResponseDTO> getAllPicturesNotForpart(
			@PathVariable("partGuid") String partGuid, HttpSession session) throws IOException, Exception {
		return DTOUtils.convertPicturesToUploadResponse(pictureService.getAllPicturesNotForPartTreeNode(partGuid),
				new File(uploadDirectory), ((User) session.getAttribute(SMSConstants.USERBEAN.name())).getFirstName());
	}

	@RequestMapping(value = "system/picture/remove/{pictureGuid}", method = RequestMethod.POST)
	public @ResponseBody String removePicturesFromPart(@PathVariable("pictureGuid") String pictureGuid)
			throws IOException, Exception {
		pictureService.removePictureByGuid(pictureGuid);
		return pictureGuid;
	}

	@Deprecated
	public File createIfuserDirectoryDoesNotExist(String guid) throws Exception {
		File userDir = new File(uploadDirectory, guid);
		if (!userDir.exists() || !userDir.isDirectory())
			FileUtils.forceMkdir(userDir);
		return userDir;
	}

	private File storeFileToUploadDirectory(File parentDirectory, MultipartFile uploadedFile) throws Exception {
		File fileToSave = new File(parentDirectory, uploadedFile.getOriginalFilename());
		InputStream ins = new ByteArrayInputStream(uploadedFile.getBytes());
		OutputStream outs = new FileOutputStream(fileToSave);
		IOUtils.copy(ins, outs);
		ins.close();
		outs.flush();
		outs.close();
		return fileToSave;
	}
	

	@RequestMapping(value = "pictures", method = RequestMethod.GET)
	public ModelAndView displayAllPictures() {
		return new ModelAndView("picture");
	}

	/*@RequestMapping(value = "pictures/all", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SystemPicture> getAllPictures() {
		return new DataTableModel<SystemPicture>(pictureService.getAllPictures());
	}*/
	
	@RequestMapping(value = "pictures/delete", method = RequestMethod.GET)
	public ModelAndView deleteAllDrawings(@RequestParam("selectedGuids") String selectedGuids) {
		try {
			pictureService.deleteAllPictures(selectedGuids.split(","));
			return new ModelAndView("system/picture", "statusMsg", "Picture(s) successfully deleted.");
		} catch (Throwable e) {
			e.printStackTrace();
			return new ModelAndView("system/picture", "errorMsg", "Unable to delete picture(s).");
		}
	}	
	
	@RequestMapping(value = "pictures/rename", method = RequestMethod.GET)
	public ModelAndView renamePicture(@RequestParam("pictureGuid") String pictureGuid, @RequestParam("newName") String newName) {
		boolean renamePictures = false;
		try {
			renamePictures = pictureService.renamePicture(pictureGuid, newName, uploadDirectory);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return renamePictures?new ModelAndView("system/picture", "statusMsg", "Picture renamed successfully")
				:new ModelAndView("system/picture", "errorMsg", "Error while renaming the picture");
	}	
	
	@RequestMapping(value = "pictures/all", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SystemPicture> getAllPictures(@RequestParam(value = "search[value]", required = false) String searchTerm, 
			@RequestParam("start") int start, @RequestParam("length") int length, 
			@RequestParam(value = "order[0][column]", required=false) int orderColumn, 
			@RequestParam(value = "order[0][dir]", required=false) String direction) {
		int totalNumberOfPicturesAfterFiltering = pictureService.getTotalNumberOfPictures(searchTerm);
		int totalNumberOfPictures = pictureService.getTotalNumberOfPictures(null);
		return new DataTableModel<SystemPicture>(pictureService.getAllPictures(searchTerm, start==0?start:start/length, length, orderColumn, direction), totalNumberOfPictures, totalNumberOfPicturesAfterFiltering);
	}
	
	@RequestMapping(value = "pictures/download/{guid}", method = {RequestMethod.GET, RequestMethod.POST})
	public void downloadPicture(@PathVariable("guid") String pictureGuid, HttpServletRequest request, HttpServletResponse response) {
		SystemPicture picture = pictureService.getPictureByGuid(pictureGuid);
		File pictureFile = new File(new File(uploadDirectory), picture.getUploadedFileName());
		if(!pictureFile.exists())
			throw new IllegalArgumentException("Uploaded file not found!!");
		try {
			logger.info("Setting response headers and blah blah");
			response.setHeader("Set-Cookie", "fileDownload=true; path=/");
			response.setHeader("Cache-Control", "must-revalidate");
			String contentType = "";
			if(FilenameUtils.getExtension(pictureFile.getName()).equalsIgnoreCase("pdf"))
				contentType = "application/pdf";
			else 
				contentType = "application/octet-stream";
			response.setHeader("Content-Type", contentType);
			response.setHeader("Content-disposition", "attachment;filename=\""+ pictureFile.getName() + "\"");
			logger.info("Going to copy file to output stream.");
			FileCopyUtils.copy(FileUtils.readFileToByteArray(pictureFile), response.getOutputStream());
			logger.info("Copy finished!");
			response.getOutputStream().flush();
		} catch (Throwable e) {
			logger.error("Unable to copy over file to output stream", e);
		}
	}

}
