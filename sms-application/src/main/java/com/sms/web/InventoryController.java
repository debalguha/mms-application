package com.sms.web;

import java.util.Collection;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.InventoryVendor;
import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.service.InventoryService;
import com.sms.persistence.service.VendorService;
import com.sms.util.DTOUtils;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.InventoryDTO;
import com.sms.web.model.InventoryVendorAssociation;

@Controller
public class InventoryController extends AbstractDateBindingController {
	@Autowired
	private InventoryService inventoryService;
	@Autowired
	private VendorService vendorService;
	
	@RequestMapping(value = "inventory/{guid}/vendor", method = RequestMethod.GET)
	public @ResponseBody Collection<SystemVendor> findVendorsSupplyingInventory(@PathVariable("guid") String inventoryGuid){
		return inventoryService.findvendorSuppliers(new String[]{inventoryGuid});
	}
	
	@RequestMapping(value = "inventory/vendor/{vendorGuid}/items", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<Inventory> findInventorySuppliedByVendorGuid(@PathVariable("vendorGuid") String vendorGuid){
		return new DataTableModel<Inventory>(inventoryService.findInvenorySuppliedByVendor(vendorGuid));
	}
	@RequestMapping(value = "inventory/vendor/{vendorGuid}/items/not", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<Inventory> findInventoryNotSuppliedByVendorGuid(@PathVariable("vendorGuid") String vendorGuid){
		return new DataTableModel<Inventory>(inventoryService.findInvenoryNotSuppliedByVendor(vendorGuid));
	}
	
	@RequestMapping(value = "inventory/items", method = RequestMethod.GET)
	public ModelAndView inventoryItemsSupplied(@RequestParam("vendorGuid") String vendorGuid){
		return new ModelAndView("inventory/vendor/vendor-items-supplied", "vendorGuid", vendorGuid);
	}
	
	@RequestMapping(value = "inventory/associate", method = RequestMethod.GET)
	public ModelAndView associateInventoryWithVendor(InventoryVendorAssociation inventoryVendorAssociation){
		inventoryService.associateInventoriesWithVendor(inventoryVendorAssociation.getVendorGuid(), inventoryVendorAssociation.getInventoryGuids());
		return new ModelAndView("inventory/vendor/vendor-items-supplied", "vendorGuid", inventoryVendorAssociation.getVendorGuid());
	}
	
	/*@RequestMapping(value = "inventory/all", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<InventoryDTO> getAllInventoryRecords(InventoryVendorAssociation inventoryVendorAssociation){
		return new DataTableModel<InventoryDTO>(DTOUtils.convertInventorysToDTO(inventoryService.findAllInventoryRecords(), inventoryService));
	}*/
	
	@RequestMapping(value = "inventory/all", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<InventoryDTO> getAllInventoryRecords(@RequestParam(value = "search[value]", required = false) String searchTerm, 
			@RequestParam("start") int start, @RequestParam("length") int length, 
			@RequestParam(value = "order[0][column]", required=false) int orderColumn, 
			@RequestParam(value = "order[0][dir]", required=false) String direction){
		int totalNumberOfInventoryRecordsAfterFiltering = inventoryService.getTotalNumberOfInventoryRecords(searchTerm);
		int totalNumberOfInventoryRecords = inventoryService.getTotalNumberOfInventoryRecords(null);
		return new DataTableModel<InventoryDTO>(DTOUtils.convertInventorysToDTO(inventoryService.findAllInventoryRecords(searchTerm, start==0?start:start/length, length==0?10:length, orderColumn, direction), inventoryService), totalNumberOfInventoryRecords, totalNumberOfInventoryRecordsAfterFiltering);
	}
	
	@RequestMapping(value = "inventory/all/valid/supplier", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<InventoryDTO> getAllInventoryRecordsThatHasSupplyingVendors(InventoryVendorAssociation inventoryVendorAssociation){
		return new DataTableModel<InventoryDTO>(DTOUtils.convertInventorysToDTO(inventoryService.findAllInventoryRecordsHavingSupplier(), inventoryService));
	}
	
	@RequestMapping(value = "inventory/activate", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView activateInventoryRecord(@RequestParam("invSelect") String [] invSelect, @RequestParam("selectedGuids") String selectedInventoryGuids){
		inventoryService.activateInventories(selectedInventoryGuids.substring(0, selectedInventoryGuids.lastIndexOf(",")).split(","));
		return new ModelAndView("inventory/inventory", "operation", "Success");
	}	
	
	@RequestMapping(value = "inventory/issuePO", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView issuePOFromInventory(@RequestParam("invSelect") String [] invSelect, @RequestParam("selectedGuids") String selectedInventoryGuids){
		Collection<Inventory> inventories = Lists.newArrayList();
		for(String inventoryGuid : selectedInventoryGuids.substring(0, selectedInventoryGuids.lastIndexOf(",")).split(",")){
			Inventory inventory = inventoryService.findInventoryByGuid(inventoryGuid);
			Set<InventoryVendor> inventoryVendors = Sets.newHashSet();
			inventoryVendors.addAll(inventory.getInventoryVendors());
			for(InventoryVendor invVendor : inventoryVendors){
				SystemVendor vendor = vendorService.findVendorForInventoryVendor(invVendor.getGuid());
				invVendor.setVendor(vendor);
			}
			inventory.setInventoryVendors(inventoryVendors);
			inventories.add(inventory);
		}
		return new ModelAndView("po/issuePOFromInventory", "inventories", inventories);
	}	
	
	@RequestMapping(value = "inventory/deActivate", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView deActivateInventoryRecord(@RequestParam("invSelect") String [] invSelect, @RequestParam("selectedGuids") String selectedInventoryGuids){
		inventoryService.deActivateInventories(selectedInventoryGuids.substring(0, selectedInventoryGuids.lastIndexOf(",")).split(","));
		return new ModelAndView("inventory/inventory", "operation", "Success");
	}	
	
	@RequestMapping(value = "inventory/edit", method = RequestMethod.GET)
	public ModelAndView editInventory(@RequestParam("guid") String guid){
		return new ModelAndView("inventory/inventory-details", "inventory", inventoryService.findInventoryByGuid(guid));
	}
	
	@RequestMapping(value = "inventory/save", method = RequestMethod.POST)
	public ModelAndView editInventory(Inventory inventory){
		inventoryService.createInventoryRecord(inventory);
		return new ModelAndView("inventory/inventory");
	}
	
	@RequestMapping(value = "inventory/part/{partGuid}", method = RequestMethod.GET)
	public @ResponseBody Inventory findInventoryByParts(@PathVariable("partGuid") String partGuid){
		return inventoryService.findInventoryOfPart(partGuid);
	}
	/*@RequestMapping(value = "inventory/activate", method = RequestMethod.POST)
	public @ResponseBody InventoryDTO activateInventoryRecord(InventoryDTO dto){
		return DTOUtils.convertInventoryToDTO(inventoryService.activateInventory(dto.getGuid()), inventoryService);
	}	
	
	@RequestMapping(value = "inventory/deActivate", method = RequestMethod.POST)
	public @ResponseBody InventoryDTO deActivateInventoryRecord(InventoryDTO dto){
		return DTOUtils.convertInventoryToDTO(inventoryService.deActivateInventory(dto.getGuid()), inventoryService);
	}	*/	
}
