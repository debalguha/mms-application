package com.sms.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Maps;
import com.sms.persistence.model.SystemPartVendor;
import com.sms.persistence.service.VendorService;

@Controller
public class PartVendorController {
	@Autowired
	private VendorService vendorService;
	
	public ModelAndView getSystemPartAndVendorData(String systemPartVendorGuid){
		SystemPartVendor systemPartVendor = vendorService.findSystemPartVendorRecordByGuid(systemPartVendorGuid);
		Map<String, Object> modelMap = Maps.newHashMap();
		modelMap.put("systemPart", systemPartVendor.getSystemPart());
		modelMap.put("vendor", systemPartVendor.getSystemVendor());
		return new ModelAndView("system/part-vendor", modelMap);
	}
}
