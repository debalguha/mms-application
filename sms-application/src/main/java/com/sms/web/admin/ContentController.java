package com.sms.web.admin;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Maps;
import com.sms.persistence.model.Content;
import com.sms.persistence.service.ContentService;
import com.sms.util.SMSConstants;

@Controller
public class ContentController {
	@Autowired
	private ContentService contentService;
	
	public Collection<Content> findAllContents(){
		return contentService.findAllContents();
	}
	@RequestMapping(value = "admin/content", method = RequestMethod.GET)
	public ModelAndView viewContents(){
		return new ModelAndView("admin/content/view-content", "contentMap", contentService.getContentSettingsMap());
	}
	@RequestMapping(value = "admin/content/{guid}", method = RequestMethod.GET)
	public @ResponseBody Content getContentt(@PathVariable("guid") String guid){
		return contentService.findContentByGuid(guid);
	}
	@RequestMapping(value = "admin/content/save", method = RequestMethod.POST)
	public ModelAndView createContentt(@ModelAttribute Content content){
		Map<String, Object> modelMap = Maps.newHashMap();
		try {
			contentService.saveContent(content);
			modelMap.put("statusMsg", "Content successfully created.");
		} catch (Exception e) {
			e.printStackTrace();
			modelMap.put("errMsg", "Unable to create content.");
		}
		modelMap.put(SMSConstants.CONTENT_SETTINGS.name(), contentService.getContentSettingsMap());
		return new ModelAndView("admin/content/view-content", modelMap);
	}
}
