package com.sms.web.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sms.persistence.model.User;
import com.sms.persistence.service.UserService;

@Controller
public class UserProfileController {
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "admin/userprofile", method = RequestMethod.POST)
	public ModelAndView saveUser(User user, @RequestParam("locationGuid") String locationGuid){
		userService.saveUser(user, locationGuid);
		return new ModelAndView("admin/user_profile", "statusMsg", "User "+user.getEmail()+" successfully updated.");
	}
}
