package com.sms.web;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sms.persistence.model.User;
import com.sms.persistence.service.UserService;

@Controller
public class UserController {
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "users/user", method = RequestMethod.GET)
	public @ResponseBody Collection<User> findUserByFirstName(@RequestParam(value = "q[term]", required = true) String q){
		return userService.searchUserWithName(q);
	}
	
	@RequestMapping(value = "users/user/{guid}", method = RequestMethod.GET)
	public @ResponseBody User findUserByGuid(@PathVariable("guid") String guid){
		return userService.findByGuid(guid);
	}
	
}
