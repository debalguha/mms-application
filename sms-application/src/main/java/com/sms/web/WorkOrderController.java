package com.sms.web;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sms.persistence.model.AbstractTree;
import com.sms.persistence.model.AbstractTreeComponent;
import com.sms.persistence.model.WorkOrder;
import com.sms.persistence.model.WorkOrderItem;
import com.sms.persistence.service.CustomerService;
import com.sms.persistence.service.LocationService;
import com.sms.persistence.service.SystemService;
import com.sms.persistence.service.UserService;
import com.sms.persistence.service.VehicleService;
import com.sms.persistence.service.WorkOrderService;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.WorkOrderDTO;
import com.sms.web.model.WorkOrderItemDTO;

@Controller
public class WorkOrderController {
	@Autowired
	private WorkOrderService workOrderService;
	@Autowired
	private UserService userService;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private VehicleService vehicleService;
	
	@RequestMapping(value = "workOrder/completed", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<WorkOrder> allCompletedWorkOrders(){
		return new DataTableModel<WorkOrder>(workOrderService.findAllCompletedWorkOrders());
	}
	
	@RequestMapping(value = "workOrder/inProgress", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<WorkOrder> allInProgressWorkOrders(){
		return new DataTableModel<WorkOrder>(workOrderService.findAllInProgressWorkOrders());
	}
	
	@RequestMapping(value = "workOrder/scheduled", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<WorkOrder> allScheduledWorkOrders(){
		return new DataTableModel<WorkOrder>(workOrderService.findAllScheduledWorkOrders());
	}
	
	@RequestMapping(value = "workOrder/edit", method = RequestMethod.GET)
	public ModelAndView editWorkOrder(@RequestParam("guid") String guid){
		return new ModelAndView("po/workorder/workorder-edit", "workorder", workOrderService.findWorkorderByGuid(guid));
	}
	
	@RequestMapping(value = "workOrder/create", method = RequestMethod.POST)
	public @ResponseBody WorkOrder createWorkOrder(@RequestBody WorkOrderDTO workOrderDTO){
		WorkOrder workOrder = new WorkOrder();
		workOrder.setGuid(Strings.isNullOrEmpty(workOrderDTO.getGuid())?null:workOrderDTO.getGuid());
		workOrder.setCompletedDate(workOrderDTO.getCompletedDate());
		workOrder.setPromisedDate(workOrderDTO.getPromisedDate());
		workOrder.setScheduledDate(workOrderDTO.getScheduledDate());
		workOrder.setCreateByUser(userService.findByGuid(workOrderDTO.getUserGuid()));
		workOrder.setCustomer(customerService.findCustomerByGuid(workOrderDTO.getCustomerGuid()));
		workOrder.setVehicle(vehicleService.findVehicleByGuid(workOrderDTO.getVehicleGuid()));
		workOrder.setLocation(locationService.getByLocationByGUID(workOrderDTO.getLocationGuid()));
		workOrder.setPriority(workOrderDTO.getPriority());
		workOrder.setSuperVisorGuid(userService.findByGuid(workOrderDTO.getSupervisorGuid()));
		workOrder.setTechUserGuid(userService.findByGuid(workOrderDTO.getTechGuid()));
		workOrder.setWorkOrderType(workOrderService.findWorkOrderTypeByGuid(workOrderDTO.getWorkOrderTypeGuid()));
		setWorkOrderItems(workOrder, workOrderDTO.getWorkOrderItems());
		return workOrderService.createWorkOrder(workOrder);
	}
	
	@RequestMapping(value = "workOrder/edit/save", method = RequestMethod.POST)
	public @ResponseBody WorkOrder editAndSaveWorkOrder(@RequestBody WorkOrderDTO workOrderDTO){
		WorkOrder workOrder = new WorkOrder();
		workOrder.setGuid(Strings.isNullOrEmpty(workOrderDTO.getGuid())?null:workOrderDTO.getGuid());
		workOrder.setCompletedDate(workOrderDTO.getCompletedDate());
		workOrder.setPromisedDate(workOrderDTO.getPromisedDate());
		workOrder.setScheduledDate(workOrderDTO.getScheduledDate());
		workOrder.setCreateByUser(userService.findByGuid(workOrderDTO.getUserGuid()));
		workOrder.setCustomer(customerService.findCustomerByGuid(workOrderDTO.getCustomerGuid()));
		workOrder.setVehicle(vehicleService.findVehicleByGuid(workOrderDTO.getVehicleGuid()));
		workOrder.setLocation(locationService.getByLocationByGUID(workOrderDTO.getLocationGuid()));
		workOrder.setPriority(workOrderDTO.getPriority());
		workOrder.setSuperVisorGuid(userService.findByGuid(workOrderDTO.getSupervisorGuid()));
		workOrder.setTechUserGuid(userService.findByGuid(workOrderDTO.getTechGuid()));
		workOrder.setWorkOrderType(workOrderService.findWorkOrderTypeByGuid(workOrderDTO.getWorkOrderTypeGuid()));
		setWorkOrderItems(workOrder, workOrderDTO.getWorkOrderItems());
		return workOrderService.editWorkOrder(workOrder);
	}

	private void setWorkOrderItems(WorkOrder workOrder, Collection<WorkOrderItemDTO> workOrderItems) {
		List<WorkOrderItem> woItems = Lists.newArrayList();
		for(WorkOrderItemDTO itemDTO : workOrderItems){
			WorkOrderItem woItem = new WorkOrderItem();
			woItem.setCompletedDate(itemDTO.getDateCompleted());
			woItem.setCreateByUser(userService.findByGuid(itemDTO.getUserGuid()));
			woItem.setTechUserGuid(userService.findByGuid(itemDTO.getTechGuid()));
			woItem.setPromisedDate(itemDTO.getDatePromised());
			woItem.setSystemTree(systemService.findSystemTreeByGUID(itemDTO.getSystemTreeGuid()));
			woItem.setWorkOrder(workOrder);
			woItems.add(woItem);
			workOrder.setWorkOrderItems(Sets.newHashSet(woItems));
		}
	}

	public Double getCommonCurrencyPrice(AbstractTreeComponent domainElement) {
		return new Double(-1);
	}

	public AbstractTreeComponent getDomainObjectBySystemTree(AbstractTree treeElement) {
		return null;
	}
}
