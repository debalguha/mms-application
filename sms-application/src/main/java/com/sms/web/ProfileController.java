package com.sms.web;

import static com.sms.util.SMSConstants.USERBEAN;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sms.persistence.model.Location;
import com.sms.persistence.model.User;
import com.sms.persistence.service.AccessLevelService;
import com.sms.persistence.service.LocationService;
import com.sms.persistence.service.UserService;
import com.sms.web.model.UserModel;

@Controller
public class ProfileController {
	private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);
	@Autowired
	private UserService userService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private AccessLevelService accessLevelService;
	
	@RequestMapping(value = "myprofile", method = RequestMethod.POST)
	public String editProfile(UserModel userModel, HttpSession session){
		logger.info("Profile edit request received: "+userModel);
		Location location = locationService.getByLocationByGUID(userModel.getLocationId());
		User user = (User)session.getAttribute(USERBEAN.name());
		user.setFirstName(!StringUtils.isBlank(userModel.getFirstName())?userModel.getFirstName():user.getFirstName());
		user.setLastName(!StringUtils.isBlank(userModel.getLastName())?userModel.getLastName():user.getLastName());
		user.setPhone(!StringUtils.isBlank(userModel.getPhone())?userModel.getPhone():user.getPhone());
		user.setPassword(!StringUtils.isBlank(userModel.getPassword())?userModel.getPassword():user.getPassword());
		user.setEmail(!StringUtils.isBlank(userModel.getEmail())?userModel.getEmail():user.getEmail());
		user.setLocation(location);
		session.setAttribute(USERBEAN.name(), userService.saveUser(user));
		return "user/home";
	}
	
}
