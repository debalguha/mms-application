package com.sms.web;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Maps;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.model.User;
import com.sms.persistence.search.SearchItemTypes;
import com.sms.persistence.service.StoreProcedureService;
import com.sms.persistence.service.SystemService;
import com.sms.util.DTOUtils;
import com.sms.util.SMSConstants;
import com.sms.web.model.DataTableModel;
import com.sms.web.model.SearchDTO;

@Controller
public class StoredProcedureController {
	@Autowired
	private CustomerController customerController;
	
	@Autowired
	private RFQController rfqController;
	
	@Autowired
	private POController poController;
	
	@Autowired
	private VehicleController vehicleController;
	
	@Autowired
	private VendorController vendorController;
	
	@Autowired
	private StoreProcedureService searchService;
	
	@Autowired
	private PartVendorController partVendorController;
	
	@Autowired
	private InventoryController inventoryController;
	
	@Autowired
	private ContactController contactController;
	
	@Autowired
	private SystemService systemService;
	
	@RequestMapping(value = "search", method = RequestMethod.GET)
	public ModelAndView navigateSearch(@RequestParam("searchTerm") String searchTerm, HttpSession session){
		return new ModelAndView("search-result", "searchTerm", searchTerm);
	}
	
	@RequestMapping(value = "search-navigation.do", method = RequestMethod.GET)
	public ModelAndView navigateSearchResult(@RequestParam("guid") String guid, @RequestParam("type") SearchItemTypes itemType, @RequestParam("searchTerm") String searchTerm, HttpServletRequest request) throws ParseException{
		switch(itemType){
		case customer : 
			return customerController.editCustomer(guid);
		case system_tree :
			return new ModelAndView("system/tree-view", "guid", guid);
		case rfq :
			return rfqController.editRFQView(guid);
		case po :
			return poController.editPO(guid);
		case vehicles :
			return vehicleController.editVehicle(guid, request);
		case inventory : 
			return inventoryController.editInventory(guid);
		case System_Parts_Vendors : 
			return partVendorController.getSystemPartAndVendorData(guid);
		case Contact : 
			return contactController.getContactDetails(guid);	
		case System_Parts_Data :
			SystemTree systemTreeForPart = systemService.getPartByGuid(guid).getSystemTree();
			if(systemTreeForPart!=null)
				return new ModelAndView("system/tree-view", "guid", systemTreeForPart.getGuid());
			else{
				Map<String, String> modelMap = Maps.newHashMap();
				modelMap.put("searchTerm", searchTerm);
				modelMap.put("statusMsg", "This 'Part' does not belong to any system tree!! Can not navigate.");
				return new ModelAndView("search-result", modelMap);
			}
		default :
			return null;
		}
	}
	
	@RequestMapping(value = "search.do", method = RequestMethod.GET)
	public @ResponseBody DataTableModel<SearchDTO> doSearch(@RequestParam("searchTerm") String searchTerm, HttpSession session) throws ParseException{
		return new DataTableModel<SearchDTO>(DTOUtils.convertSearchResultToDTO(searchService.search(searchTerm, ((User)session.getAttribute(SMSConstants.USERBEAN.name())).getGuid())));
	}
	
	@RequestMapping(value = "report/example.do", method = RequestMethod.GET)
	public ModelAndView exampleReport(@RequestParam("startDate") Date starTDate, @RequestParam("endDate") Date endDate){
		return new ModelAndView("admin/report/example-report-display", "dataSet", searchService.reportExample(starTDate, endDate));
	}
}
