<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>po-home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>

<script
	src="${pageContext.request.contextPath}/static/js/po-home.js"></script>
<script>
	var poListURL = "${pageContext.request.contextPath}/po/list";
	var rfqResponseURL = "${pageContext.request.contextPath}/rfq/response/edit";
	var inventoryDeActivateURL = "${pageContext.request.contextPath}/inventory/deActivate";
	var issuePOURL = "${pageContext.request.contextPath}/po/rfq/issuePO";
	var issueRFQURL = "${pageContext.request.contextPath}/rfq/issue";
</script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<c:if test="${not empty requestScope.statusMsg}">
			<div class="row">
				<div class="col-lg-4">
					<p class="bg-success">
						<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
						<c:out value="${requestScope.statusMsg}"/>
					</p>
				</div>
			</div>
		</c:if>		
		<form id="invForm" method="post">
			<div class="row">
				<div class="col-lg-12">
					<div style="overflow: auto; max-height: 500px; max-width: 100%;">
						<table id="po-table" class="table table table-bordered table-hover" cellspacing="0" width="100%"style="cursor:pointer">
					        <thead>
					            <tr>
					                <th>PO Number</th>
					                <th>Type</th>
					                <th>Date Issued</th>
					                <th>Status Date</th>
					                <th>Items</th>
					                <th>Vendor</th>
					                <th>RFQ #</th>
					            </tr>
					        </thead>
					        <tbody></tbody>					
						</table>
					</div>
				</div>
			</div>
		</form>
		<form id="editForm" action="${pageContext.request.contextPath}/po/edit/" method="POST">
		</form>
		<%-- <form id="linkForm" action="${pageContext.request.contextPath}/navigation" method="POST">
			<input type="hidden" name="rfqGuid" id="rfqGuid">
			<input type="hidden" name="nav-link" id="nav-link">
		</form> --%>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
</html>
