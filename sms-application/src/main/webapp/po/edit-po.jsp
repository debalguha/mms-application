<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit PO</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/styles/simple-demo.css">	
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">		
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script src="${pageContext.request.contextPath}/static/js/edit_po.js"></script>
<script src="${pageContext.request.contextPath}/static/js/po-calculation.js"></script>
	
<script>
	var inventoryURL = "${pageContext.request.contextPath}/inventory/all";
	var inventoryActivateURL = "${pageContext.request.contextPath}/inventory/activate";
	var duplicatePOURL = "${pageContext.request.contextPath}/po/duplicate";
	var deletePOURL = "${pageContext.request.contextPath}/po/delete/";
	var savePOFromEditURL = "${pageContext.request.contextPath}/po/save/fromEdit";
	var navigationURL = "${pageContext.request.contextPath}/navigation";
	var oldPONumber = "${requestScope.po.poNumber}";
</script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form id="rfqForm" method="post" action='<c:out value="${pageContext.request.contextPath}"/>/po/edit/save'>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label for="firstName">PO Type:</label> 
						<select name="poType" class="form-control" id="poType" placeholder="PO Type">
							<c:forEach items="${applicationScope.PO_TYPES}" var="item">
								<c:choose>
									<c:when test="${item.guid eq requestScope.po.poType.guid}">
										<option value="${item.guid}" selected="selected">${item.poType}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.guid}">${item.poType}</option>								
									</c:otherwise>									
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>			
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label for="firstName">RFQ ID:</label> <input type="text"
									class="form-control" id="rfqNumber" disabled="disabled" name="rfqNumnber" value="<c:out value="${requestScope.po.rfq.rfqNumber}"/>"
									placeholder="RFQ Id">
							</div>	
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label for="firstName">PO ID:</label> 
								<input type="text" class="form-control" id="poNumber" name="poNumber" placeholder="PO Id" value="${requestScope.po.poNumber}">
							</div>	
						</div>	
						<div class="col-lg-4">
							<div class="form-group">
								<label for="firstName">Delivery By Date:</label> 
				                <div class="input-group date" id="deliveryByDatePicker">
				                	<input id="deliveryByDate" type="text" class="form-control" name="deliveryByDate" value='<fmt:formatDate pattern="MM/dd/yyyy" value="${requestScope.po.deliveryByDate}" />'/>
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>						
							</div>
						</div>							
					</div>
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label for="firstName">Date Created:</label> 
								<input type="text" id="statusDateCreated" name="statusDateCreated" disabled="disabled" value='<fmt:formatDate pattern="MM/dd/yyyy" value="${requestScope.po.statusDateCreated}" />'>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label for="firstName">Issue Date:</label> 
				                <div class="input-group date" id="issueDatePicker">
				                	<input id="issueDate" type="text" class="form-control" name="issueDate" value='<fmt:formatDate pattern="MM/dd/yyyy" value="${requestScope.po.issueDate}" />'/>
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>						
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label for="firstName">Responding Vendor:</label> 
								<input type="text" readonly="readonly" class="form-control" name="vendor-name" id="vendor-name" value="${requestScope.po.vendor.name}">
								<input type="hidden" readonly="readonly" name="vendor" id="vendor" value="${requestScope.po.vendor.guid}">
							</div>
						</div>				
					</div>			
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="firstName">S&H</label> 
								<input type="number" id="sAndH" name="sAndH" class="form-control" value="${requestScope.po.sAndH}" onblur="adjustPOCalculation()">				
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="firstName">Taxes</label> 
								<input type="number" id="taxes" name="taxes" class="form-control" value="${requestScope.po.taxes}" onblur="adjustPOCalculation()">				
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="firstName">Duty</label> 
								<input type="number" id="duty" name="duty" class="form-control" value="${requestScope.po.duty}" onblur="adjustPOCalculation()">				
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="firstName">Discount</label> 
								<input type="number" id="discount" name="discount" class="form-control" value="${requestScope.po.discount}" onblur="adjustPOCalculation()">				
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="firstName">Total</label> 
								<input type="number" id="total" name="total" class="form-control" value="${requestScope.po.total}" onblur="adjustPOCalculation()" readonly="readonly">				
							</div>
						</div>			
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-lg-12">
									<label for="firstName">Items:</label> 
									<div style="overflow: auto; max-height: 200px;">
										<table id="poItemTable" class="table table-bordered" cellspacing="0" width="100%">
									        <thead>
									            <tr>
									                <th>Item</th>
									                <th>Quantity</th>
									                <th>Unit price</th>
									            </tr>
									        </thead>
									        <tbody>
								        		<c:forEach items="${requestScope.po.poItems}" var="item">
								        			<tr data-guid="${item.guid}"><td><c:out value="${item.systemPart.assemblyName}"/></td><td><input type="number" name="quantity" role="zero-digits" value="<c:out value="${item.quantity}"/>" onblur="adjustPOCalculation()"></td><td><input type="number" data-guid="${item.guid}" name="unitPrice" value="<c:out value="${item.unitPrice}"/>" onblur="adjustPOCalculation()"></td></tr>
								        		</c:forEach>							        
									        </tbody>						
										</table>
									</div>
								</div>						
							</div>
						</div>			
					</div>				
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label for="firstName">Delivery Address:</label> 
						<textarea class="form-control" id="deliveryAddress" name="deliveryAddress" rows="4" placeholder="Delivery Address"><c:out value="${requestScope.po.deliveryAddress}"></c:out></textarea>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
						<label for="firstName">Delivery Instruction:</label> 
						<textarea class="form-control" id="deliveryInstruction" name="deliveryInstruction" rows="4" placeholder="Delivery Instruction"><c:out value="${requestScope.po.deliveryInstruction}"></c:out></textarea>
					</div>						
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label for="firstName">Notes:</label> 
						<textarea class="form-control" id="notes" name="notes" rows="4" placeholder="Notes"><c:out value="${requestScope.po.notes}"></c:out></textarea>
					</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-lg-5">
					<!-- <div class="row"> -->
						<div class="col-lg-2"><button id="savePO" type="button" class="btn btn-warning">Save</button></div>
						<div class="col-lg-2"><button id="deletePO" type="button" class="btn btn-warning">Delete</button></div>
						<div class="col-lg-2"><button id="duplicatePO" type="button" class="btn btn-warning">Duplicate</button></div>								
					<!-- </div> -->
				</div>
			</div>			
			<input type="hidden" id="poGuid" name="poGuid" value='<c:out value="${requestScope.po.guid}"/>'>
			<input type="hidden" id="rfqGuid" name="rfqGuid" value='<c:out value="${requestScope.po.rfq.guid}"/>'>
		</form>
		<form id="editForm" action="${pageContext.request.contextPath}/po/edit/success/<c:out value="${requestScope.po.guid}"/>" method="POST">
		</form>
		<form id="deleteForm" action="${pageContext.request.contextPath}/po/delete" method="POST">
			<input type="hidden" id="poGuid" name="poGuid" value='<c:out value="${requestScope.po.guid})"/>'>
		</form>
	</div>	
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		adjustPOCalculation();
	});
</script>
</html>