<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>issuePOFromRFQ</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/simple-demo.css">	
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">
		
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script src="${pageContext.request.contextPath}/static/js/po_from_inventory.js"></script>
<script src="${pageContext.request.contextPath}/static/js/po-calculation.js"></script>
	
<script>
	var inventoryURL = "${pageContext.request.contextPath}/inventory/all";
	var inventoryActivateURL = "${pageContext.request.contextPath}/inventory/activate";
	var inventoryDeActivateURL = "${pageContext.request.contextPath}/inventory/deActivate";
	var issuePOURL = "${pageContext.request.contextPath}/inventory/issuePO";	
	var savePOFromInventoryURL = "${pageContext.request.contextPath}/po/save/fromInventory";
	var navigationURL = "${pageContext.request.contextPath}/navigation";
</script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form id="rfqForm" method="post" action='<c:out value="${pageContext.request.contextPath}"/>/po/fromRFQ'>
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label for="firstName">PO Type:</label> 
						<select name="poType" class="form-control" id="poType" placeholder="PO Type">
							<c:forEach items="${applicationScope.PO_TYPES}" var="item">
								<option value="${item.guid}">${item.poType}</option>
							</c:forEach>
						</select>
					</div>
				</div>			
			</div>
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">PO ID:</label> 
						<input type="text" class="form-control" id="poNumber" name="poNumber" placeholder="PO Id">
					</div>	
				</div>	
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Delivery By Date:</label> 
		                <div class="input-group date" id="deliveryByDatePicker">
		                	<input id="deliveryByDate" type="text" class="form-control" name="deliveryByDate" />
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>							
			</div>
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Date Created:</label> 
						<input type="text" id="statusDateCreated" name="statusDateCreated" disabled="disabled" value='<fmt:formatDate pattern="MM/dd/yyyy" value="${now}"/>'>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Issue Date:</label> 
		                <div class="input-group date" id="issueDatePicker">
		                	<input id="issueDate" type="text" class="form-control" name="issueDate" value=''/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>	
				<div class="col-lg-4">
					<div class="form-group">
						<label for="firstName">Responding Vendor:</label> 
						<select name="vendor" class="form-control" id="vendor">
							<c:forEach items="${requestScope.inventories}" var="inventoryItem">
								<c:forEach items="${inventoryItem.inventoryVendors}" var="item">
									<option value="${item.vendor.guid}">${item.vendor.name}</option>
								</c:forEach>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12">
							<label for="firstName">Items:</label> 
							<div style="overflow: auto; max-height: 200px;">
								<table id="rfqItemTable" class="table table-bordered" cellspacing="0" width="100%">
							        <thead>
							            <tr>
							                <th>Item</th>
							                <th>Quantity</th>
							                <th>Unit price</th>
							            </tr>
							        </thead>
							        <tbody>
						        		<c:forEach items="${requestScope.inventories}" var="inventoryItem">
						        			<tr><td><c:out value="${inventoryItem.systemPart.assemblyName}"/></td><td><input role="zero-digits" onblur="adjustPOCalculation()" type="number" name="quantity" data-guid="<c:out value="${inventoryItem.systemPart.guid}"/>"></td><td><input onblur="adjustPOCalculation()" type="number" name="unitPrice" data-guid="<c:out value="${inventoryItem.systemPart.guid}"/>""></td></tr>
						        		</c:forEach>							        
							        </tbody>						
								</table>
							</div>
						</div>						
					</div>
				</div>			
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="firstName">S&H</label> 
						<input type="number" id="sAndH" name="sAndH" class="form-control" value="" onblur="adjustPOCalculation()">				
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="firstName">Taxes</label> 
						<input type="number" id="taxes" name="taxes" class="form-control" value="" onblur="adjustPOCalculation()">				
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="firstName">Duty</label> 
						<input type="number" id="duty" name="duty" class="form-control" value="" onblur="adjustPOCalculation()">				
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="firstName">Discount</label> 
						<input type="number" id="discount" name="discount" class="form-control" value="" onblur="adjustPOCalculation()">				
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="firstName">Total</label> 
						<input type="number" id="total" name="total" class="form-control" value="" onblur="adjustPOCalculation()" readonly="readonly">				
					</div>
				</div>			
			</div>			
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label for="firstName">Delivery Address:</label> 
						<textarea class="form-control" id="deliveryAddress" name="deliveryAddress" rows="4" placeholder="Delivery Address"></textarea>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
						<label for="firstName">Delivery Instruction:</label> 
						<textarea class="form-control" id="deliveryInstruction" name="deliveryInstruction" rows="4" placeholder="Delivery Instruction"></textarea>
					</div>						
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label for="firstName">Notes:</label> 
						<textarea class="form-control" id="notes" name="notes" rows="4" placeholder="Notes"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-5">
					<!-- <div class="row"> -->
						<button id="savePO" type="button" class="btn btn-warning">Save PO</button>
					<!-- </div> -->
				</div>
			</div>			
		</form>
		<form id="navigationForm" method="POST" action="${pageContext.request.contextPath}/navigation">
		</form>
	</div>	
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
</html>
