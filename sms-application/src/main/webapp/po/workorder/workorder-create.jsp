<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create WorkOrder</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/simple-demo.css">	
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">	
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/jstree-themes/proton/style.css">		
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jstree.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/workorder-create.js"></script>
<script src="${pageContext.request.contextPath}/static/js/modal-system-tree.js"></script>
	
<script>
	var userSearchURL = "${pageContext.request.contextPath}/users/user";
	var customerSearchURL = "${pageContext.request.contextPath}/customers/customer";
	var vehicleSearchURL = "${pageContext.request.contextPath}/vehicle/search/customer/";
	var systemSearchURL = "${pageContext.request.contextPath}/system/search/itemType/";
	var navigationURL = "${pageContext.request.contextPath}/navigation";
	var saveWorkOrderURL = "${pageContext.request.contextPath}/workOrder/create";
	var treeURL = '${pageContext.request.contextPath}/system/tree/view/workorder';
</script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form id="rfqForm" method="post">
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label for="firstName">Workorder Type:</label> 
						<select class="form-control" id="workOrderType" name="workOrderType">
							<c:forEach items="${applicationScope.WORKORDER_TYPES}" var="item">
								<option value="${item.guid}">${item.type}</option>								
							</c:forEach>
						</select>
					</div>
				</div>			
			</div>
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Customer:</label> 
						<select id="customerGuid" name="customerGuid" style="width: 100%">
							<c:forEach items="${applicationScope.CUSTOMERS}" var="item">
								<option value="${item.guid}">${item.customerEndUser}</option>
							</c:forEach>
						</select>
					</div>	
				</div>	
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Vehicle:</label> 
						<select class="form-control" id="vehicleGuid" name="vehicleGuid"></select>
					</div>	
				</div>	
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Location:</label> 
						<select class="form-control" id="locationGuid" name="locationGuid">
							<c:forEach items="${applicationScope.LOCATIONS}" var="item">
								<option value="${item.guid}">${item.locationName}</option>								
							</c:forEach>
						</select>						
					</div>	
				</div>					
			</div>
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Technician:</label> 
						<select id="techGuid" name="techGuid" style="width: 100%">
							<c:forEach items="${applicationScope.USERS}" var="item">
								<option value="${item.guid}">${item.firstName}</option>	
							</c:forEach>
						</select>
					</div>	
				</div>	
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">SuperVisor:</label> 
						<select id="supervisorGuid" name="supervisorGuid" style="width: 100%">
							<c:forEach items="${applicationScope.USERS}" var="item">
								<option value="${item.guid}">${item.firstName}</option>	
							</c:forEach>						
						</select>
					</div>	
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Priority:</label> 
						<select class="form-control" id="priority" name="priority" style="width: 100%">
							<option value="1">Severe</option>
							<option value="2">High</option>
							<option value="3">Medium</option>
							<option value="4">Minor</option>
							<option value="5">Low</option>
						</select>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Scheduled Date:</label> 
		                <div class="input-group date" id="scheduledDatePicker">
		                	<input id="scheduledDate" type="text" class="form-control" name="scheduledDate"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Promised Date:</label> 
		                <div class="input-group date" id="promisedDatePicker">
		                	<input id="promisedDate" type="text" class="form-control" name="promisedDate"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Completed Date:</label> 
		                <div class="input-group date" id="completedDatePicker">
		                	<input id="completedDate" type="text" class="form-control" name="completedDate"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>	
				<div class="col-lg-3 col-md-offset-3">
					<button id="addSystemsNode" type="button" class="btn btn-info btn-lg btn-block">Add systems Node</button>				
				</div>
			</div>
			<div class="row">
				<div class="col-lg-9">
					<table id="systemsTable" class="table table-bordered" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th>Nomenclature</th>
				                <th>Type</th>
				                <th>Technician</th>
				                <th>Promised Date</th>
				                <th>Completed Date</th>
				                <th></th>
				            </tr>
				        </thead>
				        <tbody></tbody>						
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-9">
					<div class="row">
						<div class="col-lg-2"><button id="saveWO" type="button" class="btn btn-warning">Save Workorder</button></div>
					</div>
				</div>
			</div>			
			<input type="hidden" id="userGuid" name="userGuid" value='<c:out value="${sessionScope.USERBEAN.guid}"/>'>
		</form>
	</div>	
	<jsp:include page="system-tree-modal.jsp"></jsp:include>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
</html>