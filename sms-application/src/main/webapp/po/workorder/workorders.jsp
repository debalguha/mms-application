<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Scheduled Work Orders</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/static/js/workorder-scheduled.js"></script>
<script src="${pageContext.request.contextPath}/static/js/workorder-completed.js"></script>
<script src="${pageContext.request.contextPath}/static/js/workorder-inprogress.js"></script>
<script type="text/javascript">
	var scheduledWOList = "${pageContext.request.contextPath}/workOrder/scheduled";
	var inProgressWOList = "${pageContext.request.contextPath}/workOrder/inProgress";
	var completedWOList = "${pageContext.request.contextPath}/workOrder/completed";
	var tabId="${requestScope.param}";
</script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<ul id="workorderTabs" class="nav nav-tabs" role="tablist">
		   <li role="presentation" class="active"><a href="#scheduled-tab" aria-controls="scheduled-tab" role="tab" data-toggle="tab">Scheduled Workorder</a></li>
		   <li role="presentation"><a href="#inprogress-tab" aria-controls="inprogress-tab" role="tab" data-toggle="tab">Existing Workorder</a></li>
		   <li role="presentation"><a href="#completed-tab" aria-controls="completed-tab" role="tab" data-toggle="tab">Completed Workorder</a></li>
		 </ul>	
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="scheduled-tab">
				<div class="row">
					<div class="col-lg-12">
						<table id="workorder-scheduled-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					        <thead>
					            <tr>
					                <!-- <th>System</th> -->
					                <th>Location</th>
					                <th>Customer</th>
					                <th>Open Date</th>
					                <th>Due Date</th>
					                <th>Days left</th>
					                <th>Priority</th>
					                <th>Tech</th>
					                <th>Supervisor</th>
					            </tr>
					        </thead><tbody></tbody>					
						</table>
					</div>
				</div>		
			</div>
			<div role="tabpanel" class="tab-pane fade" id="inprogress-tab">
				<div class="row">
					<div class="col-lg-12">
						<table id="workorder-inprogress-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
					        <thead>
					            <tr>
					                <!-- <th>System</th> -->
					                <th>Location</th>
					                <th>Customer</th>
					                <th>Open Date</th>
					                <th>Due Date</th>
					                <th>Days left</th>
					                <th>Priority</th>
					                <th>Tech</th>
					                <th>Supervisor</th>
					            </tr>
					        </thead>
					        <tbody></tbody>					
						</table>
					</div>
				</div>		
			</div>
			<div role="tabpanel" class="tab-pane fade" id="completed-tab">
				<div class="row">
					<div class="col-lg-12">
						<table id="workorder-completed-table" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
					        <thead>
					            <tr>
					                <!-- <th>System</th> -->
					                <th>Location</th>
					                <th>Customer</th>
					                <th>Open Date</th>
					                <th>Due Date</th>
					                <th>Days left</th>
					                <th>Priority</th>
					                <th>Tech</th>
					                <th>Supervisor</th>
					            </tr>
					        </thead><tbody></tbody>					
						</table>
					</div>
				</div>		
			</div>
		</div>
	</div>	
	<form id="woEditForm" action="${pageContext.request.contextPath}/workOrder/edit">
		<input type="hidden" name="guid" id="guid">
	</form>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
<script>
	var customerListURL = '<c:out value="${pageContext.request.contextPath}"/>/customers/all';
	$(document).ready(function(){
		$('#workorderTabs a[href="#'+tabId+'"]').tab('show');
	});
</script>
</html>