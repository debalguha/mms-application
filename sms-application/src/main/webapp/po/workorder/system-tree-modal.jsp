<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="woTreeModal" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Add System Node</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="row" style="height: 460px; border-color: gray; overflow: scroll;">
							<div id="jstree-systems" style="margin-top:20px;"></div>
						</div>						
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label for="technicianModal">Technician:</label> 
									<select id="technicianModal" name="technicianModal" style="width: 100%">
										<c:forEach items="${applicationScope.USERS}" var="item">
											<option value="${item.guid}">${item.firstName} ${item.lastName}</option>
										</c:forEach>
									</select>
								</div>	
							</div>				
							<div class="col-lg-6">
								<div class="form-group">
									<label for="firstName">Promised Date:</label> 
					                <div class="input-group date" id="promisedDateModalPicker">
					                	<input id="promisedDateModal" type="text" class="form-control" name="promisedDateModal"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>						
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label for="firstName">Completed Date:</label> 
					                <div class="input-group date" id="completedDateModalPicker">
					                	<input id="completedDateModal" type="text" class="form-control" name="completedDateModal"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>						
								</div>
							</div>				
						</div>					
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="addSystemTreeBtn" type="button" class="btn btn-primary">Add System Node</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->