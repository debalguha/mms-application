<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="static/styles/style.css" />
<title>TRS</title>
</head>

<body class="dashboard">
	<div class="wrapper">
		<header class="dashboard">
		<div class="top-bar">&nbsp;</div>
		<div class="container">
			<nav>
			<ul>
				<li><a href="#">User Menu</a></li>
				<li><a href="#">Admin Menu</a></li>
				<li><a href="#">Profile</a></li>
				<li><a href="#">Site Map</a></li>

			</ul>
			</nav>
			<div class="header-right">
				<ul>
					<li><a href="#">Logout</a></li>
					<li>
						<div class="search">
							<input type="text" name="search" placeholder="Search" /><input
								type="submit" value="Go" name="submit" />
						</div>
					</li>
					<li><div class="flag">
							<img src="static/images/flag.png" />
						</div></li>

				</ul>

			</div>
		</div>
		</header>
		<div id="content">
			<div class="container">
				<h2>Welcome back, Thomas Roberts.</h2>
				<p>Information describing what the user can do on the Dashboard
					screen will go here.</p>

				<div class="user-nav">
					<ul>

						<li><a href="#">
								<div class="icon">
									<img src="static/images/user-menu-icon.png" />
								</div>

								<h3>User Menu</h3>
								<h5>Normal User Options</h5>
						</a></li>
						<li><a href="#">
								<div class="icon">
									<img src="static/images/admin-menu-icon.png" />
								</div>

								<h3>Admin Menu</h3>
								<h5>Administrative Options</h5>
						</a></li>
						<li><a href="#">
								<div class="icon">
									<img src="static/images/profile-icon.png" />
								</div>

								<h3>Profile</h3>
								<h5>User Information</h5>
						</a></li>
						<li><a href="#">
								<div class="icon">
									<img src="static/images/contact-icon.png" />
								</div>

								<h3>Contact Support</h3>
								<h5>Technical Support Options</h5>
						</a></li>
						<li><a href="#">
								<div class="icon">
									<img src="static/images/site-map-icon.png" />
								</div>

								<h3>Site Map</h3>
								<h5>Website Pages List</h5>
						</a></li>

					</ul>
				</div>
			</div>
		</div>
		<footer>
			<div class="flogo">
				<img src="static/images/footer-logo.png" />
			</div>
			<div class="copyright">© 2015 TRS INTERNATIONAL GROUP – Defense
				Consulting, Intelligence and Training For Real World Environments.
				All Rights Reserved</div>
		</footer>

	</div>

</body>
</html>
