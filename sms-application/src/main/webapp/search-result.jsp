<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Search Result</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/search-result.js"></script>
<script type="text/javascript">
	var searchTableURL = "${pageContext.request.contextPath}/search.do?searchTerm=${requestScope.searchTerm}";
</script>
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<c:if test="${not empty requestScope.statusMsg}">
			<div class="row">
				<div class="col-lg-9 col-md-offset-1">
					<div class="alert alert-successalert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<c:out value="${requestScope.statusMsg}"/>
					</div>
				</div>
			</div>
		</c:if>
		<c:if test="${not empty requestScope.errMsg}">
			<div class="row">
				<div class="col-lg-9 col-md-offset-1">
					<div class="alert alert-warning alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<c:out value="${requestScope.errMsg}"/>
					</div>
				</div>
			</div>
		</c:if>			
		<div class="row">
			<div class="col-md-9 col-md-offset-1">
				<div style="max-width: 100%; overflow: auto;">
					<table id="searchTable" width="100&" class="table table-bordered">
						<thead>
							<tr><td>GUID</td><td>Type</td><td>Description</td><td>Created On</td></tr>
						</thead>
						<tbody>
							<%-- <c:forEach items="${requestScope.resultList}" var="item">
								<tr><td><c:out value="${item[0]}"/></td><td><c:out value="${item[1]}"/></td><td><c:out value="${item[2]}"/></td><td><c:out value="${item[3]}"/></td></tr>
							</c:forEach> --%>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<form id="serachNavigationForm" action="${pageContext.request.contextPath}/search-navigation.do" method="get">
		<input type="hidden" id="guid" name="guid">
		<input type="hidden" id="type" name="type">
		<input type="hidden" id="searchTerm" name="searchTerm" value="${requestScope.searchTerm}">
	</form>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>	
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>
</body>
</html>