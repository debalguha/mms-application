<!-- /.modal -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="now" value="<%=new java.util.Date()%>" />
<div id="inventoryModalForPart" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Inventory</h4>
			</div>
			<div id="inventoryModalForPart-body" class="modal-body" style="overflow: auto; max-height: 600px;">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Quantity On Hand:</label> <input
								type="text" class="form-control" id="quantityOnHand" name="quantityOnHand">
						</div>					
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Low Quantity Threshold:</label> <input
								type="text" class="form-control" id="lowQuantityThreshold" name="lowQuantityThreshold">
						</div>					
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Standard Order Quantity:</label> <input
								type="text" class="form-control" id="standardOrderQuantity" name="standardOrderQuantity">
						</div>					
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Id Code:</label> <input
								type="text" class="form-control" id="invIdCode" name="invIdCode">
						</div>					
					</div>
				</div>		
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Adjustment Quantity:</label> <input
								type="text" class="form-control" id="adjustmentQuantity" name="adjustmentQuantity">
						</div>					
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Verified Quantity:</label> <input
								type="text" class="form-control" id="verifiedQuantity" name="verifiedQuantity">
						</div>					
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Location:</label>
							<select id="location" name="location" class="form-control" placehloder="location">
								<c:forEach items="${applicationScope.LOCATIONS}" var="item">
									<option value="${item.guid}">${item.locationName}</option>
								</c:forEach>
	
							</select>
						</div>					
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Adjustment Date:</label> 
							<input id="adjustmentDate" readonly="readonly" type="text" class="form-control" name="deliveryByDate" value='<fmt:formatDate pattern="MM/dd/yyyy" value="${now}" />'/>
						</div>					
					</div>
				</div>		
				<div class="row">
					<div class="col-md-10">
						<div class="form-group">
							<label for="phone">Remark:</label>
							<textarea class="form-control" id="remarks" name="remarks" rows="5"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button id="addInventory" type="button" class="btn btn-primary">Add To Part</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
openInventoryModal = function(elmSrc){
	var partGuidVal = $(elmSrc).closest('div').find('#partGuid').val();
	if(partGuidVal != null && partGuidVal != ''){
		$.getJSON(inventoryQueryURL+$('#partGuid').val(), function(data){
			if(data != null && data.guid.length>0){
				$('#quantityOnHand').val(data.quantityOnHand);
				$('#lowQuantityThreshold').val(data.lowQuantityThreshold);
				$('#standardOrderQuantity').val(data.standardOrderQuantity);
				$('#verifiedQuantity').val(data.verifiedQuantity);
				$('#adjustmentQuantity').val(data.adjustmentQuantity);
				$('#adjustmentDate').prop('readonly', false);
				$('#adjustmentDate').val(data.adjustmentDate);
				$('#adjustmentDate').prop('readonly', true);
				$('#remarks').val(data.remarks);
				$('#invIdCode').val(data.idCode);
			}
			$('#inventoryModalForPart').modal('toggle');
		});
	}else
		$('#inventoryModalForPart').modal('toggle');
}
</script>