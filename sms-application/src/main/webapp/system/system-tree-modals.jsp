<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div id="drawingModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Add Drawings</h4>
			</div>
			<div class="modal-body">
				<form id="addDrawingForm">
					<div style="max-height: 500px; overflow: scroll;">
						<table id="addDrawingTable" class="table table-hover">
						  <thead><tr><td>File Name</td><td>Date Uploaded</td><td>Number of Pages</td><td>Uploader</td><td></td></tr></thead>
						  <tbody></tbody>
						</table>
					</div>
				</form>			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="addDrawingsBtn" type="button" class="btn btn-primary">Add Drawings</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="pictureModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Add Pictures</h4>
			</div>
			<div class="modal-body">
				<form id="addPictureForm">
					<div style="max-height: 500px; overflow: scroll;">
						<table id="addPictureTable" class="table table-hover">
						  <thead><tr><td>File Name</td><td>Date Uploaded</td><td>Number of Pages</td><td>Uploader</td><td></td></tr></thead>
						  <tbody></tbody>
						</table>
					</div>
				</form>			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="addPicturesBtn" type="button" class="btn btn-primary">Add Pictures</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="componentTypeModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Choose Component Type</h4>
			</div>
			<div class="modal-body">
				<div class="center" style="max-height: 500px; margin-left: 40px; margin-right: 40px;">
					<table class="table table-hover table-striped">
					  <tbody>
					  	<tr><td><input type="radio" id="contractChoice" name="addChoice" value="CONTRACT"></td><td style="margin-left: 10px;">Add a Contract Element</td></tr>
					  	<tr><td><input type="radio" id="partChoice" name="addChoice" value="PART"></td><td style="margin-left: 10px;">Add a Part Element</td></tr>
					  	<tr><td><input type="radio" id="serviceChoice" name="addChoice" value="SERVICE"></td><td style="margin-left: 10px;">Add a Service Element</td></tr>
					  	<tr><td><input type="radio" id="lineItemChoice" name="addChoice" value="LINEITEM"></td><td style="margin-left: 10px;">Add a Line Item</td></tr>
					  </tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button id="openSubModalForAdd" type="button" class="btn btn-primary">Proceed</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="genericModalForAdd" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Add</h4>
			</div>
			<div id="genericModalForAdd-body" class="modal-body" style="overflow: auto; max-height: 600px;">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button id="closeAndSaveModal" type="button" class="btn btn-primary">Proceed</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="wbsTreeModal" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Add WBS Node</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="row" style="height: 460px; border-color: gray; overflow: scroll;">
							<div id="jstree-wbs" style="margin-top:20px;"></div>
						</div>						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="addWbsTreeBtn" type="button" class="btn btn-primary">Add WBS Node</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>	