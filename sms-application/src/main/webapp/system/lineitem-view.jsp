<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <script src="${pageContext.request.contextPath}/static/js/validators/lineItem-validator.js"></script>
				<div id="lineItem-view" style="display:none;">
					<form id="lineItemForm">
						<div class="row">
							<div id="vehicleStatusForLineItem" class="col-md-12"></div>
						</div>					
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="country">Nomenclature:</label> <input
										type="text" class="form-control" id="nomenclature" name="nomenclature" data-op="${param.op}"
										placeholder="Nomenclature">
								</div>					
								<div class="form-group">
									<label for="country">Line Item:</label> <input
										type="text" class="form-control" id="lineItem" name="lineItem" data-op="${param.op}"
										placeholder="Line Item">
								</div>
								<div class="form-group">
									<label for="email">ID Code:</label> <input
										type="text" class="form-control" id="idCode" name="idCode" data-op="${param.op}"
										placeholder="ID Code">
								</div>
								<div class="form-group">
									<label for="phone">Quantity:</label> 
									<input type="number" class="form-control" id="quantity" name="quantity" data-op="${param.op}" placeholder="Quantity" onblur="calculateExtendedPriceIfPossibleNew(this)">
								</div>
								<div class="form-group">
									<label for="phone">Unit Price:</label> 
									<div class="input-group">
										<span class="input-group-addon" id="unitPriceAddOn"></span>
										<input type="number" class="form-control" id="unitPrice" name="unitPrice" data-op="${param.op}" placeholder="Unit Price" onblur="calculateExtendedPriceIfPossibleNew(this)">
									</div>
								</div>								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="country">Item Type:</label> <input type="text" class="form-control" data-op="${param.op}" disabled="disabled" value="Line Item">
								</div>							
								<div class="form-group">
									<label for="phone">Service Line Item Type</label> 
									<select class="form-control" id="lineItemType" name="lineItemType" style="width: 100%" data-op="${param.op}">
										<c:forEach items="${requestScope.LINE_ITEM_TYPES}" var="item">
											<option value="${item.guid}"><c:out value="${item.lineItemType}"></c:out></option>
										</c:forEach>
									</select>
								</div>
								<div class="form-group">
									<label for="phone">Extended Price</label>
									<div class="input-group">
										<span class="input-group-addon" id="extendedPriceAddOn"></span>								
										<input type="text" step="0.01" class="form-control" id="extendedPrice" name="extendedPrice" data-op="${param.op}" readonly="readonly" placeholder="Extended Price">
									</div>
								</div>
								<div class="form-group">
									<label for="phone">Common Currency Price:</label> 
									<div class="input-group">
										<span class="input-group-addon" id="commonCurrencyPriceAddOn"></span>
										<input type="text" step="0.01" class="form-control" id="commonCurrencyPrice" name="commonCurrencyPrice" data-op="${param.op}" readonly="readonly" placeholder="Common Currency Price">
									</div>
								</div>
								<div class="form-group">
									<label for="phone">Common Current Target Price:</label> 
									<div class="input-group">
										<span class="input-group-addon" id="commonCurrentTargetPriceAddOn"></span>
										<input type="text" class="form-control" id="commonCurrentTargetPrice" name="commonCurrentTargetPrice" data-op="${param.op}" readonly="readonly" placeholder="Common Current Target Price">
									</div>
								</div>								
							</div>
						</div>
						<input type="hidden" name="guid" id="lineItemGuid">
						<button id="lineItemSubmit" type="button" class="btn btn-default active">Save</button>
					</form>
				</div>	
				<script src="${pageContext.request.contextPath}/static/js/system/line_item.js"></script>
				<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
				<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />				