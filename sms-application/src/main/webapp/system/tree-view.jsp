<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>System-tree</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/jquery-ui-1.11.4.smoothness/jquery-ui.theme.min.css">
 <link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
 <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">	
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/jstree-themes/proton/style.css">	
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/simple-demo.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">	

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>	
<script src="${pageContext.request.contextPath}/static/jquery-ui-1.11.4.smoothness/jquery-ui.min.js"></script>
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootbox.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jstree.js"></script>
<script src="${pageContext.request.contextPath}/static/js/maintain-systems.js"></script>
<script src="${pageContext.request.contextPath}/static/js/system_tree.js"></script>		
<script src="${pageContext.request.contextPath}/static/js/jquery.file.download.js"></script>	
<script src="${pageContext.request.contextPath}/static/js/demo.js"></script>
<script src="${pageContext.request.contextPath}/static/js/dmuploader.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>	
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/additional-methods.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery.number.min.js"></script>
<style type="text/css">
.modal:nth-of-type(even) {
    z-index: 1042 !important;
}
.modal-backdrop.in:nth-of-type(even) {
    z-index: 1041 !important;
}
</style>
</head>
<script type="text/javascript">
	var treeURL = '${pageContext.request.contextPath}/system/tree/view';
	var preSelectedGuid = '${requestScope.guid}';
	var customerSelectURL = "${pageContext.request.contextPath}/customers/customer/";
	var pageContextRoot = "${pageContext.request.contextPath}/";
// 	var contentProviderURL = "http://localhost:80/uploads/";
	var contentProviderURL = "http://${pageContext.request.serverName}:8080/";
	var inventoryQueryURL = "${pageContext.request.contextPath}/inventory/part/";
	var iframeViewerURL = contentProviderURL+'/ViewerJS/../';
</script>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="row" style="height: 460px; border-color: gray; overflow: scroll;">
					<div id="jstree-systems" style="margin-top:20px;"></div>
				</div>
				<div id="button-view" class="row" style="margin-top:5px;">
					<div class="col-md-3"><button id="showDrawings" type="button" class="btn btn-default active">Drawings</button></div>
					<div class="col-md-3"><button id="showPics" type="button" class="btn btn-default active">Pictures</button></div>
					<div class="col-md-3"><button id="addChoice" type="button" class="btn btn-default active" data-toggle="modal" data-target="#componentTypeModal">Add Component</button></div>
				</div>
			</div>
			<div class="col-md-9">
				<jsp:include page="part-view.jsp?op=view"></jsp:include>
				<jsp:include page="drawing-view.jsp?op=view"></jsp:include>
				<jsp:include page="picture-view.jsp?op=view"></jsp:include>
				<jsp:include page="contract-view.jsp?op=view"></jsp:include>
				<jsp:include page="lineitem-view.jsp?op=view"></jsp:include>
				<jsp:include page="service-data-view.jsp?op=view"></jsp:include>
				<jsp:include page="vendors-right-view.jsp?op=view"></jsp:include>
			</div>
		</div>
	</div>
	<jsp:include page="system-tree-modals.jsp"></jsp:include>
	<jsp:include page="inventory-modal-for-part.jsp"></jsp:include>
<!-- 		<form id="linkForm" method="POST">
			<input type="hidden" name="rfqGuid" id="rfqGuid">
			<input type="hidden" name="nav-link" id="nav-link">
		</form>	 -->
	<jsp:include page="/insecure/footer.jsp"></jsp:include>	
	<!-- <div id="demoLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
		<div id="lightbox-viewer" class='lightbox-content'></div>
	</div> -->
	<div id="demoLightbox" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
		<div class='lightbox-content'>
			<img src="http://localhost:9080/IMG_0080.JPG">
			<div class="lightbox-caption"><p>Your caption here</p></div>
		</div>
	</div>	
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
<script type="text/javascript">
var contractDatePicker;
    $(function () {
    	contractDatePicker = $('#contractDatePicker').datetimepicker({format : 'MM/DD/YYYY'});
    });
    var inventory ;
    var globalViewId='';
    $(document).ready(function(){
    	$('#addWbsTreeBtn').click(function(){
    		$('input#wbsTree[data-modal="popup"]').val(chosenGuid);
    		$('input#wbsText[data-modal="popup"]').val(chosenText);
    		$('#wbsTreeModal').modal('toggle');
    		console.log($('input#wbsText[data-modal="popup"]').val());
    	});
    });
</script>
</html>
