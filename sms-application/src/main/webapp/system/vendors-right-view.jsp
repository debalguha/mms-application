					<div id="vendors-view" style="display: none;">
						<form id="vendor-part-form">
							<div class="form-group">
								<label for="vendors">Vendors:</label> 
								<select class="form-control" id="currentVendors" name="vendors" multiple="multiple"></select>
								<button id="vendorRemoved" class="btn btn-default active" style="margin-top: 5px;">Remove</button>
							</div>
						</form>
						<div class="form-group">
							<form id="vendor-form">
								<label for="allVendors">All Vendors</label> 
								<select class="form-control" id="allVendors" name="allVendors" multiple="multiple"></select>							
								<button id="addVendor" class="btn btn-default active" style="margin-top: 5px;">Add</button>
								<button id="viewVendor" class="btn btn-default active" style="margin-top: 5px;">View</button>
							</form>
						</div>
					</div>