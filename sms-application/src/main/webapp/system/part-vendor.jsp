<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>vendor-details</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vendor-details.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<style type="text/css">
.row.vertical-divider {
  overflow: hidden;
}
.row.vertical-divider > div[class^="col-"] {
  text-align: center;
  padding-bottom: 100px;
  margin-bottom: -100px;
  border-left: 3px solid #F2F7F9;
  border-right: 3px solid #F2F7F9;
}
.row.vertical-divider div[class^="col-"]:first-child {
  border-left: none;
}
.row.vertical-divider div[class^="col-"]:last-child {
  border-right: none;
}
</style>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<ul class="nav nav-tabs" role="tablist">
		   <li role="presentation" class="active"><a href="#part-tab" aria-controls="part-tab" role="tab" data-toggle="tab">System Part</a></li>
		   <li role="presentation"><a href="#vendor-tab" aria-controls="vendor-tab" role="tab" data-toggle="tab">System Vendor</a></li>
		 </ul>	
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="part-tab">
				<div class="row" style="padding-top: 10px;">
					<div class="col-lg-9">
						<div class="row">
							<form id="partForm" action="#" method="get">
								<div class="col-md-6">
									<div class="form-group">
										<label for="country">Item Type:</label> <input type="text" class="form-control" disabled="disabled" value="Part Data">
									</div>							
									<div class="form-group">
										<label for="country">Nomenclature:</label> 
										<input type="text" class="form-control" id="nomenclature" name="nomenclature" placeholder="Nomenclature" readonly="readonly" value="${requestScope.systemPart.systemTree.nomenclature}">
									</div>
									<div class="form-group">
										<label for="assemblyNumber">Assembly Number:</label> 
										<input type="text" class="form-control" id="assemblyNumber" name="assemblyNumber" placeholder="Assembly Number" value="${requestScope.systemPart.assemblyNumber}">
									</div>
									<div class="form-group">
										<label for="lastName">Assembly Name:</label> 
										<input type="text" class="form-control" id="assemblyName" name="assemblyName" placeholder="Assembly Name" value="${requestScope.systemPart.assemblyName}">
									</div>
									<div class="form-group">
										<label for="phone">Barcode:</label> 
										<input type="text" class="form-control" id="barCode" name="barCode" placeholder="Barcode" value="${requestScope.systemPart.barCode}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="phone">Manufacturer Part Number:</label> 
										<input type="text" class="form-control" id="manufacturerPartNumber" name="manufacturerPartNumber" placeholder="Manufacturer Part Number" value="${requestScope.systemPart.manufacturerPartNumber}">
									</div>
									<div class="form-group">
										<label for="phone">Manufacturer:</label> 
										<input type="text" class="form-control" id="manufacturer" name="manufacturer" placeholder="Manufacturer" value="${requestScope.systemPart.manufacturer}">
									</div>
									<div class="form-group">
										<label for="phone">Manufacturer Part URL:</label> 
										<input type="text" class="form-control" id="manufacturerPartURL" name="manufacturerPartURL" placeholder="Manufacturer Part URL" value="${requestScope.systemPart.manufacturerPartURL}">
									</div>
									<div class="form-group">
										<label for="phone">Last Order URL:</label> 
										<input type="text" class="form-control" id="lastOrderURL" name="lastOrderURL" placeholder="Last Order URL" value="${requestScope.systemPart.lastOrderURL}">
									</div>
									<input type="hidden" name="guid" id="partGuid" value="${requestScope.systemPart.guid}">
								</div>
							</form>
						</div>					
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="vendor-tab">
				<div class="row" style="padding-top: 10px;">
					<div class="col-lg-9">
						<form id="vendorForm" action="${pageContext.request.contextPath}/vendor/save" method="POST">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label for="email">Vendor Type:</label>
										<select id="vendorTypeId" name="vendorType" class="form-control" placehloder="Vendor Type">
											<c:forEach items="${applicationScope.VENDOR_TYPES}" var="item">
												<c:choose>
													<c:when test="${item.guid eq requestScope.vendor.type.guid}">
														<option value="${item.guid}" selected="selected">${item.name}</option>
													</c:when>
													<c:otherwise>
														<option value="${item.guid}">${item.name}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>	
									</div>			
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label for="email">Vendor Location:</label>
										<select id="locationId" name="location" class="form-control" placehloder="Vendor Location">
											<c:forEach items="${applicationScope.LOCATIONS}" var="item">
												<c:choose>
													<c:when test="${item.guid eq requestScope.vendor.location.guid}">
														<option value="${item.guid}" selected="selected">${item.locationName}</option>
													</c:when>
													<c:otherwise>
														<option value="${item.guid}">${item.locationName}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>	
									</div>						
								</div>						
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">First Name:</label> 
										<input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" value="${requestScope.vendor.contact.firstName}">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">Last Name:</label> 
										<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" value="${requestScope.vendor.contact.lastName}">
									</div>				
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">Email:</label> 
										<input type="text" class="form-control" id="email" name="email" placeholder="Email" value="${requestScope.vendor.contact.email}">
									</div>				
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">Phone 1:</label> 
										<input type="text" class="form-control" id="phone1" name="phone1" placeholder="Phone 1" value="${requestScope.vendor.contact.phone1}">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">Phone 2:</label> 
										<input type="text" class="form-control" id="phone2" name="phone2" placeholder="Phone 2" value="${requestScope.vendor.contact.phone2}">
									</div>			
								</div>
							</div>	
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">Address 1:</label> 
										<textarea class="form-control" id="address1" name="address1" placeholder="Address 1" value="${requestScope.vendor.contact.address1}"></textarea>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">Address 2:</label> 
										<textarea type="text" class="form-control" id="address2" name="address2" placeholder="Address 2" value="${requestScope.vendor.contact.address2}"></textarea>
									</div>			
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">City:</label> 
										<input type="text" class="form-control" id="city" name="city" placeholder="City" value="${requestScope.vendor.contact.city}">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">State/Province :</label> 
										<input type="text" class="form-control" id="stateProvince" name="stateProvince" placeholder="State/Province" value="${requestScope.vendor.contact.stateProvince}">
									</div>				
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">Postal Code:</label> 
										<input type="text" class="form-control" id="postalCode" name="postalCode" placeholder="Postal Code" value="${requestScope.vendor.contact.postalCode}">
									</div>				
								</div>
							</div>
						</form>
					</div>			
				</div>
			</div>		
		</div>
	</div>
<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>