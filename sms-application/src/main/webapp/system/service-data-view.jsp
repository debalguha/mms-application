<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="${pageContext.request.contextPath}/static/js/validators/service-validator.js"></script>
				<div id="serviceData-view" style="display:none;">
					<div class="row">
						<div id="vehicleStatusForService" class="col-md-12"></div>
					</div>				
					<div class="row">
						<form id="serviceDataForm">
							<div class="col-md-6">
								<div class="form-group">
									<label for="country">Nomenclature:</label> <input
										type="text" class="form-control" id="nomenclature" name="nomenclature"
										placeholder="Nomenclature">
								</div>					
								<div class="form-group">
									<label for="email">ID Code:</label> <input
										type="text" class="form-control" id="idCodeService" name="idCode"
										placeholder="ID Code">
								</div>
								<div class="form-group">
									<label for="phone">Quantity:</label> <input
										type="number" class="form-control" id="quantityService" name="quantity" onblur="calculateExtendedPriceIfPossibleNew(this)"
										placeholder="Quantity">
								</div>
								<div class="form-group">
									<label for="phone">Unit Price:</label>
									<div class="input-group">
										<span class="input-group-addon" id="unitPriceAddOn"></span>									
										<input type="number" class="form-control" id="unitPriceService" name="unitPrice" onblur="calculateExtendedPriceIfPossibleNew(this)" placeholder="Unit Price">
									</div>
								</div>
								<div class="form-group">
									<label for="phone">Common Current Target Price:</label> 
									<div class="input-group">
										<span class="input-group-addon" id="commonCurrentTargetPriceAddOn"></span>
										<input type="text" class="form-control" id="commonCurrentTargetPriceService" name="commonCurrentTargetPrice" readonly="readonly" placeholder="Common Current Target Price">
									</div>
								</div>								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="country">Item Type:</label>
									<input type="text" class="form-control" disabled="disabled" value="Service Data">
								</div>							
								<div class="form-group">
									<label for="phone">Service Data Type</label> 
									<select class="form-control" id="serviceDataType" name="serviceDataType" style="width: 100%">
										<c:forEach items="${requestScope.SERVICE_DATA_TYPES}" var="item">
											<option value="${item.guid}"><c:out value="${item.serviceType}"></c:out></option>
										</c:forEach>
									</select>
								</div>
								<div class="form-group">
									<label for="phone">Extended Price</label> 
									<div class="input-group">
										<span class="input-group-addon" id="extendedPriceAddOn"></span>									
										<input type="text" class="form-control" id="extendedPriceService" name="extendedPrice" placeholder="Extended Price">
									</div>
								</div>								
								<div class="form-group">
									<label for="phone">Common Currency Price:</label> 
									<div class="input-group">
										<span class="input-group-addon" id="commonCurrencyPriceAddOn"></span>									
										<input type="text" class="form-control" id="commonCurrencyPriceService" name="commonCurrencyPrice" readonly="readonly" placeholder="Common Currency Price" value="0.0">
									</div>
								</div>
							</div>
							<input type="hidden" name="guid" id="serviceDataGuid">
						</form>
					</div>
					<button id="serviceDataSubmit" type="button" class="btn btn-default active">Save</button>
				</div>
				<script src="${pageContext.request.contextPath}/static/js/system/service_data.js"></script>
				<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
				<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
				