<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<script src="${pageContext.request.contextPath}/static/js/validators/part-validator.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/system/part-view.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/wbs-tree-modal.js"></script>
		<script type="text/javascript">
			var wbsTreeURL = "${pageContext.request.contextPath}/wbs/tree/view";
		</script>
				<div id="part-view" style="display:none;">
					<div class="row">
						<form id="partForm" action="#" method="get">
							<div class="col-md-3">
								<div class="form-group">
									<label for="country">Item Type:</label> 
									<input type="text" class="form-control" disabled="disabled" value="Part Data">
								</div>							
								<div class="form-group">
									<label for="country">Nomenclature:</label> 
									<input type="text" class="form-control" id="nomenclature" name="nomenclature" placeholder="Nomenclature" value="${requestScope.PART_EDIT.systemTree.nomenclature}">
								</div>
								<div class="form-group">
									<label for="assemblyNumber">Assembly Number:</label> 
									<input type="text" class="form-control" id="assemblyNumber" name="assemblyNumber" placeholder="Assembly Number" value="${requestScope.PART_EDIT.assemblyNumber}">
								</div>
								<div class="form-group">
									<label for="lastName">Assembly Name:</label> 
									<input type="text" class="form-control" id="assemblyName" name="assemblyName" placeholder="Assembly Name"value="${requestScope.PART_EDIT.assemblyName}">
								</div>
								<div class="form-group">
									<label for="phone">Barcode:</label> 
									<input type="text" class="form-control" id="barCode" name="barCode" placeholder="Barcode" value="${requestScope.PART_EDIT.barCode}">
								</div>
								<div class="form-group">
									<label for="country">CAGE CODE:</label> 
									<!-- <input type="text" class="form-control required" id="country" name="country" placeholder="Country"> -->
									<select class="form-control" id="cageCode" name="cageCode" style="width: 100%">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.CAGE_CODE}" var="item">
											<c:choose>
												<c:when test="${item eq requestScope.PART_EDIT.cageCode}">
													<option value="${item}" selected="selected">${item}</option>
												</c:when>
												<c:otherwise>
													<option value="${item}">${item}</option>						
												</c:otherwise>								
											</c:choose>											
										</c:forEach>
									</select>					
								</div>	
								<div class="form-group">
									<label for="country">Material Type:</label> 
									<!-- <input type="text" class="form-control required" id="country" name="country" placeholder="Country"> -->
									<select class="form-control" data-objName="materialType" id="materialType" name="materialType" style="width: 100%">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.PART_MATERIAL_TYPE}" var="item">
											<c:choose>
												<c:when test="${item.guid eq requestScope.PART_EDIT.materialType.guid}">
													<option value="${item.guid}" selected="selected">${item.materialType}</option>
												</c:when>
												<c:otherwise>
													<option value="${item.guid}">${item.materialType}</option>
												</c:otherwise>								
											</c:choose>												
										</c:forEach>
									</select>					
								</div>
								<div class="form-group">
									<label for="phone">Quantity:</label> 
									<input type="number" class="form-control" id="quantity" name="quantity" placeholder="Quantity" onblur="calculateExtendedPriceIfPossibleNew(this)" value="${requestScope.PART_EDIT.quantity}">
								</div>
								<div class="form-group">
									<label for="phone">Associate WBS:</label> 
									<input type="hidden" class="form-control" id="wbsTree" name="wbsTree" value="${requestScope.PART_EDIT.wbsTree.guid}">
									<input type="text" class="form-control" id="wbsText" name="wbsText" onfocus="openWbsTree();" value="${requestScope.PART_EDIT.wbsTree.nomenclature}">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="phone">Part Number:</label> <input
										type="text" class="form-control" id="manufacturerPartNumber" name="manufacturerPartNumber"
										placeholder="Manufacturer Part Number" value="${requestScope.PART_EDIT.manufacturerPartNumber}">
								</div>
								<div class="form-group">
									<label for="phone">Manufacturer:</label> <input
										type="text" class="form-control" id="manufacturer" name="manufacturer"
										placeholder="Manufacturer" value="${requestScope.PART_EDIT.manufacturer}">
								</div>
								<div class="form-group">
									<label for="phone">Manufacturer Part URL:</label> <input
										type="text" class="form-control" id="manufacturerPartURL" name="manufacturerPartURL"
										placeholder="Manufacturer Part URL" value="${requestScope.PART_EDIT.manufacturer}">
								</div>
								<div class="form-group">
									<label for="phone">Last Order URL:</label> <input
										type="text" class="form-control" id="lastOrderURL" name="lastOrderURL"
										placeholder="Last Order URL" value="${requestScope.PART_EDIT.lastOrderURL}">
								</div>
								<div class="form-group">
									<label for="country">Quote Currency:</label> 
									<select class="form-control" id="quoteCurrency" name="quoteCurrency" style="width: 100%">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.QUOTE_CURRENCY}" var="item">
											<c:choose>
												<c:when test="${item eq requestScope.PART_EDIT.quoteCurrency}">
													<option value="${item}" selected="selected">${item}</option>
												</c:when>
												<c:otherwise>
													<option value="${item}"><c:out value="${item}"></c:out></option>
												</c:otherwise>								
											</c:choose>												
										</c:forEach>
									</select>					
								</div>	
								<div class="form-group">
									<label for="country">Process1:</label> 
									<select class="form-control" id="process1" data-objName="partProcess" name="process1" style="width: 100%">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.PART_PROCESS}" var="item">
											<c:choose>
												<c:when test="${item.guid eq requestScope.PART_EDIT.process1.guid}">
													<option value="${item.guid}" selected="selected">${item.partProcess}</option>
												</c:when>
												<c:otherwise>
													<option value="${item.guid}">${item.partProcess}</option>
												</c:otherwise>								
											</c:choose>												
										</c:forEach>
									</select>					
								</div>	
								<div class="form-group">
									<label for="phone">Quantity Per App:</label> 
									<input type="number" class="form-control" id="quantityPerApplication" name="quantityPerApplication" 
									placeholder="Quantity Per Application" value="${requestScope.PART_EDIT.quantityPerApplication}">
								</div>	
								<div class="form-group">
									<label for="phone">Unit Price:</label> 
									<div class="input-group">
										<span class="input-group-addon" id="unitPriceAddOn"></span>
										<input type="number" class="form-control" id="unitPrice" name="unitPrice" placeholder="Unit Price" onblur="calculateExtendedPriceIfPossibleNew(this)" value="${requestScope.PART_EDIT.unitPrice}">
									</div>
								</div>								
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="phone">OEM:</label> <input
										type="text" class="form-control" id="oem" name="oem"
										placeholder="OEM" value="${requestScope.PART_EDIT.oem}">
								</div>							
								<div class="form-group">
									<label for="phone">OEM Part Number:</label> <input
										type="text" class="form-control" id="oemPartNum" name="oemPartNum"
										placeholder="OEM Part Number" value="${requestScope.PART_EDIT.oemPartNum}">
								</div>	
								<div class="form-group">
									<label for="phone">Sub ID Code:</label> <input
										type="text" class="form-control" id="subIdCode" name="subIdCode"
										placeholder="Sub ID Code" value="${requestScope.PART_EDIT.subIdCode}">
								</div>								
								<div class="form-group">
									<label for="country">UI :</label> 
									<select class="form-control" id="ui" name="ui" style="width: 100%" value="${requestScope.PART_EDIT.ui}">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.UI}" var="item">
											<c:choose>
												<c:when test="${item eq requestScope.PART_EDIT.ui}">
													<option value="${item}" selected="selected">${item}</option>
												</c:when>
												<c:otherwise>
													<option value="${item}">${item}</option>
												</c:otherwise>								
											</c:choose>												
										</c:forEach>
									</select>					
								</div>
								<div class="form-group">
									<label for="country">Category:</label> 
									<select class="form-control" id="category" name="category" style="width: 100%" value="${requestScope.PART_EDIT.category}">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.PART_CATEGORY}" var="item">
											<c:choose>
												<c:when test="${item eq requestScope.PART_EDIT.category}">
													<option value="${item}" selected="selected">${item}</option>
												</c:when>
												<c:otherwise>
													<option value="${item}">${item}</option>
												</c:otherwise>								
											</c:choose>												
										</c:forEach>
									</select>					
								</div>
								<div class="form-group">
									<label for="country">Process2:</label> 
									<select class="form-control" id="process2" data-objName="partProcess" name="process2" style="width: 100%">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.PART_PROCESS}" var="item">
											<c:choose>
												<c:when test="${item.guid eq requestScope.PART_EDIT.process2.guid}">
													<option value="${item.guid}" selected="selected">${item.partProcess}</option>
												</c:when>
												<c:otherwise>
													<option value="${item.guid}">${item.partProcess}</option>
												</c:otherwise>								
											</c:choose>	
										</c:forEach>
									</select>					
								</div>
								<div class="form-group">
									<label for="phone">Next Higher Assembly:</label> 
									<input type="text" class="form-control" id="nextHigherAssemblyText" name="nextHigherAssemblyText" value="${requestScope.PART_EDIT.nextHigherAssembly.nomenclature}" onfocus="openSystemTree(this);">
									<input type="hidden" class="form-control" id="nextHigherAssembly" name="nextHigherAssembly" value="${requestScope.PART_EDIT.nextHigherAssembly.guid}">
								</div>
								<div class="form-group">
									<label for="phone">Extended Price</label>
									<div class="input-group">
										<span class="input-group-addon" id="extendedPriceAddOn"></span>								
										<input type="text" class="form-control" id="extendedPrice" name="extendedPrice" readonly="readonly" placeholder="Extended Price" value="${requestScope.PART_EDIT.extendedPrice}">
									</div>
								</div>								
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="phone">Material Name:</label> 
									<select class="form-control" id="materialName" data-objName="materialName" name="materialName" style="width: 100%">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.PART_MATERIAL_NAME}" var="item">
											<c:choose>
												<c:when test="${item.guid eq requestScope.PART_EDIT.materialName.guid}">
													<option value="${item.guid}" selected="selected">${item.materialName}</option>
												</c:when>
												<c:otherwise>
													<option value="${item.guid}">${item.materialName}</option>
												</c:otherwise>								
											</c:choose>											
										</c:forEach>
									</select>									
								</div>
								<div class="form-group">
									<label for="phone">Material Shape:</label> 
									<input type="text" class="form-control" id="materialShape" name="materialShape" placeholder="Material Shape" value="${requestScope.PART_EDIT.materialShape}">
								</div>
								<div class="form-group">
									<label for="phone">Material Spec:</label> 
									<select class="form-control" id="materialSpec" data-objName="materialSpecification" name="materialSpec" style="width: 100%">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.PART_MATERIAL_SPEC}" var="item">
											<c:choose>
												<c:when test="${item.guid eq requestScope.PART_EDIT.materialSpec.guid}">
													<option value="${item.guid}" selected="selected">${item.materialSpecification}</option>
												</c:when>
												<c:otherwise>
													<option value="${item.guid}">${item.materialSpecification}</option>
												</c:otherwise>								
											</c:choose>												
										</c:forEach>
									</select>										
								</div>
								<div class="form-group">
									<label for="phone">Rev:</label> 
									<input type="text" class="form-control" id="rev" name="rev" placeholder="Rev" value="${requestScope.PART_EDIT.rev}">
								</div>
								<div class="form-group">
									<label for="phone">Sequence:</label> 
									<input type="text" class="form-control" id="sequence" name="sequence" placeholder="Sequence" value="${requestScope.PART_EDIT.sequence}">
								</div>						
								<div class="form-group">
									<label for="country">Process3:</label> 
									<!-- <input type="text" class="form-control required" id="country" name="country" placeholder="Country"> -->
									<select class="form-control" id="process3" data-objName="partProcess" name="process3" style="width: 100%">
										<option value="">----None----</option>
										<c:forEach items="${requestScope.PART_PROCESS}" var="item">
											<c:choose>
												<c:when test="${item.guid eq requestScope.PART_EDIT.process3.guid}">
													<option value="${item.guid}" selected="selected">${item.partProcess}</option>
												</c:when>
												<c:otherwise>
													<option value="${item.guid}">${item.partProcess}</option>
												</c:otherwise>								
											</c:choose>	
										</c:forEach>
									</select>					
								</div>
								<div class="form-group">
									<label for="phone">Common Current Target Price:</label> 
									<div class="input-group">
										<span class="input-group-addon" id="commonCurrentTargetPriceAddOn"></span>
										<input type="text" class="form-control" id="commonCurrentTargetPrice" name="commonCurrentTargetPrice" readonly="readonly" placeholder="Common Current Target Price" value="${requestScope.PART_EDIT.commonCurrentTargetPrice}">
									</div>
								</div>	
								<div class="form-group">
									<label for="phone">Common Currency Price:</label> 
									<div class="input-group">
										<span class="input-group-addon" id="commonCurrencyPriceAddOn"></span>
										<input type="text" class="form-control" id="commonCurrencyPrice" name="commonCurrencyPrice" readonly="readonly" placeholder="Common Currency Price" value="${requestScope.PART_EDIT.commonCurrencyPrice}">
									</div>
								</div>	
							</div>
							<input type="hidden" name="guid" id="partGuid" value="${requestScope.PART_EDIT.guid}">
						</form>
					</div>
					<button id="partSubmit" type="button" class="btn btn-default active">Save</button>
					<button id="createInventory" type="button" class="btn btn-default active" onclick="openInventoryModal(this);">Create Inventory</button>
					<%-- <jsp:include page="/system/wbs-tree-modal.jsp"/> --%>
				</div>	