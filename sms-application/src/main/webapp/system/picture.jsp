
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pictures</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/jquery-ui-1.11.4.smoothness/jquery-ui.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/jquery-contextmenu-2.0.0/jquery.contextMenu.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/jstree-themes/proton/style.css">

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css">

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/static/jquery-ui-1.11.4.smoothness/jquery-ui.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootbox.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery.file.download.js"></script>
<script src="${pageContext.request.contextPath}/static/js/picture.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jstree.js"></script>
<script src="${pageContext.request.contextPath}/static/js/wbs-tree-modal.js"></script>
<script src="${pageContext.request.contextPath}/static/jquery-contextmenu-2.0.0/jquery.ui.position.js"></script>
<script src="${pageContext.request.contextPath}/static/jquery-contextmenu-2.0.0/jquery.contextMenu.js"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>

<script>
	var wbsTreeURL = "${pageContext.request.contextPath}/wbs/tree/view";
</script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
<!-- 		<h3>Double click to download file.</h3> -->
		<c:if test="${not empty requestScope.statusMsg}">
			<div class="row">
				<div class="col-lg-4">
					<p class="bg-success">
						<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
						<c:out value="${requestScope.statusMsg}"/>
					</p>
				</div>
			</div>
		</c:if>	
		<c:if test="${not empty requestScope.errorMsg}">
			<div class="row">
				<div class="col-lg-4">
					<p class="bg-warning">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						<c:out value="${requestScope.errorMsg}"/>
					</p>
				</div>
			</div>
		</c:if>	
		<div class="row">
			<div class="col-lg-12">
				<table id="picture-table" class="table table-striped table-bordered" cellspacing="0" width="100%" style="cursor:pointer">
			        <thead>
			            <tr>
			                <th>File Name</th>
			                <th>Date Created</th>
			                <th></th>
			            </tr>
			        </thead>
			        <tbody></tbody>				
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<button class="btn btn-warning active" id="deletePicture">Delete Pictures</button>
			</div>
		</div>		
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
	<form id="pictureDeleteForm" action="${pageContext.request.contextPath}/pictures/delete">
		<input type="hidden" name="selectedGuids" id="selectedGuids">
	</form>
	<form id="pictureRenameForm" action="${pageContext.request.contextPath}/pictures/rename">
		<input type="hidden" name="pictureGuid" id="pictureGuid">
		<input type="hidden" name="newName" id="newName">
	</form>
	<form id="associationForm" action="${pageContext.request.contextPath}/wbs/tree/picture/associate" method="post">
		<input type="hidden" name="systemPictureGuid" id="systemPictureGuid">
		<input type="hidden" name="wbsGuid" id="wbsGuid">
	</form>
	<div class='my-menu' id="contextMenu"></div>
	<jsp:include page="/system/wbs-tree-modal.jsp"/>	
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
<script>
	var pictureListURL= '<c:out value="${pageContext.request.contextPath}"/>/pictures/all';
	var pictureDownloadURL = '<c:out value="${pageContext.request.contextPath}"/>/pictures/download/';
</script>
</html>
