<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Hello</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="static/styles/custom.css">
<link rel="stylesheet"
	href="static/styles/jstree-themes/proton/style.css">	
<link rel="stylesheet"
	href="static/jquery-fileupload/css/jquery.fileupload.css">		
<script
	src="static/js/jquery-1.11.3.min.js"></script>	
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script
	src="static/js/jstree.js"></script>
<script
	src="static/js/maintain-systems.js"></script>	
<script src="static/jquery-fileupload/js/vendor/jquery.ui.widget.js"></script>	
<script
	src="static/jquery-fileupload/js/jquery.fileupload.js"></script>

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
	        <div id="navbar" class="navbar-collapse collapse">
	          <form class="navbar-form navbar-right">
	            <div class="form-group">
			      <input type="text" class="form-control" id="searchText" aria-describedby="inputSuccess3Status" placeholder="Search">
			      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>	              
	            </div>	            	           
	          </form>
	        </div><!--/.navbar-collapse -->
	     	</div>			
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-md-5" style="height: 460px; border-color: gray; overflow: scroll;">
				<div id="jstree-systems" style="margin-top:20px;"></div>
			</div>
			<div class="col-md-5 col-md-push-1">
				<form action="#" method="get">
					<div id="part-form" style="display:none;">
						<div class="form-group">
							<label for="assemblyNumber">Assembly Number:</label> <input
								type="text" class="form-control" id="assemblyNumber" name="assemblyNumber"
								placeholder="Assembly Number">
						</div>
						<div class="form-group">
							<label for="email">WBS Root:</label> <input
								type="text" class="form-control" id="wbsRoot" name="wbsRoot"
								placeholder="WBS Root">
						</div>
						<div class="form-group">
							<label for="lastName">Assembly Name:</label> <input
								type="text" class="form-control" id="assemblyName" name="assemblyName"
								placeholder="Assembly Name">
						</div>
						<div class="form-group">
							<label for="phone">Barcode:</label> <input
								type="text" class="form-control" id="barCode" name="barCode"
								placeholder="Barcode">
						</div>
						<div class="form-group">
							<label for="phone">Manufacturer Part#:</label> <input
								type="text" class="form-control" id="manufacturerpart" name="manufacturerpart"
								placeholder="Manufacturer Part">
						</div>					
						<div class="form-group">
		                    <select class="form-control" placehloder="Combo box :)">
		                        <option value="1">Element 1</option>
		                        <option value="2">Element 2</option>
		                        <option value="3">Element 3</option>
		                        <option value="4">Element 4</option>
		                        <option value="5">Element 5</option>
		                        <option value="6">Element 6</option>
		                        <option value="7">Element 7</option>	                        
		                    </select>
						</div>
					</div>	
					<div id="vendors-view" style="display: none;">
						<div class="form-group">
							<label for="vendors">Vendors:</label> 
							<select class="form-control" id="currentVendors" name="vendors" multiple="multiple">								
							</select>
							<button id="vendorRemoved" class="btn btn-default active" style="margin-top: 5px;">Remove</button>
						</div>
						<div class="form-group">
							<label for="allVendors">All Vendors</label> 
							<select class="form-control" id="allVendors" name="allVendors" multiple="multiple">
								<c:forEach items="${VENDORS_ALL}" var="vendor">
									<option value="${vendor.guid}">${vendor.name}</option>
								</c:forEach>
							</select>							
							<button id="addVendor" class="btn btn-default active" style="margin-top: 5px;">Add</button>
							<button id="viewVendor" class="btn btn-default active" style="margin-top: 5px;">View</button>
						</div>
					</div>
				</form>
				<div id="drawing-view" style="display: none;">
					<div class="row">
						<div class="cols-sm-6">
							<div class="form-group">
								<label for="drawingTable">Drawings</label> 
								<table id="drawingTable" class="table table-condensed">
									<tbody></tbody>								
								</table>
							</div>
							<div class="form-group">
								<button id="download" class="btn btn-default active">Download</button>
								<button id="remove" class="btn btn-default active">Remove</button>
								<button id="add" class="btn btn-default active">Add From Existing</button>
							</div>
						    <span class="btn btn-success fileinput-button">
						        <i class="glyphicon glyphicon-plus"></i>
						        <span>Select files</span>
						        <!-- The file input field used as target for the file upload widget -->
						        <input id="fileupload" type="file" name="files">
						    </span>							
						    <div id="progress" class="progress">
						        <div class="progress-bar progress-bar-success"></div>
						    </div>
						    <!-- The container for the uploaded files -->
						    <div id="files" class="files"></div>								
							<!-- <div id="fileuploadDrwingButton" class="form-group">
								<button id="uploadNew" class="btn btn-default active">Upload New</button>
							</div> -->							
						</div>
						<div class="cols-sm-4"></div>
					</div>
				</div>
				<div id="picture-view" style="display: none;">
					<div class="row">
						<div class="cols-sm-6">
							<div class="form-group">
								<label for="pictureTable">pictures</label> 
								<table id="pictureTable" class="table table-condensed" style="min-width: 60px; min-height: 60px;">
									<tbody></tbody>								
								</table>
							</div>
							<div class="form-group">
								<button id="downloadPicture" class="btn btn-default active">Download</button>
								<button id="removePicture" class="btn btn-default active">Remove</button>
								<button id="addPicture" class="btn btn-default active">Add From Existing</button>
							</div>
							<div id="picture-area-div">
							  Drag and Drop Files Here<br />
							  or click to add files using the input<br />
							  <input type="file" name="files" title="Click to add Files" style="display:none;">
							</div>	
							<div id="fileuploadDivPictureButton" class="form-group">
								<button id="uploadNewPicture" class="btn btn-default active">Upload New</button>
							</div>								
						</div>
						<div class="cols-sm-4"></div>
					</div>
				</div>					
			</div>
		</div>
		<div class="row spacer">
			<div class="col-md-9 col-md-push-3">
				<button type="button" class="btn btn-default active">User 
				Menu</button>
				<button type="button" class="btn btn-default active">Site
					Map</button>
				<button type="button" class="btn btn-default active">Contact
					Support</button>
				<button type="button" class="btn btn-default active">My
					Profile</button>
				<button type="button" class="btn btn-default active">Logout</button>							
			</div>
		</div>		
	</div>
</body>
</html>