				<div id="picture-view" style="display: none;">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="pictureTable">pictures</label> 
								<table id="pictureTable" class="table table-condensed table-striped" style="min-width: 60px; min-height: 60px; background-color: #FFFFFD;">
									<tbody></tbody>								
								</table>
							</div>
							<div class="form-group">
								<button id="downloadPicture" class="btn btn-default active">Download</button>
								<button id="removePicture" class="btn btn-default active">Remove</button>
								<button id="addPicture" class="btn btn-default active">Add From Existing</button>
							</div>
							<div id="drag-and-drop-zone-picture" class="uploader">
								<div>Drag &amp; Drop Images Here</div>
								<div class="or">-or-</div>
								<div class="browser">
									<label> <span>Click to open the file Browser</span> <input
										type="file" name="files" title='Click to add Files'>
									</label>
								</div>
							</div>
						</div>
						<!-- <div class="col-md-4">
							<div id="imageViewer" class="thumbnails" style="display:block;">
								<a id="bootBoxLink" class="thumbnail"></a>
							</div>	
						</div> -->
					</div>
				</div>