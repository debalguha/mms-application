<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="${pageContext.request.contextPath}/static/js/mathjs-2.4.0.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/validators/common-validators.js"></script>
<script src="${pageContext.request.contextPath}/static/js/validators/contract-validator.js"></script>
<div id="contract-view" style="display:none;">
	<form id="contractForm">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label for="country">Item Type:</label> <input type="text"
						class="form-control" disabled="disabled" value="Contract">
				</div>
				<div class="form-group">
					<label for="country">Nomenclature:</label> <input type="text"
						class="form-control" id="nomenclature" name="nomenclature"
						placeholder="Nomenclature">
				</div>
				<div class="form-group">
					<label for="country">Country:</label>
					<!-- <input type="text" class="form-control required" id="country" name="country" placeholder="Country"> -->
					<select class="form-control" id="country" name="country"
						style="width: 100%">
						<c:forEach items="${requestScope.COUNTRY}" var="item">
							<option value="${item}"><c:out value="${item}"></c:out></option>
						</c:forEach>
					</select>
				</div>
				<div class="form-group">
					<label for="email">Agency/Company:</label>
					<!-- <input type="text" class="form-control" id="agencyCompany" name="agencyCompany" placeholder="Agency Company"> -->
					<select class="form-control" id="agencyCompany"
						name="agencyCompany" style="width: 100%">
						<c:forEach items="${requestScope.AGENCY_COMPANY}" var="item">
							<option value="${item}"><c:out value="${item}"></c:out></option>
						</c:forEach>
					</select>
				</div>
				<div class="form-group">
					<label for="lastName">Organization:</label>
					<!-- <input type="text" class="form-control" id="organization" name="organization" placeholder="Organization"> -->
					<select class="form-control" id="organization" name="organization"
						style="width: 100%">
						<c:forEach items="${requestScope.ORGANIZATIONS}" var="item">
							<option value="${item}"><c:out value="${item}"></c:out></option>
						</c:forEach>
					</select>
				</div>
				<div class="form-group">
					<label for="phone">Customer/End User:</label> <select
						class="form-control" id="customerEnduser" name="customerEnduser"
						onchange="fireCustomerChange(this);">
						<c:forEach items="${applicationScope.CUSTOMERS}" var="item">
							<option value="${item.customerEndUser}" data-guid="${item.guid}"><c:out
									value="${item.customerEndUser}" /></option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="phone">Contract Currency:</label> 
					<!-- <input type="text" class="form-control" id="contractCurrency" name="contractCurrency" placeholder="Contract Currency"> -->
					<select class="form-control" id="contractCurrency" name="contractCurrency" style="width: 100%">
						<c:forEach items="${requestScope.CONTRACT_CURRENCY}" var="item">
							<option value="${item}"><c:out value="${item}"></c:out></option>
						</c:forEach>
					</select>					
				</div>							
				<div class="form-group">
					<label for="phone">Common Currency:</label> 
					<!-- <input type="text" class="form-control" id="commonCurrency" name="commonCurrency" placeholder="Common Currency" pattern="[a-zA-Z]+"> -->
					<select class="form-control" id="commonCurrency" name="commonCurrency" style="width: 100%">
						<c:forEach items="${requestScope.COMMON_CURRENCY}" var="item">
							<option value="${item}"><c:out value="${item}"></c:out></option>
						</c:forEach>
					</select>									
				</div>
				<div class="form-group">
					<label for="exchangeRate">Exchange Rate:</label> 
					<input role="five-digits"
						type="number" class="form-control" id="exchangeRate" name="exchangeRate"
						placeholder="Exchange Rate">
				</div>
				<div class="form-group">
					<label for="phone">Contract Number:</label> <input
						type="text" class="form-control" id="contractNumber" name="contractNumber"
						placeholder="Contract Number">
				</div>
				<div class="form-group">
					<label for="phone">Contract Date:</label> 
					<div class="input-group date" id="contractDatePicker">
						<input id="contractDate" type="text" class="form-control" name="contractDate"/>
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>										
				</div>
				<div class="form-group">
					<label for="phone">Shipping Address:</label> <input
						type="text" class="form-control" id="shippingAddress" name="shippingAddress"
						placeholder="Shipping Address">
				</div>								
			</div>
		</div>
		<input type="hidden" name="guid" id="contractGuid">
		<button id="contractSubmit" type="button" class="btn btn-default active">Save</button>
	</form>
</div>
