<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Part</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/jstree-themes/proton/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/jstree-themes/proton/style.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootbox.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jstree.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jquery.number.min.js"></script>
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<%-- <form id="partForm" action="${pageContext.request.contextPath}/parts/part/update/save" method="post"> --%>
			<jsp:include page="part-view.jsp"/>
		<!-- </form> -->
		<form id="partFormNav" action="${pageContext.request.contextPath}/parts/edit" method="get">
			<input type="hidden" name="partGuid" id="partGuid" value="${requestScope.PART_EDIT.guid}">
		</form>
	</div>
	<jsp:include page="../system/inventory-modal-for-part.jsp"></jsp:include>
<div id="wbsTreeModal" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Add WBS Node</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="row" style="height: 460px; border-color: gray; overflow: scroll;">
							<div id="jstree-wbs" style="margin-top:20px;"></div>
						</div>						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="addWbsTreeBtn" type="button" class="btn btn-primary">Add WBS Node</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>	
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script type="text/javascript">
	var inventory = null;
	var treeURL = '${pageContext.request.contextPath}/system/tree/view';
	var inventoryQueryURL = "${pageContext.request.contextPath}/inventory/part/";
	$(document).ready(function(){
		$('#part-view').css('display', 'block');
		$('#nomenclature').prop("readonly", true);
		$('#partSubmit').unbind('click');
		/* $('#partSubmit').click(function(){
			$('#partForm').submit();
		}); */
		partCallbackAfterDisplay($('#partForm'));
		$('#partSubmit').click(function(){
			var part = new Object();
			$.each($("#partForm").serializeArray(), function(i, item){
				part[item.name] = item.value;
			});
			part.inventory = inventory;
			part.materialName = buildObject($('#materialName'));
			part.materialType = buildObject($('#materialType'));
			part.materialSpec = buildObject($('#materialSpec'));
			part.process1 = buildObject($('#process1'));
			part.process2 = buildObject($('#process2'));
			part.process3 = buildObject($('#process3'));
			part.nextHigherAssembly = $('#nextHigherAssembly').val()==''?null:{guid : $('#nextHigherAssembly').val()};
			console.log(part);
			$.ajax({
				type : 'POST',
				url : '${pageContext.request.contextPath}/parts/part/update/save',
				data : JSON.stringify(part),
				contentType: "application/json; charset=utf-8",
				dataType : 'json'
			}).always(function(data){
				inventory=null;
				if(data.guid != null && data.guid != ''){
					  alert('Part updated!!');
					  $('#partGuid').val(data.guid);
					  $('#partFormNav').submit();
					  //$('#jstree-systems').jstree(true).select_node($("#jstree-systems").jstree(true).get_selected('full',true)[0].original.guid);
					}else
						alert('Part create/update failed!!');
			});			
		});
		$('#addWbsTreeBtn').click(function(){
			$('#wbsTree').val(chosenGuid);
			$('#wbsText').val(chosenText);
			$('#wbsTreeModal').modal('toggle');
			console.log($('#wbsText').val());
		});
	});
</script>
</html>