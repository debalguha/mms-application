<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/styles/custom.css" />
<script
	src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>

<nav class="navbar navbar-default">
	<div class="navbar-header">
		<button class="navbar-toggle" type="button" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>

		<a class="navbar-brand">Project Name</a>
	</div>

	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<li><a id="contactSupport" href="#">Contact Support</a></li>
			<li><a id="myProfile" href="#">My Profile</a></li>
			<li><a id="logout" href="#">Logout</a></li>		
			<li class="dropdown"><a tabindex="0" data-toggle="dropdown" data-submenu> User Menu<span class="caret"></span></a>

				<ul class="dropdown-menu">
					<li id="userMenu"><a tabindex="0">Home</a>
					<li id="userMenu" class="dropdown-submenu"><a tabindex="0">Inventory</a>
						<ul class="dropdown-menu">
							<li><a id="inventory" tabindex="0">Home</a></li>
							<li><a id="inventory" tabindex="0">Inventory List</a></li>
							<li><a id="inventory" tabindex="0">RFQ</a></li>
							<li><a id="inventory" tabindex="0">Purchase Orders</a></li>
							<li><a id="inventory" tabindex="0">Vendors</a></li>
						</ul>
					</li>

					<li class="dropdown-submenu"><a tabindex="0">Another
							action</a>

						<ul class="dropdown-menu">
							<li><a tabindex="0">Sub action</a></li>
							<li><a tabindex="0">Another sub action</a></li>
							<li><a tabindex="0">Something else here</a></li>
						</ul></li>
					<li><a tabindex="0">Something else here</a></li>
					<li class="divider"></li>
					<li><a tabindex="0">Separated link</a></li>
				</ul></li>
			<li class="dropdown"><a tabindex="0" data-toggle="dropdown"
				data-submenu> Dropdown 2<span class="caret"></span>
			</a>

				<ul class="dropdown-menu">
					<li class="dropdown-submenu"><a tabindex="0">Action</a>

						<ul class="dropdown-menu">
							<li><a tabindex="0">Sub action</a></li>
							<li class="dropdown-submenu"><a tabindex="0">Another sub
									action</a>

								<ul class="dropdown-menu">
									<li><a tabindex="0">Sub action</a></li>
									<li><a tabindex="0">Another sub action</a></li>
									<li><a tabindex="0">Something else here</a></li>
								</ul></li>
							<li><a tabindex="0">Something else here</a></li>
						</ul></li>
					<li><a tabindex="0">Another action</a></li>
					<li class="dropdown-submenu"><a tabindex="0">Something
							else here</a>

						<ul class="dropdown-menu">
							<li><a tabindex="0">Sub action</a></li>
							<li><a tabindex="0">Another sub action</a></li>
							<li><a tabindex="0">Something else here</a></li>
						</ul></li>
					<li class="divider"></li>
					<li><a tabindex="0">Separated link</a></li>
				</ul></li>
		</ul>

		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown"><a tabindex="0" data-toggle="dropdown">
					Dropdown 3<span class="caret"></span>
			</a>

				<ul class="dropdown-menu">
					<li><a tabindex="0">Action</a></li>
					<li><a tabindex="0">Another action</a></li>
					<li><a tabindex="0">Something else here</a></li>
					<li class="divider"></li>
					<li><a tabindex="0">Separated link</a></li>
				</ul></li>
		</ul>
	</div>
</nav>