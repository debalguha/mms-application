<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/styles/custom.css" />
<style>
<!--
.modal-overlay {
    /* some styles to position the modal at the center of the page */
    position: fixed;
    top: 50%;
    left: 50%;
    line-height: 200px;
    margin-left: -150px;
    margin-top: -100px;
    background-color: #f1c40f;
    text-align: center;
  
    /* needed styles for the overlay */
    z-index: 10; /* keep on top of other elements on the page */
    outline: 9999px solid rgba(0,0,0,0.5);
}
-->
</style>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
<script src="${pageContext.request.contextPath}/static/js/utils.js"></script>
	<div class="modal-overlay" id="loading" style="display:none;">
		<img src="${pageContext.request.contextPath}/static/images/progress.gif" align="center"/>
	</div>
	<nav class="navbar navbar-default" style="background:rgba(221,201,179,.5);">
		<div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
<%-- 				<c:if test="${sessionScope.USERBEAN.accessLevel.accessLevelName eq 'USER'}"> --%>
<%-- 					<a class="navbar-brand" href="${pageContext.request.contextPath}/home.jsp">TRS Home</a> --%>
<%-- 				</c:if> --%>
				<c:if test="${sessionScope.USERBEAN.accessLevel.accessLevelName eq 'ADMIN'}">
					<a class="navbar-brand" href="${pageContext.request.contextPath}/admin/home.jsp">User Menu</a>
				</c:if>	          
	        </div>		
			<div>
			<ul class="nav navbar-nav">
				<!-- 				<li><a href="#">Site Map</a></li> -->
				<c:if
					test="${sessionScope.USERBEAN.accessLevel.accessLevelName eq 'USER'}">
					<li><a id="userMenu" href="#">User Menu</a></li>
					<li><a id="siteMap" href="#">Site Map</a></li>
				</c:if>
				<c:if
					test="${sessionScope.USERBEAN.accessLevel.accessLevelName eq 'ADMIN'}">
					<li><a id="siteMapAdmin" href="#">Site Map</a></li>
				</c:if>

				<li><a id="contactSupport" href="#">Contact Support</a></li>
				<li><a id="myProfile" href="#">My Profile</a></li>
				<!-- 				<li><a id="siteMap" href="#">Site Map</a></li> -->
				<li><a id="logout" href="#">Logout</a></li>				
				<li><a id="print" href="#" onclick="window.print();">Print</a></li>
			</ul>
		</div>
		
		<div id="navbar" class="navbar-collapse collapse">
	          <form id="searchNavForm" class="navbar-form navbar-right" action="${pageContext.request.contextPath}/search">
	            <div class="form-group">
	            	<div class="input-group">
				      <input name="searchTerm" type="text" class="form-control" id="searchTerm" aria-describedby="inputSuccess3Status" placeholder="Search">
				      <span id="searchSpan" class="input-group-addon"><span class=" glyphicon glyphicon-search"></span></span>
				      <!-- <span class="glyphicon glyphicon-search input-group-addon" aria-hidden="true"></span> -->	            	
	            	</div>
	            </div>	            	           
	          </form>
	        </div><!--/.navbar-collapse -->
	     </div>			
	</nav> 
	<script type="text/javascript">
		$(document).ready(function(){
			$('#searchSpan').click(function(){
				$('#searchNavForm').submit();
			});
		});
	</script>