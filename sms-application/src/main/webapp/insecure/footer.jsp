<form id="linkForm" action="${pageContext.request.contextPath}/navigation" method="get">
	<input type="hidden" name="nav-link" id="nav-link">
</form>
<footer class="footer">
  <div class="container" style="margin: 0">
	<div class="flogo">
		<img src="${pageContext.request.contextPath}/static/images/footer-logo.png" />
		@ 2015 TRS INTERNATIONAL GROUP @�� Defense
		Consulting, Intelligence and Training For Real World Environments.
		All Rights Reserved
	</div>
  </div>
</footer>
<script>
$( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
	var sessionFailed = jqxhr.getResponseHeader('sessionFailed');
	if(sessionFailed !=null && sessionFailed=='TRUE') {
		location.reload();
	}
});
</script>