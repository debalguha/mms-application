/* JS */
var feedbackMove = false;
var copiedNode = null;
buildSystemTree = function() {
	$('#jstree-systems').on('delete_node.jstree', function(e, data){
		console.log('deleting : '+data.node.original);
		$.post(pageContextRoot+'system/tree/delete/'+data.node.original.guid, function(data){
			if(data == 'SUCCESS')
				alert('Element successfully removed.');
			else
				alert('Unable to remove.');
		});
	})
	.on('ready.jstree', function(e, data){
		if(preSelectedGuid!='')
			$('#jstree-systems').jstree(true).select_node(preSelectedGuid);
	})
	.on('rename_node.jstree', function(e, data){
		if(feedbackMove || feedbackRename){
			feedbackMove = false;
			feedbackRename = false;
			return;
		}
		var nodeType = data.node.original.nodeType;
		var guid = data.node.original.guid;
		var newName = data.text
		var oldText = data.old;
		console.log('Info: '+newName+":"+guid+":"+nodeType);
		$.get(pageContextRoot+'system/tree/'+nodeType+'/rename/'+guid+'/to/'+newName, function(data){
			if(data.guid == null || data.guid == '' || data.guid == 'undefined'){
				alert('Rename failed!!');
				feedbackMove = true;
				$("#jstree-systems").jstree(true).rename_node($("#jstree-systems").jstree(true).get_selected('full',true)[0], oldText);
			}
		});
	})
	.on('changed.jstree', function(e, data) {
		console.log(data.node);
		cleanImageViewer();
		var nodeType = data.node.original.nodeType;
		if (nodeType == 'PART')
			populateParts(data.node.original);
		else if (nodeType == 'VENDORS_GROUP') {
			partNode = data.instance.get_node(data.node.parent);
			populateVendors(partNode);
		} else if (nodeType == 'DRAWINGS_GROUP') {
			partNode = data.instance.get_node(data.node.parent);
			populateDrawings(partNode);
		} else if (nodeType == 'PICTURES_GROUP') {
			partNode = data.instance.get_node(data.node.parent);
			populatePictures(partNode);
		} else if (nodeType == 'CONTRACT') {
			populateContract(data.node.original);
		} else if (nodeType == 'LINEITEM') {
			populateLineItem(data.node.original);
		} else if (nodeType == 'SERVICE') {
			populateService(data.node.original);
		}
	}).jstree({
		"core" : {
			"themes" : {
				"name" : "proton",
				"responsive" : true
			},
			'data' : {
				'url' : treeURL
			},
			"check_callback" : true
		},
		"plugins" : [ "contextmenu", "dnd","sort"],
		"contextmenu" : {
			"items" : function($node) {
				return {
					"Copy" : {
						"label" : "Copy",
						"_disabled" : function(obj){
							var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
							console.log(selectedNode);
							if(selectedNode.original.nodeType == 'VENDOR_GROUP'
								|| selectedNode.original.nodeType == 'DRAWING_GROUP'
									|| selectedNode.original.nodeType == 'PICTURE_GROUP')
								return true;
							return false;
						},
						"action" : function(obj){
							copiedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
							//copiedNode.data = copiedNode.original;
							//$("#jstree-systems").jstree(true).copy(copiedNode);
							console.log(copiedNode);
						}
					},
					"Paste" : {
						"label" : "Paste",
						"_disabled" : function(obj){
							var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
							console.log(selectedNode);
							if(selectedNode.original.nodeType == 'VENDOR_GROUP'
								|| selectedNode.original.nodeType == 'DRAWING_GROUP'
									|| selectedNode.original.nodeType == 'PICTURE_GROUP' || copiedNode == null || selectedNode.id=='#')
								return true;
							return false;
						},
						"action" : function(obj){
							var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
							doPasteNode();
						}
					},					
					"Delete" : {
						"label" : "Delete",
						"_disabled" : function(obj){							
							return false;
						},						
						"action" : function(obj) {
							var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
							console.log(selectedNode);
							if(selectedNode.original.nodeType == 'VENDOR_GROUP'
								|| selectedNode.original.nodeType == 'DRAWING_GROUP'
									|| selectedNode.original.nodeType == 'PICTURE_GROUP')
								return;
							$("#jstree-systems").jstree(true).delete_node(selectedNode);
						}
					},
					"rename" : {
						"label" : "Rename",
						"_disabled" : function(obj){	
							var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
							return (selectedNode.original.nodeType == 'VENDOR_GROUP'
								|| selectedNode.original.nodeType == 'DRAWING_GROUP'
									|| selectedNode.original.nodeType == 'PICTURE_GROUP');
						},						
						"action" : function (data) {
							feedbackRename = false;
							var inst = $.jstree.reference(data.reference),
							obj = inst.get_node(data.reference);
							inst.edit(obj);
						}
					},
					"Create" : {
						"label" : "Add component",
						"action" : false,
						"submenu" :{
							"create_contract" : {
			                    "seperator_before" : false,
			                    "seperator_after" : false,
			                    "label" : "Add Contract",
			                    //"icon": "service.png",
			                    action : function (obj) {
			                    	$("#contractChoice").prop('checked', true);
			                    	$('#genericModalForAdd').modal('toggle');
			                    }
			                },
			                "create_service" : {
			                    "seperator_before" : false,
			                    "seperator_after" : false,
			                    "label" : "Add Service",
			                    //"icon": "service.png",
			                    action : function (obj) {
			                    	$("#serviceChoice").prop('checked', true);
			                    	$('#genericModalForAdd').modal('toggle');
			                    }
			                },
			                "create_service_line" : {
			                    "seperator_before" : false,
			                    "seperator_after" : false,
			                    "label" : "Add Line Item",
			                    //"icon": "service.png",
			                    action : function (obj) {
			                    	$("#lineItemChoice").prop('checked', true);
			                    	$('#genericModalForAdd').modal('toggle');
			                    }
			                },
			                "add_part" : {
			                    "seperator_before" : false,
			                    "seperator_after" : false,
			                    "label" : "Add Part",
			                    //"icon": "service.png",
			                    action : function (obj) {
			                    	$("#partChoice").prop('checked', true);
			                    	$('#genericModalForAdd').modal('toggle');
			                    }
			                }
						}
					}
				};
			}
		}
	});
	$('#jstree-systems').bind('move_node.jstree',function(event,data){
		if(feedbackMove){
			feedbackMove = false;
			return;
		}
		console.log(event);console.log(data);
		var oldParentId = data.old_parent;
		var newParentId = data.parent;
		var currentNodeId = data.node.id;
		if(newParentId == oldParentId)
			return;
		var json = {
			'oldParentGuid' : oldParentId != '#' ? $("#jstree-systems").jstree(true).get_node(oldParentId).original.guid: oldParentId,
			'newParentGuid' : newParentId != '#' ? $("#jstree-systems").jstree(true).get_node(newParentId).original.guid : newParentId,
			'currentNodeGuid' : data.node.original.guid,
				
		};
		console.log(json);
		$.ajax({
			type : "POST",
			url : pageContextRoot+'system/tree/move',
			data : json,
			dataType : 'json',
		}).always(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				alert('Unable to move node.');
				feedbackMove = true;
				$("#jstree-systems").jstree(true).move_node(currentNodeId, oldParentId, 'first', function(){return false;});
			}
		});
	});
	$('#jstree-systems').bind('copy_node.jstree',function(event,data){
		var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
		console.log('Paste data: ');
		console.log(data);
		var json = {
			'currentNodeGuid' : getCopiedNode().original.guid,
			'newParentGuid' : selectedNode.original.guid
		};
		$.ajax({
			type : "POST",
			url : pageContextRoot+'system/tree/paste',
			data : json,
			dataType : 'json',
		}).always(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				alert('Unable to copy node.');
				feedbackMove = true;
				//$("#jstree-systems").jstree(true).move_node(currentNodeId, oldParentId, 'first', function(){return false;});
				//$("#jstree-systems").jstree('paste');
			}
		});
		nullifyCopiedNode();
	});
}
doPasteNode = function(){
	var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
	var json = {
		'currentNodeGuid' : getCopiedNode().original.guid,
		'newParentGuid' : selectedNode.original.guid
	};
	$.ajax({
		type : "POST",
		url : pageContextRoot+'system/tree/paste',
		data : json,
		dataType : 'json',
	}).always(function(data){
		if(data.guid == null || data.guid == 'undefined'){
			alert('Unable to copy node.');
			feedbackMove = true;
			//$("#jstree-systems").jstree(true).move_node(currentNodeId, oldParentId, 'first', function(){return false;});
			//$("#jstree-systems").jstree('paste');
		}else{
			$("#jstree-systems").jstree(true).create_node(selectedNode, data, 0);
		}
	});
	nullifyCopiedNode();
}
nullifyCopiedNode = function(){
	copiedNode = null;
}
getCopiedNode = function(){
	return copiedNode;
}
cleanImageViewer = function(){
	$('#bootBoxLink').empty();
	$('#imageViewer').toggle(false);
	
}