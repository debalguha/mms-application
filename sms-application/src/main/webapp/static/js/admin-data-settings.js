/* JS */
$(document).ready(function(){
	$('#data-setting').change(function(){
		fieldTypeOnChange($(this).val());
	});
	$('#addSettingsBtn').click(function(){
		$('#addFieldSettingForm').attr('action', fieldSettingSubmitURL+$('#data-setting').val());
		$('#addFieldSettingForm').submit();
	});
	$('#addFieldsBtn').click(function(){
		if($('#data-setting').val() == null || $('#data-setting').val() == '' || $('#data-setting').val() == 'undefined'){
			alert('Please select a data settings');
			return false;
		}
		$('#settingMoal').modal('toggle');
	});
});
editFieldSetting = function(guid){
	console.log($('#fieldTable tbody').data(guid));
	var setting = $('#fieldTable tbody').data(guid);
	$('#guid').val(setting.guid);
	$('#fieldName').val(setting.fieldName);
	$('#fieldType').val(setting.fieldType.guid);
	$('#required').prop('checked', setting.required==1?true:false);
	$('#active').prop('checked', setting.active==1?true:false);
	$('#settingMoal').modal('toggle');
}

removeSetting = function(guid){
	console.log(guid);
}

fieldTypeOnChange = function(elementVal){
	$('#fieldTable tbody').empty();
	$.getJSON(fieldSettingURL+elementVal).done(function(data){
		if(data != null && data.length>0){
			$.each(data, function(index, item){
				$('#fieldTable tbody').data(item.guid, item);
				$('#fieldTable tbody').append('<tr><td>'+item.fieldName+'</td><td>'+item.fieldType.fieldType+'</td><td>'+item.required+'</td><td>'+item.active+'</td><td>'+item.user.firstName+'</td><td><button class="btn btn-info" type="button" onclick = "editFieldSetting(\''+item.guid+'\')">Edit</button></td><td><button class="btn btn-info" type="button" onclick = "removeSetting(\''+item.guid+'\')">Remove</button></td></tr>');
			});
		}
	});	
}