/* JS */
var chosenGuid;
var chosenText;
buildWbsTree = function(){
	$('#jstree-wbs').on('changed.jstree', function(e, data) {
		console.log(data.node.original);
		chosenGuid = data.node.original.guid;
		chosenText = data.node.original.text;
	}).jstree({
		"core" : {
			"themes" : {
				"name" : "proton",
				"responsive" : true
			},
			'data' : {
				'url' : wbsTreeURL
			},
			"check_callback" : true
		}
	});
}
openWbsTree = function(){
	/*try {
		$('#jstree').jstree(true).destroy();
	} catch(err) {
	    console.log(err.message);
	}*/
	$('#wbsTreeModal').modal('toggle');
}