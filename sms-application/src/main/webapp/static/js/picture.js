/* JS */
var pictureTable;
var selectedGuids = new Map();
$(document).ready(function(){
	pictureTable = $('#picture-table').DataTable( {
		"dom": 'Blfrtip',
        "ajax": pictureListURL,
        "buttons": [ 'print'],
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "storedFileName" },
            { "data": "creationDate" }
        ],
        "columnDefs": [
           {
               "render": function ( data, type, row ) {
            	   return '<div class="checkbox"><label><input name="rowCheck" type="checkbox" onclick="deletePictureEnqueue(this)"></label></div>';
            	   //return '<button type="button" class="btn btn-info btn-sm" data-guid="'+row.guid+'" aria-label="downloadButton" onclick="downloadPicture(this)">Donwload</button>';
               },
               "targets": 2
           }
       ]
    });
	$('#picture-table').on('dblclick', 'tr', function(){
		downloadPicture($(this));
	});	
	$('#picture-table').on('contextmenu', 'tr', function(){
		var $tr = $(this).closest('tr');
		console.log(pictureTable.rows($tr).data()[0])
		if(!$tr.hasClass('selected'))
			$tr.addClass('selected');
		$.contextMenu({
            selector: '#picture-table tr.selected',
            build: function($trigger, e) {
            	return {
            		callback: function(key, options) {
            			if(key == 'rename'){
	            			bootbox.prompt("Please provide new name.", function(result) {                
	            				if (result != null && result.trim().length>0) {                                             
	            					$('#pictureGuid').val(pictureTable.rows($tr).data()[0].guid);
	            					$('#newName').val(result);
	            					$('#pictureRenameForm').submit();
							  	} 
	        				});
            			} else if(key = 'associate'){
            				$('#systemPictureGuid').val(pictureTable.rows($tr).data()[0].guid);
            				openWbsTree();
            			}
                    },
                    items: {
                        "rename": {name: "Rename", icon: "edit"},
                		"associate": {name: "Associate WBS"}
                    }
            	};
            }
             
        });
		$(".my-menu").contextMenu();
	});
	$('#deletePicture').click(function(){
		var guids = seperateByComma();
		if(guids == null || guids.length<=0){
			alert('Please choose a picture first.');
			return;
		}
		$('#selectedGuids').val(guids);
		$('#pictureDeleteForm').submit();
	});	
	$('#wbsTreeModal').on('show.bs.modal', function(e){
		buildWbsTree();
	});
	$('#addWbsTreeBtn').click(function(){
		$('#wbsGuid').val(chosenGuid);
		$('#associationForm').submit();
	});
});	
downloadPicture = function(item){
	var selectedJson = pictureTable.rows($(item).closest('tr')).data()[0];
	$.fileDownload(pictureDownloadURL+selectedJson.guid,
		{preparingMessageHtml : "Your download is on the way, please wait...",
		failMessageHtml : "There was a problem generating your report, please try again."
	});
}
deletePictureEnqueue = function(item){
	var selectedJson = pictureTable.rows($(item).closest('tr')).data()[0];
	 console.log(selectedJson);
	 if(!$(item).prop('checked'))
		 selectedGuids.delete(selectedJson.guid);
	 else
		 selectedGuids.set(selectedJson.guid, selectedJson);	
}
seperateByComma = function(){
	var guids = '';
	 selectedGuids.forEach(function(value, key){
		 console.log(key+'--'+value);
		 guids=guids+key+',';
	 });
	console.log('guids -->'+guids);
	return guids;
}