/* JS */
var drawingListTable;
var selectedGuids = new Map();
$(document).ready(function(){
	drawingListTable = $('#drawing-table').DataTable( {
        "ajax": drawingListURL,
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "storedFileName" },
            { "data": "creationDate" }
        ],
        "columnDefs": [
           {
               "render": function ( data, type, row ) {
            	   return '<div class="checkbox"><label><input name="rowCheck" type="checkbox" onclick="drawingEnqueue(this)"></label></div>';
            	   //return '<button type="button" class="btn btn-info btn-sm" data-guid="'+row.guid+'" aria-label="downloadButton" onclick="downloadPicture(this)">Donwload</button>';
               },
               "targets": 2
           }
       ]
    });
	$('#addDrawings').click(function(){
		$('#wbsDrawingModal').modal('toggle');
	});
	$('#addDrawingsBtn').click(function(){
		var guids = seperateByComma();
		if(guids == null || guids.length<=0){
			alert('Please choose a drawing first.');
			return;
		}
		$('#systemDrawingGuids').val(guids);
		$('#drawingAssociationForm').submit();
	});
});
populateDrawings = function(wbsJson){
	$('#drawingTable tbody').empty();
	$.getJSON(wbsDrawingURL+wbsJson.guid).done(function(data){
		$.each(data.data, function(index, item){
			var drawingRemoveBtn='<a href="#" class="btn btn-xs btn-warning" onclick="removeDrawingAssociation(\''+item.guid+'\');"><span class="glyphicon glyphicon-trash"></span> </a>';
			$('#drawingTable tbody').append('<tr><td>'+item.storedFileName+'</td><td>'+item.creationDate+'</td><td>'+drawingRemoveBtn+'</td></tr>');
		});
	});
	$('#drag-and-drop-zone-drawing').dmUploader({
		url : drawingUploadURLForWbs,
		dataType : 'json',
		extFilter : 'png;giff;bmp;jpg;PDF;pdf;zip;tar;DXF;dxf',
		extraData: {partGuid: $("#jstree-wbs").jstree(true).get_selected('full',true)[0].original.guid},
		onUploadSuccess : function(id, data) {
			var drawingRemoveBtn='<a href="#" class="btn btn-xs btn-warning" onclick="removeDrawingAssociation(\''+data.guid+'\');"><span class="glyphicon glyphicon-trash"></span> </a>';
			$('#drawingTable tbody').append('<tr><td>'+data.storedFileName+'</td><td>'+data.creationDate+'</td><td>'+drawingRemoveBtn+'</td></tr>');
			alert('upload completed successfully');
		},
		onUploadError : function(id, message) {
			alert('Upload failed!!')
		},
		onFileExtError: function(file){
		  console.log('File extension of ' + file.name + ' is not allowed');
		  alert('File extension of ' + file.name + ' is not allowed');
		}
	});		
}
removeDrawingAssociation = function(systemPictureGuid){
	var wbsGuid = $("#jstree-wbs").jstree(true).get_selected('full',true)[0].original.guid;
	$('#systemDrawingGuidsRemove').val(systemPictureGuid);
	$('#wbsGuidDrawingRemove').val(wbsGuid);
	$('#drawingAssociationRemoveForm').submit();
}
drawingEnqueue = function(item){
	var selectedJson = drawingListTable.rows($(item).closest('tr')).data()[0];
	 console.log(selectedJson);
	 if(!$(item).prop('checked'))
		 selectedGuids.delete(selectedJson.guid);
	 else
		 selectedGuids.set(selectedJson.guid, selectedJson);	
}