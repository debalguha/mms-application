/* JS */
var customerTable;
var selectedGuids = new Map();
$(document).ready(function(){
	customerTable = $('#customer-table').DataTable( {
		"dom": 'Blfrtip',
        "ajax": customerListURL,
        "buttons": [ 'print'],
        "columns": [
            { "data": "customerNumber" },
            { "data": "country" },
            { "data": "agencyCompany" },
            { "data": "organization" },
            { "data": "customerEndUser" },
            { "data": "contractNumber" }
        ],
        "columnDefs": [
           {
               "render": function ( data, type, row ) {
            	   return '<div class="checkbox"><label><input name="rowCheck" type="checkbox" onclick="deleteCustomerEnqueue(this)"></label></div>';
               },
               "targets": 6
           }
       ]
    } );
	$('#customer-table tbody').on( 'click', 'tr', function () {
		$(this).toggleClass('selected');
		var aData = customerTable.rows('.selected').data()[0];//oTable.fnGetData(oTable.fnGetPosition(this));
		$('#customerGuid').val(aData.guid);
		$('#custForm').submit();
	});
	$('#deleteCustomer').click(function(){
		var guids = seperateByComma();
		if(guids == null || guids.length<=0){
			alert('Please choose a customer first.');
			return;
		}
		$('#selectedGuids').val(guids);
		$('#customerDeleteForm').submit();
	});	
	$('#customer-table').css('background-color', '#FFFFFD');
});	
deleteCustomerEnqueue = function(item){
	var selectedJson = customerTable.rows($(item).closest('tr')).data()[0];
	 console.log(selectedJson);
	 if(!$(item).prop('checked'))
		 selectedGuids.delete(selectedJson.guid);
	 else
		 selectedGuids.set(selectedJson.guid, selectedJson);	
}
seperateByComma = function(){
	var guids = '';
	 selectedGuids.forEach(function(value, key){
		 console.log(key+'--'+value);
		 guids=guids+key+',';
	 });
	console.log('guids -->'+guids);
	return guids;
}