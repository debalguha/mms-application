/* JS */

$(document).ready(function() {
	$("#systems").click(function() {
		$("#nav-link").val("system/tree-view");		
		$("#linkForm").submit();
	});	
	$("#myProfile").click(function() {
		$("#nav-link").val("user/edit-profile");
		$("#linkForm").submit();
	});
	$("#contactSupport").click(function() {
		$("#nav-link").val("user/contact-support");
		$("#linkForm").submit();
	});
	$("#posWorkOrder").click(function() {
		$("#nav-link").val("user/pos-work-order");
		$("#linkForm").submit();
	});
	$("#maintainWbs").click(function() {
		$("#nav-link").val("user/maintain-wbs");
		$("#linkForm").submit();
	});
});
