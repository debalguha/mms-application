/* JS */
var activated = false;
var deActivated = false;
var oTableInv;
var selectedGuids = new Map();
$(document).ready(function(){
	oTableInv = $('#inventory-table').DataTable( {
		dom: 'Blfrtip',
        ajax: inventoryURL,
        buttons: [ 'print'],
        serverSide: true,
        processing: true,
        columns: [
            { "data": "idCode" },
            { "data": "wbs", "orderable": false },
            { "data": "itemDescription" },
            { "data": "highUnitPrice", "orderable": false },
            { "data": "lowUnitPrice", "orderable": false },
            { "data": "quantityOnHand" },
            { "data": "lowQuantityThreshold" }
        ],
        columnDefs: [
           {
               "render": function ( data, type, row ) {
            	   if(row.vendorAttached)
            		   return '<button type="button" class="btn btn-info btn-sm" data-guid="'+row.guid+'" aria-label="vendorButton" onclick="vendorAction(this)">Vendors</button>';
            	   else
            		   return '';
               },
               "targets": 7
           },
           {
               "render": function ( data, type, row ) {
            	   //return '<button type="button" class="btn btn-warning btn-sm" disabled="disabled">Last Order</button>';
            	   if(row.poAvailable)
            		   return '<button type="button" class="btn btn-default" aria-label="Left Align"><span class="glyphicon glyphicon-new-window" onclick="navigateToPO(\''+row.guid+'\')" aria-hidden="true"></span></button>';
            	   else
            		   return '';
               },
               "targets": 8
           },
           {
               "render": function ( data, type, row ) {           	   
            		   return '<input type="checkbox" name="invSelect" value="'+row.guid+'" onchange="checkBoxAction(this)">';
               },
               "targets": 9
           }
       ]      
    } );
    /*$('#activate').prop('disabled', true);
    $('#deActivate').prop('disabled', true);
    $('#issuePO').prop('disabled', true);
    $('#issueRFQ').prop('disabled', true);*/	
/*    $('#inventory-table').on( 'init.dt', function (){
    	initializeVendorsButton();
    	initializeCheckBoxes();
    });
    $('#inventory-table').on( 'page.dt', function (){
    	initializeVendorsButton();
    	initializeCheckBoxes();
    });*/
	$('#deActivate').click(function(){
		$('#invForm').attr('action', inventoryDeActivateURL);
		$('#selectedGuids').val(seperateByComma());
		$('#invForm').submit();
	});
	$('#activate').click(function(){
		$('#invForm').attr('action', inventoryActivateURL);
		$('#selectedGuids').val(seperateByComma());
		$('#invForm').submit();
	});	
	$('#issueRFQ').click(function(){
		var deActivatedObj = checkDeActivation();
		if(deActivatedObj != null){
			alert(deActivatedObj.itemDescription+" is deactivated. Please activate it first.")
			return false;
		}
		var noVendorObj = checkForVendor();
		if(noVendorObj != null){
			alert('No vendor is supplying "'+noVendorObj.itemDescription+'" inventory. Please add a vendor first.');
			return false;
		}		
		$('#invForm').attr('action', issueRFQURL);
		$('#selectedGuids').val(seperateByComma());
		$('#invForm').submit();
	});
	$('#issuePO').click(function(){
		var deActivatedObj = checkDeActivation();
		if(deActivatedObj != null){
			alert(deActivatedObj.itemDescription+" is deactivated. Please activate it first.")
			return false;
		}
		var noVendorObj = checkForVendor();
		if(noVendorObj != null){
			alert('No vendor is supplying "'+noVendorObj.itemDescription+'" inventory. Please add a vendor first.');
			return false;
		}	
		$('#invForm').attr('action', issuePOURL);
		$('#selectedGuids').val(seperateByComma());
		$('#invForm').submit();
	});	
	$('#inventory-table').on('dblclick', 'tr', function (){
		$(this).toggleClass('selected');
		var aData = oTableInv.rows('.selected').data()[0];
		$('#guid').val(aData.guid);
		$('#invEditForm').submit();
	});
	$('#inventory-table').css('background-color', '#FFFFFD');
	$('#createPart').click(function(){
		openPartModal();
	});
	$('#jstree-systems').jstree({
		"core" : {
			"themes" : {
				"name" : "proton",
				"responsive" : true
			},
			'data' : {
				'url' : treeURL
			}
		}
	});
});	
openPartModal = function(){
	if(!$('#part-view').is(':visible'))
		$('#part-view').toggle();
	$('#partModalForInventory').modal('toggle');

	/*$.getJSON(partModalURL).done(function(data){
		bootbox.dialog({
	        title: "Part Create",
	        message: data,
	        buttons: {
	            success: {
	                label: "Save",
	                className: "btn-success",
	                callback: function () {
	                    var name = $('#name').val();
	                    var answer = $("input[name='awesomeness']:checked").val()
	                    Example.show("Hello " + name + ". You've chosen <b>" + answer + "</b>");
	                }
	            }
	        } 
		});
	});*/
}
/*initializeVendorsButton = function(){
	 $('#inventory-table tbody button[aria-label="vendorButton"]').each(function(i, item){
		 $(item).click(function(){
    		 currentGuid = $(this).attr('data-guid');
    		 oTableVendor.ajax.url(getVendorListURL());
    		 oTableVendor.ajax.reload();
    		 $('#vendorModal').modal('toggle');
    	 });
	 });
}*/
vendorAction = function(item){
	 currentGuid = $(item).attr('data-guid');
	 oTableVendor.ajax.url(getVendorListURL());
	 oTableVendor.ajax.reload();
	 $('#vendorModal').modal('toggle');
}
checkBoxAction = function(item){
	 var selectedJson = oTableInv.rows($(item).closest('tr')).data()[0];
	 console.log(selectedJson);
	 if(!$(item).prop('checked'))
		 selectedGuids.delete(selectedJson.guid);
	 else
		 selectedGuids.set(selectedJson.guid, selectedJson);
}
initializeCheckBoxes = function(){
  	 $('#inventory-table tbody input[name="invSelect"]').on('change', function(){
		 //$('#inventory-table tbody')
		 var temp = oTableInv.rows($(this).closest('tr')).data();
		 var selectedJson = oTableInv.rows($(this).closest('tr')).data()[0];
		 console.log(selectedJson);
		 if(!$(this).prop('checked'))
			 selectedGuids.delete(selectedJson.guid);
		 else
			 selectedGuids.set(selectedJson.guid, selectedJson);
	 });
}
seperateByComma = function(){
	var guids = '';
	 selectedGuids.forEach(function(value, key){
		 console.log(key+'--'+value);
		 guids=guids+key+',';
	 });
	console.log('guids -->'+guids);
	return guids;
}
checkDeActivation = function (){
	var deactivatedObj;
	selectedGuids.forEach(function(value, key){
		 if(!value.active){
			 deactivatedObj = value;
			 return;
		 }
	 });
	return deactivatedObj;
}
checkForVendor = function(){
	var noVendorObject;
	selectedGuids.forEach(function(value, key){
		 if(!value.vendorAttached){
			 noVendorObject = value;
			 return;
		 }
	 });
	return noVendorObject;
}
navigateToPO = function(inventoryGuid){
	$('#inventoryGuidForPO').val(inventoryGuid);
	$('#poEditForm').submit();
}