/* JS */
$(document).ready(function(){
	var partTable = $('#part-table').DataTable( {
		"dom": 'Blfrtip',
        "ajax": partListURL,
        "buttons": [ 'print'],
        "columns": [
            { "data": "assemblyNumber" },
            { "data": "assemblyName" },
            { "data": "barCode" },
            { "data": "manufacturer" },
            { "data": "manufacturerPartNumber" },
            { "data": "manufacturerPartURL" },
            { "data": "lastOrderURL" }
        ]
    } );
	$('#part-table tbody').on( 'click', 'tr', function () {
		$(this).toggleClass('selected');
		//var aData = vehicleTable.rows('.selected').data()[0];
		var aData = partTable.rows($(this)).data()[0];
		console.log(aData);
		console.log(aData.guid);
		$('#partGuid').val(aData.guid);
		$('#partForm').submit();
	});
});	