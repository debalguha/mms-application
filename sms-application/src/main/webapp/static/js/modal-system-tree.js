/** JS **/
var selectedJson;
buildSystemTree = function() {
	$('#jstree-systems').on('changed.jstree', function(e, data) {
		selectedJson = data.node.original;
		if(selectedJson.nodeType == 'PART' && selectedJson.domainObject ==null){
			$.get(pageContextRoot+'system/data/part/' + selectedJson.guid, function(data) {
				selectedJson.domainObject = data;
			});
		}
	}).jstree({
		"core" : {
			"themes" : {
				"name" : "proton",
				"responsive" : true
			},
			'data' : {
				'url' : treeURL
			},
			"check_callback" : true
		},
		"plugins" : [ "search", "adv_search", "sort" ]		
	});
}