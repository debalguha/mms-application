var pictureListTable;
var selectedGuids = new Map();
$(document).ready(function(){
	pictureListTable = $('#picture-table').DataTable( {
        "ajax": pictureListURL,
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "storedFileName" },
            { "data": "creationDate" }
        ],
        "columnDefs": [
           {
               "render": function ( data, type, row ) {
            	   return '<div class="checkbox"><label><input name="rowCheck" type="checkbox" onclick="pictureEnqueue(this)"></label></div>';
            	   //return '<button type="button" class="btn btn-info btn-sm" data-guid="'+row.guid+'" aria-label="downloadButton" onclick="downloadPicture(this)">Donwload</button>';
               },
               "targets": 2
           }
       ]
    });
	$('#addPictures').click(function(){
		$('#wbsPictureModal').modal('toggle');
	});
	$('#addPicturesBtn').click(function(){
		var guids = seperateByComma();
		if(guids == null || guids.length<=0){
			alert('Please choose a picture first.');
			return;
		}
		$('#systemPictureGuids').val(guids);
		$('#pictureAssociationForm').submit();
	});
});

populatePictures = function(wbsJson){
	$('#pictureTable tbody').empty();
	$.getJSON(wbsPictureURL+wbsJson.guid).done(function(data){
		$.each(data.data, function(index, item){
			var pictureRemoveBtn='<a href="#" class="btn btn-xs btn-warning" onclick="removePictureAssociation(\''+item.guid+'\');"><span class="glyphicon glyphicon-trash"></span> </a>';
			$('#pictureTable tbody').append('<tr><td>'+item.storedFileName+'</td><td>'+item.creationDate+'</td><td>'+pictureRemoveBtn+'</td></tr>');
		});
	});
	$('#drag-and-drop-zone-picture').dmUploader({
		url : pictureUploadURLForWbs,
		dataType : 'json',
		extFilter : 'png;giff;bmp;jpg;PDF;pdf;zip;tar;DXF;dxf',
		extraData: {partGuid: $("#jstree-wbs").jstree(true).get_selected('full',true)[0].original.guid},
		onUploadSuccess : function(id, data) {
			var pictureRemoveBtn='<a href="#" class="btn btn-xs btn-warning" onclick="removePictureAssociation(\''+data.guid+'\');"><span class="glyphicon glyphicon-trash"></span> </a>';
			$('#pictureTable tbody').append('<tr><td>'+data.storedFileName+'</td><td>'+data.creationDate+'</td><td>'+pictureRemoveBtn+'</td></tr>');
			/*var innerHtml = '<tr id="'+data.guid+'"><td>' + data.fileName + '</td><td>'
					+ data.uploadDate + '</td><td>' + data.pages
					+ '</td><td>' + data.userFirstName + '</td><td><input type="radio" name="selectedDrawingGuid" value="'+data.entityGuid+'" autocomplete="off"></td</tr>';
			$('#drawingTable tbody').append(innerHtml);*/
			alert('upload completed successfully');
		},
		onUploadError : function(id, message) {
			alert('Upload failed!!')
		},
		onFileExtError: function(file){
		  console.log('File extension of ' + file.name + ' is not allowed');
		  alert('File extension of ' + file.name + ' is not allowed');
		}
	});		
}
removePictureAssociation = function(systemPictureGuid){
	var wbsGuid = $("#jstree-wbs").jstree(true).get_selected('full',true)[0].original.guid;
	$('#systemPictureGuidsRemove').val(systemPictureGuid);
	$('#wbsGuidPictureRemove').val(wbsGuid);
	$('#pictureAssociationRemoveForm').submit();
}
pictureEnqueue = function(item){
	var selectedJson = pictureListTable.rows($(item).closest('tr')).data()[0];
	 console.log(selectedJson);
	 if(!$(item).prop('checked'))
		 selectedGuids.delete(selectedJson.guid);
	 else
		 selectedGuids.set(selectedJson.guid, selectedJson);	
}
seperateByComma = function(){
	var guids = '';
	 selectedGuids.forEach(function(value, key){
		 console.log(key+'--'+value);
		 guids=guids+key+',';
	 });
	console.log('guids -->'+guids);
	return guids;
}