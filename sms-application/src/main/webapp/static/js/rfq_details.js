/* JS */
RfqItem = function(){
	var itemGuid;
	var itemQuantity;
}
RfqObj = function (){
	this.rfqItems = new Array();
	this.rfqVendors = new Array();
	this.statusDateCreated="";
	this.issueDate="";
	this.rfqNumber="";
	this.expirationDate="";
	this.notes="";
	this.rfqType="";
}

$(document).ready(function(){
	var issueDatePicker = $('#issueDatePicker').datetimepicker({format : 'MM/DD/YYYY'});
	var expirationDatePicker = $('#expirationDatePicker').datetimepicker({format : 'MM/DD/YYYY'});
	//var statusDateCreatedPicker = $('#statusDateCreatedPicker').datetimepicker({format : 'MM/DD/YYYY'});
	$('#saveRfq').click(function(){
		var rfq = new RfqObj();
		if($('#rfqGuid').val() !=null && $('#rfqGuid').val()!='')
			rfq.guid= $('#rfqGuid').val();
		rfq.statusDateCreated = $('#statusDateCreated').val();
		rfq.rfqNumber = $('#rfqNumber').val();
		rfq.issueDate = $(issueDatePicker).data('date');
		rfq.expirationDate = $(expirationDatePicker).data('date');
		rfq.notes = $('#notes').val();
		rfq.rfqType = $('#rfqType').val();
		$('input[type="text"][data-inv-guid]').each(function(i, item){
			var rfqItem = new RfqItem();
			if($(item).attr('data-inv-guid') != null && $(item).attr('data-inv-guid') != '')
				rfqItem.invGuid = $(item).attr('data-inv-guid');
			if($(item).attr('data-itm-guid') != null && $(item).attr('data-itm-guid') != '')
				rfqItem.itmGuid = $(item).attr('data-itm-guid');
			rfqItem.itemQuantity = $(item).val();
			rfq.rfqItems.push(rfqItem);
		});
		$('input[type="checkbox"][name="vendorChose"]').each(function(i, item){
			rfq.rfqVendors.push($(item).val());
		});
		console.log(rfq);
		$.ajax({
			type : 'POST',
			url : createRFQURL,
			data : JSON.stringify(rfq),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				alert('Unable to ssave RFQ.');
				feedbackMove = true;
			}else{
				$('#guid').val(data.guid);
				alert('RFQ successfully Generated');
				$('#navigationForm').submit();
			}
		});		
	});
});