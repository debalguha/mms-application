/* JS */
var emailValidated = false;
var successHtml = "<span class='glyphicon glyphicon-ok form-control-feedback' aria-hidden='true'></span><span id='inputSuccess2Status' class='sr-only'>(success)</span>";
var errorHtml = "<span class='glyphicon glyphicon-remove form-control-feedback' aria-hidden='true'></span><span id='inputError2Status' class='sr-only'>(error)</span>";
$(document).ready(function(){
	$('#email').on('blur', function(){
		//$.getJSON(contactEmailValidateURL+this.val()+'?guid='+$('#guid').val(), function(data){
		$.getJSON(contactEmailValidateURL, {guid:$('#guid').val(), email:$(this).val()}, function(data){
			if(data){
				emailValidated = true;
				$('#email').closest('div').find('span').remove();
				$('#email').closest('div').removeClass('has-error')
				$('#email').closest('div').addClass('has-success')
				$('#email').attr('aria-describedby', 'inputSuccess2Status');
				$('#email').closest('div').append(successHtml);
			}else{
				emailValidated = false;
				$('#email').closest('div').find('span').remove();
				$('#email').closest('div').removeClass('has-success')
				$('#email').closest('div').addClass('has-error')
				$('#email').attr('aria-describedby', 'inputError2Status');	
				$('#email').closest('div').append(errorHtml);
			}
		});
	});
	var oTableVendorContact = $('#contact-table').DataTable({
		ajax: contactsURL,
		dom: 'Bfrtlip',
        columns: [
	          { "data": "firstName" },
	          { "data": "lastName" },
	          { "data": "email" },
	          { "data": "phone1" }
	      ],
	      buttons: [
            {
                text: 'Add Contact',
                action: function ( e, dt, node, config ) {
                	$('#contactModalForVendor').modal('toggle');
                }
            }
         ]	      
	});
	$('#itmsSuppl').click(function(){
		if($('input[name="vendorGuid"]').val() == null || $('input[name="vendorGuid"]').val() == ''){
			alert('Please save the vendor first.')
			return false;
		}
		$('#itmsSupplyForm').submit();
	});
	$('#contact-table').on('click', 'tr', function(){
		$('#contactForm')[0].reset();
		//$('#contact-table tr.select').each(function(index, element){$(element).removeClass('selected')});
		$(this).toggleClass('selected');
		var aData = oTableVendorContact.rows('.selected').data()[0];
		console.log(aData);
		$('#contactModalForVendor').modal('toggle');
		populateContactDetails(aData);
		$(this).toggleClass('selected');
	});
	$('#vendorSubmit').click(function(){

		var vendorObj = new Object();
		vendorObj.vendorName = $('#vendorName').val();
		vendorObj.location = $('#locationId').val();
		vendorObj.vendorType = $('#vendorTypeId').val();
		vendorObj.contacts = new Array();
		oTableVendorContact.rows().every( function (rowIdx, tableLoop, rowLoop ) {
			vendorObj.contacts.push(this.rows(rowIdx).data()[0]);
		} );
		console.log(vendorObj);
		$.ajax({
			type : 'POST',
			url : vendorSubmitURL,
			data : JSON.stringify(vendorObj),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			if(!data){
				alert('Unable to save Vendor.');
				feedbackMove = true;
			}else{
				alert('Vendor details saved!!')
			}
		});	
	});
	$('#addContact').click(function(){
		$.getJSON(contactEmailValidateURL, {guid:$('#guid').val(), email:$('#email').val()}, function(data){
			if(data){
				var jsonData=new Object();
				jsonData.firstName = $('#firstName').val();
				jsonData.lastName = $('#lastName').val();
				jsonData.email = $('#email').val();
				jsonData.phone1 = $('#phone1').val();
				jsonData.phone2 = $('#phone2').val();
				jsonData.address1 = $('#address1').val();
				jsonData.address2 = $('#address2').val();
				jsonData.city = $('#city').val();
				jsonData.stateProvince = $('#stateProvince').val();
				jsonData.postalCode = $('#postalCode').val();
				$('#contactModalForVendor').modal('toggle');
				oTableVendorContact.row.add(jsonData).draw();
			}else {
				$('#email').focus();
				return false;
			}
		});
	});
});
populateContactDetails = function(jsonData){
	$('#firstName').val(jsonData.firstName);
	$('#lastName').val(jsonData.lastName);
	$('#email').val(jsonData.email);
	$('#phone1').val(jsonData.phone1);
	$('#phone2').val(jsonData.phone2);
	$('#address1').val(jsonData.address1);
	$('#address2').val(jsonData.address2);
	$('#city').val(jsonData.city);
	$('#stateProvince').val(jsonData.stateProvince);
	$('#postalCode').val(jsonData.postalCode);
}