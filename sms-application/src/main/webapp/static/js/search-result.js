/* JS */
$(document).ready(function(){
	oTableSearch = $('#searchTable').DataTable( {
        "ajax": searchTableURL,
        "columns": [
            { "data": "guid" },
            { "data": "type" },
            { "data": "description" },
            { "data": "creationDate" }
        ],
        "columnDefs": [{
            "render": function ( data, type, row ) {
            	return "<a class='search' href='#' data-guid='"+row.guid+"' data-type='"+row.type+"' onclick='navigateSearchResult(this);'>"+row.description+"</a>";
            },
            	"targets": 2
        	}
        ]
    } );
	
	$('#searchTable').css('background-color', '#FFFFFD');
	/*$('a.search').click(function(){
		
	});*/
});	
navigateSearchResult = function(link){
	console.log($(link).data('guid')+','+$(link).data('type'));
	$('#guid').val($(link).data('guid'));
	$('#type').val($(link).data('type'));
	$('#serachNavigationForm').submit();
}