adjustPOCalculation = function(){
	var totalPOPrice = parseFloat($('#sAndH').val())+parseFloat($('#taxes').val())+parseFloat($('#duty').val())-parseFloat($('#discount').val());
	$('table tbody tr').each(function(){
		totalPOPrice+=parseInt($(this).find('input[name="quantity"]').val())*parseFloat($(this).find('input[name="unitPrice"]').val());
		$('#total').val(totalPOPrice);
	});
}
$(document).ready(function(){
    $('input[role="zero-digits"]').keyup(function(){
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 0){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseInt(this.value);
            }  
         }            
         return this; //for chaining
    });
});