/* JS */
var selectedJson;
$(document).ready(function(){
	$('#scheduledDatePicker').datetimepicker({format : 'MM/DD/YYYY'});
	$('#promisedDatePicker').datetimepicker({format : 'MM/DD/YYYY'});
	$('#completedDatePicker').datetimepicker({format : 'MM/DD/YYYY'});
	var workOrderItemsGlobal  = new Array();
	var $customerSelect = $('#customerGuid').select2({});
	$customerSelect.on("select2:select", function (e) { 
		var customerGuid = e.params.data.id;
		$.getJSON(vehicleSearchURL+customerGuid, function(data){
			$.each(data, function(i, item){
				$('#vehicleGuid').append($('<option>', { 
			        text: item.serialNumber,
			        value : item.guid 
			    }));
			});
		});
	});
	$('#techGuid').select2({});
	$('#supervisorGuid').select2({});
	$('#addSystemsNode').click(function(){
		try {
			$('#jstree').jstree(true).destroy();
		} catch(err) {
		    console.log(err.message);
		}			
		$('#woTreeModal').modal('toggle');
	});
	var nomenclatureSelect;
	var promisedModalDatePicker;
	var completedModalDatePicker;
	$('#woTreeModal').on('show.bs.modal', function(e){
		promisedModalDatePicker = $('#promisedDateModalPicker').datetimepicker({format : 'MM/DD/YYYY'});
		completedModalDatePicker = $('#completedDateModalPicker').datetimepicker({format : 'MM/DD/YYYY'});
		buildSystemTree();
		$('#technicianModal').select2({});	
	});
	$('#saveWO').click(function(){
		var workOrder = new Object();
		workOrder.guid=$('#guid').val();
		workOrder.userGuid = $('#userGuid').val();
		workOrder.techGuid = $('#techGuid').val();
		workOrder.supervisorGuid = $('#supervisorGuid') .val();
		workOrder.customerGuid = $('#customerGuid').val();
		workOrder.locationGuid = $('#locationGuid').val();
		workOrder.workOrderTypeGuid = $('#workOrderType').val();
		workOrder.vehicleGuid = $('#vehicleGuid').val();
		workOrder.scheduledDate = $('#scheduledDatePicker').data('date');
		workOrder.promisedDate = $('#promisedDatePicker').data('date');
		workOrder.completedDate = $('#completedDatePicker').data('date');
		workOrder.priority = $('#priority').val();
		workOrder.workOrderItems = new Array();
		//workOrder.workOrderItems = workOrderItemsGlobal;
		$('#systemsTable tbody tr').each(function(){
			console.log($(this).data());
			workOrder.workOrderItems.push($(this).data());
		});
		console.log(workOrder);
		$.ajax({
			type : 'POST',
			url : saveWorkOrderURL,
			data : JSON.stringify(workOrder),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				alert('Unable to save Work Order.');
				feedbackMove = true;
			}else{
				alert('Work order saved successfully');
				submitFormForNavigation();
			}
		});	
	});
	$('#addSystemTreeBtn').click(function(){
		var selectedGuid = selectedJson.guid;
		var nomenclature = selectedJson.text;
		var treeItemTypeGuid = selectedJson.treeItemTypeGuid;
		var treeItemTypeText = selectedJson.nodeType;
		var technicianGuid = $('#technicianModal').val();
		var technicianText = $('#technicianModal option[value="'+technicianGuid+'"]').text();
		var completedDate = $(completedModalDatePicker).data('date');
		var promisedDate = $(promisedModalDatePicker).data('date');
		var woItem = new Object();
		woItem.systemTreeGuid = selectedGuid;
		woItem.nomenclature = nomenclature;
//		woItem.treeItemTypeText = treeItemTypeText;
//		woItem.treeItemTypeGuid = treeItemTypeGuid;
		woItem.techGuid = technicianGuid;
		woItem.technicianText = technicianText;
		woItem.datePromised = promisedDate;
		woItem.dateCompleted = completedDate;
		woItem.userGuid = $('#userGuid').val();
		console.log(selectedJson);
		console.log(woItem);
		if(selectedJson.nodeType != 'SERVICE' && selectedJson.nodeType != 'LINEITEM'){
			alert('Please select either a Service or Line Item node.')
			return false;
		}
		var tr = $('<tr><td>'+nomenclature+'</td><td>'+treeItemTypeText+'</td><td>'+technicianText+'</td><td>'+(promisedDate==null || promisedDate == 'undefined'?'':promisedDate)+'</td><td>'+(completedDate==null || completedDate=='undefined'?'':completedDate)+'</td><td><button type="button" class="btn btn-default" onClick="removeTableRow(this)">Remove</button></td></tr>');
		$('#systemsTable').find('tbody').append(tr);
		tr.data(woItem);
		/*tr.on('dblClick', function(){
			var woItemTR = $(this).data();
			$('#treeItemTypeSelect option[value="'+woItemTR.technicianGuid+'"]').prop('selected', 'selected');
		});*/
		workOrderItemsGlobal.push(woItem);
		$('#woTreeModal').modal('toggle');
	});
	$('#systemsTable').on('dblclick', 'tr', function(){
		var woItem =$(this).data();
		console.log(woItem);
		$('#woTreeModal').modal('toggle');
		$('#jstree').jstree(true).select_node(woItem.systemTreeGuid);
		
	});
	$('#systemsTable').css('background-color', '#FFFFFD');
});
submitFormForNavigation = function(){
	$("#nav-link").val("user/pos-work-order");$("#linkForm").submit();
}
removeTableRow = function(btn){
	$(btn).closest('tr').remove();
}
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}