var feedbackMove = false;
var copiedNode = null;
var feedbackRename = false;
buildWBSTree = function() {
	$('#jstree-wbs').on('rename_node.jstree', function(e, data){
		if(feedbackMove || feedbackRename){
			feedbackMove = false;
			feedbackRename = false;
			return;
		}
		var guid = data.node.original.guid;
		var newName = data.text
		var oldText = data.old;
		console.log('Info: '+newName+":"+guid);
		$.get(''+guid+'/to/'+newName, function(data){
			if(data.guid == null || data.guid == '' || data.guid == 'undefined'){
				alert('Rename failed!!');
				feedbackMove = true;
				$("#jstree-wbs").jstree(true).rename_node($("#jstree-wbs").jstree(true).get_selected('full',true)[0], oldText);
			}else{
				$('#nomenclature').val(data.nomenclature);
			}
		});
	}).on('ready.jstree', function(e, data){
		if(preSelectedGuid!='')
			$('#jstree-wbs').jstree(true).select_node(preSelectedGuid);
	}).on('changed.jstree', function(e, data) {
		populateWbs(data.node.original);
		populatePictures(data.node.original);
		populateDrawings(data.node.original);
		$('#wbsGuidPicture').val(data.node.original.guid);
		$('#wbsGuidDrawing').val(data.node.original.guid);
	}).on('delete_node.jstree', function(e, data) {
		console.log('deleting : ' + data.node.original);
		$.post(wbsDeleteNodeURL + data.node.original.guid, function(data) {
			if (data == 'SUCCESS')
				alert('Element successfully removed.');
			else
				alert('Unable to remove.');
		});
	}).jstree({
		"core" : {
			"themes" : {
				"name" : "proton",
				"responsive" : true
			},
			'data' : {
				'url' : wbsTreeURL
			},
			"check_callback" : true
		},
		"plugins" : [ "contextmenu", "dnd","sort"],
		"contextmenu" : {
			"items" : function($node) {
				return {
					"Copy" : {
						"label" : "Copy",
						"_disabled" : false,
						"action" : function(obj){
							copiedNode = $("#jstree-wbs").jstree(true).get_selected('full',true)[0];
							$("#jstree-wbs").jstree(true).copy(copiedNode);
							console.log(copiedNode);
						}
					},
					"Paste" : {
						"label" : "Paste",
						"_disabled" : function(obj){
							var selectedNode = $("#jstree-wbs").jstree(true).get_selected('full',true)[0];
							console.log(selectedNode);
							if(copiedNode == null || selectedNode.id=='#')
								return true;
							return false;
						},
						"action" : function(obj){
							var selectedNode = $("#jstree-wbs").jstree(true).get_selected('full',true)[0];
							$("#jstree-wbs").jstree(true).paste(selectedNode);
						}
					},					
					"Delete" : {
						"label" : "Delete",
						"_disabled" : function(obj){							
							return false;
						},						
						"action" : function(obj) {
							var selectedNode = $("#jstree-wbs").jstree(true).get_selected('full',true)[0];
							console.log(selectedNode);
							if(selectedNode.original.nodeType == 'VENDOR_GROUP'
								|| selectedNode.original.nodeType == 'DRAWING_GROUP'
									|| selectedNode.original.nodeType == 'PICTURE_GROUP')
								return;
							$("#jstree-wbs").jstree(true).delete_node(selectedNode);
						}
					},
					"rename" : {
						"label" : "Rename",
						"_disabled" : function(obj){	
							return false;
						},						
						"action" : function (data) {
							var inst = $.jstree.reference(data.reference),
								obj = inst.get_node(data.reference);
							inst.edit(obj);
						}
					},
					"add" : {
						"label" : "Add",
						"_disabled" : function(obj){	
							return false;
						},						
						"action" : function (data) {
							var selectedNode = $("#jstree-wbs").jstree(true).get_selected('full',true)[0];
							$("#wbsForm")[0].reset();
							$('#parentGuid').val(selectedNode.original.guid);
							$('#guid').val('');
							switchOnDisplay('wbs-view');
							$('#wbs-view').effect( "shake", "left", 4);
						}
					}
				};
			}
		}
	});
	$('#jstree-wbs').bind('move_node.jstree',function(event,data){
		if(feedbackMove){
			feedbackMove = false;
			return;
		}
		console.log(event);console.log(data);
		var oldParentId = data.old_parent;
		var newParentId = data.parent;
		var currentNodeId = data.node.id;
		if(newParentId == oldParentId)
			return;
		var json = {
			'oldParentGuid' : oldParentId != '#' ? $("#jstree-wbs").jstree(true).get_node(oldParentId).original.guid: oldParentId,
			'newParentGuid' : newParentId != '#' ? $("#jstree-wbs").jstree(true).get_node(newParentId).original.guid : newParentId,
			'currentNodeGuid' : data.node.original.guid,
				
		};
		console.log(json);
		$.ajax({
			type : "POST",
			url : 'wbs/tree/move',
			data : json,
			dataType : 'json',
		}).always(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				alert('Unable to move node.');
				feedbackMove = true;
				$("#jstree-wbs").jstree(true).move_node(currentNodeId, oldParentId, 'first', function(){return false;});
			}
		});
	});
	$('#jstree-wbs').bind('paste.jstree',function(event,data){
		var selectedNode = $("#jstree-wbs").jstree(true).get_selected('full',true)[0];
		console.log(getCopiedNode());
		console.log(selectedNode);
		var json = {
			'currentNodeGuid' : getCopiedNode().original.guid,
			'newParentGuid' : selectedNode.original.guid
		};
		$.ajax({
			type : "POST",
			url : 'wbs/tree/paste',
			data : json,
			dataType : 'json',
		}).always(function(data){
			if(data.responseText != 'SUCCESS'){
				alert('Unable to move node.');
				feedbackMove = true;
				//$("#jstree-systems").jstree(true).move_node(currentNodeId, oldParentId, 'first', function(){return false;});
				//$("#jstree-systems").jstree('paste');
			}
		});
		nullifyCopiedNode();
	});
}
nullifyCopiedNode = function(){
	copiedNode = null;
}
getCopiedNode = function(){
	return copiedNode;
}
switchOnDisplay = function(divToWorkWith) {
	$('#' + divToWorkWith).css('display', 'block');
}
switchOffDisplay = function(divToWorkWith) {
	$('#' + divToWorkWith).css('display', 'none');
}
populateWbs = function(treeNode) {
	$.get(wbsNodeLookupURL + treeNode.guid, function(data) {
		console.log(data);
		$('#nomenclature').val(data.nomenclature);
		$('#description').val(data.description);
		$('#wbs').val(data.wbs);
		$('#guid').val(data.guid);
		switchOnDisplay('wbs-view');
	});
}