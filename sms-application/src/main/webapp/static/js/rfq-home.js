/* JS */
$(document).ready(function(){
	var oTableInv = $('#rfq-table').DataTable( {
		"dom": 'Blfrtip',
        "ajax": rfqListURL,
        "buttons": [ 'print'],
        "columns": [
            { "data": "rfqNumber" },
            { "data": "rfqType" },
            { "data": "issueDate" },
            { "data": "statusDateCreated" },
            { "data": "responseCount" },
        ],
        "columnDefs": [{
           "render": function ( data, type, row ) {
        	   if(row.type == 'Canceled')
        		   return '<button type="button" data-guid="'+row.guid+'" data-action="activate" class="btn btn-info btn-sm">Activate</button>';
        	   else {
        		   return '<button type="button" data-guid="'+row.guid+'" data-action="issuePO" class="btn btn-info btn-sm" style="margin-right: 10px;">Issue PO</button><button type="button" data-guid="'+row.guid+'" data-action="cancel" style="margin-right: 10px;" class="btn btn-info btn-sm" disabled="disabled">cancel</button><button type="button" data-guid="'+row.guid+'" data-action="showResponses" class="btn btn-info btn-sm">Responses</button>';
        	   }        		   
           },
           	"targets": 5
	       }]
    } );
	$('#rfq-table').on( 'init.dt', function () {
	    $('button[data-action="showResponses"]').click(function(){
			$('#rfqForm').attr('action', rfqResponseURL);
			$('#rfqGuid').val($(this).attr('data-guid'));
			$('#rfqForm').submit();
		});	
	    $('button[data-action="issuePO"]').click(function(){
			$('#rfqForm').attr('action', issuePOURL);
			$('#rfqGuid').val($(this).attr('data-guid'));
			$('#rfqForm').submit();
		});		    
	}).on('click', 'tr', function (){
		console.log(oTableInv.row($(this)).data());
		$('#rfqGuid').val(oTableInv.row($(this)).data().guid);
		$('#rfqForm').attr('action', editRFQURL);
		$('#rfqForm').attr('method', 'get');
		$('#rfqForm').submit();
	});	
});