/* JS */
$(document).ready(function(){
	var woTable = $('#workorder-completed-table').DataTable( {
        "ajax": completedWOList,
        "columns": [
                    //{ "data": "systemTree.nomenclature" },
                    { "data": "location.locationName" },
                    { "data": "customer.customerEndUser" },
                    { "data": "creationDate" },
                    { "data": "promisedDate" },
                    { "data": '' },
                    { "data": "priority" },
                    { "data": "techUserGuid.firstName" },
                    { "data": "superVisorGuid.firstName" }
                ],        
                "columnDefs": [
                   {
                       "render": function ( data, type, row ) {
                    	   var daysToGo = moment(row.promisedDate, "MM/DD/YYYY").diff(moment(), 'days');
                    	  return daysToGo<0?0:daysToGo;
                       },
                       "targets": 4
                   }
               ]
            } );
        	$('#workorder-completed-table').css('background-color', '#FFFFFD');
        	$('#workorder-completed-table').on('dblclick', 'tr', function(){
        		var selectedJson = woTable.rows($(this)).data()[0];
        		$('#guid').val(selectedJson.guid);
        		$('#woEditForm').submit();
        	});
        });	