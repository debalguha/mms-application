/* JS */
$(document).ready(function(){
	//var issueDateMoment = moment($('#issueDate').val(), 'MM/DD/YYYY');
	var deliveryByDatePicker = $('#deliveryByDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: moment($('#issueDate').val(), 'MM/DD/YYYY')});
	var issueDatePicker = $('#issueDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: moment($('#deliveryByDate').val(), 'MM/DD/YYYY')});
	$('#savePO').click(function(){
		var po = createPO();
		console.log(po);
		$.ajax({
			type : 'POST',
			url : savePOFromEditURL,
			data : JSON.stringify(po),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				alert('Unable to save PO.');
				feedbackMove = true;
			}else{
				$('#editForm').submit();
			}
		});	
	});	
	$('#deletePO').click(function(){
		$('#deleteForm').submit();
	});
	$('#duplicatePO').click(function(){
		var po = createPO();
		if(po.poNumber == oldPONumber){
			alert("Please change the PO number before duplicating.")
			return;
		}
		po.poGuid='';
		$.ajax({
			type : 'POST',
			url : duplicatePOURL,
			data : JSON.stringify(po),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				alert('Unable to duplicate PO.');
				feedbackMove = true;
			}else{
				$('#editForm').submit();
			}
		});
	});
});	
createPO = function(){
	var po = new Object();
	po.poNumber = $('#poNumber').val();
	po.rfqGuid = $('#rfqGuid').val();
	po.poGuid = $('#poGuid').val();
	po.vendorGuid = $('#vendor').val();
	po.issueDate = $(issueDatePicker).data('date');
	po.deliveryByDate = $(deliveryByDatePicker).data('date');
	po.notes = $('#notes').val();
	po.deliveryInstruction = $('#deliveryInstruction').val();
	po.deliveryAddress = $('#deliveryAddress').val();
	po.poType = $('#poType').val();
	po.sAndH = $('#sAndH').val();
	po.taxes = $('#taxes').val();
	po.duty = $('#duty').val();
	po.discount = $('#discount').val();
	po.total = $('#total').val();
	po.poItems = new Array();
	$('#poItemTable tbody tr').each(function(i, item){
		var poItem = new Object();
		poItem.guid = $(this).find('input[name="unitPrice"]').attr('data-guid');
		poItem.unitPrice = $(this).find('input[name="unitPrice"]').val();
		poItem.quantity = $(this).find('input[name="quantity"]').val();
		po.poItems.push(poItem);
	});
	return po;
}