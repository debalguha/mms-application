/* JS */
$(document).ready(function(){
	var vehicleTable = $('#vehicle-table').DataTable({
//		"dom": 'Bfrtip',
		"dom": 'Blfrtip',
        "ajax": vehicleListURL,
        "buttons": ['print'],
        "columns": [
            { "data": "serialNumber" },
            { "data": "vehicleType.type" },
            { "data": "vehicleYear" },
            { "data": "vehicleVin" },
            { "data": "customer.customerEndUser" }
        ]
    });
	$('#vehicle-table tbody').on( 'click', 'tr', function () {
		$(this).toggleClass('selected');
		//var aData = vehicleTable.rows('.selected').data()[0];
		var aData = vehicleTable.rows($(this)).data()[0];
		console.log(aData);
		console.log(aData.guid);
		$('#vehicleGuid').val(aData.guid);
		$('#vehicleForm').submit();
	});
	
	$('#vehicle-table').css('background-color', '#FFFFFD');
});	