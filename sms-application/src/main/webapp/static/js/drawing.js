/* JS */
var drawingTable;
var selectedGuids = new Map();
$(document).ready(function(){
	drawingTable = $('#drawing-table').DataTable( {
		"dom": 'Blfrtip',
        "ajax": drawingListURL,
        "buttons": [ 'print'],
        "processing": true,
        "serverSide": true,
        "columns": [
            { "data": "storedFileName" },
            { "data": "creationDate" }
        ]
		,
        "columnDefs": [
           {
               "render": function ( data, type, row ) {
            	   return '<div class="checkbox"><label><input name="rowCheck" type="checkbox" onclick="deleteDrawingEnqueue(this)"></label></div>';
            	   //return '<button type="button" class="btn btn-info btn-sm" data-guid="'+row.guid+'" aria-label="downloadButton" onclick="deleteDrawing(this)">Delete</button>';
               },
               "targets": 2
           }
       ]
    } );
	$('#drawing-table').on('click', 'tr', function(){
		downloadDrawing($(this));
	});
	$('#drawing-table').on('contextmenu', 'tr', function(){
		var $tr = $(this).closest('tr');
		console.log(drawingTable.rows($tr).data()[0])
		if(!$tr.hasClass('selected'))
			$tr.addClass('selected');
		$.contextMenu({
            selector: '#drawing-table tr.selected',
            build: function($trigger, e) {
            	return {
            		callback: function(key, options) {
            			if(key == 'rename'){
	            			bootbox.prompt("Please provide new name.", function(result) {                
	            				if (result != null && result.trim().length>0) {                                             
	            					$('#drawingGuid').val(drawingTable.rows($tr).data()[0].guid);
	            					$('#newName').val(result);
	            					$('#drawingRenameForm').submit();
							  	} 
	        				});
            			}else if(key = 'associate'){
            				$('#systemDrawingGuid').val(drawingTable.rows($tr).data()[0].guid);
            				openWbsTree();
            			}
                    },
                    items: {
                            "rename": {name: "Rename", icon: "edit"},
                    		"associate": {name: "Associate WBS"}
                    }
            	};
            }
             
        });
		$(".my-menu").contextMenu();
	});
	$('#deleteDrawing').click(function(){
		var guids = seperateByComma();
		if(guids == null || guids.length<=0){
			alert('Please choose a customer first.');
			return;
		}
		$('#selectedGuids').val(guids);
		$('#drawingDeleteForm').submit();
	});
	$('#wbsTreeModal').on('show.bs.modal', function(e){
		buildWbsTree();
	});
	$('#addWbsTreeBtn').click(function(){
		$('#wbsGuid').val(chosenGuid);
		$('#associationForm').submit();
	});
});	
document.oncontextmenu = function() {return false;};
downloadDrawing = function(item){
	var selectedJson = drawingTable.rows($(item).closest('tr')).data()[0];
	$.fileDownload(drawingDownloadURL+selectedJson.guid,
		{preparingMessageHtml : "Your download is on the way, please wait...",
		failMessageHtml : "There was a problem generating your download, please try again."
	});
}
deleteDrawingEnqueue = function(item){
	var selectedJson = drawingTable.rows($(item).closest('tr')).data()[0];
	 console.log(selectedJson);
	 if(!$(item).prop('checked'))
		 selectedGuids.delete(selectedJson.guid);
	 else
		 selectedGuids.set(selectedJson.guid, selectedJson);	
}
seperateByComma = function(){
	var guids = '';
	 selectedGuids.forEach(function(value, key){
		 console.log(key+'--'+value);
		 guids=guids+key+',';
	 });
	console.log('guids -->'+guids);
	return guids;
}
