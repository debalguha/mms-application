/* JS */
//validateContract = function(){
//	var errors = new Array();
//	//if($('#nomenclature').val() == null || $('#nomenclature').val() == '')
//	
//}
validateContractForm = function() {
	if (validateForNullOrEmptyString($('#contractForm').find('input[name="nomenclature"]').val())) {
		alert('Nomenclature is required!');
		return;
	}
	if (validateForNullOrEmptyString($('#country').val())) {
		alert('Country is required!');
		return;
	}
	if (validateForNullOrEmptyString($('#organization').val())) {
		alert('Organization is required!');
		return;
	}
	if (validateForNullOrEmptyString($('#agencyCompany').val())) {
		alert('agencyCompanyn is required!');
		return;
	}
	if (validateForNullOrEmptyString($('#organization').val())) {
		alert('organization is required!');
		return;
	}
	if (validateForNullOrEmptyString($('#customerEnduser').val())) {
		alert('customerEnduser is required!');
		return;
	}
	if (validateForNullOrEmptyString($('#contractCurrency').val())) {
		alert('contractCurrency is required!');
		return;
	}
	if (validateForNullOrEmptyString($('#commonCurrency').val())) {
		alert('commonCurrency is required!');
		return;
	}
	if(!$('#commonCurrency').val().match(/^[a-zA-z]+$/)){
		alert('CommonCurrency shuold only have text!');
		return;
	}
	//
	if (validateForNullOrEmptyString($('#exchangeRate').val())) {
		alert('exchangeRate is required!');
		return;
	}

	if(!$.isNumeric($('#exchangeRate').val())){
		alert('Exchange rate must be numeric.');
		return false;
	}
	if (validateForNullOrEmptyString($('#contractNumber').val())) {
		alert('contractNumber is required!');
		return;
	}
	//
	//console.log(contractDatePicker.date());
	console.log($('#contract-view').find('#contractDatePicker').data('DateTimePicker').date());
	console.log($('#contract-view').find('#contractDatePicker').data('DateTimePicker').defaultDate());
	if (validateForNullOrEmptyString($(contractDatePicker).data('date'))) {
		alert('contractDate is required!');
		return;
	}
	if (validateForNullOrEmptyString($('#shippingAddress').val())) {
		alert('shippingAddress is required!');
		return;
	}

	return true;
}
$( function() {
    $('input[role="five-digits"]').keyup(function(){
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 5){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(5);
            }  
         }            
         return this; //for chaining
    });
});