/* JS */

validateServiceForm = function(){
	if ($('#serviceDataForm').find('input[name="nomenclature"]').val() == null || $('#serviceDataForm').find('input[name="nomenclature"]').val() == '') {
		alert('Nomenclature is required!');
		return;
	}
	if($('#idCodeService').val() == null || $('#idCodeService').val() == ''){
		alert('idCodeService is required!');
		return;
	}
	if($('#quantityService').val() == null || $('#quantityService').val() == ''){
		alert('quantity is required!');
		return;
	}

	if (parseInt($('#quantityService').val()) < 0
			|| (parseFloat($('#quantityService').val())-parseInt($('#quantityService').val()))>0){
		alert('Quantity can not be negative or decimal');
		return;
							
	}
//	alert(parseInt($('#quantityService').val()));
//	alert(parseFloat($('#quantityService').val()));
//	alert(parseFloat($('#quantityService').val())-parseInt($('#quantityService').val()));
//	
	
	if($('#unitPriceService').val() == null || $('#unitPriceService').val() == ''){
		alert('unit Price is required!');
		return;
	}

	if (parseInt($('#unitPriceService').val()) < 0)
	{
		alert('unit Price can not be negative');
		return;
							
	}
	
	if($('#commonCurrencyPriceService').val() == null || $('#commonCurrencyPriceService').val() == ''){
		alert('Common Currency Price is required!');
		return;
	}

	if (parseInt($('#commonCurrencyPriceService').val()) < 0)
	{
		alert('ommon Currency Price can not be negative');
		return;
							
	}
	
	return true;
    }


