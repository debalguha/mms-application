validateForNullOrEmptyString = function(itemVal){
	return itemVal == null || itemVal == '' || itemVal == 'undefined';
}