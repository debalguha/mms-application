/* JS */

validatePartForm = function() {
	if ($('#partForm').find('input[name="nomenclature"]').val() == null
			|| $('#partForm').find('input[name="nomenclature"]').val() == '') {
		alert('Nomenclature is required!');
		return;
	}
	if ($('#assemblyNumber').val() == null || $('#assemblyNumber').val() == '') {
		alert('assemblyNumber is required!');
		return;
	}
	if ($('#assemblyName').val() == null || $('#assemblyName').val() == '') {
		alert('assemblyName is required!');
		return;
	}
	if ($('#barCode').val() == null || $('#barCode').val() == '') {
		alert('barCode is required!');
		return;
	}

	if ($('#manufacturerPartNumber').val() == null
			|| $('#manufacturerPartNumber').val() == '') {
		alert('manufacturerPartNumber is required!');
		return;
	}

	if ($('#manufacturer').val() == null || $('#manufacturer').val() == '') {
		alert('manufacturer is required!');
		return;
	}

	if ($('#manufacturerPartURL').val() == null
			|| $('#manufacturerPartURL').val() == '') {
		alert('manufacturerPartURL is required!');
		return;
	}
	if ($('#lastOrderURL').val() == null || $('#lastOrderURL').val() == '') {
		alert('lastOrderURL is required!');
		return;
	}
	return true;
}
