/* JS */

validateLineItemForm = function() {
	if ($('#lineItemForm').find('input[name="nomenclature"]').val() == null
			|| $('#lineItemForm').find('input[name="nomenclature"]').val() == '') {
		alert('Nomenclature is required!');
		return;
	}
	if ($('#lineItem').val() == null || $('#lineItem').val() == '') {
		alert('lineItem is required!');
		return;
	}
	if ($('#idCode').val() == null || $('#idCode').val() == '') {
		alert('idCode is required!');
		return;
	}
	
	return true;
}
