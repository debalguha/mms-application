/* JS */
var fileUpload;
var feedbackRename = false;
String.prototype.endsWith = function(pattern) {
    var d = this.length - pattern.length;
    return d >= 0 && this.lastIndexOf(pattern) === d;
};
$(document).ready(function() {
	buildSystemTree();
	//$('#lightbox').lightbox({show: false});
	//$('#datetimepicker1').datetimepicker('disable');
	$('#downloadDrawing').click(function(){
		var guid = $("input[type='radio'][name='selectedDrawingGuid']:checked").val();
		$.fileDownload(pageContextRoot+'system/download/drawing/'+guid)
			.done(function () { 
				//$('#downloadSuccessAlert').show();
				alert('download completed.')
			})
			.fail(function () { 
				alert('Download failed.')
			});
	});
	$('#removeDrawing').click(function(){
		var guid = $("input[type='radio'][name='selectedDrawingGuid']:checked").val();
		$.post(pageContextRoot+'system/drawing/remove/'+guid, function(data){
			  $('#'+data).remove();
			  alert('Element removed!!');
		})
	});
	$('#addDrawing').click(function(event){
		event.preventDefault();
		$('#drawingModal').modal('toggle');
		var guid = $("#jstree-systems").jstree(true).get_selected('full',true)[0].id;
		$.get(pageContextRoot+'system/part/'+guid+'/not/drawings', function(data){
			$('#addDrawingTable tbody').empty();
			$.each(data, function(index, item){
				var innerHtml = '<tr id="'+item.entityGuid+'"><td>' + item.fileName + '</td><td>'
				+ item.uploadDate + '</td><td>' + item.pages
				+ '</td><td>' + item.userFirstName + '</td><td><input type="checkbox" name="drwaingGuids" value="'+item.entityGuid+'" autocomplete="off"></td</tr>';	
				$('#addDrawingTable tbody').append(innerHtml);
			});
		});
	});
	
	$('#addDrawingsBtn').click(function(event){
		event.preventDefault();
		$('#drawingModal').modal('toggle');
		var guid = $("#jstree-systems").jstree(true).get_selected('full',true)[0].id;
		$.post(pageContextRoot+'system/part/'+guid+'/drawing/add', $("#addDrawingForm").serialize(), function(data){
			console.log(data);
			if(data == 'SUCCESS'){
				populateDrawings($("#jstree-systems").jstree(true).get_node(parentId));
				alert('Drawing successfully added');
			}else
				alert('Unable to add Drawing to the part');
		});
	});
	
	$('#downloadPicture').click(function(){
		var guid = $("input[type='radio'][name='selectedPictureGuid']:checked").val();
		$.fileDownload(pageContextRoot+'system/download/picture/'+guid)
			.done(function () { 
				//$('#downloadSuccessAlert').show();
				alert('download completed.')
			})
			.fail(function () { 
				alert('Download failed.')
			});
	});
	$('#removePicture').click(function(){
		var guid = $("input[type='radio'][name='selectedPictureGuid']:checked").val();
		$.post('system/picture/remove/'+guid, function(data){
			  $('#'+data).remove();
			  alert('Element removed!!');
		})
	});	
	$('#addPicture').click(function(event){
		event.preventDefault();
		$('#pictureModal').modal('toggle');
		var parentId = $("#jstree-systems").jstree(true).get_selected('full',true)[0].parent;
		var guid = $("#jstree-systems").jstree(true).get_node(parentId).original.guid;
		$.get(pageContextRoot+'system/part/'+guid+'/not/pictures', function(data){
			$('#addPictureTable tbody').empty();
			$.each(data, function(index, item){
				var innerHtml = '<tr id="'+item.entityGuid+'"><td>' + item.fileName + '</td><td>'
				+ item.uploadDate + '</td><td>' + item.pages
				+ '</td><td>' + item.userFirstName + '</td><td><input type="checkbox" name="pictureGuid" value="'+item.entityGuid+'" autocomplete="off"></td</tr>';	
				$('#addPictureTable tbody').append(innerHtml);
			});
		});
	});
	
	$('#addPicturesBtn').click(function(event){
		event.preventDefault();
		$('#pictureModal').modal('toggle');
		var parentId = $("#jstree-systems").jstree(true).get_selected('full',true)[0].parent;
		var guid = $("#jstree-systems").jstree(true).get_node(parentId).original.guid;
		$.post(pageContextRoot+'system/part/'+guid+'/picture/add', $("#addPictureForm").serialize(), function(data){
			console.log(data);
			if(data == 'SUCCESS'){
				populatePictures($("#jstree-systems").jstree(true).get_node(parentId));
				alert('Picture successfully added');
			}else
				alert('Unable to add Picture to the part');			
		});
	});	
	
	$('#contractSubmit').click(function(event){
		//event.preventDefault();
		var guid = $("#jstree-systems").jstree(true).get_selected('full',true)[0].original.guid;
		var url = ($("#contractGuid").val() == null || $("#contractGuid").val() == '') ? pageContextRoot+"system/contract/create/"+guid:pageContextRoot+"system/contract/update";
		if(!validateContractForm())	{
			return false;
		}
		var datastring = $("#contractForm").serialize();
		console.log(datastring);
		$.post(url, datastring, function(data){
			if(data.guid != null && data.guid != ''){
			  alert('Contract updated!!');
			  feedbackRename = true;
			  $("#jstree-systems").jstree(true).rename_node($("#jstree-systems").jstree(true).get_selected('full',true)[0], data.systemTree.nomenclature);
			}else
				alert('Contract update failed!!');
		})
	});

	$('#openSubModalForAdd').click(function(){
		$('#componentTypeModal').modal('toggle');
		$('#genericModalForAdd').modal('toggle');
	});
	$('#genericModalForAdd').on('show.bs.modal', function(e){
		
		var chosenComponent = $("input:radio[name='addChoice']:checked").val();
		if(chosenComponent == 'CONTRACT'){
			cloneViewAndPopulateTheModal('contract-view', 'CONTRACT');
		}else if(chosenComponent == 'PART')
			cloneViewAndPopulateTheModal('part-view', 'PART');
		else if(chosenComponent == 'LINEITEM'){
			cloneViewAndPopulateTheModal('lineItem-view', 'LINEITEM');
			$('#genericModalForAdd-body').find('span.select2.select2-container.select2-container--default').each(function(){
				$(this).remove();
			});
			$('#genericModalForAdd-body select#lineItemType').select2({
				tags: true,
				createTag: function (params) {
					return {
						id: params.term,
						text: params.term,
						newOption: true
					}
				}				
			});
		}else if(chosenComponent == 'SERVICE'){
			cloneViewAndPopulateTheModal('serviceData-view', 'SERVICE');
			$('#genericModalForAdd-body').find('span.select2.select2-container.select2-container--default').each(function(){
				$(this).remove();
			});
			$('#genericModalForAdd-body select#serviceDataType').select2({
				tags: true,
				createTag: function (params) {
					return {
						id: params.term,
						text: params.term,
						newOption: true
					}
				}				
			});
		}
	});
	$('#closeAndSaveModal').click(function(){
		var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
		var jsonObj = convertFormToJSON($('#genericModalForAdd-body').find('form').serializeArray());
		if(globalViewId == 'PART'){
//			if(inventory == null || inventory == undefined){
//				alert('Please create inventory as well.');
//				return false;
//			}
			jsonObj.inventory = inventory;
			jsonObj.nextHigherAssembly = {guid : $('#genericModalForAdd-body').find('form').find('#nextHigherAssembly').val()};
		}
		console.log(jsonObj);
		$.ajax({
			type : 'POST',
			url : pageContextRoot+'system/tree/addComponent/'+(selectedNode == null ? 'null':selectedNode.original.guid),
			data : JSON.stringify(jsonObj),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			inventory=null;
			if(data.guid == null || data.guid == 'undefined'){
				alert('Unable to create component.');
				feedbackMove = true;
			}else{
				$('#genericModalForAdd').modal('toggle');
				$('#genericModalForAdd-body').empty();
				if(selectedNode!=null)
					$("#jstree-systems").jstree(true).create_node(selectedNode, data, 0);
				else
					$("#jstree-systems").jstree(true).create_node('#', data, 0);
				alert('Component successfully created.');
			}
		});
	});
	$('#addVendor').click(function(event){
		var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
		$.post(pageContextRoot+'system/part/'+$("#jstree-systems").jstree(true).get_node(selectedNode.parent).original.guid +'/vendors/add', $('#vendor-form').serialize(), function(result){
			if($(result).length == $('#allVendors option:selected').length)
				$('#allVendors option:selected').remove().appendTo('#currentVendors').removeAttr('selected');
		});
		//populateVendorOfPart($("#jstree-systems").jstree(true).get_selected('full',true)[0]);
		event.preventDefault();
	});
	$('#vendorRemoved').click(function(event){
		var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
		$.post(pageContextRoot+'system/part/'+$("#jstree-systems").jstree(true).get_node(selectedNode.parent).original.guid +'/vendors/remove', $('#vendor-part-form').serialize(), function(result){
			if(result)
				$('#currentVendors option:selected').remove().appendTo('#allVendors').removeAttr('selected');
		});
		event.preventDefault();
	});
	$('#showDrawings').click(function(){
		populateDrawings($("#jstree-systems").jstree(true).get_selected('full',true)[0]);
	});
	$('#showPics').click(function(){
		populatePictures($("#jstree-systems").jstree(true).get_selected('full',true)[0]);
	});
});


populateContract = function(treeNode) {
	$.get(pageContextRoot+'system/data/contract/' + treeNode.guid, function(data) {
		treeNode.domainObject = data;
		$('#country').val(data.country);
		$('#agencyCompany').val(data.agencyCompany);
		$('#customerEnduser').val(data.customerEnduser);
		$('#contractNumber').val(data.contractNumber);
		$('#contractDate').val(data.contractDate);
		//contractDatePicker = $('#contract-view').find('#contractDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: moment($('#contractDate').val(), 'MM/DD/YYYY')});
		contractDatePicker = $('#contract-view').find('#contractDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: moment()});
		$('#contract-view').find('#contractDatePicker').data('DateTimePicker').date(moment($('#contractDate').val(), 'MM/DD/YYYY'));
		$('#organization').val(data.organization);
		$('#shippingAddress').val(data.shippingAddress);
		$('#contractCurrency').val(data.contractCurrency);
		$('#commonCurrency').val(data.commonCurrency);
		$('#exchangeRate').val(data.exchangeRate);
		$('#contractGuid').val(data.guid);
		$('#contract-view').find('#nomenclature').val(data.systemTree.nomenclature);
		switchOnDisplay('contract-view');
		switchOffDisplay('part-view');
		switchOffDisplay('vendors-view');
		switchOffDisplay('drawing-view');
		switchOffDisplay('picture-view');
		switchOffDisplay('lineItem-view');
		switchOffDisplay('serviceData-view');
		initializeContractDropDowns($('#contractForm'));
	})
}

populateVendors = function(partNode) {
	console.log(partNode);
	populateVendorOfPart(partNode.original.guid);
	$.get(pageContextRoot+'system/part/' + partNode.original.guid + '/vendors/not', function(data){
		$('#allVendors').empty();
		$.each(data, function() {
			$('#allVendors').append($("<option />").val(this.guid).text(this.name));
		});
	});
	switchOffDisplay('part-view');
	switchOnDisplay('vendors-view');
	switchOffDisplay('drawing-view');
	switchOffDisplay('picture-view');
	switchOffDisplay('contract-view');
	switchOffDisplay('lineItem-view');
	switchOffDisplay('serviceData-view');
}
function populateVendorOfPart(partTreeGuid){
	$.get(pageContextRoot+'system/part/' + partTreeGuid + '/vendors', function(data){
		$('#currentVendors').empty();
		$.each(data, function() {
			$('#currentVendors').append($("<option />").val(this.guid).text(this.name));
		});
	});
}
populateDrawings = function(partNode) {
	console.log(partNode);
	$('#drag-and-drop-zone').dmUploader({
		url : pageContextRoot+'system/upload/drawing',
		dataType : 'json',
		extFilter : 'png;giff;bmp;jpg;PDF;pdf;zip;tar;DXF;dxf',
		extraData: {partGuid: partNode.original.guid},
		onUploadSuccess : function(id, data) {
			var innerHtml = '<tr id="'+data.entityGuid+'" onclick="draingHoverAction(\''+data.fileName+'\')"><td>' + data.fileName + '</td><td>'
					+ data.uploadDate + '</td><td>' + data.pages
					+ '</td><td>' + data.userFirstName + '</td><td><input type="radio" name="selectedDrawingGuid" value="'+data.entityGuid+'" autocomplete="off"></td</tr>';
			$('#drawingTable tbody').append(innerHtml);
			alert('upload completed successfully');
		},
		onUploadError : function(id, message) {
			alert('Upload failed!!')
		},
		onFileExtError: function(file){
		  console.log('File extension of ' + file.name + ' is not allowed');
		  alert('File extension of ' + file.name + ' is not allowed');
		}
	});
	switchOffDisplay('part-view');
	switchOffDisplay('vendors-view');
	switchOnDisplay('drawing-view');
	populateDrawingTable(partNode);
	switchOffDisplay('picture-view');
	switchOffDisplay('contract-view');
	switchOffDisplay('lineItem-view');
	switchOffDisplay('serviceData-view');
}
populatePictures = function(partNode) {
	console.log(partNode);
	$('#drag-and-drop-zone-picture').dmUploader({
		url : pageContextRoot+'system/upload/picture',
		dataType : 'json',
		extFilter : 'png;giff;bmp;jpg;PDF;pdf;zip;tar;DXF;dxf',
		extraData: {partGuid: partNode.original.guid},
		onUploadSuccess : function(id, data) {
			var innerHtml = '<tr id="'+data.entityGuid+'" onclick="draingHoverAction(\''+data.fileName+'\')"><td>' + data.fileName + '</td><td>'
					+ data.uploadDate + '</td><td>' + data.pages
					+ '</td><td>' + data.userFirstName + '</td><td><input type="radio" name="selectedPictureGuid" value="'+data.entityGuid+'" autocomplete="off"></td</tr>';
			$('#pictureTable tbody').append(innerHtml);
			alert('upload completed successfully');
		},
		onUploadError : function(id, message) {
			alert('Upload failed!!')
		},
		onFileExtError: function(file){
		  console.log('File extension of ' + file.name + ' is not allowed');
		  alert('File extension of ' + file.name + ' is not allowed');
		}
	});	
	switchOffDisplay('part-view');
	switchOffDisplay('vendors-view');
	switchOffDisplay('drawing-view');
	switchOnDisplay('picture-view');
	populatePictureTable(partNode);
	switchOffDisplay('contract-view');
	switchOffDisplay('lineItem-view');
	switchOffDisplay('serviceData-view');
}
switchOffDisplay = function(divToWorkWith) {
	$('#' + divToWorkWith).css('display', 'none');
}
switchOnDisplay = function(divToWorkWith) {
	$('#' + divToWorkWith).css('display', 'block');
}
populateDrawingTable = function(partNode) {
	$.get(pageContextRoot+'system/part/' + partNode.original.guid + '/drawings',
		function(data) {
			$('#drawingTable tbody').empty();
			$.each(data, function(i, item) {
				var innerHtml = '<tr id="'+item.entityGuid+'" onclick="draingHoverAction(\''+item.fileName+'\')"><td>' + item.fileName + '</td><td>'
						+ item.uploadDate + '</td><td>' + item.pages
						+ '</td><td>' + item.userFirstName + '</td><td><input type="radio" name="selectedDrawingGuid" value="'+item.entityGuid+'" autocomplete="off"></td></tr>';
				
				$('#drawingTable tbody').append(innerHtml)
			});
		}
	);
}
populatePictureTable = function(partNode) {
	$.get(pageContextRoot+'system/part/' + partNode.original.guid + '/pictures',
		function(data) {
			$('#pictureTable tbody').empty();
			$.each(data, function(i, item) {
				var innerHtml = '<tr id="'+item.entityGuid+'" onclick="draingHoverAction(\''+item.fileName+'\')"><td>' + item.fileName + '</td><td>'
						+ item.uploadDate + '</td><td>' + item.pages
						+ '</td><td>' + item.userFirstName + '</td><td><input type="radio" name="selectedPictureGuid" value="'+item.entityGuid+'" autocomplete="off"></td></tr>';
				
				$('#pictureTable tbody').append(innerHtml)
			});
		}
	);
}
function add_log(message) {
	var template = '<li>[' + new Date().getTime() + '] - ' + message + '</li>';

	$('#debug').find('ul').prepend(template);
}

function add_file(id, file) {
	var template = ''
			+ '<div class="file" id="uploadFile'
			+ id
			+ '">'
			+ '<div class="info">'
			+ '#1 - <span class="filename" title="Size: '
			+ file.size
			+ 'bytes - Mimetype: '
			+ file.type
			+ '">'
			+ file.name
			+ '</span><br /><small>Status: <span class="status">Waiting</span></small>'
			+ '</div>' + '<div class="bar">'
			+ '<div class="progress" style="width:0%"></div>' + '</div>'
			+ '</div>';

	$('#fileList').prepend(template);
}

function update_file_status(id, status, message) {
	$('#uploadFile' + id).find('span.status').html(message).addClass(status);
}

function update_file_progress(id, percent) {
	$('#uploadFile' + id).find('div.progress').width(percent);
}
function cloneViewAndPopulateTheModal(viewName, viewId){
	var clonedView = $('#'+viewName).clone();
	clonedView.find('form').attr('id', viewName+'-form-cloned');
	if(viewId == 'PART'){
		globalViewId = 'PART';
		partCallbackBeforeDisplay(clonedView);
	}else
		clonedView.find('button').remove();
	clonedView.find('form').find('input[name="guid"]').remove();
	$.each(clonedView.find('form').find('input'), function(index, item){
		$(item).val('');
		$(item).attr('data-modal', 'popup');
	});
	clonedView.find('form').append('<input type="hidden" name="VIEWID" value="'+viewId+'">');
	if(viewId == 'LINEITEM' || viewId == 'SERVICE'){
		var parentContractNode = findParentOfType($("#jstree-systems").jstree(true).get_selected('full',true)[0], 'CONTRACT');
		if(parentContractNode!=null){
			if(parentContractNode.original.domainObject == null){
				$.get(pageContextRoot+'system/data/contract/' + parentContractNode.original.guid, function(data) {
					parentContractNode.domainObject = data;
					clonedView.find('form').find('span#unitPriceAddOn').html(data.contractCurrency);
					clonedView.find('form').find('span#commonCurrentTargetPriceAddOn').html(data.contractCurrency);
					clonedView.find('form').find('span#extendedPriceAddOn').html(data.contractCurrency);
					clonedView.find('form').find('span#commonCurrencyPriceAddOn').html(data.commonCurrency);
				});
			}else{
				clonedView.find('form').find('span#unitPriceAddOn').html(parentContractNode.original.domainObject.contractCurrency);
				clonedView.find('form').find('span#commonCurrentTargetPriceAddOn').html(parentContractNode.original.domainObject.contractCurrency);
				clonedView.find('form').find('span#extendedPriceAddOn').html(parentContractNode.original.domainObject.contractCurrency);				
				clonedView.find('form').find('span#commonCurrencyPriceAddOn').html(parentContractNode.original.domainObject.commonCurrency);
			}
		}
	}
	$('#genericModalForAdd-body').empty();
	$('#genericModalForAdd-body').append(clonedView.html());
	$('#genericModalForAdd-body').find('span.select2.select2-container.select2-container--default').each(function(){
		$(this).remove();
	});	
	if(viewId == 'CONTRACT'){
		contractDatePicker = $('#genericModalForAdd-body').find('#contractDatePicker').datetimepicker({format : 'MM/DD/YYYY', useCurrent:true});
		initializeContractDropDowns($('#genericModalForAdd-body').find('form'));
	}else if(viewId == 'PART'){
		partCallbackAfterDisplay($('#genericModalForAdd-body'));
	}
	return clonedView;
}
function convertFormToJSON(formDataArray){
    
    var json = {};
    
    $.each(formDataArray, function() {
        json[this.name] = this.value || '';
    });
    
    return json;
}
function fireCustomerChange(element){
	$.getJSON(customerSelectURL+$(element).find('option:selected').data('guid')).done(function(data){
		console.log(data);
		var $form = $(element).closest('form');
		$form.find('input[name="contractCurrency"]').val(data.contractCurrency);
		$form.find('input[name="commonCurrency"]').val(data.commonCurrency);
		$form.find('input[name="exchangeRate"]').val(data.exchangeRate);
		$form.find('input[name="agencyCompany"]').val(data.agencyCompany);
		$form.find('input[name="organization"]').val(data.organization);
		$form.find('input[name="country"]').val(data.country);
	});
}
function draingHoverAction(fileName){
	popupPicture(iframeViewerURL+fileName);
	/*$('#imageViewer').toggle(true);
	$('#imageViewer').empty();
	$('<iframe>', {
		   src: iframeViewerURL+fileName,
		   id:  'myFrame',
		   frameborder: 0,
		   scrolling: 'no',
		   width: '250',
		   height: '250'
	}).appendTo('#imageViewer');*/
	//$('#bootBoxLink').empty();
	//$('#bootBoxLink').append('<img src="'+(contentProviderURL+fileName)+'" style="width: 250px; height: 150px;" onclick="popupPicture(\''+contentProviderURL+fileName+'\');"/>')
}
function populateFromJson(jsonObj, theForm){
	$(theForm).find('input[name="contractCurrency"]').val(jsonObj.domainObject.contractCurrency);
	$(theForm).find('input[name="commonCurrency"]').val(jsonObj.domainObject.commonCurrency);
	$(theForm).find('input[name="exchangeRate"]').val(jsonObj.domainObject.exchangeRate);	
}
function popupPicture(imageURL){
	var modalContent = !(imageURL.endsWith('pdf') || imageURL.endsWith('PDF')) ? ('<div class="row"><div class="col-md-12"><img src="'+imageURL+'" style="width:100%;"/></div></div>') : ('<div id="popupDiv" class="row"><div class="col-md-12"><iframe src="' +imageURL+ '" width="800" height="500" allowfullscreen webkitallowfullscreen id="myFrame"></iframe></div></div>');
	bootbox.dialog({
		size : 'large',
		title : 'Document Viewer',
		//message : '<div class="row"><div class="col-md-12"><img src="'+imageURL+'" style="width:100%;"/></div></div>'
		message : modalContent
	});
}
function findParentOfType(selectedNode, type){
	if(selectedNode == null)
		return null;
	do{
		if(selectedNode.original != null && selectedNode.original.nodeType == type){
			return selectedNode;
		}
		selectedNode = $("#jstree-systems").jstree(true).get_node(selectedNode.parent);
	}while(selectedNode.id != '#');
	return null;
}
function initializeContractDropDowns(form){
	$(form).find('select#agencyCompany').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	$(form).find('select#organization').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	$(form).find('select#commonCurrency').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	$(form).find('select#contractCurrency').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});	
	$(form).find('select#country').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});		
}