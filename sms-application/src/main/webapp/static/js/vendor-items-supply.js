/* JS */
$(document).ready(function(){
	var oTableNotVendor = $('#itemsNotSupplying').DataTable( {
		"ajax": "../inventory/vendor/"+vendorGuid+"/items/not",
        //"scrollY": '50vh',
        "scrollCollapse": true,	
        "columnDefs": [{
            "render": function ( data, type, row ) {
                return '<input type="checkbox" name="inventoryGuids" value="'+row.guid+'">';
            },
            "targets": 6
        }],	        
        "columns": [
            { "data": "idCode" },
            { "data": "systemPart.assemblyName" },
            { "data": "systemPart.assemblyNumber" },
            { "data": "quantityOnHand" },
            { "data": "standardOrderQuantity" },
            { "data": "lowQuantityThreshold" }
        ]  
    } );
	var oTableVendor = $('#itemsSupplying').DataTable( {
        "ajax": "../inventory/vendor/"+vendorGuid+"/items",
        //"scrollY": "50vh",
        "scrollCollapse": true,   
        "columns": [
            { "data": "idCode" },
            { "data": "systemPart.assemblyName" },
            { "data": "systemPart.assemblyNumber" },
            { "data": "quantityOnHand" },
            { "data": "standardOrderQuantity" },
            { "data": "lowQuantityThreshold" }
        ]
    } );
	$('#addInventory').click(function(){
		var rowCollection =  oTableNotVendor.$("input:checked", {"page": "all"});
		var invGuids = new Array();
		$.each(rowCollection, function(i,item){
			console.log(item.value);
			invGuids.push(item.value);
		});
		console.log(invGuids);
	})
	
});