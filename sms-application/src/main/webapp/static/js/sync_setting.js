/* JS */
$(document).ready(function(){
	var syncsettingTable = $('#syncSetting-table').DataTable({
        "ajax": syncsettingListURL,
        "columns": [	
            { "data": "url" },
            { "data": "frequencyHours" },
            { "data": "startHour" },
            { "data": "active" },
        ]
    });

});	