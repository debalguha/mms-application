/* JS */
$(document).ready(function(){
	var deliveryByDatePicker = $('#deliveryByDatePicker').datetimepicker({format : 'MM/DD/YYYY'});
	var issueDatePicker = $('#issueDatePicker').datetimepicker({format : 'MM/DD/YYYY'});
	$('#savePO').click(function(){
		var po = new Object();
		po.poNumber = $('#poNumber').val();
		po.rfqGuid = $('#rfqGuid').val();
		po.vendorGuid = $('#vendor').val();
		po.issueDate = $(issueDatePicker).data('date');
		po.deliveryByDate = $(deliveryByDatePicker).data('date');
		po.notes = $('#notes').val();
		po.deliveryInstruction = $('#deliveryInstruction').val();
		po.deliveryAddress = $('#deliveryAddress').val();
		po.poType = $('#poType').val();
		po.sAndH = $('#sAndH').val();
		po.taxes = $('#taxes').val();
		po.duty = $('#duty').val();
		po.discount = $('#discount').val();
		po.total = $('#total').val();		
		po.poItems = new Array();
		$('#rfqItemTable tbody tr').each(function(i, item){
			var poItem = new Object();
			poItem.guid = $(this).find('input[name="unitPrice"]').attr('data-guid');
			poItem.unitPrice = $(this).find('input[name="unitPrice"]').val();
			poItem.quantity = $(this).find('input[name="quantity"]').val();
			po.poItems.push(poItem);
		});
		console.log(po);
		$.ajax({
			type : 'POST',
			url : savePOFromRFQURL,
			data : JSON.stringify(po),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				alert('Unable to save PO.');
				feedbackMove = true;
			}else{
				$('#nav-link').val("po/po-home");
				alert('PO successfully Generated');
				$('#nlinkForm').attr('action', navigationURL);
				$('#nlinkForm').submit();
			}
		});	
	});
});