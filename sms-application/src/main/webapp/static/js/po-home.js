/* JS */
$(document).ready(function(){
	var oTablePO = $('#po-table').DataTable( {
		"dom": 'Blfrtip',
        "ajax": poListURL,
        "buttons": [ 'print'],
        "columns": [
            { "data": "poNumber" },
            { "data": "poType.poType" },
            { "data": "issueDate" },
            { "data": "statusDateCreated" },
            { "data": "poItems.length" },
            { "data": "vendor.name" },            
        ],
        "columnDefs": [
			{
			    "render": function ( data, type, row ) {
			 	   if(row.rfq!=null && row.rfq!='')
			 		   return row.rfq.rfqNumber;
			 	   else
			 		   return '';
			    },
			    "targets": 6
			}
         ]
    } );
	$('#po-table tbody').on( 'click', 'tr', function () {
		$(this).toggleClass('selected');
		var poGuid = oTablePO.rows('.selected').data()[0].guid;
		var urlToSubmit = $('#editForm').attr('action')+poGuid;
		$('#editForm').attr('action', urlToSubmit)
		$('#editForm').submit();
	});
	$('#po-table').css('background-color', '#FFFFFD');
});