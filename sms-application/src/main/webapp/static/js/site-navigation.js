/* JS */

$(document).ready(function() {
	$("#system-tree").click(function() {$("#nav-link").val("system/tree-view");$("#linkForm").submit();});
	$("#systems").click(function() {$("#nav-link").val("system/home");$("#linkForm").submit();});
	$("#parts").click(function() {$("#nav-link").val("system/parts");$("#linkForm").submit();});
	$("#myProfile").click(function() {$("#nav-link").val("user/edit-profile");$("#linkForm").submit();});
	$("#userMenu").click(function() {$("#nav-link").val("user/home");$("#linkForm").submit();});
	$("#siteMap").click(function() {$("#nav-link").val("user/site_map");$("#linkForm").submit();});
	$("#siteMapAdmin").click(function() {$("#nav-link").val("admin/site_map_admin");$("#linkForm").submit();});
	$("#contactSupport").click(function() {$("#nav-link").val("user/contact-support");$("#linkForm").submit();});
	$('#logout').click(function(){$("#nav-link").val("login");$("#linkForm").submit();});
	$("#posWorkOrder").click(function() {$("#nav-link").val("user/pos-work-order");$("#linkForm").submit();});
	$("#maintainWbs").click(function() {$("#nav-link").val("user/maintain-wbs");$("#linkForm").submit();});
	$('#purchaseOrders').click(function() {$("#nav-link").val("po/po-home");$("#linkForm").submit();});
	$("#maintainWbs").click(function() {$("#nav-link").val("user/maintain-wbs");$("#linkForm").submit();});
	$("#customerRecords").click(function() {$("#nav-link").val("customer/home");$("#linkForm").submit();});
	$("#inventory-list").click(function() {$("#nav-link").val("inventory/inventory");$("#linkForm").submit();});
	$("#vendors").click(function() {$("#nav-link").val("inventory/vendor/vendors");$("#linkForm").submit();});
	$("#inventory").click(function() {$("#nav-link").val("inventory/home");$("#linkForm").submit();});
	$("#rfqs").click(function() {$("#nav-link").val("rfq/home");$("#linkForm").submit();});
	$("#posWorkOrder").click(function() {$("#nav-link").val("user/pos-work-order");$("#linkForm").submit();});
	$("#maintainWbs").click(function() {$("#nav-link").val("user/maintain-wbs");$("#linkForm").submit();});
    $("#pictures").click(function() {$("#nav-link").val("system/picture");$("#linkForm").submit();});
    $("#drawings").click(function() {$("#nav-link").val("system/drawing");$("#linkForm").submit();});
    
    $("#createWorkOrder").click(function() {$("#nav-link").val("po/workorder/workorder-create");$("#linkForm").submit();});
    $("#completedWorkOrder").click(function() {$("#nav-link").val("po/workorder/workorders#param=completed-tab");$("#linkForm").submit();});
    $("#inProgressWorkOrder").click(function() {$("#nav-link").val("po/workorder/workorders#param=inprogress-tab");$("#linkForm").submit();});
    $("#scheduledWorkOrder").click(function() {$("#nav-link").val("po/workorder/workorders#param=scheduled-tab");$("#linkForm").submit();});    
/*    $("#completedWorkOrder").click(function() {$("#nav-link").val("po/workorder/workorder-completed");$("#linkForm").submit();});
    $("#inProgressWorkOrder").click(function() {$("#nav-link").val("po/workorder/workorder-inprogress");$("#linkForm").submit();});
    $("#scheduledWorkOrder").click(function() {$("#nav-link").val("po/workorder/workorder-scheduled");$("#linkForm").submit();});*/
    //completedWorkOrder
    
    $('#userProfiles').click(function(){$("#nav-link").val("admin/user_profile");$("#linkForm").submit();});
    $('#reporting').click(function(){$("#nav-link").val("admin/report");$("#linkForm").submit();});
    $('#auditlog').click(function(){$("#nav-link").val("admin/audit-view");$("#linkForm").submit();});
    $('#dataSettings').click(function(){$("#nav-link").val("admin/data/view-data");$("#linkForm").submit();});
    $('#syncSettings').click(function(){$("#nav-link").val("admin/sync_setting");$("#linkForm").submit();});
    $('#contentMgmt').click(function(){$("#nav-link").val("admin/content/view-content");$("#linkForm").submit();});

    
    $('#customer-list').click(function(){$("#nav-link").val("customer/customers");$("#linkForm").submit();});
    $('#vehicles').click(function(){$("#nav-link").val("vehicle/vehicles");$("#linkForm").submit();});
    $('#cancel').click(function(){history.back();});
    
    $.ajaxSetup({
        beforeSend:function(){
            // show gif here, eg:
            $("#loading").show();
        },
        complete:function(){
            // hide gif here, eg:
            $("#loading").hide();
        }
    });
});
