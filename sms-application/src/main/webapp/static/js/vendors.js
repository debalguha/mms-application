/* JS */
$(document).ready(function(){
	oTableVendor = $('#vendor-list-table').DataTable( {
		"dom": 'Blfrtip',
        //"ajax": vendorListURL,
		"buttons": ['print'],
        "columns": [
            { "data": "name" },
            { "data": "type.name" },
            { "data": "location.locationName" }
        ]
    } );
	$('#vendor-list-table tbody').on( 'click', 'tr', function () {
		$(this).toggleClass('selected');
		var aData = oTableVendor.rows('.selected').data()[0];//oTable.fnGetData(oTable.fnGetPosition(this));
		$('#guid').val(aData.guid);
		$('#vendorForm').submit();
	});
	
	$('#vendor-list-table').css('background-color', '#FFFFFD');
});	