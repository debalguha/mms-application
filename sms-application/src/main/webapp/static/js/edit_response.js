/* JS */
$(document).ready(function(){
	var deliveryByDatePicker = $('#deliveryByDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate : $('#deliveryByDate').val()!=null && $('#deliveryByDate').val()!=''? moment($('#deliveryByDate').val(), 'MM/DD/YYYY'): moment()});
	$('#vendorResponse').on('change', function(){
		//alert('Changed-'+$(this).val());
		$.get(rfqVendorResponseURL+$(this).val()).done(function(data){
			if(data.guid == null || data.guid == ''){
				$.get(rfqVendorResponseURL+$(this).val()).done(function(data){
					cleanAndPopulateItemsTable(data);
				});
			}else{
				$('#responseId').val(data.responseId);
				$('#deliveryByDatePicker').data('DateTimePicker').date(moment(data.deliveryByDate, 'MM/DD/YYYY'));
				cleanAndPopulateItemsTable(data.rfqResponseItems);
			}
		});
	});
	$('#addResponse').click(function(){
		
		var response = new Object();
		response.accepted=$('#accepted').val();
		response.rfqGuid = $('#rfqGuid').val();
		response.vendorGuid = $('#vendorResponse').val();
		response.notes = $('#notes').val();
		response.responseId = $('#responseId').val();
		response.deliveryByDate =  $(deliveryByDatePicker).data('date');
		response.items = new Array();
		$('input[name="unitPrice"]').each(function(i, item){
			var responseItem = new Object();
			responseItem.guid = $(this).attr('data-guid');
			responseItem.unitPrice = $(this).val();
			response.items.push(responseItem);
		});
		console.log(response);
		if(response.responseId.trim() == ''){
			bootbox.confirm("There is no Response ID, a default one will be generated. Would you like to proceed?", function(result) {
				if(result)
					postRFQResponseRecord(response);
			}); 
		}else
			postRFQResponseRecord(response);
	});
});
postRFQResponseRecord = function(response){
	$.ajax({
		type : 'POST',
		url : saveRFQURL,
		data : JSON.stringify(response),
		contentType: "application/json; charset=utf-8",
		dataType : 'json'
	}).always(function(data){
		if(data.guid == null || data.guid == 'undefined'){
			alert('Unable to save Response.');
			feedbackMove = true;
		}else{
			$('#nav-link').val("rfq/home");
			//alert('Response successfully generated');
			if($('#responseId').val() == ''){
				bootbox.alert("Response successfully generated. Please note the response id: "+data.responseId, function() {});
			}else
				bootbox.alert("Response successfully updated.", function() {});
			$('#linkForm').attr('action', navigationURL);
			$('#linkForm').submit();
		}
	});
}
cleanAndPopulateItemsTable = function(data){
	$('#rfqVendorTable tbody').empty();
	$.each(data, function(i, responseItem){
		$('#rfqVendorTable tbody').append('<tr><td>'+responseItem.systemPart.assemblyName+'</td><td>'+responseItem.quantity+'</td><td><input type="text" name="unitPrice" data-guid="'+responseItem.guid+'" value="'+responseItem.unitPrice+'"></td></tr>');
	});
}