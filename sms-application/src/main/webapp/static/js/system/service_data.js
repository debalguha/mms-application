/* JS */
$(document).ready(function() {
	$('#serviceDataType').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	$('#serviceDataSubmit').click(function(event){
		//event.preventDefault();
		var guid = $("#jstree-systems").jstree(true).get_selected('full',true)[0].original.guid;
		var url = ($("#serviceDataGuid").val() == null || $("#serviceDataGuid").val() == '') ? pageContextRoot+"system/serviceData/create/"+guid:pageContextRoot+"system/serviceData/update";
		if(!validateServiceForm())	{
			return false;
		}
		/*var datastring = $("#serviceDataForm").serialize();
		console.log(datastring);
		$.post(pageContextRoot+'system/serviceData/update', datastring, function(data){
			if(data.guid != null && data.guid != ''){
			  alert('Service updated!!');
			  feedbackRename = true;
			  $("#jstree-systems").jstree(true).rename_node($("#jstree-systems").jstree(true).get_selected('full',true)[0], data.systemTree.nomenclature);
			  populateService($("#jstree-systems").jstree(true).get_selected('full',true)[0].original);
			}else
				alert('Service update failed!!');
		})*/
		var serviceData = new Object();
		$.each($("#serviceDataForm").serializeArray(), function(i, item){
			serviceData[item.name] = item.value;
		});
		console.log(serviceData);
		$.ajax({
			type : 'POST',
			url : url,
			data : JSON.stringify(serviceData),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			if(data.guid != null && data.guid != ''){
				  alert('Service updated!!');
				  feedbackRename = true;
				  $("#jstree-systems").jstree(true).rename_node($("#jstree-systems").jstree(true).get_selected('full',true)[0], data.systemTree.nomenclature);
				  populateService($("#jstree-systems").jstree(true).get_selected('full',true)[0].original);
				}else
					alert('Service update failed!!');
		});		
	});	
	
});
populateService = function(treeNode) {
	$.get(pageContextRoot+'system/data/serviceData/' + treeNode.guid, function(data) {
		treeNode.domainObject = data;
		$('#idCodeService').val(data.idCode);
		$('#quantityService').val(data.quantity);
		$('#unitPriceService').val(data.unitPrice);
		$('#extendedPriceService').val(data.extendedPrice);
		$('#commonCurrencyPriceService').val(data.commonCurrencyPrice);
		$('#commonCurrentTargetPrice').val(data.commonCurrentTargetPrice);
		if($('#serviceDataType option[value="'+data.serviceDataType.guid+'"]').length==0){
			var option = $('<option></option>').attr('selected', true).text(data.serviceDataType.serviceDataType).val(data.serviceDataType.guid);
			option.appendTo($("#serviceDataType"));
			$('#serviceDataType').trigger('change');
		}else{
			$('#serviceDataType').val(data.serviceDataType.guid);
			$('#serviceDataType').trigger('change');
		}
			
		$('#serviceDataGuid').val(data.guid);
		$('#serviceData-view').find('#nomenclature').val(data.systemTree.nomenclature);
		switchOnDisplay('serviceData-view');
		switchOffDisplay('lineItem-view');
		switchOffDisplay('contract-view');
		switchOffDisplay('part-view');
		switchOffDisplay('vendors-view');
		switchOffDisplay('drawing-view');
		switchOffDisplay('picture-view');
		var parentContractNode = findParentOfType($("#jstree-systems").jstree(true).get_selected('full',true)[0], 'CONTRACT');
		if(parentContractNode!=null){
			if(parentContractNode.original.domainObject == null){
				$.get(pageContextRoot+'system/data/contract/' + parentContractNode.original.guid, function(data) {
					parentContractNode.domainObject = data;
					$('#serviceData-view span#unitPriceAddOn').text(data.contractCurrency);
					$('#serviceData-view span#extendedPriceAddOn').text(data.contractCurrency);
					$('#serviceData-view span#commonCurrencyPriceAddOn').text(data.commonCurrency);
					$('#serviceData-view span#commonCurrentTargetPriceAddOn').text(data.commonCurrency);
				});
			}else{
				$('#serviceData-view span#unitPriceAddOn').text(parentContractNode.original.domainObject.contractCurrency);
				$('#serviceData-view span#extendedPriceAddOn').text(parentContractNode.original.domainObject.contractCurrency);
				$('#serviceData-view span#commonCurrencyPriceAddOn').text(parentContractNode.original.domainObject.commonCurrency);
				$('#serviceData-view span#commonCurrentTargetPriceAddOn').text(parentContractNode.original.domainObject.commonCurrency);
			}
		}
		$.get(pageContextRoot+'vehicles/for/nomenclature', {value : treeNode.text}, function(data) {
			$('#vehicleStatusForService').empty();
			$('#vehicleStatusForService').append('<p>* '+data+' end items have been created for this one.</p>')
		});
	});
}