/* JS */
$(document).ready(function() {
	$('#lineItemType').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	$('#lineItemSubmit').click(function(event){
		//event.preventDefault();
		var guid = $("#jstree-systems").jstree(true).get_selected('full',true)[0].original.guid;
		var url = ($("#lineItemGuid").val() == null || $("#lineItemGuid").val() == '') ? pageContextRoot+"system/lineitem/create/"+guid:pageContextRoot+"system/lineitem/update";		
		if(!validateLineItemForm ())	{
			return false;
		}
		/*var datastring = $("#lineItemForm").serialize();
		console.log(datastring);
		$.post(url, datastring, function(data){
			if(data.guid != null && data.guid != ''){
			  alert('Line Item updated!!');
			  feedbackRename = true;
			  $("#jstree-systems").jstree(true).rename_node($("#jstree-systems").jstree(true).get_selected('full',true)[0], data.systemTree.nomenclature);
			  populateLineItem($("#jstree-systems").jstree(true).get_selected('full',true)[0].original);
			}else
				alert('Line Item update failed!!');
		});*/
		var lineItem = new Object();
		$.each($("#lineItemForm").serializeArray(), function(i, item){
			lineItem[item.name] = item.value;
		});
		console.log(lineItem);
		$.ajax({
			type : 'POST',
			url : url,
			data : JSON.stringify(lineItem),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			if(data.guid != null && data.guid != ''){
			  alert('Line Item updated!!');
			  feedbackRename = true;
			  $("#jstree-systems").jstree(true).rename_node($("#jstree-systems").jstree(true).get_selected('full',true)[0], data.systemTree.nomenclature);
			  populateLineItem($("#jstree-systems").jstree(true).get_selected('full',true)[0].original);
			}else
				alert('Line Item update failed!!');
		});
	});
});
populateLineItem = function(treeNode) {
	$.get(pageContextRoot+'system/data/lineitem/' + treeNode.guid, function(data) {
		treeNode.domainObject = data;
		$('input#lineItemGuid').val(data.guid);
		$('input#lineItem[data-op="view"]').val(data.lineItem);
		$('input#idCode[data-op="view"]').val(data.idCode);
		$('input#nomenClature[data-op="view"]').val(data.nomenClature);
		$('input#quantity[data-op="view"]').val(data.quantity);
		$('input#unitPrice[data-op="view"]').val(data.unitPrice);
		$('input#extendedPrice[data-op="view"]').val(data.extendedPrice);
		$('input#commonCurrencyPrice[data-op="view"]').val(data.commonCurrencyPrice);
		$('input#lineItemGuid[data-op="view"]').val(data.guid);
		$('input#commonCurrentTargetPrice[data-op="view"]').val(data.commonCurrentTargetPrice);
		
		if($('#lineItemType option[value="'+data.lineItemType.guid+'"]').length==0){
			var option = $('<option></option>').attr('selected', true).text(data.lineItemType.lineItemType).val(data.lineItemType.guid);
			option.appendTo($("#lineItemType"));
			$('#lineItemType').trigger('change');
		}else{
			$('#lineItemType').val(data.lineItemType.guid);
			$('#lineItemType').trigger('change');
		}
		
		$('#lineItem-view').find('input#nomenclature[data-op="view"]').val(data.systemTree.nomenclature);
		switchOnDisplay('lineItem-view');
		switchOffDisplay('contract-view');
		switchOffDisplay('part-view');
		switchOffDisplay('vendors-view');
		switchOffDisplay('drawing-view');
		switchOffDisplay('picture-view');
		switchOffDisplay('serviceData-view');
		var parentContractNode = findParentOfType($("#jstree-systems").jstree(true).get_selected('full',true)[0], 'CONTRACT');
		if(parentContractNode!=null){
			if(parentContractNode.original.domainObject == null){
				$.get(pageContextRoot+'system/data/contract/' + parentContractNode.original.guid, function(data) {
					parentContractNode.domainObject = data;
					$('#lineItem-view span#unitPriceAddOn').text(data.contractCurrency);
					$('#lineItem-view span#extendedPriceAddOn').text(data.contractCurrency);
					$('#lineItem-view span#commonCurrencyPriceAddOn').text(data.commonCurrency);
					$('#lineItem-view span#commonCurrentTargetPriceAddOn').text(data.commonCurrency);
				});
			}else{
				$('#lineItem-view span#unitPriceAddOn').text(parentContractNode.original.domainObject.contractCurrency);
				$('#lineItem-view span#extendedPriceAddOn').text(parentContractNode.original.domainObject.contractCurrency);
				$('#lineItem-view span#commonCurrencyPriceAddOn').text(parentContractNode.original.domainObject.commonCurrency);
				$('#lineItem-view span#commonCurrentTargetPriceAddOn').text(parentContractNode.original.domainObject.commonCurrency);
			}
		}
		$.get(pageContextRoot+'vehicles/for/nomenclature', {value : treeNode.text}, function(data) {
			$('#vehicleStatusForLineItem').empty();
			$('#vehicleStatusForLineItem').append('<p>* '+data+' end items have been created for this one.</p>')
		});		
	});
}