/* JS */
$(document).ready(function(){
	$('#addInventory').click(function(){
		inventory = new Object();
		inventory.quantityOnHand = $('#quantityOnHand').val();
		inventory.lowQuantityThreshold = $('#lowQuantityThreshold').val();
		inventory.standardOrderQuantity = $('#standardOrderQuantity').val();
		inventory.idCode = $('#invIdCode').val();
		inventory.adjustmentQuantity = $('#adjustmentQuantity').val();
		inventory.adjustmentDate = $('#adjustmentDate').val();
		inventory.remarks = $('#remarks').val();
		inventory.verifiedQuantity = $('#verifiedQuantity').val();
		inventory.location = {guid : $('#location').val()};
		$('#inventoryModalForPart').modal('toggle');
	});
	$('#partSubmit').click(function(event){
		
		var guid = $("#jstree-systems").jstree(true).get_selected('full',true)[0].original.guid;
		var url = ($("#partGuid").val() == null || $("#partGuid").val() == '') ? pageContextRoot+"system/part/create/"+guid:pageContextRoot+"system/part/update";
		/*if(!validatePartForm())	{
			return false;
		}*/
		var part = new Object();
		$.each($("#partForm").serializeArray(), function(i, item){
			part[item.name] = item.value;
		});
		part.inventory = inventory;
		part.materialName = buildObject($('#materialName'));
		part.materialType = buildObject($('#materialType'));
		part.materialSpec = buildObject($('#materialSpec'));
		part.process1 = buildObject($('#process1'));
		part.process2 = buildObject($('#process2'));
		part.process3 = buildObject($('#process3'));
		part.nextHigherAssembly = $('#nextHigherAssembly').val()==''?null:{guid : $('#nextHigherAssembly').val()};
		console.log(part);
		$.ajax({
			type : 'POST',
			url : url,
			data : JSON.stringify(part),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			inventory=null;
			if(data.guid != null && data.guid != ''){
				  alert('Part updated!!');
				  feedbackRename = true;
				  $("#jstree-systems").jstree(true).rename_node($("#jstree-systems").jstree(true).get_selected('full',true)[0], data.systemTree.nomenclature);
				  populateParts($("#jstree-systems").jstree(true).get_selected('full',true)[0].original);
				  //$('#jstree-systems').jstree(true).select_node($("#jstree-systems").jstree(true).get_selected('full',true)[0].original.guid);
				}else
					alert('Part create/update failed!!');
		});
	});	
	$('#wbsTreeModal').on('show.bs.modal', function(e){
		$(this).css('z-index', $('#genericModalForAdd').css('z-index')+10);
		buildWbsTree();
	});
});
partCallbackBeforeDisplay = function(element){
	$(element).find('#partSubmit').remove();
	$(element).find('#createInventory').click(function(){
		$('#inventoryModalForPart').modal('toggle');
	});
}
partCallbackAfterDisplay = function(element){
	$(element).find('#quoteCurrency').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	$(element).find('#ui').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	$(element).find('#cageCode').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	
	$(element).find('#materialName').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});$(element).find('#materialSpec').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});$(element).find('#materialType').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});	$(element).find('#category').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});	$(element).find('#process3').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});	$(element).find('#process2').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});	$(element).find('#process1').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	try{
		var selectedNodeJson = $("#jstree-systems").jstree(true).get_selected('full',true)[0].original;
		if(selectedNodeJson!=null){
			$(element).find('#nomenclature').val(selectedNodeJson.text);
		}
	} catch(e){}
}
populateAdditionalPartData = function(data){
	if($('#process1 option[value="'+data.process1.guid+'"]').length==0)
		$("#process1").append($('<option>', {value:data.process1.guid, text:data.process1.partProcess}));
	$('#process1').val(data.process1.guid);
	
	if($('#process2 option[value="'+data.process2.guid+'"]').length==0)
		$("#process2").append($('<option>', {value:data.process2.guid, text:data.process2.partProcess}));
	$('#process2').val(data.process2.guid);
	
	if($('#process3 option[value="'+data.process2.guid+'"]').length==0)
		$("#process3").append($('<option>', {value:data.process3.guid, text:data.process3.partProcess}));
	$('#process3').val(data.process3.guid);
	
	
	if($('#materialName option[value="'+data.materialName.guid+'"]').length==0)
		$("#materialName").append($('<option>', {value:data.materialName.guid, text:data.materialName.materialName}));
	$('#materialName').val(data.materialName.guid);
	
	if($('#materialType option[value="'+data.materialType.guid+'"]').length==0)
		$("#materialType").append($('<option>', {value:data.materialType.guid, text:data.materialType.materialType}));
	$('#materialType').val(data.materialType.guid);
	
	if($('#materialSpec option[value="'+data.materialSpec.guid+'"]').length==0)
		$("#materialSpec").append($('<option>', {value:data.materialSpec.guid, text:data.materialSpec.materialSpecification}));
	$('#materialSpec').val(data.materialSpec.guid);
	
	$('#materialShape').val(data.materialShape);
	$('#rev').val(data.rev);
	$('#sequence').val(data.sequence);
	$('#cageCode').val(data.cageCode);
	if($('#cageCode option').find(data.cageCode).length<=0)
		$("#cageCode").append($('<option>', {value:data.cageCode, text:data.cageCode}));
	$('#wbsTree').val(data.wbsTree.guid);
	$('#wbsText').val(data.wbsTree.nomenclature);
}
buildObject = function(element){
	var obj = new Object();
	var text = element.find("option:selected").text();
	var value = element.find("option:selected").val();
	if(text!=value)
		obj['guid'] = value;
	obj[element.data('objname')] = text;
	if(value == '')
		return null;
	return obj;
}
openSystemTree = function(element){
	var treeDialog = bootbox.dialog({
		size : 'large',
		title : 'Choose next higher assembly',
		//message : '<div class="row"><div class="col-md-12"><img src="'+imageURL+'" style="width:100%;"/></div></div>'
		message : '<div id="jstree_higher-assembly" style="height: 460px; border-color: gray; overflow: scroll;"></div>'
	});
	treeDialog.init(function(){
		$('#jstree_higher-assembly').on('changed.jstree', function(e, data) {
			$(element).val(data.node.text);
			$(element).parent().find('#nextHigherAssembly').val(data.node.id);
			treeDialog.modal('toggle');
		}).jstree({
			"core" : {
				"themes" : {
					"name" : "proton",
					"responsive" : true
				},
				'data' : {
					'url' : treeURL
				},
				"check_callback" : true
			}			
		});
	});
}
populateParts = function(treeNode) {
	$.get(pageContextRoot+'system/data/part/' + treeNode.guid, function(data) {
		treeNode.domainObject = data;
		$('#assemblyNumber').val(data.assemblyNumber);
		$('#assemblyName').val(data.assemblyName);
		$('#barCode').val(data.barCode);
		$('#manufacturerPartNumber').val(data.manufacturerPartNumber);
		$('#partGuid').val(data.guid);
		$('#manufacturer').val(data.manufacturer);
		$('#manufacturerPartURL').val(data.manufacturerPartURL);
		$('#lastOrderURL').val(data.lastOrderURL);
		
		$('#oem').val(data.oem);
		$('#oemPartNum').val(data.oemPartNum);
		$('#subIdCode').val(data.subIdCode);
		if($('#ui option').find(data.ui).length<=0)
			$("#ui").append($('<option>', {value:data.ui, text:data.ui}));
		$('#ui').val(data.ui);
		if($('#category option').find(data.category).length<=0)
			$("#category").append($('<option>', {value:data.category, text:data.category}));
		$('#category').val(data.category);
		$('#quoteCurrency').val(data.quoteCurrency);
		if($('#quoteCurrency option').find(data.quoteCurrency).length<=0)
			$("#quoteCurrency").append($('<option>', {value:data.quoteCurrency, text:data.quoteCurrency}));
		populateAdditionalPartData(data); //find in part-view.js
		$('#part-view').find('#nomenclature').val(data.systemTree.nomenclature);
		$('#part-view').find('#nextHigherAssembly').val(data.nextHigherAssembly.guid);
		$('#part-view').find('#nextHigherAssemblyText').val(data.nextHigherAssembly.nomenclature);
		$('#part-view').find('#quantity').val(data.quantity);
		$('#part-view').find('#unitPrice').val(data.unitPrice);
		$('#part-view').find('#commonCurrencyPrice').val(data.commonCurrencyPrice);
		$('#part-view').find('#extendedPrice').val(data.extendedPrice);
		$('#part-view').find('#commonCurrentTargetPrice').val(data.commonCurrentTargetPrice);
		var parentContractNode = findParentOfType($("#jstree-systems").jstree(true).get_selected('full',true)[0], 'CONTRACT');
		if(parentContractNode!=null){
			if(parentContractNode.original.domainObject == null){
				$.get(pageContextRoot+'system/data/contract/' + parentContractNode.original.guid, function(data) {
					parentContractNode.domainObject = data;
					$('#part-view span#unitPriceAddOn').text(data.contractCurrency);
					$('#part-view span#extendedPriceAddOn').text(data.contractCurrency);
					$('#part-view span#commonCurrencyPriceAddOn').text(data.commonCurrency);
					$('#part-view span#commonCurrentTargetPriceAddOn').text(data.commonCurrency);
				});
			}else{
				$('#part-view span#unitPriceAddOn').text(parentContractNode.original.domainObject.contractCurrency);
				$('#part-view span#extendedPriceAddOn').text(parentContractNode.original.domainObject.contractCurrency);
				$('#part-view span#commonCurrencyPriceAddOn').text(parentContractNode.original.domainObject.commonCurrency);
				$('#part-view span#commonCurrentTargetPriceAddOn').text(parentContractNode.original.domainObject.commonCurrency);
			}
		}
		switchOnDisplay('part-view');
		switchOffDisplay('vendors-view');
		switchOffDisplay('drawing-view');
		switchOffDisplay('picture-view');
		switchOffDisplay('contract-view');
		switchOffDisplay('lineItem-view');
		switchOffDisplay('serviceData-view');
		partCallbackAfterDisplay($('#partForm'));
	});
}