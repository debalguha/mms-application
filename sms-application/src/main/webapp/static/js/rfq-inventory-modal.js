/* JS */
var inventoriesAdded = new Array();
var vendorsAdded = new Array();
var oTableInv;
var selectedGuids = new Map();
$(document).ready(function(){
	$('#removeVendor').click(function(){
		$('input[type="checkbox"][name="vendorChose"]').each(function(i, item){
			if($(this).prop('checked')){
				var index = $.inArray($(this).val(), vendorsAdded);
				vendorsAdded.splice(index, 1);
				$(this).closest('tr').remove();
			}
		});
	});
	$('#addVendor').click(function(){
		$.getJSON(remainingVendorLookupURL+$('#rfqGuid').val()).done(function(data){
			if(data.length>0){
				bootbox.dialog({
					size: 'small',
					title: 'More Vendors',
					message: getVendorsList(data),
					buttons: {
						add: {
							label: 'Add Vendor',
							className: "btn-warning",
							callback: function(){
								vendorPopupChooseAction();
							}
						}
					}
				});				
			}else{
				bootbox.dialog({
				  title: "That html",
				  message: '<p>No more vendors are supplying these inventories.</p>'
				});				
			}
		});
	})
	$('#addItem').click(function(){
		bootbox.dialog({
			size: 'large',
			title: "Inventory List",
			message: '<table id="inventory-table" class="table table-hover" cellspacing="0" width="100%">'+
						        '<thead>'+
						            '<tr>'+
						                '<th>ID Code</th>'+
						                '<th>WBS</th>'+
						                '<th>Item Description</th>'+
						                '<th>High Unit Price</th>'+
						                '<th>Low Unit Price</th>'+
						                '<th>Quantity On Hand</th>'+
						                '<th>Low Quantity Threshold</th>'+
						                '<th>Choose</th>'+
						            '</tr>'+
						        '</thead>'+
						        '<tbody></tbody>'+					
							'</table>'+
					        '<script> '+	
					    	'oTableInv = $("#inventory-table").DataTable( {'+
					            '"ajax": inventoryURL,'+
					            '"columns": ['+
					                '{ "data": "idCode" },'+
					                '{ "data": "wbs" },'+
					                '{ "data": "itemDescription" },'+
					                '{ "data": "highUnitPrice" },'+
					                '{ "data": "lowUnitPrice" },'+
					                '{ "data": "quantityOnHand" },'+
					                '{ "data": "lowQuantityThreshold" },'+
					            '],'+
				                '"columnDefs": ['+
	                               '{'+
	                                   '"render": function ( data, type, row ) {'+
	                                	   'return "<input type=\'checkbox\' onclick=\'checkBoxAction(this)\'>";'+
	                                   '},'+
	                                   '"targets": 7'+
	                               '}]'+
					    	'});'+
					    '</script>',
			buttons: {
				add: {
					label: "Add to RFQ",
					className: "btn-warning",
					callback: function(){
						selectedGuids.forEach(function(value, key){
							if($.inArray(key, inventoriesAdded)<0){
								inventoriesAdded.push(key);
								$('#rfqItemTable').find('tbody').append('<tr><td>'+value.idCode+'</td><td>'+value.quantityOnHand+'</td><td>'+value.lowQuantityThreshold+'</td><td><input type="text" name="quantity" data-inv-guid="'+value.guid+'"></td></tr>');
								$.getJSON(vendorLookupByPartURLPrefix+value.guid+vendorLookupByPartURLSuffix).done(function(data){
									$.each(data, function(index, item){
										$('#rfqVendorTable').find("tbody").append('<tr><td>'+item.name+'</td><td></td><td></td><td><input type="checkbox" name="vendorChose" value="'+item.guid+'"></td></tr>');
									});
								});
							}
						});
					}
				}
			}
		});
	});
});
checkBoxAction = function(item){
	 var selectedJson = oTableInv.rows($(item).closest('tr')).data()[0];
	 console.log(selectedJson);
	 if(!$(item).prop('checked'))
		 selectedGuids.delete(selectedJson.guid);
	 else
		 selectedGuids.set(selectedJson.guid, selectedJson);
}
getVendorsList = function(data){
	var html;
	var htmlPrefix = '<table id="vendor-table" class="table table-hover" cellspacing="0" width="100%">'+
							 '<thead>'+
							     '<tr>'+
							         '<th>Name</th>'+
							         '<th>Choose</th>'+
							     '</tr>'+
							 '</thead>'+
							 '<tbody>';
	html = html+htmlPrefix;
	$.each(data, function(index, item){
		html+='<tr><td>'+item.name+'</td><td><input type=\'checkbox\' name=\'vendorModalChoose\' data-name=\''+item.name+'\' data-guid=\''+item.guid+'\'></td></tr>';
	});
   var htmlSuffix = '</tbody></table>';
   return html+htmlSuffix;
						
}
vendorPopupChooseAction = function(){
	$('input[type="checkbox"][name="vendorModalChoose"]').each(function(i, item){
		if($(this).prop('checked')){
			if($.inArray(vendorsAdded, $(this).data('guid'))<0){
				vendorsAdded.push($(this).data('guid'));	
				$('#rfqVendorTable').find('tbody').append('<tr><td>'+$(this).data('name')+'</td><td></td><td></td><td><input type="checkbox" name="vendorChose" value="'+$(this).data('guid')+'"></td></tr>');
			}
		}
	});
}
