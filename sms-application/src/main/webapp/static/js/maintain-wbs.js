/* JS */
$(document).ready(function(){
	buildWBSTree();
	$('#wbsSubmit').click(function(){
		var selectedNode = $("#jstree-wbs").jstree(true).get_selected('full',true)[0];
		var datastring = $("#wbsForm").serialize();
		console.log(datastring);
		$.post('wbs/tree/create', datastring).done(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				alert('Update failed!!');
			}else{
				alert('Update Successfully!')
				if(selectedNode!=null){
					if($('#guid').val() == '')
						$("#jstree-wbs").jstree(true).create_node(selectedNode, data, 0);
					else{
						feedbackRename = true;
						$("#jstree-wbs").jstree(true).rename_node($("#jstree-wbs").jstree(true).get_selected('full',true)[0], $('#nomenclature').val());
					}
				}else
					$("#jstree-wbs").jstree(true).create_node(null, data, 0);
			}
		});
	});
	$('#wbsAdd').click(function(){
		var selectedNode = $("#jstree-wbs").jstree(true).get_selected('full',true)[0];
		$("#wbsForm")[0].reset();
		if(selectedNode!=null)
			$('#parentGuid').val(selectedNode.original.guid);
		$('#guid').val('');
		$('#wbsTab a:first').tab('show')
		switchOnDisplay('wbs-view');
		$('#wbs-view').effect( "shake", "left", 4);
	});
});
