/* JS */
$(document).ready(function(){
	buildSystemTree();
	$('#addSystemTreeBtn').click(function(){
		/* if(selectedJson!=null && selectedJson.nodeType != 'PART'){
			alert('Please choose a part node. You have chosen '+selectedJson.nodeType);
			return false;
		} */
		if(selectedJson!=null){
			$('#nomenclature').val(selectedJson.text);
			if(selectedJson.domainObject != null)
				$('#partNumber').val(selectedJson.domainObject.assemblyNumber);
			$('#systemTree').val(selectedJson.guid);
			findNextHigherAssemblies($('#nextHigherAssembly'));
		}
		$('#woTreeModal').modal('toggle');
	});
	$('#woTreeModal').on('show.bs.modal', function (e) {
		selectedJson = null;
	  	var nodeId = $.get(treeComponentURL, {nomenClature: $('#nomenclature').val(), partNumber: $('#partNumber').val()}, function(data){
			treeId = data;
			console.log($('#jstree-systems').jstree(true).search(treeId));
			$.each(data, function(index, obj){
				$('#jstree-systems').jstree(true).search(obj.nomenclature);
			 	$('#jstree-systems').jstree(true).select_node(obj.guid);
			});
	  	});
	})	
	$('#deleteButton').click(function(){
		$('#vehicleDelete').val('delete');
		$('#vehicleForm').submit();
	});
});
findNextHigherAssemblies = function(element){
	$.get(pageContextRoot+'vehicles/with/nomenclature', {value: $('#nomenclature').val()}, function(data){
		$(element).empty();
		$(element).append($("<option></option>")
		         .attr("value","")
		         .text("----None----"));		
		$.each(data, function(index, item){
			$(element).append($("<option></option>")
			         .attr("value",item.guid)
			         .text(item.serialNumber));
		});
	});
}