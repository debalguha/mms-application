/* JS */
$(document).ready(function(){
	$('#agencyCompany').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
	$('#country').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});	
	$('#organization').select2({
		  tags: true,
		  createTag: function (params) {
		    return {
		      id: params.term,
		      text: params.term,
		      newOption: true
		    }
		  }
	});
});
