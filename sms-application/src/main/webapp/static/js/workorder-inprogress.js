/* JS */
$(document).ready(function(){
	var woTable = $('#workorder-inprogress-table').DataTable( {
        "ajax": inProgressWOList,
        "columns": [
                    //{ "data": "systemTree.nomenclature" },
                    { "data": "location.locationName" },
                    { "data": "customer.customerEndUser" },
                    { "data": "creationDate" },
                    { "data": "promisedDate" },
                    { "data": '' },
                    { "data": "priority" },
                    { "data": "techUserGuid.firstName" },
                    { "data": "superVisorGuid.firstName" }
                ],        
                "columnDefs": [
                   {
                       "render": function ( data, type, row ) {
                    	   var daysToGo = moment(row.promisedDate, "MM/DD/YYYY").diff(moment(), 'days');
                    	  return daysToGo<0?0:daysToGo;
                       },
                       "targets": 4
                   }
               ]
            } );
        	$('#workorder-inprogress-table').css('background-color', '#FFFFFD');
        	$('#workorder-inprogress-table').on('dblclick', 'tr', function(){
        		var selectedJson = woTable.rows($(this)).data()[0];
        		$('#guid').val(selectedJson.guid);
        		$('#woEditForm').submit();
        	});
        });	