calculateExtendedPriceIfPossibleNew = function(control){
	var quantity = $(control).closest('form').find('input[name="quantity"]').val();
	var unitPrice = $(control).closest('form').find('input[name="unitPrice"]').val();
	if(quantity=='' || unitPrice == '')
		return;
	$(control).closest('form').find('input[name="extendedPrice"]').prop("readonly", false);
	$(control).closest('form').find('input[name="extendedPrice"]').val($.number(parseFloat(unitPrice) * parseInt(quantity), 2));
	$(control).closest('form').find('input[name="extendedPrice"]').prop("readonly", true);
	//$.number($(control).closest('form').find('input[name="extendedPrice"]').val(), 2 )
	var selectedNode = null;
	try{
		selectedNode=$("#jstree-systems").jstree(true).get_selected('full',true)[0];
		while(selectedNode != null && selectedNode.original.nodeType!='CONTRACT' && selectedNode.parent!='#'){
			selectedNode = $("#jstree-systems").jstree(true).get_node(selectedNode.parent);
		}
	}catch(e){}
	
	if(selectedNode == null || selectedNode.original.nodeType!='CONTRACT')
		return;
	if(selectedNode.original.domainObject !=null){
		$(control).closest('form').find('input[name="commonCurrencyPrice"]').prop("readonly", false);
		if(selectedNode.original.domainObject.exchangeRate != '')
			$(control).closest('form').find('input[name="commonCurrencyPrice"]').val($.number(parseFloat(unitPrice) * parseInt(quantity) * parseFloat(selectedNode.original.domainObject.exchangeRate), 2));
		else
			$(control).closest('form').find('input[name="commonCurrencyPrice"]').val('0.00');
		$(control).closest('form').find('input[name="commonCurrencyPrice"]').prop("readonly", true);
	} else {
		$.getJSON(pageContextRoot+'system/data/contract/'+selectedNode.original.guid, function(data){
			$(control).closest('form').find('input[name="commonCurrencyPrice"]').prop("readonly", false);
			if(data.exchangeRate != '')
				$(control).closest('form').find('input[name="commonCurrencyPrice"]').val($.number(parseFloat(unitPrice) * parseInt(quantity) * parseFloat(data.exchangeRate), 2));
			else
				$(control).closest('form').find('input[name="commonCurrencyPrice"]').val('0.00');
			$(control).closest('form').find('input[name="commonCurrencyPrice"]').prop("readonly", true);
		});
	}
}