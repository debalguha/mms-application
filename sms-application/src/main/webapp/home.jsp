<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Main-Home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="static/styles/custom.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/styles/style.css" />
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/static/js/user-home.js"></script>

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div id="navbar" class="navbar-collapse collapse">
				<form class="navbar-form navbar-right">
					<div class="form-group">
						<input type="text" class="form-control" id="searchText"
							aria-describedby="inputSuccess3Status" placeholder="Search">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
					</div>
				</form>
			</div>
			<!--/.navbar-collapse -->
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
<!-- 				<h2>Welcome back, Thomas Roberts.</h2> -->
				tsaddler
				<h2>Welcome back, Tsaddler</h2>
				<p>Information describing what the user can do on the Dashboard
					screen will go here.</p>
				<div class="user-nav">
					<ul>
						<li><a href="user/home.jsp">
								<div class="icon">
									<img src="static/images/user-menu-icon.png" />
								</div>
								<h3>User Menu</h3>
								<h5>Normal User Options</h5>
						</a></li>
						<li><a href="user/edit-profile.jsp">
								<div class="icon">
									<img src="static/images/profile-icon.png" />
								</div>

								<h3>Profile</h3>
								<h5>User Information</h5>
						</a></li>
						<li><a href="user/contact-support.jsp">
								<div class="icon">
									<img src="static/images/contact-icon.png" />
								</div>

								<h3>Contact Support</h3>
								<h5>Technical Support Options</h5>
						</a></li>
						<li><a href="#">
								<div class="icon">
									<img src="static/images/site-map-icon.png" />
								</div>

								<h3>Site Map</h3>
								<h5>Website Pages List</h5>
						</a></li>

					</ul>
				</div>
			</div>
		</div>
		<form id="linkForm" action="navigation" method="post">
			<input type="hidden" name="nav-link" id="nav-link">
		</form>
	</div>
	<footer class="footer">
	<div class="flogo">
		<img src="static/images/footer-logo.png" />
	</div>
	<!-- <div class="container">
		  <p class="text-muted">Place sticky footer content here.</p>
		</div> -->
	<div class="copyright">� 2015 TRS INTERNATIONAL GROUP – Defense
		Consulting, Intelligence and Training For Real World Environments. All
		Rights Reserved</div>
	</footer>
	<!-- 	<footer>
		<div class="flogo">
			<img src="static/images/footer-logo.png" />
		</div>
		<div class="copyright">© 2015 TRS INTERNATIONAL GROUP – Defense
			Consulting, Intelligence and Training For Real World Environments.
			All Rights Reserved</div>
	</footer> -->
</body>
<script>
	$(document).ready(function() {
		$('*').each(function(i, item){
			$(item).css("font-family", "source_sans_proregular");
		});
	});
</script>
</html>