<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>pos-work-orders</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/user-home.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/styles/style.css" />

</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="row">
					<div class="col-md-6">
						<div id="createWorkOrder" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
							<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Create Work Order</p>
						</div>		
					</div>			
					<div class="col-md-6">
						<div id="inProgressWorkOrder" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
							<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Existing Work Order</p>
						</div>		
					</div>
				</div>
				<div class="row" style="margin-top: 30px;">
					<div class="col-md-6">
						<div id="completedWorkOrder" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
							<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Completed Work Order</p>
						</div>		
					</div>					
					<div class="col-md-6">
						<div id="scheduledWorkOrder" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
							<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Scheduled Work Order / PMCS</p>
						</div>		
					</div>
				</div>
				<div class="row" style="margin-top: 30px;">
					<div class="col-md-12">
						<div id="maintainWbs" style="width: 350px; height: 50px; display: table; cursor: pointer; background-color: #CDB79E;">
							<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Maintain WBS</p>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
	<form id="linkForm" action="navigation" method="post">
		<input type="hidden" name="nav-link" id="nav-link">
	</form>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>	
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>
