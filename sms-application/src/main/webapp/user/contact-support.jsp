<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ContactSupport</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>	
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var occurredDatePicker = $('#occurredDatePicker').datetimepicker({format : 'MM/DD/YYYY'});	
});
</script>	
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<c:if test="${not empty requestScope.statusMsg}">
			<div>
				<p class="bg-success">
					<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					<c:out value="${requestScope.statusMsg}"/>
				</p>
			</div>
		</c:if>
		<form action="${pageContext.request.contextPath}/support/email" method="post">
			<div class="row">
				<div class="col-md-6 col-md-push-1">
					<div class="form-group">
						<label for="title">Title</label> 
						<input type="text" class="form-control" id="subject" name="subject" placeholder="Title">
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-3 col-md-push-1">
					<div class="form-group">
						<label for="phone">Date Of occurrence:</label> 
		                <div class="input-group date" id="occurredDatePicker">
		                    <input id="occurredDate" type="text" class="form-control" name="occurredDate"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>										
					</div>			
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-md-push-1">
					<div class="form-group">
	                    <select class="form-control" name="severity">
	                        <option value="1">Very High</option>
	                        <option value="2">High</option>
	                        <option value="3">Medium</option>
	                        <option value="4">Low</option>
	                        <option value="5">Minor</option>	                        
	                    </select>
					</div>				
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-push-1">
					<div class="form-group">
						<label for="detailedDesc">Detailed Description of Problem of Issue</label> 
						<textarea cols="800" rows="10" class="form-control" id="description" name="description" placeholder="Description"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2 col-md-push-1">
					<button type="submit" class="btn btn-default active">Submit</button>
				</div>
			</div>
		</form>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>
