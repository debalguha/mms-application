<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User-Home</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap-submenu.min.css" />
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap-submenu.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>


</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>

	<div class="row">
		<div class="col-xs-6 col-md-3">
			<ul>
				<h4 style="color: #50403b">System</h4>
				<li><a style="color: #575757" id="systems" href="#">System Menu</a></li>
				<li><a style="color: #575757" id="system-tree" href="#">Systems</a></li>
				<li><a style="color: #575757" id="parts" href="#">Parts</a></li>
				<li><a style="color: #575757" id="pictures" href="#">Pictures</a></li>
				<li><a style="color: #575757" id="drawings" href="#">Drawings</a></li>
			</ul>
		</div>

		<div class="col-xs-6 col-md-3">
			<ul>
				<h4 style="color: #50403b">Customer Records</h4>
				<li><a style="color: #575757" id="customerRecords" href="#">Customers Menu</a></li>
				<li><a style="color: #575757" id="customer-list" href="#">Customers</a></li>
				<li><a style="color: #575757" id="vehicles" href="#">End Items & Serialized Components</a></li>
			</ul>
		</div>


		<div class="col-xs-6 col-md-3">
			<ul>
				<h4 style="color: #50403b">POS/WorkOrders</h4>
				<li><a style="color: #575757" id="posWorkOrder" href="#">POS/
						Work Order Menu</a></li>
				<li><a style="color: #575757" id="createWorkOrder" href="#">Create
						Work Order</a></li>
				<li><a style="color: #575757" id="inProgressWorkOrder" href="#">Existing
						Work Order</a></li>
				<li><a style="color: #575757" id="completedWorkOrder" href="#">Completed
						Work Order</a></li>
				<li><a style="color: #575757" id="scheduledWorkOrder" href="#">Scheduled
						Work Order</a></li>
			</ul>
		</div>
		<div class="col-xs-6 col-md-3">
			<ul>
				<h4 style="color: #50403b">Inventory</h4>
				<li><a style="color: #575757" id="inventory" href="#">Inventory Menu</a></li>
				<li><a style="color: #575757" id="inventory-list" href="#">Inventory</a></li>
				<li><a style="color: #575757" id="rfqs" href="#">RFQs</a></li>
				<li><a style="color: #575757" id="purchaseOrders" href="#">Purchase
						Orders</a></li>
				<li><a style="color: #575757" id="vendors" href="#">Vendors</a></li>
			</ul>

		</div>

	</div>



	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
</html>