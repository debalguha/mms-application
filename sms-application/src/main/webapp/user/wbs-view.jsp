				<div id="wbs-view" style="display:none;">
					<form id="wbsForm">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="country">Nomenclature:</label> <input 
										type="text" class="form-control" id="nomenclature" name="nomenclature"
										placeholder="Nomenclature">
								</div>
								<div class="form-group">
									<label for="email">wbs:</label> <input
										type="text" class="form-control" id="wbs" name="wbs"
										placeholder="Wbs">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-10">
								<div class="form-group">
									<label for="lastName">Description:</label> <textarea  rows="5" cols="500"
										type="text" class="form-control" id="description" name="description"
										placeholder="Description"></textarea>
								</div>
							</div>
						</div>
						<input type="hidden" name="guid" id="guid">
						<input type="hidden" name="parentGuid" id="parentGuid">
						<button id="wbsSubmit" type="button" class="btn btn-default active">Save</button>
					</form>
				</div>