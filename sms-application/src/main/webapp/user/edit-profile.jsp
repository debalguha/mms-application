<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EditProfile</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<div class="row spacer-half">
			<div class="col-md-6 col-md-push-1">
				<form action="${pageContext.request.contextPath}/myprofile" method="post">
					<div class="form-group">
						<label for="firstName">First Name:</label> <input type="text"
							class="form-control" id="firstName" name="firstName"
							placeholder="First Name" value="${USERBEAN.firstName}">
					</div>
					<div class="form-group">
						<label for="email">Last Name:</label> <input type="text"
							class="form-control" id="lastName" name="lastName"
							placeholder="Last Name" value="${USERBEAN.lastName}">
					</div>
					<div class="form-group">
						<label for="lastName">Email:</label> <input type="text"
							class="form-control" id="email" name="email" placeholder="Email"
							value="${USERBEAN.email}">
					</div>
					<div class="form-group">
						<label for="phone">Phone:</label> <input type="text"
							class="form-control" id="phone" name="phone" placeholder="Phone"
							value="${USERBEAN.phone}">
					</div>
					<div class="form-group">
						<label for="phone">Location:</label>
						<select id="locationId" name="locationId" class="form-control" placehloder="location">
							<c:forEach items="${applicationScope.LOCATIONS}" var="item">
								<c:choose>
									<c:when test="${item.guid eq USERBEAN.location.guid}">
										<option value="${item.guid}" selected="selected">${item.locationName}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.guid}">${item.locationName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>

						</select>
					</div>
					<div class="form-group">
						<label for="newPassword">new Password</label> <input
							type="password" class="form-control" id="password" name="password"
							placehnewer="New Passowrd">
					</div>
					<button type="submit" class="btn btn-default active">Save</button>
					<button type="reset" class="btn btn-default active">Cancel</button>
				</form>
			</div>
			<div class="col-md-2 col-md-push-3">
				<img class="img-circle img-responsive img-center"
					src="http://placehold.it/200x200" alt="">
			</div>
		</div>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>	
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>
