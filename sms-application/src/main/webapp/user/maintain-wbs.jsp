<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Maintain-WBS</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/jstree-themes/proton/style.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/simple-demo.css">

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jstree.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/wbs_tree.js"></script>
<script src="${pageContext.request.contextPath}/static/js/maintain-wbs.js"></script>
<script src="${pageContext.request.contextPath}/static/js/wbs-drawing.js"></script>
<script src="${pageContext.request.contextPath}/static/js/wbs-picture.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/demo.js"></script>
<script src="${pageContext.request.contextPath}/static/js/dmuploader.js"></script>
<script type="text/javascript">
	var wbsPictureURL = "${pageContext.request.contextPath}/wbs/tree/picture/";
	var wbsDrawingURL = "${pageContext.request.contextPath}/wbs/tree/drawing/";
	var pictureListURL = "${pageContext.request.contextPath}/pictures/all";
	var drawingListURL = "${pageContext.request.contextPath}/drawings/all";
	var preSelectedGuid = "${requestScope.chosenGuid}";
	var wbsNodeLookupURL = "${pageContext.request.contextPath}/wbs/tree/node/";
	var wbsTreeURL = "${pageContext.request.contextPath}/wbs/tree/view";
	var wbsRenameNodeURL = "${pageContext.request.contextPath}/wbs/tree/rename/";
	var wbsDeleteNodeURL = "${pageContext.request.contextPath}/wbs/tree/delete/";
	var wbsMoveNodeURL = "${pageContext.request.contextPath}/wbs/tree/move";
	var wbsPasteNodeURL = "${pageContext.request.contextPath}/wbs/tree/paste";
	var pictureUploadURLForWbs = "${pageContext.request.contextPath}/wbs/upload/picture";
	var drawingUploadURLForWbs = "${pageContext.request.contextPath}/wbs/upload/drawing";
</script>
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<c:if test="${not empty requestScope.statusMsg}">
			<div class="row">
				<div class="col-lg-4">
					<p class="bg-success">
						<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
						<c:out value="${requestScope.statusMsg}" />
					</p>
				</div>
			</div>
		</c:if>
		<c:if test="${not empty requestScope.errorMsg}">
			<div class="row">
				<div class="col-lg-4">
					<p class="bg-warning">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						<c:out value="${requestScope.errorMsg}" />
					</p>
				</div>
			</div>
		</c:if>
		<div class="row">
			<div class="col-md-5"
				style="height: 460px; border-color: gray; overflow: scroll;">
				<div id="jstree-wbs" style="margin-top: 20px;"></div>
			</div>
			<div class="col-md-6 col-md-push-1">
				<ul id="wbsTab" class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#details-tab"
						aria-controls="details-tab" role="tab" data-toggle="tab">Details</a></li>
					<li role="presentation"><a href="#drawing-tab"
						aria-controls="drawing-tab" role="tab" data-toggle="tab">Drawings</a></li>
					<li role="presentation"><a href="#picture-tab"
						aria-controls="picture-tab" role="tab" data-toggle="tab">Pictures</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active"
						id="details-tab">
						<div class="row">
							<div class="col-md-12">
								<jsp:include page="wbs-view.jsp"></jsp:include>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="drawing-tab">
						<div class="row" style="padding-top: 10px;">
							<div class="col-md-12">
								<table id="drawingTable"
									class="table table-bordered table-hover"
									style="background-color: white;">
									<thead>
										<tr>
											<th>File Name</th>
											<th>Date Created</th>
											<th></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<button id="addDrawings" type="button"
									class="btn btn-default active">Associate Drawing</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id="drag-and-drop-zone-drawing" class="uploader" style="background-color: white;">
									<div>Drag &amp; Drop Images Here</div>
									<div class="or">-or-</div>
									<div class="browser">
										<label> <span>Click to open the file Browser</span> <input
											type="file" name="files" title='Click to add Files'>
										</label>
									</div>
								</div>							
							</div>
						</div>						
					</div>
					<div role="tabpanel" class="tab-pane fade" id="picture-tab">
						<div class="row" style="padding-top: 10px;">
							<div class="col-md-12">
								<table id="pictureTable" class="table table-bordered table-hover" style="background-color: white;">
									<thead>
										<tr>
											<th>File Name</th>
											<th>Date Created</th>
											<th></th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<button id="addPictures" type="button"
									class="btn btn-default active">Associate Picture</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id="drag-and-drop-zone-picture" class="uploader" style="background-color: white;">
									<div>Drag &amp; Drop Images Here</div>
									<div class="or">-or-</div>
									<div class="browser">
										<label> <span>Click to open the file Browser</span> <input
											type="file" name="files" title='Click to add Files'>
										</label>
									</div>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 10px;"></div>
		<div class="row" style="margin-top: 10px;">
			<div class="col-md-2">
				<button id="wbsAdd" type="button" class="btn btn-info active">Add
					Component</button>
			</div>
		</div>		
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
	<jsp:include page="/user/picture-modal.jsp"></jsp:include>
	<jsp:include page="/user/drawing-modal.jsp"></jsp:include>
	<form id="pictureAssociationForm"
		action="${pageContext.request.contextPath}/wbs/tree/pictures/associate"
		method="post">
		<input type="hidden" name="wbsGuidPicture" id="wbsGuidPicture">
		<input type="hidden" name="systemPictureGuids" id="systemPictureGuids">
	</form>
	<form id="pictureAssociationRemoveForm"
		action="${pageContext.request.contextPath}/wbs/tree/picture/association/remove"
		method="post">
		<input type="hidden" name="wbsGuidPictureRemove"
			id="wbsGuidPictureRemove"> <input type="hidden"
			name="systemPictureGuidsRemove" id="systemPictureGuidsRemove">
	</form>
	<form id="drawingAssociationForm"
		action="${pageContext.request.contextPath}/wbs/tree/drawings/associate"
		method="post">
		<input type="hidden" name="wbsGuidDrawing" id="wbsGuidDrawing">
		<input type="hidden" name="systemDrawingGuids" id="systemDrawingGuids">
	</form>
	<form id="drawingAssociationRemoveForm"
		action="${pageContext.request.contextPath}/wbs/tree/drawing/association/remove"
		method="post">
		<input type="hidden" name="wbsGuidDrawingRemove"
			id="wbsGuidDrawingRemove"> <input type="hidden"
			name="systemDrawingGuidsRemove" id="systemDrawingGuidsRemove">
	</form>
</body>
<script
	src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>
