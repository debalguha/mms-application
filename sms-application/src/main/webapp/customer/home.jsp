<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer_Home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/static/js/customer-home.js"></script>

</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="row">
					<div class="col-md-6">
						<div id="customer-list" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
							<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Customers</p>
						</div>					
					</div>
					<div class="col-md-6">
						<div id="vehicles" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
							<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">End Items & Serialized Components</p>
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
<script>
	var customerListURL = '<c:out value="${pageContext.request.contextPath}"/>/customers/all';
</script>
</html>
