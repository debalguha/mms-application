<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer_Details</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/customer-details.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form action="${pageContext.request.contextPath}/customer/save" method="post">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="country">customer #:</label> 
						<input type="text" class="form-control" id="customerNumber" name="customerNumber" value="${requestScope.customer.customerNumber}">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="country">Country:</label> 
						<%-- <input type="text" class="form-control" id="country" name="country" value="${requestScope.customer.country}"> --%>
						<select class="form-control" id="country" name="country" style="width: 100%" value="${requestScope.customer.country}">
							<c:forEach items="${requestScope.COUNTRY}" var="item">
								<c:choose>
									<c:when test="${item eq requestScope.customer.country}">
										<option value="${item}" selected="selected"><c:out value="${item}"></c:out></option>
									</c:when>
									<c:otherwise>
										<option value="${item}"><c:out value="${item}"></c:out></option>								
									</c:otherwise>
								</c:choose>								
							</c:forEach>
						</select>						
					</div>				
				</div>
			</div>
			<div class="row">			
				<div class="col-md-4">
					<div class="form-group">
						<label for="country">Agency/Company:</label> 
						<%-- <input type="text" class="form-control" id="agencyCompany" name="agencyCompany" value="${requestScope.customer.agencyCompany}"> --%>
						<select class="form-control" id="agencyCompany" name="agencyCompany" style="width: 100%" value="${requestScope.customer.agencyCompany}">
							<c:forEach items="${requestScope.AGENCY_COMPANY}" var="item">
								<c:choose>
									<c:when test="${item eq requestScope.customer.agencyCompany}">
										<option value="${item}" selected="selected"><c:out value="${item}"></c:out></option>
									</c:when>
									<c:otherwise>
										<option value="${item}"><c:out value="${item}"></c:out></option>								
									</c:otherwise>
								</c:choose>								
							</c:forEach>
						</select>						
					</div>			
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="country">Organization:</label> 
						<%-- <input type="text" class="form-control" id="organization" name="organization" value="${requestScope.customer.organization}"> --%>
						<select class="form-control" id="organization" name="organization" style="width: 100%" value="${requestScope.customer.organization}">
							<c:forEach items="${requestScope.ORGANIZATIONS}" var="item">
								<c:choose>
									<c:when test="${item eq requestScope.customer.organization}">
										<option value="${item}" selected="selected"><c:out value="${item}"></c:out></option>
									</c:when>
									<c:otherwise>
										<option value="${item}"><c:out value="${item}"></c:out></option>								
									</c:otherwise>	
								</c:choose>							
							</c:forEach>
						</select>						
					</div>
				</div>
			</div>
			<div class="row">				
				<div class="col-md-4">
					<div class="form-group">
						<label for="country">Customer/End User:</label> 
						<input type="text" class="form-control" id="customerEndUser" name="customerEndUser" value="${requestScope.customer.customerEndUser}">
					</div>				
				</div>
			</div>
			<div class="row">				
				<div class="col-md-4">
					<div class="form-group">
						<label for="country">Contract #:</label> 
						<%-- <input type="text" class="form-control" id="contractNumber" name="contractNumber" value="${requestScope.customer.contractNumber}"> --%>
						<select class="form-control" id="contractNumber" name="contractNumber" style="width: 100%">
							<c:forEach items="${requestScope.CONTRACT_NOMENCLATURE}" var="item">
								<c:choose>
									<c:when test="${item eq requestScope.customer.contractNumber}">
										<option value="${item}" selected="selected"><c:out value="${item}"></c:out></option>
									</c:when>
									<c:otherwise>
										<option value="${item}"><c:out value="${item}"></c:out></option>								
									</c:otherwise>
								</c:choose>								
							</c:forEach>
						</select>						
					</div>			
				</div>			
			</div>
			<input type="hidden" name="guid" value="${requestScope.customer.guid}">
			<button id="customerSave" type="submit" class="btn btn-default active">Save</button>
			<button type="button" class="btn btn-default active" onclick="goBack()">Cancel
				<script>
					function goBack() {
						window.history.back();
					}
				</script>			
			</button>
			<!-- <button onclick="goBack()">Cancel</button>
			<script>
				function goBack() {
					window.history.back();
				}
			</script> -->
		</form>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>
