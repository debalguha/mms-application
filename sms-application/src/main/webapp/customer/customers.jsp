<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css">

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/static/js/customer-home.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>

</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
<!-- 		<h4>Double click on a customer record to view details.</h4> -->
		<c:if test="${not empty requestScope.statusMsg}">
			<div class="row">
				<div class="col-lg-4">
					<p class="bg-success">
						<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
						<c:out value="${requestScope.statusMsg}"/>
					</p>
				</div>
			</div>
		</c:if>	
		<c:if test="${not empty requestScope.errorMsg}">
			<div class="row">
				<div class="col-lg-4">
					<p class="bg-warning">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						<c:out value="${requestScope.errorMsg}"/>
					</p>
				</div>
			</div>
		</c:if>		
		<div class="row">
			<div class="col-lg-12">
				<table id="customer-table" class="table table-striped table-bordered table-hover"  cellspacing="0" width="100%" style="cursor:pointer;">
				
			        <thead>
			            <tr>
			                <th>Customer #</th>
			                <th>Country</th>
			                <th>Agency/Company</th>
			                <th>Organization</th>
			                <th>Customer/End User</th>
			                <th>Contract #</th>
			                <th></th>
			            </tr>
			        </thead>					
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<form id="custForm" action="${pageContext.request.contextPath}/customer/edit" method="post">
					<input type="hidden" name="customerGuid" id="customerGuid">
					<button id="customerSave" type="submit" class="btn btn-default active">New Customer</button>
					<button type="button" class="btn btn-warning active" id="deleteCustomer">Delete Customers</button>
					<button type="button" class="btn active" onclick="goBack()">Cancel</button>
					<script>
						function goBack() {
							window.history.back();
						}
					</script>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
	<form id="customerDeleteForm" action="${pageContext.request.contextPath}/customers/delete">
		<input type="hidden" name="selectedGuids" id="selectedGuids">
	</form>	
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
<script>
	var customerListURL = '<c:out value="${pageContext.request.contextPath}"/>/customers/all';
</script>
</html>