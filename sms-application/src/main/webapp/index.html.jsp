<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User-Home</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap-submenu.min.css" />
<%-- <script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script> --%>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap-submenu.min.js"></script>
</head>

<body>
<nav class="navbar navbar-default">
  <div class="navbar-header">
    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>

    <a class="navbar-brand">Project Name</a>
  </div>

  <div class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a tabindex="0" data-toggle="dropdown" data-submenu>
          Dropdown<span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
          <li class="dropdown-submenu">
            <a tabindex="0">Action</a>

            <ul class="dropdown-menu">
              <li><a tabindex="0">Sub action</a></li>
              <li class="dropdown-submenu">
                <a tabindex="0">Another sub action</a>

                <ul class="dropdown-menu">
                  <li><a tabindex="0">Sub action</a></li>
                  <li><a tabindex="0">Another sub action</a></li>
                  <li><a tabindex="0">Something else here</a></li>
                </ul>
              </li>
              <li><a tabindex="0">Something else here</a></li>
              <li class="dropdown-submenu">
                <a tabindex="0">Another action</a>

                <ul class="dropdown-menu">
                  <li><a tabindex="0">Sub action</a></li>
                  <li><a tabindex="0">Another sub action</a></li>
                  <li><a tabindex="0">Something else here</a></li>
                </ul>
              </li>
            </ul>
          </li>

          <li class="dropdown-submenu">
            <a tabindex="0">Another action</a>

            <ul class="dropdown-menu">
              <li><a tabindex="0">Sub action</a></li>
              <li><a tabindex="0">Another sub action</a></li>
              <li><a tabindex="0">Something else here</a></li>
            </ul>
          </li>
          <li><a tabindex="0">Something else here</a></li>
          <li class="divider"></li>
          <li><a tabindex="0">Separated link</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a tabindex="0" data-toggle="dropdown" data-submenu>
          Dropdown 2<span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
          <li class="dropdown-submenu">
            <a tabindex="0">Action</a>

            <ul class="dropdown-menu">
              <li><a tabindex="0">Sub action</a></li>
              <li class="dropdown-submenu">
                <a tabindex="0">Another sub action</a>

                <ul class="dropdown-menu">
                  <li><a tabindex="0">Sub action</a></li>
                  <li><a tabindex="0">Another sub action</a></li>
                  <li><a tabindex="0">Something else here</a></li>
                </ul>
              </li>
              <li><a tabindex="0">Something else here</a></li>
            </ul>
          </li>
          <li><a tabindex="0">Another action</a></li>
          <li class="dropdown-submenu">
            <a tabindex="0">Something else here</a>

            <ul class="dropdown-menu">
              <li><a tabindex="0">Sub action</a></li>
              <li><a tabindex="0">Another sub action</a></li>
              <li><a tabindex="0">Something else here</a></li>
            </ul>
          </li>
          <li class="divider"></li>
          <li><a tabindex="0">Separated link</a></li>
        </ul>
      </li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a tabindex="0" data-toggle="dropdown">
          Dropdown 3<span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
          <li><a tabindex="0">Action</a></li>
          <li><a tabindex="0">Another action</a></li>
          <li><a tabindex="0">Something else here</a></li>
          <li class="divider"></li>
          <li><a tabindex="0">Separated link</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
<script>
$(document).ready(function(){
	  $('.dropdown > a[tabindex]').on('keydown', function(event) {
	    if (event.keyCode == 13) {
	      $(this).dropdown('toggle');
	    }
	  });
	  $('.dropdown-menu > .disabled, .dropdown-header').on('click.bs.dropdown.data-api', function(event) {
	    event.stopPropagation();
	  });

	  $('[data-submenu]').submenupicker();	  
});
</script>
</body>
</html>
