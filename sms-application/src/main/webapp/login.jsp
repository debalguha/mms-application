<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="static/styles/style.css" />
<title>TRS</title>
</head>

<body>
	<div class="wrapper">
		<header class="home">
		<div class="logo">
			<img src="static/images/logo.png" />
		</div>
		</header>
		<div id="content">
			<div class="login-wrapper">
				<div class="login-box">
					<h5>User Login</h5>
					<h6>Please enter login information.</h6>
					<c:if test="${not empty requestScope.errMsg}">
						<div class="alert alert-danger"><c:out value="${requestScope.errMsg}"/></div>
					</c:if>
					<form action="login" method="post">
						<p class="field">
							<input type="text" name="email" placeholder="User Name" />
						</p>
						<p class="field">
							<input type="password" name="password" placeholder="Password" />
						</p>
						<p class="register-text">New User Registration</p>
						<p class="submit">
							<input type="submit" name="login" value="Login" />
						</p>
					</form>
				</div>
			</div>
		</div>
		<footer>
		<div class="flogo">
			<img src="static/images/footer-logo.png" />
		</div>
		<div class="copyright">© 2015 TRS INTERNATIONAL GROUP – Defense
			Consulting, Intelligence and Training For Real World Environments.
			All Rights Reserved</div>
		</footer>

	</div>

</body>
</html>
