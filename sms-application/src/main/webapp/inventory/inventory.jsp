<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>inventory</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/jstree-themes/proton/style.css">	
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jstree.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script>
	var inventoryURL = "${pageContext.request.contextPath}/inventory/all";
	var inventoryActivateURL = "${pageContext.request.contextPath}/inventory/activate";
	var inventoryDeActivateURL = "${pageContext.request.contextPath}/inventory/deActivate";
	var issuePOURL = "${pageContext.request.contextPath}/inventory/issuePO";
	var issueRFQURL = "${pageContext.request.contextPath}/rfq/issue";
	var vendorListURL = "${pageContext.request.contextPath}/vendors/inventory/";
	var partModalURL= "${pageContext.request.contextPath}/inventory/createPart";
	var treeURL = '${pageContext.request.contextPath}/system/tree/view';
	var pageContextRoot = "${pageContext.request.contextPath}/";
	var currentGuid = "";
	var oTableVendor;
	getVendorListURL = function(){
		return vendorListURL + currentGuid;
	}
</script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form id="invForm" method="post">
<!-- 			<h4>Double click on a inventory record to view details.</h4> -->
			<div class="row">
				<div class="col-lg-12">
					<div style="overflow: auto; max-height: 500px; max-width: 100%;">
						<jsp:include page="/inventory/inventory-list.jsp"></jsp:include>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1"><button id="activate" type="button" class="btn btn-default">Activate</button></div>
				<div class="col-sm-1"><button id="deActivate" type="button" class="btn btn-default">Deactivate</button></div>
				<div class="col-sm-1"><button id="issuePO" type="button" class="btn btn-default">Issue PO</button></div>
				<div class="col-sm-1"><button id="issueRFQ" type="button" class="btn btn-default">Issue RFQ</button></div>
				<div class="col-sm-1"><button id="createPart" type="button" class="btn btn-default">Create Part</button></div>
			</div>
			<input type="hidden" name="inventoryGuid" id="inventoryGuid">
			<input type="hidden" name="selectedGuids" id="selectedGuids">
		</form>
		<form id="invEditForm" method="get" action="${pageContext.request.contextPath}/inventory/edit">
			<input type="hidden" name="guid" id="guid">
		</form>
		<form id="poEditForm" method="get" action="${pageContext.request.contextPath}/po/edit/inventory">
			<input type="hidden" name="inventoryGuidForPO" id="inventoryGuidForPO">
		</form>		
		<jsp:include page="vendor-modal.jsp"></jsp:include>
		<jsp:include page="part-modal.jsp"></jsp:include>
		<jsp:include page="../system/inventory-modal-for-part.jsp"></jsp:include>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>	
<script type="text/javascript">
$(document).ready(function(){
	$('#partSubmit').unbind('click');
	$('#partSubmit').bind('click', function(){
		$('#partModalForInventory').modal('toggle');
		var selectedNode = $("#jstree-systems").jstree(true).get_selected('full',true)[0];
		var jsonObj = convertFormToJSON($('#partForm').serializeArray());
		jsonObj.inventory = inventory;
		jsonObj.VIEWID='PART';
		console.log(jsonObj);
		$.ajax({
			type : 'POST',
			url : pageContextRoot+'system/tree/addComponent/'+(selectedNode == null ? 'null':selectedNode.original.guid),
			data : JSON.stringify(jsonObj),
			contentType: "application/json; charset=utf-8",
			dataType : 'json'
		}).always(function(data){
			if(data.guid == null || data.guid == 'undefined'){
				bootbox.alert('Unable to create part.', function(){});
			}else{
				bootbox.alert('Part successfully created.', function(){});
				$("#nav-link").val("inventory/inventory");
				$("#linkForm").submit();
			}
		});
	});
	partCallbackAfterDisplay($('#partForm'));
});
function convertFormToJSON(formDataArray){
    
    var json = {};
    
    $.each(formDataArray, function() {
        json[this.name] = this.value || '';
    });
    
    return json;
}
</script>
</html>