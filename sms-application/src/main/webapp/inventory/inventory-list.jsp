<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/jq-2.1.4,dt-1.10.10,b-1.1.0,b-print-1.1.0,r-2.0.0/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/s/bs/jq-2.1.4,dt-1.10.10,b-1.1.0,b-print-1.1.0,r-2.0.0/datatables.min.js"></script> -->

	<script src="${pageContext.request.contextPath}/static/js/inventory.js"></script>

						<table id="inventory-table" class="table table-striped table-bordered" cellspacing="0" width="100%"style="cursor:pointer">
					        <thead>
					            <tr>
					                <th>ID Code</th>
					                <th>WBS</th>
					                <th>Item Description</th>
					                <th>High Unit Price</th>
					                <th>Low Unit Price</th>
					                <th>Quantity On Hand</th>
					                <th>Low Quantity Threshold</th>
					                <th>Vendor Supplying</th>
					                <th>Last Order Detail</th>
					                <th>Choose</th>
					            </tr>
					        </thead>
					        <tbody></tbody>					
						</table>