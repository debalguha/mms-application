<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>vendor-items-supplied</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.0.1/js/dataTables.buttons.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/vendor-items-supply.js"></script>
<script>
	var vendorGuid = '<c:out value="${requestScope.vendorGuid}"/>';
</script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form action="associate">
			<input type="hidden" name="vendorGuid" value="${requestScope.vendorGuid}">
			<div class="row">
				<h3>Items Not Supplying</h3>
				<div class="col-lg-12">
					<table id="itemsNotSupplying" class="table table-striped table-bordered" style="background-color: white;">
					  <thead><tr><td>ID Code</td><td>Assembly Name</td><td>Assembly Number</td><td>Quantity on hand</td><td>Standard Order Quantity</td><td>Low Threshold</td><td>Choose</td></tr></thead>
					  <tbody></tbody>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<button id="addInventory" type="submit" class="btn btn-primary">Add Inventory</button>
				</div>
			</div>
			<div class="row">
				<h3>Items Supplying</h3>
				<div class="col-lg-12">
					<table id="itemsSupplying" class="table table-striped table-bordered" style="background-color: white;">
					  <thead><tr><td>ID Code</td><td>Assembly Name</td><td>Assembly Number</td><td>Quantity on hand</td><td>Standard Order Quantity</td><td>Low Threshold</td></tr></thead>
					  <tbody></tbody>
					</table>			
				</div>
			</div>	
			<div class="row">
				<div class="col-md-9">
					<button onclick="goBack()" class="btn btn-default active">Cancel</button>
					<script>
						function goBack() {
							window.history.back();
						}
					</script>
				</div>
			</div>	
		</form>
		<form id="vendorForm" action="vendors/vendor" method="post">
			<input type="hidden" name="guid" id="guid">
		</form>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>	
</html>