<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div id="contactModalForVendor" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Contact Details</h4>
			</div>
			<div class="modal-body">	
				<form id="contactForm">
					<jsp:include page="contact-segment.jsp"></jsp:include>
				</form>
			</div>
			<div class="modal-footer">
				<button id="addContact" type="button" class="btn btn-default">Add Contact</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->			