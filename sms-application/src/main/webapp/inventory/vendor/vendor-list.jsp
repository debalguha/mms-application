<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script src="${pageContext.request.contextPath}/static/js/vendors.js"></script>

<table id="vendor-list-table" class="table table-bordered table-hover" cellspacing="0" width="100%" style="cursor:pointer">
       <thead>
           <tr bgcolor="white">
               <th>Name #</th>
               <th>Type</th>
               <th>Location</th>
               <th>Field 4</th>
               <th>Field 5</th>
               <th>Field 6</th>
           </tr>
       </thead>					
</table>
