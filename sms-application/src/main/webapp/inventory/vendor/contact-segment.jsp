<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="email">First Name:</label> <input
							type="text" class="form-control" id="firstName" name="firstName"
							placeholder="First Name" value="${requestScope.contact.firstName}">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="email">Last Name:</label> <input
							type="text" class="form-control" id="lastName" name="lastName"
							placeholder="Last Name" value="${requestScope.contact.lastName}">
					</div>				
				</div>
				<div class="col-md-4">
					<div class="form-group has-feedback">
						<label for="email">Email:</label> <input
							type="text" class="form-control" id="email" name="email"
							placeholder="Email" value="${requestScope.contact.email}">
					</div>				
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="email">Phone 1:</label> <input
							type="text" class="form-control" id="phone1" name="phone1"
							placeholder="Phone 1" value="${requestScope.contact.phone1}">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="email">Phone 2:</label> <input
							type="text" class="form-control" id="phone2" name="phone2"
							placeholder="Phone 2" value="${requestScope.contact.phone2}">
					</div>			
				</div>
			</div>	
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="email">Address 1:</label> <textarea
							class="form-control" id="address1" name="address1"
							placeholder="Address 1" value="${requestScope.contact.address1}"></textarea>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="email">Address 2:</label> <textarea
							type="text" class="form-control" id="address2" name="address2"
							placeholder="Address 2" value="${requestScope.contact.address2}"></textarea>
					</div>			
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="email">City:</label> <input
							type="text" class="form-control" id="city" name="city"
							placeholder="City" value="${requestScope.contact.city}">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="email">State/Province :</label> <input
							type="text" class="form-control" id="stateProvince" name="stateProvince"
							placeholder="State/Province" value="${requestScope.contact.stateProvince}">
					</div>				
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="email">Postal Code:</label> <input
							type="text" class="form-control" id="postalCode" name="postalCode"
							placeholder="Postal Code" value="${requestScope.contact.postalCode}">
					</div>				
				</div>
			</div>