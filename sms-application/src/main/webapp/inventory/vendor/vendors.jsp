<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Vendors</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css">
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
	var vendorListURL = '${pageContext.request.contextPath}/vendors/all';
	var oTableVendor;
</script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
<!-- 		<h4>Double click on a Vendor record to view details.</h4> -->
		<div class="row">
			<div class="col-lg-12">
				<jsp:include page="vendor-list.jsp"></jsp:include>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<form id="addvendorForm" action="${pageContext.request.contextPath}/vendor/add" method="GET">
							<button id="addVendor" type="submit" class="btn btn-default active">Add
								Vendor</button>
							<button type="button" class="btn btn-default active" onclick="goBack()">Cancel</button>
							<script>
								function goBack() {
									window.history.back();
								}
							</script>
					</form>
				</div>
			</div>
		</div>
		<form id="vendorForm" action="${pageContext.request.contextPath}/vendors/vendor" method="post">
			<input type="hidden" name="guid" id="guid">
		</form>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
<script type="text/javascript">
$('#vendor-list-table').on( 'init.dt', function () {
    console.log( 'Table initialisation complete: '+new Date().getTime() );
});
$(document).ready(function(){
	while(oTableVendor == null);
	oTableVendor.ajax.url(vendorListURL);
	oTableVendor.ajax.reload();
});
</script>
</html>