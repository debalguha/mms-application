<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>vendor-details</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css">
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vendor-details.js"></script>
<script type="text/javascript">
	var contactsURL = "${pageContext.request.contextPath}/vendor/contact?guid=${requestScope.vendor.guid}";
	var vendorSubmitURL = "${pageContext.request.contextPath}/vendor/save";
	var contactEmailValidateURL = "${pageContext.request.contextPath}/contact/validate";
</script>
<style type="text/css">
.contact-element{
	overflow: hidden;
	margin-top: 15px;
	padding-bottom: 15px;
}
.media-body {
    display: block;
    width: auto;
}
</style>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form id="vendorForm" action="${pageContext.request.contextPath}/vendor/save" method="POST">
			<div class="row">
				<div class="col-lg-4">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="vendorName">Vendor Name</label>
								<input type="text" class="form-control" name="vendorName" id="vendorName" value="${requestScope.vendor.name}">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="email">Vendor Type:</label>
								<select id="vendorTypeId" name="vendorType" class="form-control" placehloder="Vendor Type">
									<c:forEach items="${applicationScope.VENDOR_TYPES}" var="item">
										<c:choose>
											<c:when test="${item.guid eq requestScope.vendor.type.guid}">
												<option value="${item.guid}" selected="selected">${item.name}</option>
											</c:when>
											<c:otherwise>
												<option value="${item.guid}">${item.name}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>	
							</div>			
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="email">Vendor Location:</label>
								<select id="locationId" name="location" class="form-control" placehloder="Vendor Location">
									<c:forEach items="${applicationScope.LOCATIONS}" var="item">
										<c:choose>
											<c:when test="${item.guid eq requestScope.vendor.location.guid}">
												<option value="${item.guid}" selected="selected">${item.locationName}</option>
											</c:when>
											<c:otherwise>
												<option value="${item.guid}">${item.locationName}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>	
							</div>						
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button id="vendorSubmit" type="button" class="btn btn-default active">Save</button>
							<!-- 					<button type="reset" class="btn btn-default active">Cancel</button> -->
							<button class="btn btn-default active" onclick="goBack()">Cancel</button>
							<button id="itmsSuppl" type="button" class="btn btn-warning">Items Supplied</button>
							<script>
								function goBack() {
									window.history.back();
								}
							</script>
						</div>
					</div>					
				</div>
				<div class="col-lg-7">
					<h4>Contacts:</h4>
					<table id="contact-table" class="table table-striped table-hovered" style="background-color: white;">
						<thead>
							<tr><td>First Name</td><td>Last Name</td><td>Mail</td><td>Phone1</td></tr>
						</thead>
						<%-- <c:forEach items="${requestScope.contacts}" var="item">
							<tr>
								<td><c:out value="${item.contact.firstName}"/></td><td><c:out value="${item.contact.lastName}"/></td>
								<td><c:out value="${item.contact.email}"/></td><td><c:out value="${item.contact.phone1}"/></td>
							</tr>
						</c:forEach> --%>
					</table>
				</div>
			</div>
			<input type="hidden" id="guid" name="guid" value="${requestScope.vendor.guid}">
		</form>	
		<form id="itmsSupplyForm" action="../inventory/items" action="POST">
			<input type="hidden" id="vendorGuidForSubmit" name="vendorGuid" value="${requestScope.vendor.guid}">
		</form>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
	<jsp:include page="vendor-contact-modal.jsp"/>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>