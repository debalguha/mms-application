<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>vendor-details</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/vendor-details.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<c:if test="${not empty requestScope.statusMsg}">
			<div class="row">
				<div class="col-lg-9 col-md-offset-1">
					<div class="alert alert-warning alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<c:out value="${requestScope.statusMsg}"/>
					</div>
				</div>
			</div>
		</c:if>	
		<form id="vendorForm" action="${pageContext.request.contextPath}/contact/save" method="POST">
			<jsp:include page="/inventory/vendor/contact-segment.jsp"></jsp:include>
			<div class="row">
				<div class="col-md-4">
					<button type="submit" class="btn btn-default">Save</button>
					<!-- 					<button type="reset" class="btn btn-default active">Cancel</button> -->
					<button class="btn btn-default" onclick="goBack()">Cancel</button>
					<script>
						function goBack() {
							window.history.back();
						}
					</script>
				</div>
			</div>
			<input type="hidden" id="guid" name="guid" value="${requestScope.contact.guid}">		
		</form>
	</div>	
</body>
</html>