<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>vendor-details</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/vendor-details.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form action="${pageContext.request.contextPath}/inventory/save" method="post">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="phone">Part:</label> <input
							type="text" class="form-control" id="part" name="part" readonly="readonly" value="${requestScope.inventory.systemPart.assemblyName}">
					</div>					
				</div>			
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="phone">Quantity On Hand:</label> <input
							type="text" class="form-control" id="quantityOnHand" name="quantityOnHand" value="${requestScope.inventory.quantityOnHand}">
					</div>					
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="phone">Low Quantity Threshold:</label> <input
							type="text" class="form-control" id="lowQuantityThreshold" name="lowQuantityThreshold" value="${requestScope.inventory.lowQuantityThreshold}">
					</div>					
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="phone">Standard Order Quantity:</label> <input
							type="text" class="form-control" id="standardOrderQuantity" name="standardOrderQuantity" value="${requestScope.inventory.standardOrderQuantity}">
					</div>					
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="phone">Id Code:</label> <input
							type="text" class="form-control" id="idCode" name="idCode" value="${requestScope.inventory.idCode}">
					</div>					
				</div>
			</div>	
			<div class="row">
				<div class="col-md-4">
					<button type="submit" class="btn btn-default active">Save</button>
					<button id="cancel" type="button" class="btn btn-default active">Cancel</button>
				</div>		
			</div>	
			<input type="hidden" name="guid" id="guid" value="${requestScope.inventory.guid}">
		</form>
	</div>
<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>