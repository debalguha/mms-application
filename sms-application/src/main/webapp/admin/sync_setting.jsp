<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SyncSetting</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/sync_setting.js"></script>


</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<table id="syncSetting-table" class="table table-striped table-bordered table-hover"  cellspacing="0" width="100%">
			        <thead>
			            <tr>
			                <th>url</th>
			                <th>frequency_hours</th>
			                <th>start_hour</th>
			                <th>active</th>
			            </tr>
			        </thead>					
				</table>
			</div>
		</div>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>

<script>
	var syncsettingListURL = '<c:out value="${pageContext.request.contextPath}"/>/sync/all';
</script>
</html>