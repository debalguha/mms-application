<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Example Report</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/simple-sidebar.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script type="text/javascript">
	var userSelctURL = "${pageContext.request.contextPath}/users/user/";
	$(document).ready(function(){
		var deliveryByDatePicker = $('#startDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: moment()});
		var issueDatePicker = $('#endDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: moment()});		
	});
</script>
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<jsp:include page="/admin/report/report-left-nav.jsp"></jsp:include>
			</div>
			<div class="col-md-9">
				<p>Choose from left navigation.</p>
				<form method="get" action='${pageContext.request.contextPath}/report/example.do'>
					<div id="example-report">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label for="firstName">Start Date:</label> 
					                <div class="input-group date" id="startDatePicker">
					                	<input id="startDate" type="text" class="form-control" name="startDate"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>									
								</div>							
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="firstName">End Date:</label> 
					                <div class="input-group date" id="endDatePicker">
					                	<input id="endDate" type="text" class="form-control" name="endDate"/>
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>									
								</div>							
							</div>							
						</div>
						<div class="row">
							<div class="col-lg-3">
								<button id="savePO" type="submit" class="btn btn-warning">Submit</button>
								<button id="cancel" type="button" class="btn btn-warning">Cancel</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
</html>