<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report Result</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/simple-sidebar.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css">


<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>

<!-- <script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script> -->
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<table id="report-table" class="display nowrap" cellspacing="0" width="100%" style="background: white;">
					<thead>
						<tr><td>name</td><td>id</td><td>xtype</td><td>uid</td><td>info</td><td>status</td><td>base_schema_var</td><td>replinfo</td><td>parent_obj</td><td>crdate</td><td>ftcatid</td><td>schema_ver</td><td>stats_schema_ver</td><td>type</td><td>userstat</td>
						<td>sysstat</td><td>indexdel</td><td>refdate</td><td>version</td><td>deltrig</td><td>instrig</td><td>updtrig</td><td>seltrig</td><td>category</td><td>cache</td></tr>
					</thead>
					<tbody>
						<c:forEach items="${requestScope.dataSet}" var="item">
							<tr>
								<c:forEach var="i" begin="0" end="24">
									<td><c:out value="${item[i]}"/></td>
								</c:forEach>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row" style="margin-top: 10px;">
			<div class="col-lg-3">
				<button id="cancel" type="button" class="btn btn-warning">Cancel</button>
			</div>
		</div>		
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(document).ready(function (){
		$('#report-table').DataTable({
			"dom": 'Blfrtip',
			"buttons": [ 'print'],
			"scrollX": true
		});
	});
</script>
</html>