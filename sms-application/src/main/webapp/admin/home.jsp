<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin-Home</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap-submenu.min.css" />
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap-submenu.min.js"></script>
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<!-- <div id="wrap"> -->
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="row">
						<div class="col-md-4">
							<div id="auditlog" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
								<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Audit Log</p>
							</div>					
						</div>
						<div class="col-md-4">
							<div id="reporting" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color:#CDB79E;">
								<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Reporting</p>
							</div>
						</div>
						<div class="col-md-4">
							<div id="dataSettings" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color:#CDB79E;">
								<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Data Settings</p>
							</div>
						</div>						
					</div>
					<div class="row" style="margin-top: 30px;">
						<div class="col-md-4">
							<div id="syncSettings" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
								<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Sync Settings</p>
							</div>					
						</div>
						<div class="col-md-4">
							<div id="contentMgmt" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
								<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">Content Management</p>
							</div>						
						</div>
						<div class="col-md-4">
							<div id="userProfiles" style="width: 150px; height: 150px; display: table; cursor: pointer; background-color: #CDB79E;">
								<p style="text-align:center; vertical-align: middle; display: table-cell; margin: auto; font-size: 18px;">User Profiles</p>
							</div>						
						</div>						
					</div>
				</div>
			</div>
		</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
</html>