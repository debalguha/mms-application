<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DataSetting</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.6/full/ckeditor.js"></script>
<script type="text/javascript">
	var userSelctURL = "${pageContext.request.contextPath}/users/user/";
	var settingsGuidToSelect="${requestScope.settingsGuid}";
</script>

</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<form action="${pageContext.request.contextPath}/admin/content/save" method="post">
		<div class="container">
			<c:if test="${not empty requestScope.statusMsg}">
				<div class="row">
					<div class="col-lg-9 col-md-offset-1">
						<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<c:out value="${requestScope.statusMsg}"/>
						</div>
					</div>
				</div>
			</c:if>
			<c:if test="${not empty requestScope.errMsg}">
				<div class="row">
					<div class="col-lg-9 col-md-offset-1">
						<div class="alert alert-warning alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<c:out value="${requestScope.errMsg}"/>
						</div>
					</div>
				</div>
			</c:if>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label for="users">Content Name:</label> 
						<select id="data-setting" class="form-control" name="content-settings" size="25" onchange="getContent(this);">
							<c:forEach items="${requestScope.CONTENT_SETTINGS}" var="item">
								<option value="${item.key}"><c:out value="${item.value}"></c:out></option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="col-md-10">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="contentName">Content Name:</label>
								<input class="form-control" name="contentName" id="contentName">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="contentName">Content Type:</label>
								<select id="contentType" class="form-control" name="contentType" name="contentType">
									<c:forEach items="${applicationScope.CONTENT_TYPE}" var="item">
										<option value="${item.guid}"><c:out value="${item.contentTypeName}"></c:out></option>
									</c:forEach>
								</select>								
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="content">WISYWIG Content</label>
					            <textarea name="content" id="content" rows="10" cols="80"></textarea>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-md-1">
							<button id="reset" type="reset" class="btn btn-default active" onclick="newContent();">New Content</button>						
						</div>
					</div>
				</div>
			</div>
		</div>
		<input name="guid" type="hidden" id="guid">
	</form>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script type="text/javascript">
var ckEditor = CKEDITOR.replace('content');
getContent = function(element){
	$.getJSON('${pageContext.request.contextPath}/admin/content/'+$(element).val()).done(function(data){
		$('#contentName').val(data.contentName);
		$('#contentType').find('option[value="'+data.contentType.guid+'"]').attr('selected', 'selected');
		$('#guid').val(data.guid);
		ckEditor.setData(data.content);
	});
}
newContent = function(){
	$('#guid').val('');
	ckEditor.setData('');
}
</script>
</html>
