<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User-Home</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap-submenu.min.css" />
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap-submenu.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>


</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>

	<div class="row">
		<div class="col-xs-6 col-md-3">
			<ul>
				<li><a style="color: #575757" id="auditlog" href="#">Audit
						Log</a></li>
			</ul>
		</div>

		<div class="col-xs-6 col-md-3">
			<ul>
				<li><a style="color: #575757" id="reporting" href="#">Reporting</a></li>
			</ul>
		</div>


		<div class="col-xs-6 col-md-3">
			<ul>
				<li><a style="color: #575757" id="dataSettings" href="#">Data
						Setting</a></li>

			</ul>
		</div>
		<div class="col-xs-6 col-md-3">
			<ul>
				<li><a style="color: #575757" id="syncSettings" href="#">Sync
						Setting</a></li>

			</ul>

		</div>
		<div class="col-xs-6 col-md-3">
			<ul>
				<li><a style="color: #575757" id="contentManagement" href="#">Content
						Management</a></li>

			</ul>

		</div>
		<div class="col-xs-6 col-md-3">
			<ul>
				<li><a style="color: #575757" id="userProfiles" href="#">User
						Profile</a></li>

			</ul>

		</div>

	</div>



	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
</html>