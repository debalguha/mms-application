<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DataSetting</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/admin-data-settings.js"></script>
<script type="text/javascript">
	var userSelctURL = "${pageContext.request.contextPath}/users/user/";
	var settingsGuidToSelect="${requestScope.settingsGuid}";
</script>

</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<c:if test="${not empty requestScope.statusMsg}">
			<div>
				<p class="bg-success">
					<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					<c:out value="${requestScope.statusMsg}"/>
				</p>
			</div>
		</c:if>
		<form action="${pageContext.request.contextPath}/admin/data" method="post">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					<label for="users">Record Type:</label> 
						<select id="data-setting" class="form-control" name="data-setting" size="25">
							<c:forEach items="${applicationScope.DATA_SETTINGS}" var="item">
								<option value="${item.guid}"><c:out value="${item.dataTable}"></c:out></option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<p>Additional Fields for: <label id="dataLabel"></label></p>
					<div class="row">
						<div class="col-md-12">
							<table id="fieldTable" class="table table-striped table-hover"  cellspacing="0" width="100%">
								<thead>
									<tr>
										<td>Field Name</td><td>Data Type</td><td>Required</td><td>Active</td><td>Added By</td><td></td><td></td>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
					<div class="row">
<!-- 						<div class="col-md-2"> -->
							<button type="button" class="btn btn-default active" id="addFieldsBtn">Add Field</button>
							<button id="cancel" type="button" class="btn btn-default active">Cancel</button>
							
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
<div id="settingMoal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Add Fields</h4>
			</div>
			<div class="modal-body">
				<form id="addFieldSettingForm" method="post">
					<div class="form-group">
						<label for="firstName">Field Name:</label> 
						<input type="text" class="form-control" id="fieldName" name="fieldName" placeholder="Field Name">
					</div>
					<div class="form-group">
						<label for="firstName">Field Type:</label> 
						<select id="fieldType" name="fieldType" class="form-control">
							<c:forEach items="${applicationScope.DATA_SETTING_FIELD_TYPES}" var="item">
								<option value="${item.guid}"><c:out value="${item.fieldType}"/></option>
							</c:forEach>
						</select>
					</div>
					<div class="checkbox">
					  <label>
					    <input type="checkbox" id="required" name="required" value="1"> Required
					  </label>
					  <label>
					    <input type="checkbox" id="active" name="active" value="1"> Active
					  </label>					  
					</div>
					<input type="hidden" id="guid" name="guid">
				</form>			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="addSettingsBtn" type="button" class="btn btn-warning">Add Field</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>	
</body>
<script type="text/javascript">
	var fieldSettingURL = "${pageContext.request.contextPath}/admin/data/fields/";
	fieldSettingSubmitURL = "${pageContext.request.contextPath}/admin/data/create/";
	$(document).ready(function(){
		if(settingsGuidToSelect!=null && settingsGuidToSelect.length>0){
			fieldTypeOnChange(settingsGuidToSelect);
			settingsGuidToSelect='';
		}
		
	});
</script>
</html>
