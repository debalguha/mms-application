<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Audit_Log</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/simple-sidebar.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	var userSelctURL = "${pageContext.request.contextPath}/users/user/";
	$(document).ready(function(){
		var deliveryByDatePicker = $('#startDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: moment()});
		var issueDatePicker = $('#endDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: moment()});		
	});
</script>
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form method="get" action='<c:out value="${pageContext.request.contextPath}"/>/audit.do'>
			<div class="row">
				<div class="col-md-10 col-md-push-1">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label for="firstName">Start Date:</label> 
				                <div class="input-group date" id="startDatePicker">
				                	<input id="startDate" type="text" class="form-control" name="startDate"/>
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>									
							</div>							
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="firstName">End Date:</label> 
				                <div class="input-group date" id="endDatePicker">
				                	<input id="endDate" type="text" class="form-control" name="endDate"/>
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>									
							</div>							
						</div>							
					</div>
				</div>
			</div>
			<div class="row" style="margin-bottom: 10px;">
				<div class="col-md-2 col-md-push-1">
					<button id="savePO" type="submit" class="btn btn-warning">Submit</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-push-1" style="background: white;">
					<table id="audit-table" class="display nowrap" cellspacing="0" width="100%" >
						<thead>
							<tr><td>Entry Type</td><td>User</td><td>Action</td><td>Notes</td><td>File Associated</td><td>Client IP</td><td>Host Ip</td><td>Host Name</td></tr>
						</thead>
						<tbody>
							<c:forEach items="${requestScope.auditLogs}" var="item">
								<tr>
									<td><c:out value="${item.creationDate}"/></td>
									<td><c:out value="${item.user.email}"/></td>
									<td><c:out value="${item.action}"/></td>
									<td><c:out value="${item.notes}"/></td>
									<td><c:out value="${item.associatedFile}"/></td>
									<td><c:out value="${item.clientIpAddress}"/></td>
									<td><c:out value="${item.hostIpAddress}"/></td>
									<td><c:out value="${item.hostNameAddress}"/></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>			
				</div>
			</div>			
		</form>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script type="text/javascript">
	$(document).ready(function (){
		$('#audit-table').DataTable({
			"scrollX": true
		});
	});
</script>
</html>