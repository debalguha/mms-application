<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User_Profile</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/admin-user-profile.js"></script>
<script type="text/javascript">
	var userSelctURL = "${pageContext.request.contextPath}/users/user/";
</script>
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<c:if test="${not empty requestScope.statusMsg}">
			<div>
				<p class="bg-success">
					<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					<c:out value="${requestScope.statusMsg}"/>
				</p>
			</div>
		</c:if>	
		<form action="${pageContext.request.contextPath}/admin/userprofile" method="post">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
					<label for="users">User:</label> 
						<select id="guid" class="form-control" name="guid" size="25">
							<c:forEach items="${sessionScope.USERS}" var="item">
								<option value="${item.guid}"><c:out value="${item.email}"></c:out></option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="col-md-6">
						<div class="form-group">
							<label for="firstName">First Name:</label> 
							<input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name">
						</div>
						<div class="form-group">
							<label for="email">Last Name:</label> 
							<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name">
						</div>
						<div class="form-group">
							<label for="lastName">Email:</label> 
							<input type="text" class="form-control" id="email" name="email" placeholder="Email">
						</div>
						<div class="form-group">
							<label for="phone">Phone:</label> 
							<input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
						</div>
						<div class="form-group">
							<select id="locationGuid" name="locationGuid" class="form-control" placehloder="location">
								<c:forEach items="${applicationScope.LOCATIONS}" var="item">
									<option value="${item.guid}">${item.locationName}</option>
								</c:forEach>
	
							</select>
						</div>
						<div class="form-group">
							<label for="newPassword">new Password</label> 
							<input type="password" class="form-control" id="password" name="password">
						</div>
						<button type="submit" class="btn btn-default active">Save</button>
						<button id="delete" type="button" class="btn btn-default active">Delete</button>
				</div>
				<div class="col-md-2 col-md-push-3">
					<img class="img-circle img-responsive img-center"
						src="http://placehold.it/200x200" alt="">
				</div>
			</div>
		</form>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>
