<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>rfq-response-list</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/styles/style.css" /> --%>
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>

<script
	src="${pageContext.request.contextPath}/static/js/rfq-home.js"></script>
<script>
	var rfqListURL = "${pageContext.request.contextPath}/rfq/list";
	var rfqEditResponseURL = "${pageContext.request.contextPath}/rfq/response/edit";
	var inventoryDeActivateURL = "${pageContext.request.contextPath}/inventory/deActivate";
	var issuePOURL = "${pageContext.request.contextPath}/inventory/issuePO";
	var issueRFQURL = "${pageContext.request.contextPath}/rfq/issue";
</script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form id="invForm" method="post">
			<div class="row">
				<div class="col-lg-12">
					<div style="overflow: auto; max-height: 500px; max-width: 100%;">
						<table id="rfq-table" class="table table-hover" cellspacing="0" width="100%">
					        <thead>
					            <tr>
					                <th>RFQ Number</th>
					                <th>Type</th>
					                <th>Date Issued</th>
					                <th>Status Date</th>
					                <th>Responses</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					        <tbody></tbody>					
						</table>
					</div>
				</div>
			</div>
		</form>
		<form id="rfqForm" method="get">
			<input type="text" id="rfqGuid" name="rfqGuid">
		</form>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>