<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>rfq-details</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/styles/simple-demo.css">	
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/styles/style.css" /> --%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">	
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script
	src="${pageContext.request.contextPath}/static/js/rfq_details.js"></script>
	
<script>
	var inventoryURL = "${pageContext.request.contextPath}/inventory/all";
	var inventoryActivateURL = "${pageContext.request.contextPath}/inventory/activate";
	var inventoryDeActivateURL = "${pageContext.request.contextPath}/inventory/deActivate";
	var issuePOURL = "${pageContext.request.contextPath}/inventory/issuePO";
	var issueRFQURL = "${pageContext.request.contextPath}/rfq/issueRFQ";
	var createRFQURL = "${pageContext.request.contextPath}/rfq/create";
</script>
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form id="rfqForm" method="post">
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">RFQ ID:</label> <input type="text"
							class="form-control" id="rfqNumber" name="rfqNumnber"
							placeholder="RFQ Id">
					</div>	
				</div>	
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Date Created:</label> 
		                <input id="statusDateCreated" disabled="disabled" type="text" class="form-control" name="statusDateCreated" value='<fmt:formatDate pattern="MM/dd/yyyy" value="${now}"/>'/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Issue Date:</label> 
		                <div class="input-group date" id="issueDatePicker">
		                    <input id="contractDate" type="text" class="form-control" name="issueDate"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>							
					</div>
				</div>	
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Expiration Date:</label>
		                <div class="input-group date" id="expirationDatePicker">
		                    <input id="contractDate" type="text" class="form-control" name="expirationDate"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">RFQ Type:</label>
		                <div class="input-group date">
		                	<select class="form-control" id="rfqType" name="rfqType">
		                		<c:forEach items="${applicationScope.RFQ_TYPES}" var="item">
		                			<option value='<c:out value="${item.guid}"/>'><c:out value="${item.type}"/></option>
		                		</c:forEach>
		                	</select>
		                </div>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label for="firstName">Items:</label> 
						<div style="overflow: auto; max-height: 200px;">
							<table id="rfqItemTable" class="table table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>Item</th>
						                <th>Quantity on Hand</th>
						                <th>Standard quantity</th>
						                <th>Order Quantity</th>
						            </tr>
						        </thead>
						        <tbody>
						        	<c:forEach items="${requestScope.inventories}" var="item" varStatus="counter">
						        		<tr><td><c:out value="${item.idCode}"/></td><td><c:out value="${item.quantityOnHand}"/></td>
						        		<td><c:out value="${item.standardOrderQuantity}"/></td>
						        		<td><input type="text" name="quantity-"<c:out value="${item.standardOrderQuantity}"/>  data-inv-guid="<c:out value="${item.guid}"/>"></td></tr>
						        	</c:forEach>
						        </tbody>						
							</table>
						</div>
<!-- 						<div class="row">
							<div class="col-sm-1"><button id="addItem" type="button" class="btn btn-warning" disabled="disabled">Add Items</button></div>
						</div> -->
					</div>
				</div>
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12">
							<label for="firstName">Invited Vendors:</label> 
							<div style="overflow: auto; max-height: 200px;">
								<table id="rfqVendorTable" class="table table-bordered" cellspacing="0" width="100%">
							        <thead>
							            <tr>
							                <th>Vendor</th>
							                <th>Response</th>
							                <th>Response Status</th>
							                <th>Choose</th>
							            </tr>
							        </thead>
							        <tbody>
							        	<c:forEach items="${requestScope.vendors}" var="item">
							        		<tr><td><c:out value="${item.name}"></c:out></td><td></td><td></td><td><input type="checkbox" name="vendorChose" value="<c:out value="${item.guid}"></c:out>"></td></tr>
							        	</c:forEach>					        
							        </tbody>						
								</table>
							</div>
						</div>
					</div>
<!-- 					<div class="row">
						<div class="col-lg-9">
							<div class="row">
								<div class="col-lg-2"><button id="addVendor" type="button" class="btn btn-warning" disabled="disabled">Add</button></div>
								<div class="col-lg-2"><button id="acceptResponse" type="button" class="btn btn-warning" disabled="disabled">Accept</button></div>
								<div class="col-lg-2"><button id="rejectResponse" type="button" class="btn btn-warning" disabled="disabled">Reject</button></div>
								<div class="col-lg-2"><button id="issuePO" type="button" class="btn btn-warning" disabled="disabled">Issue PO</button></div>								
							</div>
						</div>
					</div> -->
					
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label for="firstName">Notes:</label> <textarea
							class="form-control" id="notes" name="notes" rows="4"
							placeholder="Notes"></textarea>
					</div>
				</div>				
			</div>
			<div class="row">
				<div class="col-lg-9">
					<div class="row">
						<div class="col-lg-1"><button id="saveRfq" type="button" class="btn btn-warning">Save</button></div>
					</div>
				</div>
			</div>			
			<input type="hidden" name="guid" id="guid">
		</form>
		<form id="navigationForm" method="GET" action="${pageContext.request.contextPath}/inventory/home.jsp">
			<input type="hidden" name="rfqGuid" id="rfqGuid">
		</form>
	</div>	
<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
</html>