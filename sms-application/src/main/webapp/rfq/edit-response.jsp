<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>edit-response</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">	
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/styles/simple-demo.css">	
<%-- <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/styles/style.css" /> --%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">		
<script
	src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootbox.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script src="${pageContext.request.contextPath}/static/js/edit_response.js"></script>

	
<script>
	var inventoryURL = "${pageContext.request.contextPath}/inventory/all";
	var inventoryActivateURL = "${pageContext.request.contextPath}/inventory/activate";
	var inventoryDeActivateURL = "${pageContext.request.contextPath}/inventory/deActivate";
	var issuePOURL = "${pageContext.request.contextPath}/inventory/issuePO";
	var rfqVendorResponseItemURL = '${pageContext.request.contextPath}/rfq/<c:out value="${requestScope.rfqEntity.guid}"/>/response/vendor/';
	var rfqVendorResponseURL = '${pageContext.request.contextPath}/rfq/<c:out value="${requestScope.rfqEntity.guid}"/>/response/vendor/';
	var saveRFQURL = "${pageContext.request.contextPath}/rfq/response/save";
	var navigationURL = "${pageContext.request.contextPath}/navigation";
</script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>		
	</nav>
	<div class="container">
		<form id="rfqForm" method="post">
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">RFQ ID:</label> <input type="text"
							class="form-control" id="rfqNumber" disabled="disabled" name="rfqNumnber" value="<c:out value="${requestScope.rfqEntity.rfqNumber}"/>"
							placeholder="RFQ Id">
					</div>	
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Response ID:</label> 
						<c:choose>
							<c:when test="${not empty requestScope.rfqResponse}"> 
								<input type="text" class="form-control" id="responseId" name="responseId" placeholder="Response Id" value="${requestScope.rfqResponse.responseId}">
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" id="responseId" name="responseId" placeholder="Response Id">
							</c:otherwise>
						</c:choose>
					</div>	
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Acceptance</label> 
						<select class="form-control" name="accepted" id="accepted">
							<c:choose>
								<c:when test="${requestScope.rfqResponse.accepted eq 1}"> 
									<option value="1" selected="selected">Yes</option>
									<option value="0">No</option>
								</c:when>
								<c:otherwise>
									<option value="1">Yes</option>
									<option value="0" selected="selected">No</option>									
								</c:otherwise>
							</c:choose>
						</select>
					</div>				
				</div>		
			</div>
			<div class="row">	
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Responding Vendor:</label> 
						<select name="vendorResponse" class="form-control" id="vendorResponse" placeholder="Response Id">
							<c:forEach items="${requestScope.rfqVendors}" var="item">
								<c:choose>
									<c:when test="${not empty requestScope.rfqResponse}"> 
										<c:choose>
											<c:when test="${item.guid eq requestScope.rfqResponse.systemVendor.guid}">
												<option value="${item.guid}" selected="selected">${item.name}</option>
											</c:when>
											<c:otherwise>
												<option value="${item.guid}">${item.name}</option>
											</c:otherwise>										
										</c:choose>										
									</c:when>
									<c:otherwise>
										<option value="${item.guid}">${item.name}</option>
									</c:otherwise>
								</c:choose>									
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Delivery By Date:</label> 
		                <div class="input-group date" id="deliveryByDatePicker">
		                	<c:choose>
		                		<c:when test="${not empty requestScope.rfqResponse}">
		                			<input id="deliveryByDate" type="text" class="form-control" name="deliveryByDate" value="<fmt:formatDate value="${requestScope.rfqResponse.deliveryByDate}" pattern="MM/dd/YYYY"/>"/>
		                		</c:when>
		                		<c:otherwise>
		                			<input id="deliveryByDate" type="text" class="form-control" name="deliveryByDate" value=''/>
		                		</c:otherwise>
		                	</c:choose>		                    
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label for="firstName">Accepted On:</label> 
						<input type="text" class="form-control" value="<fmt:formatDate value="${requestScope.rfqResponse.acceptedDate}" pattern="MM/dd/YYYY"/>">
					</div>
				</div>			
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12">
							<label for="firstName">Items:</label> 
							<div style="overflow: auto; max-height: 200px;">
								<table id="rfqVendorTable" class="table table-bordered" cellspacing="0" width="100%" style="background-color: wwhite;">
							        <thead>
							            <tr>
							                <th>Item</th>
							                <th>Quantity</th>
							                <th>Unit price</th>
							            </tr>
							        </thead>
							        <tbody>
							        	<c:choose>
								        	<c:when test="${not empty requestScope.rfqResponse}">
									        	<c:forEach items="${requestScope.rfqResponse.rfqResponseItems}" var="item">
									        		<tr><td><c:out value="${item.rfqItem.systemPart.assemblyName}"/></td><td><c:out value="${item.quantity}"/></td><td><input type="text" name="unitPrice" data-guid="<c:out value="${item.guid}"/>" value="<c:out value="${item.unitPrice}"/>"></td></tr>
									        	</c:forEach>
								        	</c:when>
								        	<c:otherwise>
								        		<c:forEach items="${requestScope.rfqEntity.rfqItems}" var="item">
								        			<tr><td><c:out value="${item.systemPart.assemblyName}"/></td><td><c:out value="${item.quantity}"/></td><td><input type="text" name="unitPrice" data-guid="<c:out value="${item.guid}"/>" value="<c:out value="${item.unitPrice}"/>"></td></tr>
								        		</c:forEach>
								        	</c:otherwise>
							        	</c:choose>
							        </tbody>						
								</table>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="firstName">Notes:</label> 
								<c:choose>
									<c:when test="${not empty requestScope.rfqResponse}">
										<textarea class="form-control" id="notes" name="notes" rows="4" placeholder="Notes">
											<c:out value="${requestScope.rfqResponse.notes}"/>
										</textarea>
									</c:when>
									<c:otherwise>
										<textarea class="form-control" id="notes" name="notes" rows="4" placeholder="Notes"></textarea>
									</c:otherwise>
								</c:choose>
							</div>
						</div>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-5">
					<div class="row">
						<div class="col-lg-3"><button id="addResponse" type="button" class="btn btn-warning">Save Response</button></div>
						<div class="col-lg-2"><button id="issuePO" type="button" class="btn btn-warning" disabled="disabled">Issue PO</button></div>								
					</div>
				</div>
			</div>			
			<input type="hidden" id="rfqGuid" name="rfqGuid" value='<c:out value="${requestScope.rfqEntity.guid})"/>'>
		</form>
	</div>	
<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
</html>