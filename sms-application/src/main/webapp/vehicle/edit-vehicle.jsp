<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="now" class="java.util.Date"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New_Vehicle</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/bootstrap3-datetimepicker.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/styles/jstree-themes/proton/style.css">	

<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="${pageContext.request.contextPath}/static/js/bootstrap3-datetimepicker.js"></script>
<script src="${pageContext.request.contextPath}/static/js/jstree.js"></script>
<script src="${pageContext.request.contextPath}/static/js/edit-vehicle.js"></script>
<script src="${pageContext.request.contextPath}/static/js/modal-system-tree.js"></script>
</head>
<body>
	<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
		<form id="vehicleForm" action="${pageContext.request.contextPath}/vehicle/save" method="post">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="country">End Items & Serialized Components Type:</label> 
						<select name="vehicleType" class="form-control" id="vehicleType">
							<c:forEach items="${applicationScope.VEHICLE_TYPES}" var="item">
								<c:choose>
									<c:when test="${item.guid eq requestScope.vehicle.vehicleType.guid}">
										<option value="${item.guid}" selected="selected">${item.type}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.guid}">${item.type}</option>								
									</c:otherwise>									
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>			
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="country">Serial #:</label> 
						<input type="text" class="form-control" id="serialNumber" name="serialNumber" value="${requestScope.vehicle.serialNumber}">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="country">VIN:</label> 
						<input type="text" class="form-control" id="vehicleVin" name="vehicleVin" value="${requestScope.vehicle.vehicleVin}">
					</div>				
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="country">Nomenclature:</label> 
						<input type="text" class="form-control" id="nomenclature" name="nomenclature" value="${requestScope.vehicle.nomenclature}">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="country">Part Number:</label> 
						<input type="text" class="form-control" id="partNumber" name="partNumber" value="${requestScope.vehicle.partNumber}">
					</div>	
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<button id="system-tree-lookup" type="button" class="btn btn-default" style="vertical-align: bottom;" data-toggle="modal" data-target="#woTreeModal">
						  <span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> Lookup
						</button>
					</div>				
				</div>
			</div>	
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="country">Next Higher Assembly:</label> 
						<select class="form-control" id="nextHigherAssembly" name="nextHigherAssembly" style="width: 100%">
							<option value="">----None----</option>
							<c:if test="${not empty requestScope.vehicle.nextHigherAssembly}">
								<option value="${requestScope.vehicle.nextHigherAssembly.guid}" selected="selected"><c:out value="${requestScope.vehicle.nextHigherAssembly.serialNumber}"></c:out></option>
							</c:if>
						</select>
						<%-- <input type="text" class="form-control" id="nextHigherAssembly" name="nextHigherAssembly" value="${requestScope.vehicle.nextHigherAssembly}"> --%>
						<%-- <select class="form-control" id="nextHigherAssembly" name="nextHigherAssembly" style="width: 100%" value="${requestScope.vehicle.nextHigherAssembly.guid}">
							<c:forEach items="${applicationScope.VEHICLES}" var="item">
								<c:choose>
									<c:when test="${item eq requestScope.vehicle.nextHigherAssembly}">
										<option value="${item.guid}" selected="selected"><c:out value="${item.serialNumber}"></c:out></option>
									</c:when>
									<c:otherwise>
										<option value="${item.guid}"><c:out value="${item.serialNumber}"></c:out></option>								
									</c:otherwise>
								</c:choose>								
							</c:forEach>
						</select> --%>						
					</div>				
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label for="country">Vehicle Year:</label> 
						<input type="text" maxlength="4" class="form-control" name="vehicleYear" value="${requestScope.vehicle.vehicleYear}">
					</div>			
				</div>				
				<div class="col-md-3">
					<div class="form-group">
						<label for="country">Customer/End User:</label> 
						<select name="customer" class="form-control" id="vehicleType">
							<c:forEach items="${applicationScope.CUSTOMERS}" var="item">
								<c:choose>
									<c:when test="${item.guid eq requestScope.vehicle.customer.guid}">
										<option value="${item.guid}" selected="selected">${item.customerEndUser}</option>
									</c:when>
									<c:otherwise>
										<option value="${item.guid}">${item.customerEndUser}</option>								
									</c:otherwise>									
								</c:choose>
							</c:forEach>
						</select>
					</div>				
				</div>
			</div>			
			<div class="row">
				<div class="col-lg-3">
					<div class="form-group">
						<label for="firstName">Date Placed in Service:</label> 
		                <div class="input-group date" id="serviceDatePicker">
		                	<input id="serviceDate" type="text" class="form-control" name="serviceDate" value='<fmt:formatDate pattern="MM/dd/yyyy" value="${requestScope.vehicle.serviceDate}" />'/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<label for="firstName">Warranty Start Date:</label> 
		                <div class="input-group date" id="warrantyStartDatePicker">
		                	<input id="warrantyStartDate" type="text" class="form-control" name="warrantyStartDate" value='<fmt:formatDate pattern="MM/dd/yyyy" value="${requestScope.vehicle.warrantyStartDate}" />'/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<label for="firstName">Warranty End Date:</label> 
		                <div class="input-group date" id="warrantyEndDatePicker">
		                	<input id="warrantyEndDate" type="text" class="form-control" name="warrantyEndDate" value='<fmt:formatDate pattern="MM/dd/yyyy" value="${requestScope.vehicle.warrantyEndDate}" />'/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>						
					</div>
				</div>
			</div>		
			<input type="hidden" name="guid" value="${requestScope.vehicle.guid}">
			<input type="hidden" id="systemTree" name="systemTree" value="">
			<input type="hidden" id="vehicleDelete" name="vehicleDelete" value="">
			<button id="customerSave" type="submit" class="btn btn-default active">Submit</button>
			<button id="deleteButton" type="button" class="btn btn-default active">Delete</button>
			<button onclick="goBack()" class="btn btn-default active">Cancel</button>
			<script>
				function goBack() {
					window.history.back();
				}
			</script>
		</form>
	</div>
	<jsp:include page="system-tree-modal.jsp"></jsp:include>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script type="text/javascript">
var serviceDatePicker = $('#serviceDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: $('#serviceDate').val()!=''?moment($('#serviceDate').val(), 'MM/DD/YYYY'):moment()});
var warrantyStartDatePicker = $('#warrantyStartDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: $('#warrantyStartDate').val()!=''?moment($('#warrantyStartDate').val(), 'MM/DD/YYYY'):moment()});
var warrantyEndDatePicker = $('#warrantyEndDatePicker').datetimepicker({format : 'MM/DD/YYYY', defaultDate: $('#warrantyEndDate').val()!=''?moment($('#warrantyEndDate').val(), 'MM/DD/YYYY'):moment()});
var treeURL = '${pageContext.request.contextPath}/system/tree/view';
var treeComponentURL = '${pageContext.request.contextPath}/system/tree/searchByNomenclatureAndPart'
var pageContextRoot = '${pageContext.request.contextPath}/';
</script>
</html>
