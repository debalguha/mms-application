<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Vehicles</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.8/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.0/css/buttons.dataTables.min.css">	
<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.3.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.8/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/vehicle.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.1.0/js/buttons.print.min.js"></script>
</head>
<body>
<jsp:include page="/insecure/site-navigation.jsp"></jsp:include>
	<div class="container">
<!-- 		<h4>Double click on a vehicle record to view details.</h4> -->
		<div class="row">
			<div class="col-lg-12">
				<table id="vehicle-table" class="table table-striped table-bordered table-hover"  cellspacing="0" width="100%" style="cursor:pointer">
			        <thead>
			            <tr>
			                <th>Serial #</th>
			                <th>Type</th>
			                <th>Year of Manufacturing</th>
			                <th>VIN</th>
			                <th>Customer/End User</th>
			            </tr>
			        </thead>					
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<form id="vehicleForm" action="${pageContext.request.contextPath}/vehicle/edit" method="post">
					<input type="hidden" name="vehicleGuid" id="vehicleGuid">
					<button id="addVehicle" type="submit" class="btn btn-default active" onclick="clearHiddenField();">New End Items & Serialized Components</button>
					<button onclick="goBack()" class="btn btn-default active">Cancel</button>
			<script>
				function goBack() {
					window.history.back();
				}
			</script>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/insecure/footer.jsp"></jsp:include>
</body>
<script src="${pageContext.request.contextPath}/static/js/site-navigation.js"></script>
<script>
	var vehicleListURL = '<c:out value="${pageContext.request.contextPath}"/>/vehicle/all';
	clearHiddenField = function(){
		$('#vehicleGuid').val('');
	}
</script>
</html>