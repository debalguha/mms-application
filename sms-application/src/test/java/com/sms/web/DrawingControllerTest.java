package com.sms.web;

import static org.mockito.Mockito.when;

import java.io.File;
import java.util.Collection;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import com.sms.persistence.service.SystemService;
import com.sms.persistence.service.UserService;
import com.sms.util.SMSConstants;
import com.sms.web.model.FileUploadDTO;
import com.sms.web.model.UploadResponseDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext-test.xml")
public class DrawingControllerTest {
	@Autowired
	private DrawingController controller;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SystemService systemService;
	
	//@Test
	public void shouldBeAbleToUploadDrawing() throws Exception {
		HttpSession mockSession = Mockito.mock(HttpSession.class);
		when(mockSession.getAttribute(SMSConstants.USERBEAN.name())).thenReturn(userService.findByEmail("debalguha-1400255092@gmail.com"));
		FileUploadDTO mockUploadDTO = Mockito.mock(FileUploadDTO.class);
		MultipartFile mockMultiPartFile = Mockito.mock(MultipartFile.class);
		when(mockMultiPartFile.getBytes()).thenReturn(FileUtils.readFileToByteArray(new File("C:/Users/dguha/Downloads/scanned-Debal.pdf")));
		when(mockMultiPartFile.getOriginalFilename()).thenReturn("scanned-Debal.pdf");
		when(mockUploadDTO.getPartGuid()).thenReturn("A22BBF51-8FB1-49D2-96C3-D843A5AA8F09");
		when(mockUploadDTO.getFile()).thenReturn(mockMultiPartFile);
		Assert.assertNotNull(controller.uploadDrawing(mockUploadDTO, mockSession));
	}
	
	//@Test
	public void shouldBeAbleToGetAllDrawingsForPart() throws Exception {
		HttpSession mockSession = Mockito.mock(HttpSession.class);
		when(mockSession.getAttribute(SMSConstants.USERBEAN.name())).thenReturn(userService.findByEmail("debalguha-1400255092@gmail.com"));
		Collection<UploadResponseDTO> uploadResponseDTOs = controller.getAllDrawingsForpart("A22BBF51-8FB1-49D2-96C3-D843A5AA8F09", mockSession);
		Assert.assertNotNull(uploadResponseDTOs);
		Assert.assertFalse(uploadResponseDTOs.isEmpty());
	}
	
	@Test
	public void shouldBeAbleToRemoveADrawingByGuid() throws Exception {
		Assert.assertNotNull(controller.removeDrawingsFromPart("FAE11D7D-5960-46E1-94D7-5F49A0DACC99"));
		Assert.assertNull(systemService.findSystemTreeByGUID("F9CE9BA2-E4E0-4246-B8D9-B796E7D27B9A"));
	}
}
