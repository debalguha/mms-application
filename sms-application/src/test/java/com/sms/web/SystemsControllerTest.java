package com.sms.web;

import java.util.Collection;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.service.UserService;
import com.sms.web.model.TreeDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext-test.xml")
public class SystemsControllerTest {
	@Autowired
	private SystemsController controller;
	
	@Autowired
	private UserService userService;
	
	@Test
	public void shouldBeAbleToShowSystemTree() throws Exception {
		Collection<TreeDTO> systemTree = controller.showSystemTree(null);
		Assert.assertNotNull(systemTree);
		Assert.assertFalse(systemTree.isEmpty());
		System.out.println(systemTree.size());
	}
	
	//@Test
	public void testReadFromClasspath() throws Exception {
		String aString = "log4j.properties";
		System.out.println(IOUtils.readLines(getClass().getClassLoader().getResourceAsStream(aString)));
	}
}
