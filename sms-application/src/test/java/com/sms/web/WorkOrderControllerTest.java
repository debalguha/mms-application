package com.sms.web;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.Lists;
import com.sms.persistence.model.User;
import com.sms.persistence.model.Vehicle;
import com.sms.persistence.model.WorkOrder;
import com.sms.persistence.service.CustomerService;
import com.sms.persistence.service.LocationService;
import com.sms.persistence.service.SystemService;
import com.sms.persistence.service.UserService;
import com.sms.persistence.service.VehicleService;
import com.sms.persistence.service.WorkOrderService;
import com.sms.web.model.WorkOrderDTO;
import com.sms.web.model.WorkOrderItemDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext-test.xml")
public class WorkOrderControllerTest {
	@Autowired
	private WorkOrderService workOrderService;
	@Autowired
	private WorkOrderController woController;
	@Autowired
	private UserService userService;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private VehicleService vehicleService;
	@Test
	@Ignore
	public void shouldBeAbleToCreateWorkOrder() throws Exception {
		Assert.assertNotNull(woController.createWorkOrder(createAWorkOrderDTO()));
	}
	public WorkOrderDTO createAWorkOrderDTO(){
		Date now = new Date();
		User anUser = userService.findByEmail("debalguha@gmail.com");
		Vehicle aVehicle = vehicleService.findAllVehicles().iterator().next();
		List<WorkOrderItemDTO> itemDTOs = Lists.newArrayListWithCapacity(1);
		WorkOrderItemDTO itemDTO = new WorkOrderItemDTO();
		itemDTO.setDateCompleted(now);
		itemDTO.setDatePromised(now);
		itemDTO.setSystemTreeGuid(systemService.findSystemTreeByItemTypeAndNomenclature(systemService.findTreeItemTypeByName("Part").getGuid(), "NOM").iterator().next().getGuid());
		itemDTO.setTechGuid(anUser.getGuid());
		itemDTO.setUserGuid(anUser.getGuid());
		itemDTOs.add(itemDTO);
		WorkOrderDTO wo = new WorkOrderDTO();
		wo.setCompletedDate(now);
		wo.setCustomerGuid(aVehicle.getCustomer().getGuid());
		wo.setLocationGuid(locationService.findAllLocations().iterator().next().getGuid());
		wo.setPromisedDate(now);
		wo.setScheduledDate(now);
		wo.setSupervisorGuid(anUser.getGuid());
		wo.setTechGuid(anUser.getGuid());
		wo.setUserGuid(anUser.getGuid());
		wo.setVehicleGuid(aVehicle.getGuid());
		wo.setWorkOrderTypeGuid(workOrderService.findAllWorkOrderTypes().iterator().next().getGuid());
		wo.setWorkOrderItems(itemDTOs);
		return wo;
	}
	@Test
	public void shouldBeAbleToSubmitWorkOrder() throws Exception {
		WorkOrderDTO woDTO = createAWorkOrderDTO();
		WorkOrder workOrder = getRestTemplate().postForObject("http://localhost:8080/sms-application/workOrder/create", woDTO, WorkOrder.class);
		Assert.assertNotNull(workOrder);
		System.out.println("Response: "+workOrder);
		//workOrderService.deleteWorkOrder(workOrder);
	}
	//@Test
	public void deleteAllWOs(){
		for(WorkOrder workOrder : workOrderService.findAllWorkOrders())
			workOrderService.deleteWorkOrder(workOrder);
	}
	public RestTemplate getRestTemplate(){
		MappingJackson2HttpMessageConverter jackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
		jackson2HttpMessageConverter.setObjectMapper(new CustomJacksonMapper());
		List<HttpMessageConverter<?>> converters = Lists.newArrayList();
		converters.add(new StringHttpMessageConverter());
		converters.add(new FormHttpMessageConverter());
		converters.add(jackson2HttpMessageConverter);
		RestTemplate template = new RestTemplate();
		template.setMessageConverters(converters);
		return template;
	}
}
