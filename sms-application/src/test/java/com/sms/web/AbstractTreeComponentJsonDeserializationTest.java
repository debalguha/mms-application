package com.sms.web;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sms.persistence.model.LineItem;
import com.sms.persistence.model.SystemServiceData;

public class AbstractTreeComponentJsonDeserializationTest {
	@Test
	public void testDeserializationOfLineItem() throws Exception{
		String json = "{\"nomenclature\":\"Car+Pickup\",\"lineItem\":\"Car+Pickup\",\"idCode\":\"CP-001\",\"quantity\":\"1\",\"unitPrice\":\"2711\",\"lineItemType\":\"901E943B-9710-4CE8-9B65-8C43DC8010FE\",\"extendedPrice\":\"2,711.000\",\"commonCurrencyPrice\":\"1,762.15\",\"commonCurrentTargetPrice\":\"779.55\",\"guid\":\"\"}";
		ObjectMapper mapper = new CustomJacksonMapper();
		LineItem val = mapper.readValue(json, LineItem.class);
		Assert.assertNotNull(val);
		System.out.println(val);
	}
	
	@Test
	public void testDeserializationOfServiceData() throws Exception {
		String json = "{\"nomenclature\":\"Car Wash\",\"idCode\":\"CAR_WASH_001\",\"quantity\":\"12\",\"unitPrice\":\"299.12\",\"commonCurrentTargetPrice\":\"\",\"serviceDataType\":\"B5FB5B52-CCCF-44D8-A80F-3C2DFE2C99C2\",\"extendedPrice\":\"3,589.44\",\"commonCurrencyPrice\":\"2,333.14\",\"guid\":\"B0877B3B-AB11-48AE-8D8A-5D96AC438B7C\"}";
		ObjectMapper mapper = new CustomJacksonMapper();
		SystemServiceData val = mapper.readValue(json, SystemServiceData.class);
		Assert.assertNotNull(val);
		System.out.println(val);
	}
}
