package com.sms.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class MailSendDaemonTest {
	@Autowired
	private JavaMailSender mailSender;
	@Test
	public void shouldBeAbleToSendAnEmail(){
		SimpleMailMessage supportMessage = new SimpleMailMessage();
		supportMessage.setFrom("shaditrstechspprt@gmail.com");
		supportMessage.setTo("debalguha@gmail.com");
		supportMessage.setSubject("TEST");
		supportMessage.setText("TEST");
		mailSender.send(supportMessage);
	}
}
