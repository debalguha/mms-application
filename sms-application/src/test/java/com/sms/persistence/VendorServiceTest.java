package com.sms.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.model.SystemVendor;
import com.sms.persistence.service.LocationService;
import com.sms.persistence.service.VendorService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class VendorServiceTest {
	@Autowired
	private VendorService vendorService;
	@Autowired
	private LocationService locationService;
	@Test
	public void shouldBeAbleToCreate10Vendors() throws Exception {
		for(int i=0;i<10;i++){
			SystemVendor vendor = new SystemVendor();
			//vendor.setContact(contact);
			vendor.setLocation(locationService.findLocationByName("HQ"));
			vendor.setName("test vendor-"+i);
			vendor.setType(vendorService.findVendorTypeByName("Supply"));
			vendorService.createVendor(vendor);
		}
	}

}
