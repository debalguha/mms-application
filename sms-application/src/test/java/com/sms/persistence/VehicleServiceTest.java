package com.sms.persistence;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.sms.persistence.model.Customer;
import com.sms.persistence.model.Vehicle;
import com.sms.persistence.model.VehicleType;
import com.sms.persistence.service.CustomerService;
import com.sms.persistence.service.VehicleService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class VehicleServiceTest {
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private CustomerService customerService;
	
	@Test
	public void createVehicles() throws Exception {
		Collection<VehicleType> allVehicleTypes = vehicleService.findAllVehicleTypes();
		List<Customer> allCustomers = Lists.newArrayList(customerService.findAll());
		for(VehicleType vehicleType : allVehicleTypes){
			for(int i=0;i<5;i++){
				Vehicle vehicle = new Vehicle();
				//vehicle.setSerialNumber(RandomUtils.nextInt());
				vehicle.setCustomer(allCustomers.get(RandomUtils.nextInt(10)));
				vehicle.setVehicleType(vehicleType);
				//vehicle.setVehicleVin(RandomUtils.nextInt());
				vehicle.setVehicleYear(2015);
				vehicleService.createVehicle(vehicle);
			}
		}
	}
}
