package com.sms.persistence;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.model.SystemDrawing;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.model.SystemTreeItemType;
import com.sms.persistence.service.DrawingService;
import com.sms.persistence.service.SystemService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class DrawingServiceTest {
	@Autowired
	private SystemService systemService;
	
	@Autowired
	private DrawingService drawingService;
	
	//@Test
	public void shouldBeAbleToCreateHunfdredDrawings() throws Exception {
		SystemTreeItemType drawingType = systemService.findTreeItemTypeByName("Drawing");
		for(int i=0;i<100;i++){
			SystemDrawing drawing = new SystemDrawing();
			SystemTree drawingNode = new SystemTree();
			drawingNode.setParent(null);
			drawingNode.setCreationDate(new Date());
			drawingNode.setDescription("My_Random_Picture_"+i);
			drawingNode.setNomenclature("My_Random_Picture_"+i);
			drawingNode.setTreeItemType(drawingType);
			drawing.setCreationDate(new Date());
			//drawing.setGuid(UUID.randomUUID().toString());
			String fileName = "My_Random_File-"+i+".pdf";
			drawing.setStoredFileName(fileName);
			drawing.setUploadedFileName(fileName);
			drawing.setSystemTree(drawingNode);
			drawingService.createDrawing(drawing);
		}
	}
	
	//@Test
	public void shouldBeAbleToObtainDrawingsNotWithPart() throws Exception {
		Collection<SystemPart> allParts = systemService.getAllParts();
		Collection<SystemDrawing> allDrawings = drawingService.getAllDrawings(0, 10);
		for(SystemPart aPart : allParts){
			int drawingCount = CollectionUtils.isEmpty(drawingService.getAllDrawingsForPart(aPart.getGuid()))?0:drawingService.getAllDrawingsForPart(aPart.getGuid()).size();
			try {
				Assert.assertEquals((allDrawings.size()-drawingCount), drawingService.getAllDrawingsNotForPart(aPart.getGuid()).size());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	@Test
	public void shouldBeAbleToRetrieveAllPagesOfDrawing() throws Exception{
		Collection<? extends SystemDrawing> drawingPage = drawingService.getAllDrawings("8", 0, 10, 0, "asc");
		Assert.assertNotNull(drawingPage);
		Assert.assertFalse(drawingPage.isEmpty());
		drawingPage = drawingService.getAllDrawings("8", 1, 10, 0, "asc");
		Assert.assertNotNull(drawingPage);
		Assert.assertFalse(drawingPage.isEmpty());
		drawingPage = drawingService.getAllDrawings("8", 2, 10, 0, "asc");
		Assert.assertNotNull(drawingPage);
		Assert.assertFalse(drawingPage.isEmpty());
	}
}
