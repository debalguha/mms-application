package com.sms.persistence.model;

import org.junit.Assert;
import org.junit.Test;

public class AccessLevelConverterTest {
	@Test
	public void shouldBeAbleToConvertToDatabaseColumn() throws Exception {
		Assert.assertEquals(new Integer(1), new AccessLevelConverter().convertToDatabaseColumn(ACCESS_LEVEL.USER));
	}
	@Test
	public void shouldBeAbleToConvertToEntityAttribute() throws Exception {
		Assert.assertEquals(ACCESS_LEVEL.ADMIN, new AccessLevelConverter().convertToEntityAttribute(new Integer(2)));
	}
	
}
