package com.sms.persistence;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.model.Inventory;
import com.sms.persistence.model.SystemPart;
import com.sms.persistence.service.InventoryService;
import com.sms.persistence.service.SystemService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class InventoryServiceTest {
	@Autowired
	private InventoryService invService;
	@Autowired
	private SystemService systemService;
	
	@Test
	public void shouldBeAbleToCreateInventoryForAllParts() throws Exception {
		Collection<SystemPart> allParts = systemService.findAllParts();
		for(SystemPart part : allParts){
			Inventory inventory = new Inventory();
			inventory.setActive(1);
			inventory.setIdCode(part.getAssemblyNumber());
			inventory.setLowQuantityThreshold(5);
			inventory.setQuantityOnHand(40);
			inventory.setStandardOrderQuantity(10);
			inventory.setSystemPart(part);
			invService.createInventoryRecord(inventory);
		}
	}
	
	@Test
	public void shouldBeAbleToCheckIfIsPOAvailable() throws Exception {
		System.out.println(!invService.isPOAvailable("B2359B3E-7221-445B-A4BD-57A9226EF93B"));
	}
}
