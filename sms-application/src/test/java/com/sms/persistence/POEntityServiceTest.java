package com.sms.persistence;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.service.POEntityService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class POEntityServiceTest {
	@Autowired
	private POEntityService poEntityService;
	
	@Test
	@Ignore
	public void shouldBeAbleToFindLastPOForInventory() throws Exception {
		System.out.println(poEntityService.findLastPOForInventory("B2359B3E-7221-445B-A4BD-57A9226EF93B"));
	}
}
