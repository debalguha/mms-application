package com.sms.persistence;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.model.SystemPart;
import com.sms.persistence.model.SystemPicture;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.model.SystemTreeItemType;
import com.sms.persistence.service.PictureService;
import com.sms.persistence.service.SystemService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class PictureServiceTest {
	@Autowired
	private SystemService systemService;
	
	@Autowired
	private PictureService pictureService;
	
	@Test
	public void shouldBeAbleToCreateHundredPictures() throws Exception {
		SystemTreeItemType drawingType = systemService.findTreeItemTypeByName("Picture");
		for(int i=0;i<100;i++){
			SystemPicture drawing = new SystemPicture();
			SystemTree drawingNode = new SystemTree();
			drawingNode.setParent(null);
			drawingNode.setCreationDate(new Date());
			drawingNode.setDescription("My_Random_Picture_forPics"+i);
			drawingNode.setNomenclature("My_Random_Picture_forPics"+i);
			drawingNode.setTreeItemType(drawingType);
			drawing.setCreationDate(new Date());
			//drawing.setGuid(UUID.randomUUID().toString());
			String fileName = "My_Random_PicFile-"+i+".pdf";
			drawing.setStoredFileName(fileName);
			drawing.setUploadedFileName(fileName);
			drawing.setSystemTree(drawingNode);
			pictureService.createPicture(drawing);
		}
	}
	
	//@Test
	public void shouldBeAbleToObtainPicturesNotWithPart() throws Exception {
		Collection<SystemPart> allParts = systemService.getAllParts();
		Collection<SystemPicture> allDrawings = pictureService.getAllPictures();
		for(SystemPart aPart : allParts){
			int drawingCount = CollectionUtils.isEmpty(pictureService.getAllPicturesForPart(aPart.getGuid()))?0:pictureService.getAllPicturesForPart(aPart.getGuid()).size();
			try {
				Assert.assertEquals((allDrawings.size()-drawingCount), pictureService.getAllPicturesNotForPart(aPart.getGuid()).size());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
