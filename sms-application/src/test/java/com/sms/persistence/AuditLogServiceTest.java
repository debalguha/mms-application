package com.sms.persistence;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.sms.persistence.model.AuditLog;
import com.sms.persistence.model.User;
import com.sms.persistence.service.AuditLogService;
import com.sms.persistence.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class AuditLogServiceTest {
	@Autowired
	private AuditLogService auditLogService;
	
	@Autowired
	private UserService userService;
	
	@Test
	public void shouldBeAbleToCreateAuditLogs() throws Exception {
		List<User> allUsers = Lists.newArrayList(userService.findAllUsers());
		Collection<AuditLog> auditLogs = Lists.newArrayList();
		for(int i=0;i<100;i++)
			auditLogs.add(new AuditLog(new Date(), "Test - "+i, "Test - "+i, "Test - "+i, "Test - "+i, "Test - "+i, "Test - "+i, "Test - "+i, allUsers.get(RandomUtils.nextInt(3))));
		auditLogService.createAuditLogs(auditLogs);
	}
}
