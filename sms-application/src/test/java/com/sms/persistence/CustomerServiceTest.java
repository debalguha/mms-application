package com.sms.persistence;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.model.Customer;
import com.sms.persistence.service.CustomerService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class CustomerServiceTest {
	@Autowired
	private CustomerService customerService;
	
	@Test
	@Ignore
	public void createCustomers() throws Exception{
		for(int i=0;i<10;i++){
			String customerNumber = "Cust-#-"+i;
			String country = "USA";
			String agencyCompany = "GBS";
			String organization = "IBM";
			String customerEndUser = "Tom Maslyk";
			Date customerDate = new Date();
			String contractNumber = "CC_IB_US_"+i;
			Customer customer = new Customer(customerNumber, country, agencyCompany, organization, customerEndUser, customerDate, contractNumber);
			customerService.createACustomer(customer);
		}
	}
	
	@Test
	public void updateCustomerNames() throws Exception {
		int i=0;
		for(Customer customer : customerService.findAll()){
			customer.setCustomerEndUser(customer.getCustomerEndUser().concat("-").concat(String.valueOf(i)));
			customerService.createACustomer(customer);
			i++;
		}
	}
}
