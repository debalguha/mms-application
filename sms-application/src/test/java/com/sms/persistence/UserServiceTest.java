package com.sms.persistence;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.model.AccessLevel;
import com.sms.persistence.model.Location;
import com.sms.persistence.model.User;
import com.sms.persistence.service.AccessLevelService;
import com.sms.persistence.service.LocationService;
import com.sms.persistence.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class UserServiceTest {
	@Autowired
	private UserService userService;
	@Autowired
	private AccessLevelService accessLevelService;
	@Autowired
	private LocationService locationService;
	
	@Test
	public void createUserTest() throws Exception {
		AccessLevel accessLevel = accessLevelService.findAccessLevelByName("Admin");
		Location location = locationService.findLocationByName("HQ");
		User user = new User("Munia", "Mukherjee", "mukherjee.munia@gmail,com", "passw0rd", "", location, accessLevel);
		Assert.assertNotNull(userService.saveUser(user));
	}
	//@Test
	public void shouldBeAbleToUpdateAnUserProfile() throws Exception {
		AccessLevel accessLevel = accessLevelService.findAccessLevelByName("USER");
		Location location = locationService.findLocationByName("HQ");
		String email = "tsaddler-"+RandomUtils.nextInt()+"@gmail.com";
		User user = new User("tsaddler", "tsaddler", email, "password", "123456", location, accessLevel);
		Assert.assertNotNull(userService.saveUser(user));
		user.setPassword("0987654321");
		user.setLocation(locationService.findLocationByName("TQ"));
		Assert.assertNotNull(userService.saveUser(user));
		Assert.assertEquals("TQ", userService.findByEmail(email).getLocation().getLocationName());
		Assert.assertEquals("0987654321", userService.findByEmail(email).getPassword());
	}
}
