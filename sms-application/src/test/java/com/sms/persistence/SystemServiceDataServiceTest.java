package com.sms.persistence;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.model.ServiceDataType;
import com.sms.persistence.model.SystemServiceData;
import com.sms.persistence.service.SystemServiceDataService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class SystemServiceDataServiceTest {
	@Autowired
	private SystemServiceDataService serviceDataService;
	
	@Test
	@Ignore
	public void shouldBeAbleToCreateSystemServiceWithServiceDataType() throws Exception{
		SystemServiceData serviceData = serviceDataService.findBySystemTreeGuid("9BDAE0BD-CF39-46C9-B9AF-94299F2A6F21");
		serviceData.setServiceDataType(new ServiceDataType("BOBO_TYPE"));
		Assert.assertNotNull(serviceDataService.createSystemService(serviceData));
	}
}
