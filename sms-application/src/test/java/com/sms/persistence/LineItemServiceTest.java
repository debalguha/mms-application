package com.sms.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.service.ContractService;
import com.sms.persistence.service.LineItemService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class LineItemServiceTest {
	@Autowired
	private LineItemService lineItemService;
	
	@Autowired
	private ContractService contractService;
	
	@Test
	public void shouldBeAbleToCalculateCommonCurrencyTargetPrice(){
		System.out.println(lineItemService.calculateCommonCurrentTargetPrice(lineItemService.findBySystemTreeGuid("C0A8D8AB-9646-4F87-9240-0F9605FE70E2")));
		//System.out.println(contractService.calculateCommonCurrentTargetPrice(contractService.findBySystemTreeGuid("93B6FA7C-9337-46C6-9A84-CA8BD7EEF2CF")));
	}
}
