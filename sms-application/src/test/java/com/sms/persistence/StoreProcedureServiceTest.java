package com.sms.persistence;

import static com.sms.util.DTOUtils.convertSearchResultToDTO;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sms.persistence.service.StoreProcedureService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class StoreProcedureServiceTest {
	@Autowired
	private StoreProcedureService searchService;
	
	@Test
	public void shouldBeAbleToSearch() throws Exception {
		assertNotNull(convertSearchResultToDTO(searchService.search("PO", "86E97899-92EB-44B5-8B04-ACF4342B3827")));
	}
	
	@Test
	public void shouldBeAbleToReportExample() throws Exception {
		DateTime dt = new DateTime();
		List<Object[]> report = searchService.reportExample(dt.minusMonths(2).toDate(), dt.toDate());
		assertNotNull(report);
		assertFalse(report.isEmpty());
	}
}
