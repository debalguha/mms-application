package com.sms.persistence;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sms.persistence.model.SystemTree;
import com.sms.persistence.service.SystemService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class SystemServiceTest {
	@Autowired
	private SystemService systemService;
	@Test
	public void shouldBeAbleToFindATreeByGUID() throws Exception {
		String guidToFind = "377F8EE7-7471-4761-981F-6DD0FD305754";
		SystemTree root = systemService.findSystemTreeByGUID(guidToFind);
		Assert.assertNotNull(root);
		Assert.assertFalse(root.getChildren().isEmpty());
		Assert.assertEquals(4, root.getChildren().size());
		new ObjectMapper().writeValue(System.out, root);
	}
}
