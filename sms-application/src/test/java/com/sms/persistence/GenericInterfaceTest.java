package com.sms.persistence;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import org.junit.Test;

import com.sms.persistence.repo.CustomerRepo;

public class GenericInterfaceTest {
	@Test
	public void testGenericInterface() throws Exception{
		Type[] types = CustomerRepo.class.getGenericInterfaces();
		for(Type type : types){
			Type[] actualTypeArguments = ((ParameterizedType)type).getActualTypeArguments();
			System.out.println(((ParameterizedType)type).getOwnerType().toString());
			System.out.println(((ParameterizedType)type).getRawType().toString());
		}
		System.out.println(CustomerRepo.class.getGenericSuperclass());
	}
}
