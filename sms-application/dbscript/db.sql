
CREATE TABLE trssms.dbo.users (
	user_id int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	First_Name varchar(100),
	Last_Name varchar(100),
	email varchar(100),
	password varchar(100),
	Access_Level int NOT NULL,
	date_created datetime NOT NULL DEFAULT (getdate()),
	phone varchar(100),
	location_id int NOT NULL DEFAULT ((1))
);
CREATE TABLE trssms.dbo.content (
	content_id int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	content_type int,
	content_name varchar(100),
	content text,
	date_created datetime NOT NULL DEFAULT (getdate())
);
CREATE TABLE trssms.dbo.content_types (
	content_type int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	content_type_name varchar(100),
	date_created datetime NOT NULL DEFAULT (getdate())
);
CREATE TABLE trssms.dbo.email_log (
	email_id int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	fromwho varchar(255),
	towho varchar(255),
	subject varchar(255),
	body text,
	status varchar(100),
	timestamp datetime NOT NULL
);



CREATE TABLE trssms.dbo.locations (
	location_id int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	Location_Name varchar(100) NOT NULL,
	date_created datetime NOT NULL DEFAULT (getdate())
);
CREATE TABLE trssms.dbo.sync_log (
	id int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	url varchar(100),
	status text,
	date_created datetime NOT NULL DEFAULT (getdate())
);
CREATE TABLE trssms.dbo.sync_settings (
	id int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	url varchar(100),
	frequency_hours int NOT NULL DEFAULT ((24)),
	start_hour int NOT NULL DEFAULT ((2)),
	active int NOT NULL DEFAULT ((0)),
	date_created datetime NOT NULL DEFAULT (getdate())
);

CREATE TABLE trssms.dbo.wbs (
	id int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	parent_id int NOT NULL DEFAULT ((0)),
	wbs varchar(100),
	nomenclature varchar(100),
	description varchar(2000) NOT NULL DEFAULT (''),
	date_created datetime NOT NULL DEFAULT (getdate())
);

CREATE TABLE trssms.dbo.access_levels (
	access_level int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	Access_Level_Name varchar(100) NOT NULL,
	date_created datetime NOT NULL DEFAULT (getdate())
);

CREATE TABLE trssms.dbo.support_emails (
	id int NOT NULL,
	GUID uniqueidentifier NOT NULL DEFAULT (newid()),
	email varchar(100),
	date_created datetime NOT NULL DEFAULT (getdate())
);

insert into locations values (1, newid(), 'HQ', getdate());
insert into locations values (2, newid(), 'TQ', getdate());

insert into user values ();